#ifndef O_RUN_BAYES_H
#define O_RUN_BAYES_H

// Global variables used in o_run_bayes

int optInterrupt;   // Global variable read in bayesapp.c/UserMonitor()
double  *tmpCube;      // temporary Cube to be used in UserBuild()
double  **zbitstmp1;
int    *ibitstmp;

// Mutex functions implemented in bayesapp.c
void init_mutex();
void destroy_mutex();

// Variables to store the likelihood and restart
FILE *fh_restart;

#endif
