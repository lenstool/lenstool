double arithmetic_mean(int n, double U[n]);

int flatten_array(int n, int m, double V[n][m], double W[n*m]);

double min_array(int n, double U[n]);

double max_array(int n, double U[n]);

int knotx_from_ctrlpts(int n, int m, int p, double P[n][m], double U[n+p+1]);

int knoty_from_ctrlpts(int n, int m, int p, double P[n][m], double U[m+p+1]);

int index_knot_linear(int n, int p, double u, double U[n+p+1]);

int Bspline_basis_function(int i, int n, double u, int p, double U[n+p+1], double N[p+1]);

double Bspline_surface_point(int n, int p, double U[n+p+1],int m,int q, double V[m+q+1],double ctrl_pts[n][m], double u,double v);

int Bspline_basis_function_der(int i,int n, double u, int p, int d, double U[p+n+1], double N_der[p+1]);

double Bspline_surface_point_der(int n, int p, double U[n+p+1],int m,int q, double V[m+q+1],double ctrl_pts[n][m], double u,double v,int nx,int ny);

double *alloc_and_copy_vector(int n, double Tocopy[n]);

double **alloc_and_copy_table(int n, int m, double **Tocopy);

double **controle_point_derive(int n, int p, int m, int q,double **P,double *U,double *V,int axes);

double deboor_algorithms(int n,int p, int k, double u, double U[n+p+1], double P[n]);
