#ifndef O_GLOBAL_H
#define O_GLOBAL_H

// Global variables for o_global 

struct shear   shm[NAMAX];
struct galaxie multi[NFMAX][NIMAX];
struct z_lim   zlim[NFMAX];
struct z_lim   zalim;
struct galaxie *srcfit;  // points for source plane fitting
struct z_lim   zlim_s[NFMAX];
struct pot     lmin_s[NLMAX], lmax_s[NLMAX], prec_s[NLMAX];
struct sigposStr sigposAs_s, sigposAs;
struct matrix  amplifi_mat[NFMAX][NIMAX], amplifi_matinv[NFMAX][NIMAX];
//struct pixlist plo[100000]; /*list of points that compose the polygon around the image to inverse DEPRECATED*/

double z_dlsds;
double chi_im, chip, chix, chiy, chia, chil, chis, chi_vel,chi_mass;    //I added chi_vel and chi_mass TV
//double amplifi[NFMAX][NIMAX];
double **imo, **wo, **soo, **ero, lhood_wo;
double ***cubeo,***ercubeo;

double drima;   /* distance ratio between the image to inverse and the main lens*/
double **sm_p;      /* optimization spline map : initial save */
double *sflux;
double **fluxmap;
int **nimage;
int    nshmap;
int    optim_z;
int    block_s[NLMAX][NPAMAX];
int  **imuo;
/*number of points of the polygon around the image to inverse*/

double distmin[NFMAX]; /* List of minimal euclidean distances between
                          the images of a same familly i*/

int    nwarn;   // warning emitted during image plane chi2

double *np_b0;  // non parametric accelerator
//int    nplo;    // number of points for criticinv.c DEPRECATED

// mod bclement shapemodel_contour
struct  pixlist *imo_pl;    // list of points inside the polygons for shapemodel
struct  pixlist *imo_pl_dpl;    // list of dpl inside the polygons for shapemodel
int imo_npl;

// mod bclement shapemodel_contour_ext
struct  pixlist *imo_pl_ext;    // list of points inside the psf-extended polygons for shapemodel
struct  pixlist *imo_pl_dpl_ext;    // list of dpl inside the psf_extended polygons for shapemodel
int imo_npl_ext;

#endif
