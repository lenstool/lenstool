#include <math.h>
#include <wcslib.h>
#include "lt.h"
#include "constant.h"
#include "fonction.h"


static int  readlist(struct point I[NPOINT], char file[NIMAX]);

/*
*       nom:        do_itos - lesn-tool
*       auteur:     Jean-Paul Kneib
*       date:       oct 94
*       place:      IoA cambridge
*/

/*
* transform a list of pixels of an image to the source plan
* quick version - PSF = dirac
*
* Global variables used :
* - M, imFrame, ps
* - in e_dpl() : G, lens, lens_table
* - in err_invim() : iline, radial, tangent, nrline, ntline
*/

void    do_itos(double **im, struct pixlist *pl, int npl, double dlsds, double zs,
                double **source, double **erreur,
                int **imult)
{
    extern  struct g_pixel  imFrame;
    register    int i, j, k, is, js;
    const extern  struct  g_mode  M;
    const extern  struct g_pixel  ps;
    struct  point   A, B;
    double  xx;


    double ech=1.0;

    if(M.flux!=0)  ech=1.0/((double)ps.ech*ps.ech);

    // TO FORCE TO USE THE FIRST LOOP
    // TODO : CORRECT THE SECOND LOOP (WCS)
    imFrame.wcsinfo = NULL;

    if (imFrame.wcsinfo == NULL)
    {
        /*According to the subsampling in the image plane
         *for each subpixel in the image plane*/
        for (k = 0; k < npl; k++)
    {
            for (i = 0; i < imFrame.ech; i++)
                for (j = 0; j < imFrame.ech; j++)
                {
   // shifted and missing the last row and column of the oversampled pixel
                    //A.y = (double)pl[k].i + imFrame.pixely * (-.5 + (double)i / xx);
                    //A.x = (double)pl[k].j + imFrame.pixelx * (-.5 + (double)j / xx);
   // 1st correction but slightly too far from the edges
                    //A.y = (double)pl[k].i + imFrame.pixely * (-.5 +  (double)(i+1) / xx);
                    //A.x = (double)pl[k].j + imFrame.pixelx * (-.5 +  (double)(j+1) / xx);
   // 2nd correction to avoid this effect
                    xx = (double)imFrame.ech;
                    A.y = (double)pl[k].i + imFrame.pixely * (-.5 +  (double)(2*i+1) / (2.0*xx));
                    A.x = (double)pl[k].j + imFrame.pixelx * (-.5 +  (double)(2*j+1) / (2.0*xx));


                    /*convert relative wc in arcsec from image plane to source plane*/
                    e_dpl(&A, dlsds, zs, &B);

                    /*convert from relative wc in arcsec to pixel in the source image*/
                    is = (int) ((B.y - ps.ymin) / ps.pixely + 0.5); // 0.5 to avoid truncation effect
                    js = (int) ((B.x - ps.xmin) / ps.pixelx + 0.5); // 0.5 to avoid truncation effect

                    /*fill the multiplicity, source and error images*/
                    if (is >= 0 && js >= 0 && is < ps.ny && js < ps.nx)
                    {
                        imult[is][js] += 1;
                        source[is][js] = ((imult[is][js] - 1) * source[is][js] +
                                          pl[k].flux*ech)
                                         / ((double)imult[is][js]);
                        erreur[is][js] = ((imult[is][js] - 1) * erreur[is][js] +
                                          pl[k].flux * pl[k].flux*ech*ech)
                                         / ((double)imult[is][js]);
                    }

                }   /*end of for each subpixel in the image plane*/
      }
    } /*end of if(imFrame.wcsinfo==NULL)*/
    else
    {
        int *stat, ncoord_max, ncoord;
        double *pixcrd, *imgcrd, *phi, *theta, *world;
        ncoord_max = npl*imFrame.ech*imFrame.ech;
        pixcrd = (double *)malloc(ncoord_max*2*sizeof(double));
        imgcrd = (double *)malloc(ncoord_max*2*sizeof(double));
        world = (double *)malloc(ncoord_max*2*sizeof(double));
        phi = (double *)malloc(ncoord_max*sizeof(double));
        theta = (double *)malloc(ncoord_max*sizeof(double));
        stat = (int *)malloc(ncoord_max*sizeof(int));

        // Compute source plane coordinates
        ncoord = 0;
        for (k = 0; k < npl; k++)
        {
            A.y = imFrame.ymin + imFrame.pixely * (((double) pl[k].i) - .5 * (imFrame.ech - 1.) / imFrame.ech);
            xx = imFrame.xmin + imFrame.pixelx * (((double) pl[k].j) - .5 * (imFrame.ech - 1.) / imFrame.ech);

            for (i = 0; i < imFrame.ech; i++)
            {
                A.x = xx;
                for (j = 0; j < imFrame.ech; j++)
                {
                    e_dpl(&A, dlsds, zs, &B);
                    world[2*ncoord] = B.x / (-3600.) / cos(M.ref_dec * DTR) + M.ref_ra;
                    world[2*ncoord+1] = B.y / 3600. + M.ref_dec;
                    A.x += imFrame.pixelx / imFrame.ech * ((double) j);
                    ncoord++;
                }
                A.y += imFrame.pixely / imFrame.ech * ((double) i);
            }
        }

        wcss2p(imFrame.wcsinfo, ncoord, 2, world, phi, theta, imgcrd, pixcrd, stat);

        // Compute images
        ncoord = 0;
        for (k = 0; k < npl; k++)
            for (i = 0; i < imFrame.ech; i++)
                for (j = 0; j < imFrame.ech; j++)
                {
                    is = (int)imgcrd[2*ncoord];
                    js = (int)imgcrd[2*ncoord+1];
                    ncoord++;

                    if ((is >= 0) && (js >= 0) && (is < ps.ny) && (js < ps.nx))
                    {
                        imult[is][js] += 1;
                        source[is][js] = ((imult[is][js] - 1) * source[is][js] +
                                          pl[k].flux*ech)
                                         / ((double)imult[is][js]);
                        erreur[is][js] = ((imult[is][js] - 1) * erreur[is][js] +
                                          pl[k].flux * pl[k].flux*ech*ech)
                                         / ((double)imult[is][js]);
                    }
                }

        free(world); free(pixcrd); free(imgcrd);
        free(phi); free(theta); free(stat);
    } /*end of if imFrame.wcsinfo!=NULL*/

    for (is = 0; is < ps.ny; is++)
        for (js = 0; js < ps.nx; js++)
            erreur[is][js] -= source[is][js] * source[is][js];

}

void    newdo_itos(double **im, double dlsds, double zs,
                double **source, double **erreur,
                int **imult)
{
    extern  struct g_pixel  imFrame;
    extern struct point gsource_global[NGGMAX][NGGMAX];  
    const extern  struct  g_mode  M;
    const extern  struct g_pixel  ps;
    int is,k,js,kk,kcont;

    double ech=1.0;

    struct point I[10][NPOINT];
    int npcont[10];

    struct point spos;
    struct point pImage[NIMAX]; // list of image positions for a given source position

    int ncoord_max, ncoord;

    /*
     * Read and store all contours temporarily
     */

    for (k = 0; k < imFrame.ncont; k++)
    {
        npcont[k] = readlist(I[k], imFrame.contfile[k]);
    }

    if(M.flux!=0)  ech=1.0/((double)ps.ech*ps.ech);

    /*
     * Initialise full grid
     */
    e_unlensgrid(gsource_global, dlsds, zs);

    // Allocate memory for WCS transformation
    int *stat, status;
    double *pixcrd, *imgcrd, *phi, *theta, *world, *impix;
#define MAX_IMAGES 2  // number of images per source pixel on average
    ncoord_max = MAX_IMAGES*ps.nx*ps.ny;
    world = (double*)malloc(ncoord_max*2*sizeof(double));
    impix = (double*)malloc(ncoord_max*2*sizeof(double));

    /*
     * For each pixel in the source plane
     */
    ncoord = 0;
#pragma omp parallel for schedule(static,1) private(spos,pImage,is,js,kk,kcont)
    for (is = 0; is < ps.nx; is++)
        for(js = 0; js < ps.ny; js++)
        {
            source[js][is]=0.0;
            imult[js][is] = 0;

            spos.x=ps.xmin+((double)is)*ps.pixelx;
            spos.y=ps.ymin+((double)js)*ps.pixely;

            /*
             * Compute all image positions from this source location
             */
            int ni = e_lens_P(spos,pImage,dlsds,zs);

            // ensure we have enough space in world and impix, otherwise realloc
#pragma omp critical
            if ( ncoord + ni >= ncoord_max )
            {
                ncoord_max *= 2;
                world = realloc(world, ncoord_max*2*sizeof(double));
                impix = realloc(impix, ncoord_max*2*sizeof(double));
            }
            /*
             * Loop over all images
             */
            for (kk = 0; kk < ni; kk++)
            {
                /*
                * If the image is within one contour then keep its coordinates
                */
                for (kcont = 0; kcont < imFrame.ncont; kcont++)
                {
                    if(inconvexe(pImage[kk],npcont[kcont],I[kcont])>0)
#pragma omp critical
                    {
                        world[2*ncoord] = pImage[kk].x / (-3600.) / cos(M.ref_dec * DTR) + M.ref_ra;
                        world[2*ncoord+1] = pImage[kk].y / 3600. + M.ref_dec;
                        impix[2*ncoord] = is; impix[2*ncoord+1] = js;
                        ncoord++;
                    }
                }
            }
        }

    // Transform world to pixel coordinates
    pixcrd = (double *)malloc(ncoord_max*2*sizeof(double));
    imgcrd = (double *)malloc(ncoord_max*2*sizeof(double));
    phi = (double *)malloc(ncoord_max*sizeof(double));
    theta = (double *)malloc(ncoord_max*sizeof(double));
    stat = (int *)malloc(ncoord_max*sizeof(int));

    wcss2p(imFrame.wcsinfo, ncoord, 2, world, phi, theta, imgcrd, pixcrd, stat);

    double xpix, ypix;
    // For the pixels in the contours put flux of closest pixel.
    for ( k = 0; k < ncoord; k++ )
    {
        is = impix[2*k]; js = impix[2*k+1];
        xpix = pixcrd[2*k]; ypix = pixcrd[2*k+1];

        imult[js][is] += 1;
        //nearest pixel
        //source[js][is]=(source[js][is]*(imult[js][is]-1)+ech*im[(int)(ypix-0.5)][(int)(xpix-0.5)])/((double)imult[js][is]);
        //do bilinear interpolation instead
        source[js][is]=(source[js][is]*(imult[js][is]-1)+ech*bilinear(im,xpix,ypix,imFrame.nx,imFrame.ny))/((double)imult[js][is]);
    }

    free(world); free(pixcrd); free(imgcrd); free(impix);
    free(phi); free(theta); free(stat);

}

/****************************************************************
 * In the cleanlens mode, read the contour file
 * Return
 *  - I the array of point filled with the contour
 *  - k the number of elements in I
 */

static int  readlist(struct point I[NPOINT], char file[NIMAX])
{
    int n, k = 0;
    int stop = 0;
    FILE    *IN;

    IN = fopen(file, "r");
    if ( IN != NULL )
        while (stop != -1)
        {
            stop = fscanf(IN, "%d%lf%lf\n", &n, &I[k].x, &I[k].y);

            if (stop == 3)
                k++;
            else if (stop > 0)
                RAISE(E_PARFILE, "ERROR: %s is not in the right format\n", file);
        }
    else
        RAISE(E_PARFILE, "ERROR: Opening the contour file %s\n", file);

    fclose(IN);
    return(k);
}

