#include<stdio.h>
#include<math.h>
#include<fonction.h>
#include<constant.h>
#include<dimension.h>
#include<structure.h>

/****************************************************************/
/*      nom:        e_time              */
/*      auteur:     Jean-Paul Kneib         */
/*      date:       10/02/92            */
/*      place:      Toulouse            */
/****************************************************************
 * Return the time delay at point <pi>
 */
double  e_time(struct point pi, double dlsds, double zs)
{
    const extern  struct  pot lens[];
    const extern  struct  g_cosmo C;

    double  cst, dl, time;
    struct  point   gr;

    dl = distcosmo1(lens[0].z);
    cst = (1 + lens[0].z) * dl / dlsds * th_a2_day / C.h;   /* time delay in days */

    gr = e_grad(&pi, dlsds, zs);
    gr.x *= dlsds;
    gr.y *= dlsds;
    time = cst * ((gr.x * gr.x + gr.y * gr.y) / 2. - e_pot(pi, dlsds));

    return(time);
}

/* Return the time delay at the position of an image
 * Warning: This function cannot be used during optimization 
 * because it call e_grad_gal() with NULL np_b0 argument.
 *
 * Args:
 *  - image: a galaxie structure with mandatory attributes C, grad, z, dr
 */
double  e_time_gal(struct galaxie *image)
{
    const extern  struct  pot lens[];
    const extern  struct  g_cosmo C;

    double  cst, dl, time;
    struct  point   gr;

    dl = distcosmo1(lens[0].z);
    cst = (1 + lens[0].z) * dl / image->dr * th_a2_day / C.h;   /* time delay in days */

    gr = e_grad_gal(image, NULL);
    gr.x *= image->dr;
    gr.y *= image->dr;
    time = cst * ((gr.x * gr.x + gr.y * gr.y) / 2. - e_pot(image->C, image->dr));

    return(time);
}
