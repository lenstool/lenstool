#include<stdio.h>
#include<math.h>
#include<fonction.h>
#include<constant.h>
#include<dimension.h>
#include<structure.h>

/*
file e_skew.c

Peter Taylor
9-Dec-2016
Mullard Space Science Laboratory

content: function to compute relevant parts for a skewed potential see Taylor et al. 2016 or 2017. See e_grad.c file potential 812 and 813 for more details
*/

/*code for potential 813*/
double f_weight(double x, double y, double alpha, double beta, double rc, double rcslope)
{
return ((1 + alpha*atan((beta*sqrt(pow(x,2) + pow(y,2)))/rc)*((x*cos(rcslope))/sqrt(pow(x,2) + pow(y,2)) + (y*sin(rcslope))/sqrt(pow(x,2) + pow(y,2)))));
}

double f_weightx(double x, double y, double alpha, double beta, double rc, double rcslope)
{
return ((alpha*atan((beta*sqrt(pow(x,2) + pow(y,2)))/rc)*(-((pow(x,2)*cos(rcslope))/pow(pow(x,2) + pow(y,2),1.5)) + cos(rcslope)/sqrt(pow(x,2) + pow(y,2)) - (x*y*sin(rcslope))/pow(pow(x,2) + pow(y,2),1.5))+ (alpha*beta*x*((x*cos(rcslope))/sqrt(pow(x,2) + pow(y,2)) + (y*sin(rcslope))/sqrt(pow(x,2) + pow(y,2))))/(rc*sqrt(pow(x,2) + pow(y,2))*(1 + (pow(beta,2)*(pow(x,2) + pow(y,2)))/pow(rc,2)))));
}

double f_weighty(double x, double y, double alpha, double beta, double rc, double rcslope )
{
return ((alpha*atan((beta*sqrt(pow(x,2) + pow(y,2)))/rc)*(-((x*y*cos(rcslope))/pow(pow(x,2) + pow(y,2),1.5)) - (pow(y,2)*sin(rcslope))/pow(pow(x,2) + pow(y,2),1.5) + sin(rcslope)/sqrt(pow(x,2) + pow(y,2))) + (alpha*beta*y*((x*cos(rcslope))/sqrt(pow(x,2) + pow(y,2)) + (y*sin(rcslope))/sqrt(pow(x,2) + pow(y,2))))/(rc*sqrt(pow(x,2) + pow(y,2))*(1 + (pow(beta,2)*(pow(x,2) + pow(y,2)))/pow(rc,2)))));
}

double f_weightxx(double x, double y, double skew, double beta, double rc, double weightangle )
{
return (((2*skew*beta*x*(-((pow(x,2)*cos(weightangle))/pow(pow(x,2) + pow(y,2),1.5)) + cos(weightangle)/sqrt(pow(x,2) + pow(y,2)) - (x*y*sin(weightangle))/pow(pow(x,2) + pow(y,2),1.5)))/(rc*sqrt(pow(x,2) + pow(y,2))*(1 + (pow(beta,2)*(pow(x,2) + pow(y,2)))/pow(rc,2))) + skew*((-2*pow(beta,3)*pow(x,2))/(pow(rc,3)*sqrt(pow(x,2) + pow(y,2))*pow(1 + (pow(beta,2)*(pow(x,2) + pow(y,2)))/pow(rc,2),2))- (beta*pow(x,2))/(rc*pow(pow(x,2) + pow(y,2),1.5)*(1 + (pow(beta,2)*(pow(x,2) + pow(y,2)))/pow(rc,2))) + beta/(rc*sqrt(pow(x,2) + pow(y,2))*(1 + (pow(beta,2)*(pow(x,2) + pow(y,2)))/pow(rc,2))))*((x*cos(weightangle))/sqrt(pow(x,2) + pow(y,2)) + (y*sin(weightangle))/sqrt(pow(x,2) + pow(y,2))) + skew*atan((beta*sqrt(pow(x,2) + pow(y,2)))/rc)*(((-2*x)/pow(pow(x,2) + pow(y,2),1.5) + x*((3*pow(x,2))/pow(pow(x,2) + pow(y,2),2.5) - pow(pow(x,2) + pow(y,2),-1.5)))*cos(weightangle)+ y*((3*pow(x,2))/pow(pow(x,2) + pow(y,2),2.5) - pow(pow(x,2) + pow(y,2),-1.5))*sin(weightangle))));
}

double f_weightxy(double x, double y, double skew, double beta, double rc, double weightangle )
{
return ((skew*atan((beta*sqrt(pow(x,2) + pow(y,2)))/rc)*((3*pow(x,2)*y*cos(weightangle))/pow(pow(x,2) + pow(y,2),2.5) - (y*cos(weightangle))/pow(pow(x,2) + pow(y,2),1.5) + (3*x*pow(y,2)*sin(weightangle))/pow(pow(x,2) + pow(y,2),2.5) - (x*sin(weightangle))/pow(pow(x,2) + pow(y,2),1.5)) + (skew*beta*y*(-((pow(x,2)*cos(weightangle))/pow(pow(x,2) + pow(y,2),1.5)) + cos(weightangle)/sqrt(pow(x,2) + pow(y,2)) - (x*y*sin(weightangle))/pow(pow(x,2) + pow(y,2),1.5)))/(rc*sqrt(pow(x,2) + pow(y,2))*(1 + (pow(beta,2)*(pow(x,2) + pow(y,2)))/pow(rc,2))) + (skew*beta*x*(-((x*y*cos(weightangle))/pow(pow(x,2) + pow(y,2),1.5)) - (pow(y,2)*sin(weightangle))/pow(pow(x,2) + pow(y,2),1.5) + sin(weightangle)/sqrt(pow(x,2) + pow(y,2))))/(rc*sqrt(pow(x,2) + pow(y,2))*(1 + (pow(beta,2)*(pow(x,2) + pow(y,2)))/pow(rc,2))) - (2*skew*pow(beta,3)*x*y*((x*cos(weightangle))/sqrt(pow(x,2) + pow(y,2)) + (y*sin(weightangle))/sqrt(pow(x,2) + pow(y,2))))/(pow(rc,3)*sqrt(pow(x,2) + pow(y,2))*pow(1 + (pow(beta,2)*(pow(x,2) + pow(y,2)))/pow(rc,2),2)) - (skew*beta*x*y*((x*cos(weightangle))/sqrt(pow(x,2) + pow(y,2)) + (y*sin(weightangle))/sqrt(pow(x,2) + pow(y,2))))/(rc*pow(pow(x,2) + pow(y,2),1.5)*(1 + (pow(beta,2)*(pow(x,2) + pow(y,2)))/pow(rc,2)))));
}

double f_weightyy(double x, double y, double skew, double beta, double rc, double weightangle )
{
return (((2*skew*beta*y*(-((x*y*cos(weightangle))/pow(pow(x,2) + pow(y,2),1.5)) - (pow(y,2)*sin(weightangle))/pow(pow(x,2) + pow(y,2),1.5) + sin(weightangle)/sqrt(pow(x,2) + pow(y,2))))/(rc*sqrt(pow(x,2) + pow(y,2))*(1 + (pow(beta,2)*(pow(x,2) + pow(y,2)))/pow(rc,2))) + skew*((-2*pow(beta,3)*pow(y,2))/(pow(rc,3)*sqrt(pow(x,2) + pow(y,2))*pow(1 + (pow(beta,2)*(pow(x,2) + pow(y,2)))/pow(rc,2),2))- (beta*pow(y,2))/(rc*pow(pow(x,2) + pow(y,2),1.5)*(1 + (pow(beta,2)*(pow(x,2) + pow(y,2)))/pow(rc,2))) + beta/(rc*sqrt(pow(x,2) + pow(y,2))*(1 + (pow(beta,2)*(pow(x,2) + pow(y,2)))/pow(rc,2))))*((x*cos(weightangle))/sqrt(pow(x,2) + pow(y,2)) + (y*sin(weightangle))/sqrt(pow(x,2) + pow(y,2))) + skew*atan((beta*sqrt(pow(x,2) + pow(y,2)))/rc)*(x*((3*pow(y,2))/pow(pow(x,2) + pow(y,2),2.5) - pow(pow(x,2) + pow(y,2),-1.5))*cos(weightangle) + ((-2*y)/pow(pow(x,2) + pow(y,2),1.5) + y*((3*pow(y,2))/pow(pow(x,2) + pow(y,2),2.5) - pow(pow(x,2) + pow(y,2),-1.5)))*sin(weightangle))));
}


/*Now the code for potential 812*/

double f_newx (double x, double y, double alpha, double beta)
{
return(((pow(1 - pow(alpha,2),0.75)*x)/(1 + alpha*((x*cos(beta))/sqrt(pow(x,2) + pow(y,2)) + (y*sin(beta))/sqrt(pow(x,2) + pow(y,2))))));
}

double f_newx_x (double x, double y, double alpha, double beta)
{
return((-((alpha*pow(1 - pow(alpha,2),0.75)*x*(-((pow(x,2)*cos(beta))/pow(pow(x,2) + pow(y,2),1.5)) + cos(beta)/sqrt(pow(x,2) + pow(y,2)) - (x*y*sin(beta))/pow(pow(x,2) + pow(y,2),1.5)))/pow(1 + alpha*((x*cos(beta))/sqrt(pow(x,2) + pow(y,2)) + (y*sin(beta))/sqrt(pow(x,2) + pow(y,2))),2)) + pow(1 - pow(alpha,2),0.75)/(1 + alpha*((x*cos(beta))/sqrt(pow(x,2) + pow(y,2)) + (y*sin(beta))/sqrt(pow(x,2) + pow(y,2))))));
}

double f_newx_y (double x, double y, double alpha, double beta)
{
return((-((alpha*pow(1 - pow(alpha,2),0.75)*x*(-((x*y*cos(beta))/pow(pow(x,2) + pow(y,2),1.5)) - (pow(y,2)*sin(beta))/pow(pow(x,2) + pow(y,2),1.5) + sin(beta)/sqrt(pow(x,2) + pow(y,2))))/pow(1 + alpha*((x*cos(beta))/sqrt(pow(x,2) + pow(y,2)) + (y*sin(beta))/sqrt(pow(x,2) + pow(y,2))),2))));
}

double f_newx_xx (double x, double y, double alpha, double beta)
{
return(((-2*alpha*pow(1 - pow(alpha,2),0.75)*(-((pow(x,2)*cos(beta))/pow(pow(x,2) + pow(y,2),1.5)) + cos(beta)/sqrt(pow(x,2) + pow(y,2)) - (x*y*sin(beta))/pow(pow(x,2) + pow(y,2),1.5)))/pow(1 + alpha*((x*cos(beta))/sqrt(pow(x,2) + pow(y,2)) + (y*sin(beta))/sqrt(pow(x,2) + pow(y,2))),2) + pow(1 - pow(alpha,2),0.75)*x*((2*pow(alpha,2)*pow(-((pow(x,2)*cos(beta))/pow(pow(x,2) + pow(y,2),1.5)) + cos(beta)/sqrt(pow(x,2) + pow(y,2)) - (x*y*sin(beta))/pow(pow(x,2) + pow(y,2),1.5),2))/pow(1 + alpha*((x*cos(beta))/sqrt(pow(x,2) + pow(y,2)) + (y*sin(beta))/sqrt(pow(x,2) + pow(y,2))),3) - (alpha*((3*pow(x,3)*cos(beta))/pow(pow(x,2) + pow(y,2),2.5) - (3*x*cos(beta))/pow(pow(x,2) + pow(y,2),1.5) + (3*pow(x,2)*y*sin(beta))/pow(pow(x,2) + pow(y,2),2.5) - (y*sin(beta))/pow(pow(x,2) + pow(y,2),1.5)))/pow(1 + alpha*((x*cos(beta))/sqrt(pow(x,2) + pow(y,2)) + (y*sin(beta))/sqrt(pow(x,2) + pow(y,2))),2))));
}

double f_newx_yy (double x, double y, double alpha, double beta)
{
return((pow(1 - pow(alpha,2),0.75)*x*((2*pow(alpha,2)*pow(-((x*y*cos(beta))/pow(pow(x,2) + pow(y,2),1.5)) - (pow(y,2)*sin(beta))/pow(pow(x,2) + pow(y,2),1.5) + sin(beta)/sqrt(pow(x,2) + pow(y,2)),2))/pow(1 + alpha*((x*cos(beta))/sqrt(pow(x,2) + pow(y,2)) + (y*sin(beta))/sqrt(pow(x,2) + pow(y,2))),3) - (alpha*((3*x*pow(y,2)*cos(beta))/pow(pow(x,2) + pow(y,2),2.5) - (x*cos(beta))/pow(pow(x,2) + pow(y,2),1.5) + (3*pow(y,3)*sin(beta))/pow(pow(x,2) + pow(y,2),2.5) - (3*y*sin(beta))/pow(pow(x,2) + pow(y,2),1.5)))/pow(1 + alpha*((x*cos(beta))/sqrt(pow(x,2) + pow(y,2)) + (y*sin(beta))/sqrt(pow(x,2) + pow(y,2))),2))));
}

double f_newx_xy (double x, double y, double alpha, double beta)
{
return(((2*pow(alpha,2)*pow(1 - pow(alpha,2),0.75)*x*(-((pow(x,2)*cos(beta))/pow(pow(x,2) + pow(y,2),1.5)) + cos(beta)/sqrt(pow(x,2) + pow(y,2)) - (x*y*sin(beta))/pow(pow(x,2) + pow(y,2),1.5))*(-((x*y*cos(beta))/pow(pow(x,2) + pow(y,2),1.5)) - (pow(y,2)*sin(beta))/pow(pow(x,2) + pow(y,2),1.5) + sin(beta)/sqrt(pow(x,2) + pow(y,2))))/pow(1 + alpha*((x*cos(beta))/sqrt(pow(x,2) + pow(y,2)) + (y*sin(beta))/sqrt(pow(x,2) + pow(y,2))),3) - (alpha*pow(1 - pow(alpha,2),0.75)*x*((3*pow(x,2)*y*cos(beta))/pow(pow(x,2) + pow(y,2),2.5) - (y*cos(beta))/pow(pow(x,2) + pow(y,2),1.5) + (3*x*pow(y,2)*sin(beta))/pow(pow(x,2) + pow(y,2),2.5) - (x*sin(beta))/pow(pow(x,2) + pow(y,2),1.5)))/pow(1 + alpha*((x*cos(beta))/sqrt(pow(x,2) + pow(y,2)) + (y*sin(beta))/sqrt(pow(x,2) + pow(y,2))),2) - (alpha*pow(1 - pow(alpha,2),0.75)*(-((x*y*cos(beta))/pow(pow(x,2) + pow(y,2),1.5)) - (pow(y,2)*sin(beta))/pow(pow(x,2) + pow(y,2),1.5) + sin(beta)/sqrt(pow(x,2) + pow(y,2))))/pow(1 + alpha*((x*cos(beta))/sqrt(pow(x,2) + pow(y,2)) + (y*sin(beta))/sqrt(pow(x,2) + pow(y,2))),2)));
}

double f_newy (double x, double y, double alpha, double beta)
{
return(((pow(1 - pow(alpha,2),0.75)*y)/(1 + alpha*((x*cos(beta))/sqrt(pow(x,2) + pow(y,2)) + (y*sin(beta))/sqrt(pow(x,2) + pow(y,2))))));
}

double f_newy_x (double x, double y, double alpha, double beta)
{
return((-((alpha*pow(1 - pow(alpha,2),0.75)*y*(-((pow(x,2)*cos(beta))/pow(pow(x,2) + pow(y,2),1.5)) + cos(beta)/sqrt(pow(x,2) + pow(y,2)) - (x*y*sin(beta))/pow(pow(x,2) + pow(y,2),1.5)))/pow(1 + alpha*((x*cos(beta))/sqrt(pow(x,2) + pow(y,2)) + (y*sin(beta))/sqrt(pow(x,2) + pow(y,2))),2))));
}

double f_newy_y (double x, double y, double alpha, double beta)
{
return((-((alpha*pow(1 - pow(alpha,2),0.75)*y*(-((x*y*cos(beta))/pow(pow(x,2) + pow(y,2),1.5)) - (pow(y,2)*sin(beta))/pow(pow(x,2) + pow(y,2),1.5) + sin(beta)/sqrt(pow(x,2) + pow(y,2))))/pow(1 + alpha*((x*cos(beta))/sqrt(pow(x,2) + pow(y,2)) + (y*sin(beta))/sqrt(pow(x,2) + pow(y,2))),2)) + pow(1 - pow(alpha,2),0.75)/(1 + alpha*((x*cos(beta))/sqrt(pow(x,2) + pow(y,2)) + (y*sin(beta))/sqrt(pow(x,2) + pow(y,2))))));
}

double f_newy_xx (double x, double y, double alpha, double beta)
{
return((pow(1 - pow(alpha,2),0.75)*y*((2*pow(alpha,2)*pow(-((pow(x,2)*cos(beta))/pow(pow(x,2) + pow(y,2),1.5)) + cos(beta)/sqrt(pow(x,2) + pow(y,2)) - (x*y*sin(beta))/pow(pow(x,2) + pow(y,2),1.5),2))/pow(1 + alpha*((x*cos(beta))/sqrt(pow(x,2) + pow(y,2)) + (y*sin(beta))/sqrt(pow(x,2) + pow(y,2))),3) - (alpha*((3*pow(x,3)*cos(beta))/pow(pow(x,2) + pow(y,2),2.5) - (3*x*cos(beta))/pow(pow(x,2) + pow(y,2),1.5) + (3*pow(x,2)*y*sin(beta))/pow(pow(x,2) + pow(y,2),2.5) - (y*sin(beta))/pow(pow(x,2) + pow(y,2),1.5)))/pow(1 + alpha*((x*cos(beta))/sqrt(pow(x,2) + pow(y,2)) + (y*sin(beta))/sqrt(pow(x,2) + pow(y,2))),2))));
}

double f_newy_yy (double x, double y, double alpha, double beta)
{
return(((-2*alpha*pow(1 - pow(alpha,2),0.75)*(-((x*y*cos(beta))/pow(pow(x,2) + pow(y,2),1.5)) - (pow(y,2)*sin(beta))/pow(pow(x,2) + pow(y,2),1.5) + sin(beta)/sqrt(pow(x,2) + pow(y,2))))/pow(1 + alpha*((x*cos(beta))/sqrt(pow(x,2) + pow(y,2)) + (y*sin(beta))/sqrt(pow(x,2) + pow(y,2))),2) + pow(1 - pow(alpha,2),0.75)*y*((2*pow(alpha,2)*pow(-((x*y*cos(beta))/pow(pow(x,2) + pow(y,2),1.5)) - (pow(y,2)*sin(beta))/pow(pow(x,2) + pow(y,2),1.5) + sin(beta)/sqrt(pow(x,2) + pow(y,2)),2))/pow(1 + alpha*((x*cos(beta))/sqrt(pow(x,2) + pow(y,2)) + (y*sin(beta))/sqrt(pow(x,2) + pow(y,2))),3) - (alpha*((3*x*pow(y,2)*cos(beta))/pow(pow(x,2) + pow(y,2),2.5) - (x*cos(beta))/pow(pow(x,2) + pow(y,2),1.5) + (3*pow(y,3)*sin(beta))/pow(pow(x,2) + pow(y,2),2.5) - (3*y*sin(beta))/pow(pow(x,2) + pow(y,2),1.5)))/pow(1 + alpha*((x*cos(beta))/sqrt(pow(x,2) + pow(y,2)) + (y*sin(beta))/sqrt(pow(x,2) + pow(y,2))),2))));
}


double f_newy_xy (double x, double y, double alpha, double beta)
{
return(((2*pow(alpha,2)*pow(1 - pow(alpha,2),0.75)*y*(-((pow(x,2)*cos(beta))/pow(pow(x,2) + pow(y,2),1.5)) + cos(beta)/sqrt(pow(x,2) + pow(y,2)) - (x*y*sin(beta))/pow(pow(x,2) + pow(y,2),1.5))*(-((x*y*cos(beta))/pow(pow(x,2) + pow(y,2),1.5)) - (pow(y,2)*sin(beta))/pow(pow(x,2) + pow(y,2),1.5) + sin(beta)/sqrt(pow(x,2) + pow(y,2))))/pow(1 + alpha*((x*cos(beta))/sqrt(pow(x,2) + pow(y,2)) + (y*sin(beta))/sqrt(pow(x,2) + pow(y,2))),3) - (alpha*pow(1 - pow(alpha,2),0.75)*y*((3*pow(x,2)*y*cos(beta))/pow(pow(x,2) + pow(y,2),2.5) - (y*cos(beta))/pow(pow(x,2) + pow(y,2),1.5) + (3*x*pow(y,2)*sin(beta))/pow(pow(x,2) + pow(y,2),2.5) - (x*sin(beta))/pow(pow(x,2) + pow(y,2),1.5)))/pow(1 + alpha*((x*cos(beta))/sqrt(pow(x,2) + pow(y,2)) + (y*sin(beta))/sqrt(pow(x,2) + pow(y,2))),2) - (alpha*pow(1 - pow(alpha,2),0.75)*(-((pow(x,2)*cos(beta))/pow(pow(x,2) + pow(y,2),1.5)) + cos(beta)/sqrt(pow(x,2) + pow(y,2)) - (x*y*sin(beta))/pow(pow(x,2) + pow(y,2),1.5)))/
pow(1 + alpha*((x*cos(beta))/sqrt(pow(x,2) + pow(y,2)) + (y*sin(beta))/sqrt(pow(x,2) + pow(y,2))),2)));
}










