static int chi2_img( double *chi2, double *lh0 )
{
#ifndef CHIRES
    const extern struct g_mode    M;
#endif
    const extern struct g_image   I;
    extern double chip, chia, chix, chiy;
    extern struct galaxie   multi[NFMAX][NIMAX];
    extern int nwarn; /*counts each time a barycenter is not found in a
                             small source triangle. see e_im_prec() function*/
    const extern int optim_z;
    

#ifdef CHIRES
    double rmss_tot, rmsi_tot;  // image rms tot in source and image plane
    int ntot; // total number of multiple images

    fprintf(OUT, "chi multiples\n");
    fprintf(OUT, " N    ID    z   Narcs   chip     chix    chiy    chia  rmss  rmsi  nwarn\n");

    ntot = rmss_tot = rmsi_tot = 0;
#endif
    chip = chia = chix = chiy = 0.;
    nwarn = 0;
    
    int i;
    int det_stop = 0; // if det_step then we should stop and return -1
    
    // Define the number of threads we want to use 
    int num_threads = 1;
#ifdef _OPENMP
    num_threads = omp_get_max_threads();
#endif
    if (num_threads > I.n_mult)
      num_threads = I.n_mult;
#ifdef CHIRES 
   num_threads = 1;
#endif
//    omp_set_nested(1);
    /* for each familly */
    #pragma omp parallel for schedule(dynamic, 1) num_threads(num_threads)
    for (i = 0; i < I.n_mult; i++)
    {
        if (det_stop)  //we should stop end return -1 after cycle
	    continue;
       
        struct point Ps[NIMAX];
        struct point multib[NIMAX];
        int    nimages;  // number of valid images in multib
        struct point Bs; /*barycenter of a familly of I.mult[i] sources*/
        struct galaxie *pima; // pointer to an arclet
       
        double I2x, I2y; //sigma and sigma^2
        double dLhood0;  // lh0 for a given familly of multiple images (I.forme=10 || sigposAs.bk !=0)
        double dx, dy; //error in position in the source plane
        int j;
        int    n_famille = i;
       
#ifdef CHIRES
        double chipi, chixi, chiyi, chiai;
        double rmss, rmsi;   // image rms in source and image plane
        chipi = chixi = chiyi = chiai = 0.;
        rmss = rmsi = 0.;
#endif
        dLhood0 = 0.;

        // In all cases
        I2x = I2y = I.sig2pos[i][0];

        /* optim_z can be different from 0 with the o_runz*() functions*/
        if (optim_z == 0)
            /* compute the sources positions in Ps*/
            o_dpl(I.mult[i], multi[i], Ps);
        else
        {
            for (j = 0; j < I.mult[i]; j++)
            {
                pima = &multi[i][j];
                Ps[j].x = pima->C.x - pima->Grad.x * multi[i][0].dr;
                Ps[j].y = pima->C.y - pima->Grad.y * multi[i][0].dr;
            }

            Ps[I.mult[i]] = Ps[0];
        }

        /*Bs contains the barycenter of the I.mult[i] sources positions Ps*/
        Bs = bcentlist(Ps, I.mult[i]);

        // *********************************
        // SINGLE IMAGE systems
        //
        if ( I.mult[i] == 1 )
        {
	    //TODO: chi2SglImage isn't thread save yet!!!
	    #pragma omp critical
	     {
		chip += chi2SglImage( &multi[i][0], &Ps[0] );
	     }
        }
        else
            // **************************************
            // MULTIPLE IMAGES systems
            //
        {
            /*return in multib a list of image for each arclet of familly i*/
            nimages = unlens_bc(Ps, Bs, multi[i], multib, I.mult[i], n_famille, &det_stop);
#ifndef CHIRES
            if ( nimages != I.mult[i] && M.inverse > 2 )
            {
#ifdef DEBUG
	        #pragma omp critical
		 {
		    printf( "Abord chi2 with system %s. %d/%d images found ",multi[i][0].n,nimages,I.mult[i]);
		    for( j = 0; j < I.mult[i]; j++ )
		      if( multib[j].x != multi[i][j].C.x ) 
			printf( "%s ", multi[i][j].n);
		    else
		      printf( "[%s] ", multi[i][j].n);		    
		    printf( "\n");
		 }
#endif
               #pragma omp atomic
	       det_stop++;     //we "return -1"	       	       
            }
	    if (det_stop)
	        continue;
#endif	     
            // For each image in familly i
            for (j = 0; j < I.mult[i]; j++ )
            {
                pima = &multi[i][j];

                if ( I.forme == -1 )
                {
                    I2x = I2y = I.sig2pos[i][j];
                }
                else if ( I.forme == -10 )
                {
                    I2x = I2y = pima->E.a * pima->E.b;
                }
                else if ( I.forme == -11 )
                {
                    I2x = pima->E.a * cos(pima->E.theta);
                    I2y = pima->E.b * cos(pima->E.theta);
                    I2x = I2x * I2x;
                    I2y = I2y * I2y;
                }

                // update normalisation factor for image ij
                dLhood0 += log( 4.*PI * PI * (I2x * I2y) );

                dx = multi[i][j].C.x - multib[j].x;
                dy = multi[i][j].C.y - multib[j].y;
	       
	        #pragma omp atomic
                chip += dx * dx / I2x + dy * dy / I2y;
#ifdef CHIRES
	        double rmsij, rmssj, chipij;
                chipi += dx * dx / I2x + dy * dy / I2y;
                chipij = dx * dx / I2x + dy * dy / I2y;
                rmsi  += dx * dx + dy * dy;
                rmsij  = dx * dx + dy * dy;
                // source plane rmss
                dx = Bs.x - Ps[j].x;
                dy = Bs.y - Ps[j].y;
                rmss += dx * dx + dy * dy;	        
	        rmssj = dx * dx + dy * dy;
	       
	        //TODO: do we need warnj variable here?
                int warnj = 0;   
	        
                #pragma omp critical
	        fprintf(OUT, "%2d %6s %.3lf   1   %7.2lf  %6.2lf  %6.2lf  %6.2lf  %.2lf  %.2lf    %d\n", i, multi[i][j].n, multi[i][j].z, chipij, 0., 0., 0., sqrt(rmssj), sqrt(rmsij), warnj);
#endif
            } //end "for j" cycle

            // update the total statistics
	    #pragma omp atomic
            *lh0 += dLhood0;

            // TODO: Implement chix, chiy and chia calculation in image plane
            /*
	     * #pragma omp critical
	     * {
	         for (j=0;j<I.mult[i];j++)
                  {
                   da=diff_mag(multi[i][j],multib[j]);
                   chia += da*da/I.sig2amp;
                   };
	         } */
#ifdef CHIRES
	    #pragma omp critical
	     {
		rmss_tot += rmss;
		rmsi_tot += rmsi;
		ntot += nimages;
		nwarn += I.mult[i] - nimages;
		
		if ( nimages != 0 )
		  {
		     rmss = sqrt(rmss / nimages);
		     rmsi = sqrt(rmsi / nimages);
		  }
		
		fprintf(OUT, "%2d %6s %.3lf   %d   %7.2lf  %6.2lf  %6.2lf  %6.2lf  %.2lf  %.2lf    %d\n",
			i, multi[i][0].n, multi[i][0].z, I.mult[i], chipi, chixi, chiyi, chiai, sqrt(rmss), sqrt(rmsi), I.mult[i] - nimages);
	     } //end omp critical
#endif	   
        } // end of case with I.mult[i] > 1

    } /*end for each familly*/
 
   if (det_stop)
     return -1;
    
   
#ifdef CHIRES   
    // Total statistics
    rmss_tot = sqrt(rmss_tot / ntot);
    rmsi_tot = sqrt(rmsi_tot / ntot);
    fprintf(OUT, "chimul                %7.2lf  %6.2lf  %6.2lf  %6.2lf  %.2lf  %.2lf    %d\n", chip, chix, chiy, chia, rmss_tot, rmsi_tot, nwarn);
    fprintf(OUT, "log(Likelihood)       %7.2lf\n",-0.5*(chip+(*lh0)));
#endif

    *chi2 = chip + chia + chix + chiy;
#ifdef DEBUG
    printf( "All images found!\n");
#endif
    return 0; // no error, no warning
}

