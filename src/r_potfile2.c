#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<math.h>
#include<fonction.h>
#include<constant.h>
#include<dimension.h>
#include<structure.h>

/****************************************************************/
/*                 Program  : grille				*/
/*                 Version  : 1 mai 1992			*/
/*                 Location : Obs. Toulouse			*/
/*                 Auteur   : jean-paul				*/
/****************************************************************/

void r_potfile2(FILE *IN,FILE *OUT)
{
	extern struct	g_pot	Q;
	extern  struct  g_mode          M;
	int	nplens;
	char	second[20],third[FILENAME_SIZE+10];
	
	
	Q.ircut=Q.isigma=Q.islope=0;
	Q.type=81;
	Q.corekpc=5.; Q.mag0=17.;
	Q.cutkpc1=50.;Q.slope1=4.; Q.sigma1=200.;
	
	
	NPRINTF(stderr,"READ: pot file");
	
	fmot(IN,second);
	while(strcmp(second,"end")) 
    {
        flire(IN,third);
        CHECK_THIRD(FILENAME_SIZE+10)

		if (!strcmp(second,"filein"))
		{
			sscanf(third,"%d%s",&Q.ftype,Q.potfile); 
			fprintf(OUT,"\t%s\t%d %s\n",second,Q.ftype,Q.potfile);
		}
		if (!strcmp(second,"type"))
		{
            sscanf(third,"%d",&Q.type); 
            fprintf(OUT,"\t%s\t%d\n",second,Q.type);
		}
		if (!strcmp(second,"zlens") || !strcmp(second,"z_lens") )
		{
            sscanf(third,"%lf",&Q.zlens); 
            fprintf(OUT,"\t%s\t%lf\n",second,Q.zlens);
		}
		if (!strcmp(second,"mag0"))
		{
            sscanf(third,"%lf",&Q.mag0); 
            fprintf(OUT,"\t%s\t%lf\n",second,Q.mag0);
		}
		if (!strcmp(second,"corekpc"))
		{
            sscanf(third,"%lf",&Q.corekpc); 
            fprintf(OUT,"\t%s\t%lf\n",second,Q.corekpc);
		}
		if (!strcmp(second,"cutkpc"))
		{
            sscanf(third,"%d%lf%lf",&Q.ircut,&Q.cutkpc1,&Q.cutkpc2); 
			
            fprintf(OUT,"\t%s\t%d %lf %lf\n",second,Q.ircut,Q.cutkpc1,Q.cutkpc2);
        }
		if (!strcmp(second,"slope"))
		{
            sscanf(third,"%d%lf%lf",&Q.islope,&Q.slope1,&Q.slope2);
			
            fprintf(OUT,"\t%s\t%d %lf %lf\n",second,Q.islope,Q.slope1,Q.slope2);
	    }
		if (!strcmp(second,"sigma"))
		{
            sscanf(third,"%d%lf%lf",&Q.isigma,&Q.sigma1,&Q.sigma2);
			
            fprintf(OUT,"\t%s\t%d %lf %lf\n",second,Q.isigma,Q.sigma1,Q.sigma2);
        }

        // Read the next line
	    fmot(IN,second);
	}
	fprintf(OUT,"\t%s\n",second);
	
	
	/* initialize the potential from file */
	
	if (Q.zlens<=0.)
        RAISE(E_PARFILE, "FATAL ERROR: z_lens not defined in potfile\n");
}
