#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<math.h>
#include<float.h>
#include<fonction.h>
#include<constant.h>
#include<dimension.h>
#include<structure.h>

#undef CHIRES

static double lhood0;	// normalizing term in the Likelihood definition (only useful for the forme=10 mode)


static double chi2SglImage( struct galaxie *pima, struct point *ps );
static void chi2_src( double *chi2, double *lhood0);
static int chi2_img( double *chi2, double *lhood0);



/********************************************************/
/*		fonction: o_chi 			*/
/*		auteur: jpk				*/
/********************************************************
 * Return the total chi2 value
 * 
 * Global variables used :
 * - chip, chia, chix, chiy, chil, chis, chi_im, M, G, I, lens, shm, nshmap, arclet
 *   multi, cl, narclet, n_famille, nwarn, optim_z, elix, SC, amplifi_mat, amplifi_matinv
 *   map_p, map_axx, map_ayy
 * - in sig2posS() : amplifi, n_famille
 * - in sig2posSj() : amplifi, n_famille
 * - in sig2posS4j() : G, lens, lens_table
 * - in distcosmo1() : C
 * - in e_zeroamp() : G, lens, lens_table
 * - in weight_baryc() : amplifi, n_famille, G, lens, lens_table, multi, C
 * - in fmin_ell() : elix, SC, G, lens, lens_table
 * - in o_flux() : n_famille, multi, G, lens, lens_table
 * - in o_chi_flux() : I
 * - in amplif() : I, amplifi, G, lens, C, lens_table
 * - in amplif_mat() : I, multi, amplifi_mat, G, lens, lens_table, C
 * - in amplif_matinv() : I, multi, amplifi_matinv, G, lens, lens_table, C
 * - in dratio() : C
 * - in e_unmag() : G, lens, lens_table
 * - in chi_invim() : pi, ps, M, iline, radial, tangent, nrline, ntline, 
 * 				      CL, lens, flagr, flagt, G, lens_table
 * - in sp_set() : v_xx, v_yy
 * - in o_dpl() : G, lens, lens_table
 * - in o_mag_m() : G, lens, lens_table
 * - in unlens_bc() : it, nwarn, n_famille, distmin, G, lens, lens_table
 */
#ifdef CHIRES
FILE *OUT;

void o_chires(char *filename)
#else
double o_chi()
#endif
{
	/* variables externes */
	
	extern struct g_mode    M;
//	extern struct g_source  S;
	extern struct g_grille  G;
	extern struct g_image   I;
//	extern struct g_cosmo   C;
	extern struct g_dyn     D;
	extern struct pot       lens[NLMAX];
//	extern struct ipot      ip;
	extern struct shear     shm[];
	extern struct galaxie   arclet[NAMAX];
	extern struct cline     cl[];
	extern double chil,chis,chi_im,chi_vel,chi_mass;
	extern int nshmap;
	extern int narclet; //,block[][NPAMAX]; 
	
	extern double elix;
	extern struct point SC;
//	extern double amplifi[NIMAX][NIMAX];
	
	extern double **map_p,**map_axx,**map_ayy;
	extern int nplo,**imuo;
	extern double drima,**imo,**ero,**soo;
	extern struct pixlist plo[];

	/* local variables */
	
	register int i;
	double chi_mult;
    //double chi_vel;
	double x,chish,chi;
	double ells, wht_ell; /*error and weight in ellipticity 
										in the image plane*/	
//	double diff_mag();
	double qp,dp,tp;
	struct ellipse ell_sou,source,ampli; /* variables locales images */
	
#ifdef CHIRES	
	OUT=fopen(filename,"w");
#endif
	
	
	/*variables initialisation*/
	chi = 0.;
	chish = 0.;
	chi_im = 0.;
	chis = 0.;
	chi_mult = 0.;
	chil = 0.;
    chi_vel = 0.;
	chi_mass = 0.;
	lhood0 = 0;
	
	/* spline mapping */
	if (lens[0].type==10)
	{
		sp_set(map_p,G.nx,G.ny,map_axx,map_ayy);
	/*
		wr_pot("p.ipx",map_p);
		wr_mass("m.ipx",map_axx,map_ayy);
	*/
	}
	
	/* if cleanset is true in cleanlens*/
	if(M.iclean!=0)
	{
		/* chi_im is the error associated to the transformation image -> source*/
		chi_im=chi_invim(imo,plo,nplo,drima,soo,ero,imuo);
#ifdef CHIRES
		fprintf(OUT,"chi_im %.2lf\n",chi_im);
#endif
	}
	
	/* if arcletstat is true in image keyword*/
	if(I.stat!=0)
	{
#ifdef CHIRES
		fprintf(OUT,"chis arclets\n");
#endif
			
		if ((I.stat==1)||(I.stat==2))
		{
			for (i=0;i<narclet;i++)
			{
				/* optimisation du z */
				if (I.stat==2)
				{
					if(indexCmp(I.nza,arclet[i+1].n)) //TO CHECK (previous version : I.nza!=i+1)
					{
						SC=arclet[i].C;
						ampli=e_unmag(&SC,arclet[i].dr);
						elix=fabs(arclet[i].eps*
						cos(2*(arclet[i].E.theta-ampli.theta)));
						arclet[i].dr=
							zero_t(arclet[i].dr,arclet[i].dr+.05,fmin_ell);
					}
					else
						arclet[i].dr=I.drarclet;
				}
		
				/* calcul de ts optimise */
		
				ampli=e_unmag_gal(&arclet[i],arclet[i].dr);
				isoima(&arclet[i].E,&ampli,&ell_sou);
				x=ell_sou.b/ell_sou.a;
				x=.5*(1./x-x);
				
				if (I.stat==2)
				{
					if(indexCmp(I.nza,arclet[i+1].n)) //TO CHECK (previous version : I.nza!=i+1)
						chis+=x*x;
					else
						chis+=50.*x*x;
				}
				else
					chis+=x*x;
				
			};	/*end for (i=0;i<narclet;i++)*/
		} /* if ((I.stat==1)||(I.stat==2))*/
		
		else if (I.stat==3)	/* optimisation de l'orientation */
		{
			for (i=0;i<narclet;i++)
			{
				ampli=e_unmag_gal(&arclet[i],arclet[i].dr);
				x=arclet[i].tau*sin( 2.*(arclet[i].E.theta-ampli.theta));
				chis+=x*x;
#ifdef CHIRES
				fprintf(OUT,"%d %s %.2lf\n",i,arclet[i].n,x*x);
#endif
			}
		}
		else if (I.stat==4)	/* optimisation orientation + ellipticite*/
		{
			for (i=0;i<narclet;i++)
			{
				ampli=e_unmag_gal(&arclet[i],arclet[i].dr);
				qp=fabs(ampli.b/ampli.a);
				dp=(1.+qp*qp)/2./qp;
				tp=fabs(1.-qp*qp)/2./qp;
				x=dp*arclet[i].tau*cos( 2.*(arclet[i].E.theta-ampli.theta))
					-tp*arclet[i].dis;
				chis+=x*x;
			}
			//chis /=narclet;
		}
		else if (I.stat==5)	/* optimisation orientation + ellipticite*/
		{
			for (i=0;i<narclet;i++)
			{
				ampli=e_unmag_gal(&arclet[i],arclet[i].dr);
				isoima(&arclet[i].E,&ampli,&source);
				x=fabs( (source.a*source.a-source.b*source.b)
					/2./source.a*source.b);
				chis+=x*x/0.001;
			}
			//chis /=narclet;
		}
		else if (I.stat==6)	/* optimisation ellipticite a la Marshall*/
		{
			// Intrinsic ellipticity distribution width:
			if( I.dsigell == -1. )
				I.sig2ell = I.sigell*I.sigell;
			
			// TODO include shape estimation error in quadrature:
			wht_ell = 1./I.sig2ell;
			
			// Compute the normalisation factor
			if( I.dsigell != -1. )
				lhood0 += narclet * log( 2*PI*I.sig2ell );
			
			for (i=0;i<narclet;i++)
			{
				// Get amplification matrix:
				ampli=e_unmag_gal(&arclet[i],arclet[i].dr);
				// Apply amplification matrix to image shape to get source shape:
				isoima(&arclet[i].E,&ampli,&source);
				// In weak lensing regime, have that e = e_s + g, or that e_s = e - g
				// i.e. in this limit source contains the residual that goes into chi-sq 
				ells = (source.a-source.b)/(source.a+source.b);
				// TODO: check that this operation (computing source plane shape and inserting into
				//       Gaussian likelihood) is correct. In simple simulation it was coming out as 
				//       2*narclet/4 or so...
				chis+=ells*ells*wht_ell;
			}
		}		
		else
            RAISE(E_PARFILE, "ERROR in o_chi/arclet: I.stat = %d\n",I.stat);

#ifdef CHIRES
		fprintf(OUT,"chis %.2lf\n",chis);
#endif
			
	} /*end if(I.stat!=0) */
	
	/* if shearmap is true in image keyword*/
	if (I.shmap!=0)
	{
		chish=0;
		for (i=0;i<nshmap;i++)
		{
			ampli=e_unmag(&shm[i].C,I.drsh);
			qp=fabs(ampli.b/ampli.a);
			tp=fabs(1.-qp*qp)/2./qp;
			ampli.theta +=PI/2.;
			x=(shm[i].mx-tp*cos(2.*ampli.theta))/shm[i].err;
			chish+=x*x;
			x=(shm[i].my-tp*sin(2.*ampli.theta))/shm[i].err;
			chish+=x*x;
		}
		
		chish /=nshmap;
		chis +=chish;
	} /*end if (I.shmap!=0)*/
	
	/* if I.n_mult is true in image keyword*/
	if (I.n_mult!=0)
	{
		if (I.forme>=0)
			// Source plan chi2
		    chi2_src( &chi_mult, &lhood0 );    
		
		else
		{
			// Image plane chi2
			tp = lhood0;
			x = chi2_img( &chi_mult, &lhood0 );      
			
			
#ifndef CHIRES
			// This case should never happens in o_chires().
			// this #ifndef condition aims only at avoiding the compilation warning 
			if( x != 0 ) return -1;
#endif
/*
//Uncomment if you want to chain image plane and source plane
			if( x != 0.  )
			{
				chi_mult = 0.; lhood0 = tp; 
				chi2_src(&chi_mult, &lhood0 );
			}
*/
		}
		
	} /*end of optimization with the arclets*/
	
	/* if there are constraints on the critical lines positions in image keyword*/
	if (I.npcl!=0)
	{
    	double ampa,ampb,d;
    	struct point A,B, E;
#ifdef CHIRES
		fprintf(OUT,"chi critical ligne\n");
#endif
		chil=0.;
		for(i=0;i<I.npcl;i++)
		{
			A.x=cl[i].C.x-cl[i].dl*cos(cl[i].phi);
			A.y=cl[i].C.y-cl[i].dl*sin(cl[i].phi);
			B.x=cl[i].C.x+cl[i].dl*cos(cl[i].phi);
			B.y=cl[i].C.y+cl[i].dl*sin(cl[i].phi);
			ampa=e_amp(&A,cl[i].dlsds);
			ampb=e_amp(&B,cl[i].dlsds);
	
			if (ampa*ampb<0.)
				// look for the critical line position between A and B
				// the precision if fixed by PREC_ZERO
				E=e_zeroamp(A,B,cl[i].dlsds);
			else
			{
				if ((B.x!=A.x)&&(ampb!=ampa))
					E.x=(ampb*A.x-ampa*B.x)/(ampb-ampa);
				else
					E.x=A.x;
				if ((B.y!=A.y)&&(ampb!=ampa))
					E.y=(ampb*A.y-ampa*B.y)/(ampb-ampa);
				else
					E.y=A.y;
			}
	
			d=dist(E,cl[i].C)/cl[i].dl;
			chil+=d*d;
#ifdef CHIRES
			fprintf(OUT,"%d  %.2lf %.2lf %.2lf\n",i,cl[i].C.x,cl[i].C.y,d*d);	
		}
		
		fprintf(OUT,"chil %.2lf\n",chil);
#else
		}
#endif
	}

	///////////////////////////////////////	
	if (D.dynnumber == 1 || D.dynnumber == 2 || D.dynnumber == 3)  
    {
        extern struct pot  lens[NLMAX];
	    double  vel_model;     //TV Oct2011
		double  mass_model;
		
		
		if (D.dynnumber == 1 || D.dynnumber == 2) 
			
		{
		
	    vel_model = lens[0].sigma/1.473972264;      //Transformation of the velocity in "our" velocity
		
	    chi_vel = (D.dynvel - vel_model) * (D.dynvel - vel_model) / D.dynevel / D.dynevel;
		
	    lhood0 +=  log( 2.*PI*(D.dynevel*D.dynevel) );    // NORMALIZATION
			
		}	
			
		
		if (D.dynnumber == 2) 
			
		{
			
			mass_model = mass2d_NFW(lens[0].sigma,D.refradius,lens[0].rckpc);  //Analitical mass
			
			chi_mass = (D.indmass - mass_model)*(D.indmass - mass_model) / D.indemass / D.indemass;
			
			lhood0 +=  log( 2.*PI*(D.indemass*D.indemass) );    // NORMALIZATION
			
		}
		
		if (D.dynnumber == 3) 
			
		{
			
			mass_model = mass3d_NFW(lens[0].sigma,D.refradius,lens[0].rckpc);  //Analitical mass
			
			chi_mass = (D.indmass - mass_model)*(D.indmass - mass_model) / D.indemass / D.indemass;
			
			lhood0 +=  log( 2.*PI*(D.indemass*D.indemass) );    // NORMALIZATION
			
		}
#ifdef CHIRES
    	fprintf(OUT,"chi_vel %7.4lf\n",chi_vel);
		fprintf(OUT,"chi_mass %7.4lf\n",chi_mass);
#endif
	}
	////////////////
	
	chi=chi_im+chis+chil+chi_vel+chi_mass + Min(chi_mult,100000000.);

#ifdef CHIRES
	fprintf(OUT,"\nchitot %.2lf\n",chi);
	fclose(OUT);
#endif

	/* NPRINTF(stderr,"chip=%.3lf",chip); */
	
	/* NPRINTF(stderr,f"chia=%.3lf  chip=%.3lf  chix=%.3lf  chiy=%.3lf\n",chia,chip,chix,chiy); */
#ifndef CHIRES	
	return(chi);
#endif
}

#ifndef CHIRES
double getLhood0() {return(lhood0);}
#endif


/* Return the chi2 for a single image.
 * Parameters : 
 * - pima : pointer to an observed single image
 * - ps : pointer to the corresponding predicted source position
 */ 
static double chi2SglImage( struct galaxie *pima, struct point *ps )
{
	extern struct g_image I;
	extern double grid_dr;	// source to image plane grid dlsds ratio
	struct bitriplet Tsol[NIMAX];	// list of triangles containing predicted 
						//arclets for a single obs image
	struct point Bs; /*barycenter of a familly of I.mult[i] sources*/	
	double chi2, I2x, I2y, dx, dy;
	int j, nimages;

#ifdef CHIRES
	double rmsi_tot;
	
	rmsi_tot = 0.;
#endif
	
	chi2 = 0;
	
	I2x = I2y = I.sig2pos;
	
	// Check if the grid is initialize to the image redshift
	if( grid_dr != pima->dr )
	{
		grid_dr = pima->dr;
		e_unlensgrid(grid_dr);
	}
	
	//raise chip to a high value if we have more than 1 image
	nimages = inverse(ps, Tsol);
	if( nimages > 1 )
	{
		if( fabs(I.forme) == 10 )
			I2x = I2y = pima->E.a * pima->E.a;
		else if( fabs(I.forme) == 11 )
		{
			I2x = pima->E.a * cos(pima->E.theta);
			I2y = pima->E.b * cos(pima->E.theta);
			I2x = I2x * I2x;
			I2y = I2y * I2y;
		}

		// compute chip as the total distance between the predicted images 
		// and the observed image
		for( j = 0; j < nimages; j++ )
		{
			Bs=barycentre(&Tsol[j].i);
			dx = Bs.x - pima->C.x;
			dy = Bs.y - pima->C.y;
			chi2 += dx*dx/I2x + dy*dy/I2y;
#ifdef CHIRES
			rmsi_tot += dx*dx + dy*dy;
#endif
		}
	}
	
#ifdef CHIRES
	if( nimages != 0 )
		rmsi_tot = sqrt(rmsi_tot/nimages);

	fprintf(OUT,"?? %6s %.3lf   %d   %7.2lf  %6.2lf  %6.2lf  %6.2lf  %.2lf  %.2lf    %d\n",
		pima->n,pima->z,nimages,chi2,0.,0.,0.,0.,rmsi_tot,0);
#endif
	
	return(chi2);
}

/*******************************************************
 * Optimization with the arclet shape in the SOURCE PLANE.
 *******************************************************/
//static void chi2_src( double *chi2, double *lhood0 )
static void chi2_src( double *chi2, double *lhood0)     //I changed this
{
	extern struct g_image   I;
	extern double chip, chia, chix, chiy;
	extern struct galaxie   multi[NIMAX][NIMAX];
	extern int n_famille;
	extern int optim_z;
	extern struct matrix amplifi_mat[NIMAX][NIMAX],amplifi_matinv[NIMAX][NIMAX];

	struct point Ps[NIMAX];
	struct point Bs; //barycenter of a familly of I.mult[i] sources
	struct galaxie *pima; // pointer to an arclet
	
	double MA,MB,MC,MD;
	double sigx2,sigy2,da,fluxS;// ,siga2;
	
	double Ix,Iy,I2x,I2y; //sigma and sigma^2 in the source plane
	double dLhood0;  // lhood0 for a given familly of multiple images (I.forme=10 || dsigpos!=-1)
	double dx,dy,dx0; //error in position in the source plane
	register int i,j;
	
	
	extern double  chi_vel;        //TV Oct2011
	extern struct g_pot		P;   //TV Oct2011
	extern struct	g_dyn	D;   //TV Oct2011
	double dLhood0_vel;     //lhood0 using the velocity dispersion as a constriction  
	
#ifdef CHIRES
	struct point multib[NIMAX];
	int    nimages;  // number of valid images in multib 
	extern int nwarn; /*counts each time a barycenter is not found in a
							 small source triangle. see e_im_prec() function*/

	double chi22,chixi,chiyi;
	double rmss, rmsi;	// image rms in source and image plane
	double rmss_tot, rmsi_tot;	// image rms tot in source and image plane
	int ntot; // total number of multiple images

	fprintf(OUT,"chi multiples \n");
	fprintf(OUT," N    ID    z   Narcs   chip     chix    chiy    chia  rmss  rmsi  nwarn\n");
	
	ntot = rmss_tot = rmsi_tot = 0;
	nwarn=0;
#endif		
	chip=chix=chiy=chia=0.;
	
	// Set the image position error in image plane if not optimized
	if( I.dsigposAs == -1. )
		I.sig2pos=I.sigposAs*I.sigposAs;

	// In all cases
	I2x = I2y = I.sig2pos;	
	
	// It's not very necessary for I.forme==5 but it can be worth that
	// in the rest of the code all the images have their amplification
	// computed at some point, even if it's not necessary today (EJ: 07/07).
	if( I.forme <= 7 || I.forme == 10 )
	    	amplif();	//amplification of each image of all families

	if( I.forme == 8 )
	    	amplif_mat();

	if( I.forme == 9 )
	    	amplif_matinv();

	/* for each familly of arclets*/
	for(i=0;i<I.n_mult;i++)
	{
#ifdef CHIRES
		chixi=0.;
		chiyi=0.;
		rmss=0.;
		rmsi=0.;
		chi22=0.;
#endif				
		dLhood0=0.;
		n_famille=i;

		if( I.forme <= 3 || I.forme == 10 )
			// etendue method. Source plane error averaged
			// over all the images of the system.
			I2x=I2y=sig2posS(I.sig2pos,I.mult[i]);
	
		if( I.forme == 10 )
			Ix = I2x / I.sig2pos; // Ix: normalised etendue source plane 

		/*optim_z can be not null only with the o_runz*() functions*/	
		if (optim_z == 0)  
			/*Compute the sources positions in Ps */
			o_dpl(I.mult[i],multi[i],Ps);
		else
		/*Compute the source position for each arclet*/
		{
			for (j=0;j<I.mult[i];j++)
			{
				pima = &multi[i][j];
				Ps[j].x=pima->C.x - pima->Grad.x*multi[i][0].dr; 
				Ps[j].y=pima->C.y - pima->Grad.y*multi[i][0].dr;
			}
			
			Ps[I.mult[i]]=Ps[0];
		        
			/* NPRINTF(stderr," dr=%.3lf Ps=%.3lf\n",multi[i][0].dr,Ps[0].x); */
		}

		/*Find the barycenter position of the computed sources*/
		if (I.forme == 4 || I.forme == 6)
			Bs=weight_baryc(Ps,multi[i],I.mult[i]);
		else
			Bs=bcentlist(Ps,I.mult[i]);  /* barycentre */

		// ****************************************************
		// If we contraint with a SINGLE IMAGE...
		// Exception!!! -->constraint in the image plane
		//
		if( I.mult[i] == 1 )
		{
			chip += chi2SglImage( &multi[i][0], &Ps[0] ); 
		}
		else
		/******************************************************
		 * constraint with MULTIPLE IMAGES (SOURCE PLANE)
		 * in each familly i, for each arclet j, compute chip
		 **/
		for (j=0;j<I.mult[i];j++)
		{
			if( I.forme == 4  || I.forme == 7 )
				// sqrt(etendue) method. Image plane error = seeing 
				I2x=I2y=sig2posSj(I.sigposAs,multi[i],I.mult[i],j); 
			else if( I.forme == 5 || I.forme == 6 )
				// brute force method with 4 points
				I2x=I2y=sig2posS4j(I.sigposAs,multi[i],Ps[j],j);
			else if( I.forme == 10 )
				// etendue method. Different image plane errors
				I2x=I2y= Ix * multi[i][j].E.a*multi[i][j].E.b;
			else if( I.forme == 11 )
				// etendue method. Elliptical error in the source plane 
				// considering the image size as 1sigma error
				sig2posSe(multi[i],I.mult[i],&I2x,&I2y);
			
			/* distance in arcsec in the source plane between barycenter 
			 * and computed source position for each arclet*/
			dx=Bs.x-Ps[j].x;
			dy=Bs.y-Ps[j].y;
			
			/*modify the error in position according to the shear and 
			 * convergence parameters in the source plane*/
			 // TODO: What happens because I2x and I2y are defined in image plane
			if (I.forme == 8)
			{
				MA=amplifi_mat[n_famille][j].a;
				MB=amplifi_mat[n_famille][j].b;
				MC=amplifi_mat[n_famille][j].c;
				MD=amplifi_mat[n_famille][j].d;
				dx0=dx;
				dx=MA*dx0+MD*dy;
				dy=MB*dx0+MC*dy;
			}

			/*modify the sigma according to the shear and convergence
			 * parameters in the source plane*/
			if (I.forme == 9)
			{
			    MA=amplifi_matinv[n_famille][j].a;
			    MB=amplifi_matinv[n_famille][j].b;
			    MC=amplifi_matinv[n_famille][j].c;
			    MD=amplifi_matinv[n_famille][j].d;
			    Ix=(MA+MD)*I.sigposAs;
			    Iy=(MB+MC)*I.sigposAs;
			    I2x=Ix*Ix;
			    I2y=Iy*Iy;
			}
			
			chip += dx*dx/I2x+dy*dy/I2y;
			// update normalisation factor for image ij
			dLhood0 += log( 4.*PI*PI*(I2x * I2y) );
					
#ifdef CHIRES
			// chi2 and rms for familly i
			chi22 += dx*dx/I2x + dy*dy/I2y;
			rmss += dx*dx + dy*dy;
#endif				
		} /*end for each image*/

		/*optimization with the ellipticity of the arclets*/
		if ( I.forme == 1 || I.forme == 3 )
		{
//					chix=chiy=chia=0.;
			o_mag_m(I.mult[i],multi[i]);
			for (j=0;j<I.mult[i];j++)
			{
				o_shape(j,multi[i],&dx,&sigx2,&dy,&sigy2,&da);
				chix += dx*dx/sigx2;
				chiy += dy*dy/sigy2;
				if (I.forme==3)
					chia += da*da/I.sig2amp;
			}
		}
		
		/*optimization with the flux of the arclets*/
		if ( I.forme == 2 )
		{
			chia=0.;
			o_flux(I.mult[i],&fluxS);
			for (j=0;j<I.mult[i];j++)
			{
				o_chi_flux(j,multi[i],&da,fluxS);
				/* NPRINTF(stderr,"da=%.3lf sig2=%.3lf\n",da,I.sig2flux);*/
				chia += da*da/I.sig2flux;
			    /* NPRINTF(stderr,"chia=%.3lf\n",chia); */
			}
		}

		// update the total statistics
		if( I.dsigposAs != -1 )
			*lhood0 += dLhood0;

#ifdef CHIRES
		/* If chires(): compute the rms in the image plane
		   multib contains the predicted image positions 
		   in source plane.
		   multib contains the same size as multi
		   in case of lost images, the predicted image position is
		   the observed image position.
		 */
		nimages = unlens_bc(Ps,Bs,multi[i],multib,I.mult[i]);
		for( j = 0 ; j < nimages; j++ )
		{
			dx = multib[j].x - multi[i][j].C.x;
			dy = multib[j].y - multi[i][j].C.y;
			rmsi += dx*dx + dy*dy; 
		}
		rmss_tot += rmss;
		rmsi_tot += rmsi;
		ntot += nimages;
		nwarn += I.mult[i]-nimages; 
		
		if( nimages != 0 )
		{
			rmss = sqrt(rmss/nimages);
			rmsi = sqrt(rmsi/nimages);
		}
		
		fprintf(OUT,"%2d %6s %.3lf   %d   %7.2lf  %6.2lf  %6.2lf  %6.2lf  %.3lf  %.2lf    %d\n",
			i,multi[i][0].n,multi[i][0].z,I.mult[i],chi22,chix,chiy,chia,rmss,rmsi,I.mult[i]-nimages);
			
	} /*end for each familly i ( in chires() ) */
	rmss_tot = sqrt(rmss_tot/ntot);
	rmsi_tot = sqrt(rmsi_tot/ntot);
	fprintf(OUT,"chimul                %7.2lf  %6.2lf  %6.2lf  %6.2lf  %.3lf  %.2lf    %d\n",
				chip,chix,chiy,chia,rmss_tot,rmsi_tot,nwarn);
	
	
	//fprintf(OUT,"chi_vel %7.4lf\n",chi_vel);       //THIS IS MINE
	
	

#else
	} /*end for each familly i ( in o_chi() )*/
#endif


	*chi2 = chip + chia + chix + chiy + chi_vel;

}

static int chi2_img( double *chi2, double *lhood0 )
{
	extern struct g_image   I;
	extern double chip, chia, chix, chiy;
	extern struct galaxie   multi[NIMAX][NIMAX];
	extern int n_famille;
	extern int nwarn; /*counts each time a barycenter is not found in a
							 small source triangle. see e_im_prec() function*/
	extern int optim_z;
	
    extern struct pot       lens[NLMAX];
	struct point Ps[NIMAX];
	struct point multib[NIMAX];
	int    nimages;  // number of valid images in multib
	struct point Bs; /*barycenter of a familly of I.mult[i] sources*/
	struct galaxie *pima; // pointer to an arclet
		
	double I2x,I2y; //sigma and sigma^2 
	double dLhood0;  // lhood0 for a given familly of multiple images (I.forme=10 || dsigpos!=-1)
	
	double dx,dy; //error in position in the source plane
	register int i,j;
	
	
#ifdef CHIRES
	double chi22,chixi,chiyi;
	double rmss, rmsi;	// image rms in source and image plane
	double rmss_tot, rmsi_tot;	// image rms tot in source and image plane
	int ntot; // total number of multiple images
	
		
	fprintf(OUT,"chi multiples \n");
	fprintf(OUT," N    ID    z   Narcs   chip     chix    chiy    chia  rmss  rmsi  nwarn\n");
	
	ntot = rmss_tot = rmsi_tot = 0;
#endif		
	chip=chia=chix=chiy=0.;
	nwarn=0;

	// Set the image position error in image plane if not optimized
	if( I.dsigposAs == -1. )
		I.sig2pos=I.sigposAs*I.sigposAs;

	// In all cases
	I2x = I2y = I.sig2pos;	
	 
	/* for each familly */
	for(i=0;i<I.n_mult;i++)
	{
#ifdef CHIRES
		chi22=0.;
		chixi=0.;
		chiyi=0.;
		rmss=0.;
		rmsi=0.;
#endif				
		dLhood0=0.;
		n_famille=i;
				
		/* optim_z can be different from 0 with the o_runz*() functions*/
		if (optim_z == 0)  
			/* compute the sources positions in Ps*/
			o_dpl(I.mult[i],multi[i],Ps);
		else
		{
			for (j=0;j<I.mult[i];j++)
			{
				pima = &multi[i][j];
				Ps[j].x=pima->C.x - pima->Grad.x*multi[i][0].dr;
				Ps[j].y=pima->C.y - pima->Grad.y*multi[i][0].dr;
			}
			
			Ps[I.mult[i]]=Ps[0];
		}
		
		/*Bs contains the barycenter of the I.mult[i] sources positions Ps*/
		Bs=bcentlist(Ps,I.mult[i]);
					
#ifdef CHIRES
		// Compute the rms in the source plane
		for( j = 0; j < I.mult[i]; j++ )
		{
			dx=Bs.x-Ps[j].x;
			dy=Bs.y-Ps[j].y;
			rmss += dx*dx + dy*dy;				
		} 
#endif			
	
		// *********************************
		// SINGLE IMAGE systems
		//
		if( I.mult[i] == 1 )
		{
			chip += chi2SglImage( &multi[i][0], &Ps[0] );
		}
		else
		// **************************************
		// MULTIPLE IMAGES systems
		//
		{
			/*return in multib a list of image for each arclet of familly i*/
			nimages = unlens_bc(Ps,Bs,multi[i],multib,I.mult[i]);
#ifndef CHIRES
			if( nimages != I.mult[i] )
				return -1.;
#endif					
			// For each image in familly i
			for( j = 0; j < I.mult[i]; j++ )
			{
				pima = &multi[i][j];
				
				if( I.forme == -10 )
				{
					I2x = I2y = pima->E.a * pima->E.b;
				} 
				else if( I.forme == -11 )
				{
					I2x = pima->E.a * cos(pima->E.theta);
					I2y = pima->E.b * cos(pima->E.theta);
					I2x = I2x * I2x;
					I2y = I2y * I2y;
				}
				
				// update normalisation factor for image ij
				dLhood0 += log( 4.*PI*PI*(I2x * I2y) );
				
				dx = multi[i][j].C.x - multib[j].x;
				dy = multi[i][j].C.y - multib[j].y;
				chip += dx*dx/I2x + dy*dy/I2y;
				
								
				chip = chip; //+ chi_vel; //+ chi_rs;   //This is mine 
				
#ifdef CHIRES
				chi22 += dx*dx/I2x + dy*dy/I2y;
				rmsi += dx*dx + dy*dy;
#endif
			}
			
			// update the total statistics
			if( I.dsigposAs != -1 )
				*lhood0 += dLhood0;
			
			/* for (j=0;j<I.mult[i];j++)
	     		 {
	            da=diff_mag(multi[i][j],multib[j]);
	       		 chia += da*da/I.sig2amp;
	     		 }; */
#ifdef CHIRES  						 
			rmss_tot += rmss;
			rmsi_tot += rmsi;
			ntot += nimages;
			nwarn += I.mult[i] - nimages;
			
			if( nimages != 0 )
			{
				rmss = sqrt(rmss/nimages);
				rmsi = sqrt(rmsi/nimages);
			}
			
			fprintf(OUT,"%2d %6s %.3lf   %d   %7.2lf  %6.2lf  %6.2lf  %6.2lf  %.2lf  %.2lf    %d\n",
				i,multi[i][0].n,multi[i][0].z,I.mult[i],chi22,chix,chiy,chia,rmss,rmsi,I.mult[i]-nimages);
				
			
		} // end of case with I.mult[i] > 1
		
	} /*end for each familly*/
	
	// Total statistics
	rmss_tot = sqrt(rmss_tot/ntot);
	rmsi_tot = sqrt(rmsi_tot/ntot);
	fprintf(OUT,"chimul                %7.2lf  %6.2lf  %6.2lf  %6.2lf  %.2lf  %.2lf    %d\n",
		chip,chix,chiy,chia,rmss_tot,rmsi_tot,nwarn);
	
	
	
	
	
			
#else	
		} // end of case with I.mult[i] > 1
	} /*end for each familly*/
#endif 
	

	*chi2 = chip + chia + chix + chiy;

	
	return 0; // no error, no warning
}
