#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<math.h>
#include<fonction.h>
#include<constant.h>
#include<dimension.h>
#include<structure.h>
#include "lt.h"


/****************************************************************/
/*      nom:        e-pixel             */
/*      auteur:     Jean-Paul Kneib         */
/*      date:       10/02/92            */
/*      place:      Toulouse            */
/****************************************************************
 * Create a FITS file containing the computed images of the arcs
 * and arclets in the source plane
 */
void o_pixel_source(double **zz, int nx, int ny, double xmin, double pixelx, double ymin, double pixely, int ns, struct galaxie *source);

void    e_pixel(int np, char *iname, char *sname, struct galaxie *source)
{
    const extern struct g_frame   F;
    const extern struct g_mode    M;
    const extern struct g_observ  O;
    //const extern    struct  g_large L;

    double xmin, ymin, xmax, ymax, dx, dy;
    double f, scale;
    int nx, ny, ny_sav;
    double **zz;

    xmin = F.xmin;
    xmax = F.xmax;
    ymin = F.ymin;
    ymax = F.ymax;

    dx = xmax - xmin;
    dy = ymax - ymin;
    nx = np;    // default: assign np to nx and adjust ny
    scale = dx / (nx-1);
    ny = dy / scale + 1;  // warning: trunc(ny) behavior 
//    f = dx / scale;
//    nx = (int) f;  // BUG in C
//    f = dy / scale;
//    ny = (int) f;
    if (O.setbin)
    {
        if ( O.bin * nx <= NTMAX && O.bin * ny <= NTMAX )
        {
            nx = O.bin * nx;
            ny = O.bin * ny;
            scale = scale / ((double) O.bin);
        }
        else
            RAISE(E_RUNTIME, "ERROR: reaching maximal size of array\n");
    }

    dx = (nx - 1) * scale;
    dy = (ny - 1) * scale;
    xmax = xmin + dx;
    ymax = ymin + dy;

    NPRINTF(stderr, "\timage (%d,%d) s=%.3lf [%.3lf,%.3lf] [%.3lf,%.3lf]\n",
            nx, ny, scale, xmin, xmax, ymin, ymax);

    zz = (double **) alloc_square_double(ny, nx);

    o_pixel(zz, nx, ny, scale, xmin, xmax, ymin, ymax, source, NULL, NULL);

    if (O.setseeing)
	d_seeing_omp(zz, nx, ny, scale); // mod bclement shapemodel_opt

    ny_sav = ny;
    if (O.setbin)
        d_binning(zz, &nx, &ny, O.bin);

    if (O.bruit)
        d_bruiter(zz, nx, ny);

    NPRINTF(stderr, "COMP: image of arcs and arclets --> %s\n", iname);

    if ( M.iref > 0 )
        wrf_fits_abs(iname, zz, nx, ny, xmin, xmax, ymin, ymax, M.ref_ra, M.ref_dec);
    else
        wrf_fits(iname, zz, nx, ny, xmin, xmax, ymin, ymax);

    free_square_double(zz, ny_sav);

    // generate a simulated image of the source
    const extern struct g_source  S;
    extern struct g_pixel ps;

    NPRINTF(stdout, "Simulated %ld sources\n", S.ns);

    // define a source box
    if( strcmp(M.centerfile, "") )
    {
    	double dlsds = source[0].dl0s / source[0].dos;
        ps.pixelx = scale / ps.ech;
        ps.pixely = scale / ps.ech;
        s_sourcebox(&ps, M.centerfile, dlsds, source[0].z);
    }
    else
    {
        dx = ps.xmax - ps.xmin;
        dy = ps.ymax - ps.ymin;
        ps.pixelx = dx / (ps.nx-1);
        ps.pixely = dy / (ps.ny-1);
    }

    zz = alloc_square_double(ps.ny, ps.nx);
	o_pixel_source(zz, ps.nx, ps.ny, ps.xmin, ps.pixelx, ps.ymin, ps.pixely, S.ns, source);

    if ( M.iref > 0 )
        wrf_fits_abs(sname, zz, ps.nx, ps.ny, ps.xmin, ps.xmax, ps.ymin, ps.ymax, M.ref_ra, M.ref_dec);
    else
        wrf_fits(sname, zz, ps.nx, ps.ny, ps.xmin, ps.xmax, ps.ymin, ps.ymax);

    free_square_double(zz, ps.ny);
}

/* Function to simulate an image from a source
 * Called by o_chi() and e_pixel()
 * zz is filled in a field defined by champ section with xmin, ymin, xmax, ymax
 *
 *
 * Notes: not optimal because of multiplane (gradient is computed multiple times per pixel, one for each source).
 */ 
void o_pixel(double **zz, int nx, int ny, double scale, double xmin, 
        double xmax, double ymin, double ymax, struct galaxie *source, double **dpl_x, double **dpl_y)
{

    const extern struct g_mode    M;
    const extern struct g_source  S;
    extern struct pot lens[]; 
    
    int j, k, l;
    struct point pi, ps;
    
    for (j = 0; j < ny; j++)
    {
        for (k = 0; k < nx; k++)
        {
	    zz[j][k] = 0.0;
        }
    }
    
    // mod bclement shapemodel_opt
    // dpl_x and dpl_y are only assigned if lens model is fixed and pre-computed for a zs_ref=100 in o_global()
    // TODO : save computation time of this integral by storing the value dlsds_ref somewhere
    double dlsds_ref = 1;
    if(dpl_x)
    {
	double zs_ref = 100.0;
	dlsds_ref = dratio(lens[0].z, zs_ref);
    }
    
//#pragma omp parallel for private(ps,pi,j,k,l)

    for (l = 0 ; l < S.ns ; l++ )
    {
 // TODO : can use collapse(2) or collapse(3) and put pragma before the source loop
#pragma omp parallel for schedule(static,1) private(ps,pi,j,k)
        for (j = 0; j < ny; j++)
        {
	    pi.y = ymin + j * scale;
            for (k = 0; k < nx; k++)
            {
                pi.x = xmin + k * scale;

		// mod bclement shapemodel_opt
		if(dpl_x)
		{
		    double fdr;
		    fdr = source[l].dr / dlsds_ref;
		    ps.x = pi.x - dpl_x[j][k]*fdr;
		    ps.y = pi.y - dpl_y[j][k]*fdr;
		}
		else
		{
		    e_dpl(&pi,source[l].dr,source[l].z,&ps);
		}

                double f = d_profil(ps.x, ps.y, &source[l]);
                /*                if ((f>sqrt(O.SKY/O.gain)) || (f>source[l].I0/100.))
                                    zz[j][k]+=d_integrer(source[l],pi.x,pi.y,scale/2.,f,0);
                        else
                */
                zz[j][k] += f;
            }
        }
    }

}

/* Function to render the surface brightness of sources
 */
void o_pixel_source(double **zz, int nx, int ny, double xmin, double pixelx, double ymin, double pixely, int ns, struct galaxie *source)
{
    struct point A; 
    int j, k, l;
    for ( j = 0; j < ny; j++ )
    {
        A.y = ymin + ((double)j ) * pixely;
        for ( k = 0; k < nx; k++ )
        {
            A.x = xmin + ((double)k ) * pixelx;
            zz[j][k] = 0.;
            for( l = 0; l < ns; l++ )
                zz[j][k] += d_profil(A.x, A.y, &source[l]);
        }
    }
}


// mod bclement shapemodel_contour
/****************************************************************/
/*      nom:        o_pixel_contour             */
/*      auteur:     Benjamin Clement       */
/*      date:       13/02/19            */
/*      place:      Lyon/Geneva                */
/****************************************************************
 * Simulate an image from a source inside a user-defined list of contours
 * Called by o_chi() and o_chires()
 */
void o_pixel_contour(double **zz, int nx, int ny, double scale, double xmin, 
        double xmax, double ymin, double ymax, struct galaxie *source, struct pixlist *pl, struct pixlist *pl_dpl, int npl)
{
    const extern struct g_mode    M;
    const extern struct g_source  S;
    const extern struct g_observ  O;
    extern struct pot lens[]; 

    int j, k, l, i, n;
    struct point pi, ps;

// TODO : only zero the array inside and around contours
    for (j = 0; j < ny; j++)
    {
        for (k = 0; k < nx; k++)
        {
	    zz[j][k] = 0.0;
        }
    }
    
    for (l = 0 ; l < S.ns ; l++ )
    {
 // TODO : can use collapse(2) or collapse(3) and put pragma before the source loop ?
#pragma omp parallel for schedule(static) private(ps,pi,j,k)
	for(n = 0; n < npl; n++)
	{
	    pi.y = pl[n].i;
	    pi.x = pl[n].j;
	    j = pl[n].ii;
	    k = pl[n].jj;

	    // compute the source position from pre-computed dpl in o_global
	    if(pl_dpl)
	    {
		ps.x = pi.x - pl_dpl[n].i*source[l].dr;
		ps.y = pi.y - pl_dpl[n].j*source[l].dr;
	    }
	    else // recompute dpl
	    {
		e_dpl(&pi,source[l].dr,source[l].z,&ps);
	    }

	    double f = d_profil(ps.x, ps.y, &source[l]);
	    zz[j][k] += f;
	}
    }
    //wrf_fits_abs("tmp_oversampled.fits", zz, nx, ny, xmin, xmax, ymin, ymax, M.ref_ra, M.ref_dec);
}


/****************************************************************/
/*      nom:        pixelsource             */
/*      auteur:     Johan Richard       */
/*      date:       14/07/16            */
/*      place:      Lyon                */
/****************************************************************
 * Create a FITS file containing the computed image
 * of a pixelised source plane
 * pixel = 2 in runmode
 */
void    pixelsource(int np, char *iname)
{
    const extern struct g_frame   F;
    const extern struct g_mode    M;
    const extern struct g_observ  O;
    extern struct g_pixel ps;

    //const extern    struct  g_large L;
    extern struct pot lens[]; 

    double xmin, ymin, xmax, ymax, dx, dy;
    double f, scale;
    int nx, ny, ny_sav;
    double **zz;
    double **ims;

    struct point pi,pis;
    int sx,sy;
   
    int i,j;

    double zs = M.zclean;
    double dlsds_ref = dratio(lens[0].z, zs);

    xmin = F.xmin;
    xmax = F.xmax;
    ymin = F.ymin;
    ymax = F.ymax;

    dx = xmax - xmin;
    dy = ymax - ymin;
    nx = np;    // default: assign np to nx and adjust ny
    scale = dx / (nx-1);
    ny = round(dy / scale + 1.);  // to prevent trunc(ny) behavior 
//    f = dx / scale;
//    nx = (int) f;  // BUG in C
//    f = dy / scale;
//    ny = (int) f;
    if (O.setbin)
    {
        if ( O.bin * nx <= NTMAX && O.bin * ny <= NTMAX )
        {
            nx = O.bin * nx;
            ny = O.bin * ny;
            scale = scale / ((double) O.bin);
        }
        else
            RAISE(E_RUNTIME, "ERROR: reaching maximal size of array\n");
    }

    dx = (nx - 1) * scale;
    dy = (ny - 1) * scale;
    xmax = xmin + dx;
    ymax = ymin + dy;

    NPRINTF(stderr, "\timage (%d,%d) s=%.3lf [%.3lf,%.3lf] [%.3lf,%.3lf]\n",
            nx, ny, scale, xmin, xmax, ymin, ymax);

    zz = (double **) alloc_square_double(ny, nx);


    NPRINTF(stderr, "READ: sframe\n");

    ps.format=3;
    ps.pixelx=0.0;
    ps.pixely=0.0;
    ims = (double **) readimage(&ps);

    for(j=0;j<ny;j++)
    {  
       pi.y = ymin + j * scale;
       for (i=0;i<nx;i++)
       {
          zz[j][i]=0.0;
          pi.x = xmin + i * scale;
          e_dpl(&pi, dlsds_ref, zs, &pis);
          sx=(int)((pis.x-ps.xmin)/ps.pixelx);
          sy=(int)((pis.y-ps.ymin)/ps.pixely);
          if((sx>=0)&&(sx<ps.nx)&&(sy>=0)&&(sy<ps.ny)) 
          {
             zz[j][i]=ims[sy][sx];
          }
       }
    }

    //if (O.setseeing)
    //    d_seeing(zz, nx, ny, scale);
    if (O.setseeing)
	d_seeing_omp(zz, nx, ny, scale); // mod bclement shapemodel_opt

    ny_sav = ny;
    if (O.setbin)
        d_binning(zz, &nx, &ny, O.bin);

    if (O.bruit)
        d_bruiter(zz, nx, ny);

    NPRINTF(stderr, "COMP: image of pixelised source plane --> %s\n", iname);

    if ( M.iref > 0 )
        wrf_fits_abs(iname, zz, nx, ny, xmin, xmax, ymin, ymax, M.ref_ra, M.ref_dec);
    else
        wrf_fits(iname, zz, nx, ny, xmin, xmax, ymin, ymax);

    free_square_double(zz, ny_sav);

}

