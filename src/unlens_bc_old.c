#include<stdio.h>
#include<math.h>
#include "fonction.h"
#include "constant.h"
#include"dimension.h"
#include "structure.h"
#include "omp.h"

//#define DDEBUG
/****************************************************************/
/*      nom:        e_unlens            */
/*      auteur:     Jean-Paul Kneib         */
/*      date:       10/02/92            */
/*      place:      Toulouse
 *****************************************************************
 * This function performs a kind of iterative inversion process to get
 * the position of one arclet knowing the position of the source Bs.

 * Return in multib a list of arclet positions in the image plane
 * corresponding to the Bs source position. Return the number of
 * valid positions in multib.
 *
 * If it has been possible to find a triangle in the source plane that
 * contains Bs and is smaller than the minimum surface required (defined
 * by the DMIN constant), then multib[i] contains the barycenter
 * of this triangle in the image plane, otherwise it contains the observed
 * arclet position multi[i] and the warn global variable is incremented.
 *
 *
 * Parameters :
 * - n : number of arclets in multi[]
 * - multi[] : list of arclets for a source
 * - Ps[] : list of sources corresponding to the arclets of multi[]
 * - Bs : barycenter in the source plane of the list of arclets in multi[]
 *
 * Global varibles used :
 * - distmin (constant)
 * - in e_im_prec() : it, G, lens, lens_table
 * - in e_transform() : G, lens, lens_table
 * - in e_amp() : G, lens, lens_table
 */
void e_powell( struct point Bs, double dlsds,
               struct point *p, struct point *xi,
               double ftol, double *fret);
void amoeba(struct point Bs, double dlsds, struct point *p, double *y, double ftol );

//det_stop: if det_stop than we should stop imediately 
//          It can happen in openmp mode then chi2_img 
//          have already decided return -1. 
int  unlens_bc(const struct point *Ps,
               struct point Bs,
               struct galaxie *multi,
               struct point *multib,
               int n,
	       int n_famille,
	       int *det_stop)
{
    const extern  double  distmin[NFMAX];

    int    nimages; // number of valid images in multib
    int    i;

#ifdef DDEBUG
    // plot the TE and the Tfinal triangle in red in the image plane
    // and in blue in the source plane
    const extern struct g_mode  M;
    struct point CG;
    struct ellipse amp;
    char   parity[5];
    FILE  *dbg;

    //g_grid(2, 150, multi[0].z);

    //CL.nplan = 1;
    //CL.cz[0] = multi[0].z;
    //criticnew(1);
    //w_critic();

    dbg = fopen( "imsearch.reg", "w");
    fprintf( dbg, "fk5;point(%lf,%lf) # point=cross color=green\n", // barycentre position
             M.ref_ra + Bs.x / -3600 / cos(M.ref_dec*DTR),
             M.ref_dec + Bs.y / 3600 );
    for ( i = 0; i < n; i++ )
    {
        fprintf( dbg, "fk5;point(%lf,%lf) # point=cross color=red text={%s}\n", // image position
                 M.ref_ra + multi[i].C.x / -3600 / cos(M.ref_dec*DTR),
                 M.ref_dec + multi[i].C.y / 3600,
                 multi[i].n );
        fprintf( dbg, "fk5;point(%lf,%lf) # point=cross color=blue text={%s}\n",    // source position
                 M.ref_ra + Ps[i].x / -3600 / cos(M.ref_dec*DTR),
                 M.ref_dec + Ps[i].y / 3600,
                 multi[i].n );
    }
    fclose(dbg);
#endif

    nimages = 0;

//    int num_threads = omp_get_max_threads();
//    if (num_threads > n)
//       num_threads = n;
//    #pragma omp parallel for schedule(dynamic, 1)  num_threads(num_threads)
//    don't forget discoment "atomic" pragma below!!!!
    for (i = 0; i < n; i++)
    {
       if (*det_stop)
	 continue;
//       fprintf(stderr, "%d %d %d\n",omp_get_level(), omp_get_team_size(1),omp_get_team_size(2));
        struct bitriplet TE;
        struct bitriplet Tfinal; // list of final triangles found. (source and image planes)
        // bi-triangle definition

        /*  TE.i.a=multi[i].C;
            TE.i.b.x=TE.i.a.x +2.*(Bs.x-Ps[i].x) -(Bs.y-Ps[i].y);
            TE.i.b.y=TE.i.a.y +2.*(Bs.y-Ps[i].y) -(Bs.x-Ps[i].x);
            TE.i.c.x=TE.i.a.x +2.*(Bs.x-Ps[i].x) +(Bs.y-Ps[i].y);
            TE.i.c.y=TE.i.a.y +2.*(Bs.y-Ps[i].y) +(Bs.x-Ps[i].x); */

        /*distance between the barycenter of the sources and a particular source
         * normalized by the amplification factor at the arclet position*/
        double dd = 0.7 * dist(Bs, Ps[i]) / e_amp_gal(&multi[i]);

        /*minimal distance between the images of a familly*/
        double d_min = 0.4 * distmin[n_famille];

        /*if the distance in the source plane is larger than the
         * smallest distance between 2 images*/
        if ( d_min < fabs(dd) )
            dd = d_min;

        /* TE.i represents an equilateral triangle with Cx,Cy at its center*/
        TE.i.a.x = multi[i].C.x;
        TE.i.a.y = multi[i].C.y + 2.*dd;
        TE.i.b.x = multi[i].C.x + 1.7 * dd;
        TE.i.b.y = multi[i].C.y - dd;
        TE.i.c.x = multi[i].C.x - 1.7 * dd;
        TE.i.c.y = multi[i].C.y - dd;


        /* Compute a triplet of simulated sources for the corresponding
         * triplet of simulated images. We assume that Bs is in TE.s */
        e_transform(&TE.i, multi[i].dr, &TE.s);

        /*count the loops needed to reach the smallest source triangle*/
        int it = 0;

        /*return the smallest couple of triangles in which Bs is located
         * in the source triangle TE.s and the image triangle TE.i contains
         * or is close to the current arclet position multi[i].C in the image
         * plane. Tfinal.i is a subtriangle of TE.i and Tfinal.s its associated
         * triangle in the source plane.
         * It's a kind of dichotomic process to get the
         * position of one arclet and its source with precision. */
        
        e_im_prec(&TE, &Bs, multi[i].dr, &it, &Tfinal);


#ifdef DDEBUG
        CG = barycentre(&Tfinal.i);
        amp = e_unmag(&CG, multi[i].dl0s,multi[i].dos,multi[i].z);
        if ( amp.a > 0 && amp.b > 0 )
            strcpy( parity, "++" );
        else if ( amp.a <= 0 && amp.b > 0 )
            strcpy( parity, "-+" );
        else if ( amp.a > 0 && amp.b <= 0 )
            strcpy( parity, "+-" );
        else
            strcpy( parity, "--" );

        dbg = fopen( "imsearch.reg", "a");
        fprintf( dbg, "fk5;# text(%lf,%lf) color=cyan text={%s}\n", // source position
                 M.ref_ra + (multi[i].C.x + 3.) / -3600 / cos(M.ref_dec*DTR),
                 M.ref_dec + (multi[i].C.y + 3.) / 3600,
                 parity );
        fclose(dbg);
#endif

//      Di=dist(barycentre(Tfinal.i),multi[i].C);
//      CG.x=Tfinal.i.a.x;
//      CG.y=Tfinal.i.a.y;
        // NPRINTF(stderr,"Ai=%.3lf Aig=%.3lf Di=%.3lf\n",1./e_amp(multi[i].C,dlsds),1./e_amp(CG,dlsds),Di);

        double D = dist(barycentre(&Tfinal.s), Bs); //,Di;

        if ( D < 0.1 )
        {
            multib[i] = barycentre(&Tfinal.i);
	    
//	    #pragma omp atomic
            nimages++;
        }
        else
            //Bs is too far and cannot be reached by the e_im_prec() function
        {
            multib[i] = multi[i].C;
            //NPRINTF(stderr,"WARNING: could not find the searched image\n");
            // NPRINTF(stderr,"Ai=%.3lf Aig=%.3lf D:%lf i:%d j:%d it:%d\n",1./e_amp(multi[i].C,dlsds),1./e_amp(CG,dlsds),D,n_famille,i,it);

            // number of valid images in multib (do not count the current image)
        }
    }

    /* If 2 images are at the same place it's not a problem. They both count in the chi2
    // Test if 2 images are at the same place
    i = 0;
    multib[n] = multib[0];
    while( i < n && (D = dist( multib[i], multib[i+1])) > DMIN ){i++;};
    */
    return nimages; // number of valid images in multib
}

