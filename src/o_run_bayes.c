#include<stdio.h>
#include<signal.h>
#include<stdlib.h>
#include<string.h>
#include<math.h>
#include<fonction.h>
#include<constant.h>
#include<dimension.h>
#include<structure.h>
#include "bayesys3.h"
#include "userstr.h"
#include "lt.h"
#include "o_run_bayes.h"

/****************************************************************/
/*      nom:        o_run_bayes         */
/*      auteur:     Eric Jullo          */
/*      date:       01/06               */
/*      place:      ESO, Chile          */
/****************************************************************
 * Optimise the potential parameters using the bayesys3 algorithm.
 *
 */
typedef void (*sighandler_t)(int);
static void signalReset(int);

UserCommonStr *userCommon_init(int nDim);
void userCommon_free(UserCommonStr *userCommon);

double  o_run_bayes()
{
    extern struct g_mode    M;
    extern struct g_image   I;
    extern struct g_grille  G;
    extern struct g_pixel imFrame,wFrame,ps;
// Addressing
    CommonStr     Common[1];
    ObjectStr     *Objects;
    UserCommonStr *UserCommon;
    int           nDim;
    long int      i, j;
    FILE *burnin;

    // Get the number of free parameters, and write header of bayes.dat
    nDim = bayesHeader(NULL);
    

#ifdef DEBUG
    burnin = fopen( "burnin.dbg.dat", "w" );
#else
    burnin = fopen( "burnin.dat", "w" );
#endif
    fclose(burnin);
 

    //Common Bayesys parameters
    Common->Ndata = 0;
    Common->ENSEMBLE    = 10;         // # objects in ensemble
    if ( M.minchi0 > 10 )
        Common->ENSEMBLE    = (int)ceil(M.minchi0);
    Common->Method      = -1;         // Algorithm method
    Common->Rate        = M.rate;     // Speed of calculation (dimensionless)
    Common->Iseed       = M.restart_seed;       // Random seed, -ve is time seed
    Objects = (ObjectStr *)malloc((unsigned int) Common->ENSEMBLE * sizeof(ObjectStr));

    //Prepare restart operations
    if(M.restart != 0)
    {
        fh_restart = fopen(LHOOD_BUFNAME, "r+b");
        if(fh_restart == NULL)
            fh_restart = fopen(LHOOD_BUFNAME, "a+b"); //Create file
    }

    // Initialize Bayesys for MassInf. WL only with a Grid
    if( G.nmsgrid != G.nlens )
    {
	NPRINTF(stderr,"Stating G.nmsgrid != G.nlens\n ");
        if( I.stat == 8 || I.n_mult != 0 )
        {
            extern long int narclet;
            extern struct g_pot P[NPOTFILE];
            extern struct galaxie   arclet[];
            extern struct galaxie  multi[NFMAX][NIMAX];
            extern struct g_frame F;
            double gam1, gam2;
            struct matrix grad2;
            struct point grad;
            long int ilens;
            double e;
            int k;
 	    NPRINTF(stderr,"I.n_mult = %d\n",I.n_mult);
            int Ndata = narclet;
            for ( i = 0; i < I.n_mult; i++ ) //Adding data from multfile if any
                Ndata += I.mult[i];
            int nmult = Ndata - narclet;

            Ndata = 2 * Ndata;  //Each data point has two dimensions x and y 
            double *Data = (double *)malloc((unsigned int)(Ndata * sizeof(double)));
            double *Acc = (double *)malloc((unsigned int)(Ndata * sizeof(double)));
            double *zbitstmp0 = (double *)calloc(Ndata, sizeof(double));
            zbitstmp1 = alloc_square_double(G.nlens - G.nmsgrid + 2 * I.n_mult, Ndata); 
            ibitstmp = (int *)calloc(Ndata, sizeof(int));    
	    NPRINTF(stderr,"Filling up of zbits and ibits matrices\n");

            // Contribution from the grid potentials
            for( ilens = 0; ilens < G.nlens - G.nmsgrid; ilens++ )
            { 
                int nimage = 0;
                for ( i = 0; i < I.n_mult; i++ )
                    for ( j = 0; j < I.mult[i]; j++ )
                    {
                        zbitstmp1[ilens][nimage] = multi[i][j].np_grad[ilens].x * multi[i][j].dr;
                        zbitstmp1[ilens][nmult + nimage] = multi[i][j].np_grad[ilens].y * multi[i][j].dr;
                        nimage++;
                    }

                for( i = 0; i < narclet; i++ )
                {
                    gam1 = 0.5 * (arclet[i].np_grad2a[ilens] - arclet[i].np_grad2c[ilens]) / arclet[i].dos; 
                    gam2 = arclet[i].np_grad2b[ilens] / arclet[i].dos; 
                    zbitstmp1[ilens][2 * nmult + i] = 2. * gam1;   // e1 defined as (a^2 - b^2) / (a^2 + b^2) and kap <<1
                    zbitstmp1[ilens][2 * nmult + narclet + i] = 2. * gam2; // g2 (see B&S01 Eq 4.6)
                 }
            }
     
            // Contribution from the fixed potentials
            int nimage = 0;
            for( ilens = G.no_lens; ilens < G.nplens[0]; ilens++ )
            {
                nimage = 0;
                for ( i = 0; i < I.n_mult; i++ )
                    for ( j = 0; j < I.mult[i]; j++ )
                    {
                        grad = e_grad_pot(&multi[i][j].C, ilens);
                        zbitstmp0[nimage] += grad.x * multi[i][j].dr;
                        zbitstmp0[nmult + nimage] += grad.y * multi[i][j].dr;
                        nimage++;
                    }

                for( i = 0; i < narclet; i++ )
                {
                    grad2 = e_grad2_pot(&arclet[i].C, ilens);
                    zbitstmp0[2 * nmult + i] += (grad2.a - grad2.c) * arclet[i].dr;
                    zbitstmp0[2 * nmult + narclet + i] += 2. * grad2.b * arclet[i].dr;
                }
            }
    
            // Contribution from the potfile, if it is fixed 
            for( k = 0; k < G.npot; k++ )
                if( P[k].isigma == 0 && P[k].ircut == 0 )
                    for( ilens = G.nplens[k]; ilens < G.nplens[k+1]; ilens++ )
                    {
                        nimage = 0;
                        for ( i = 0; i < I.n_mult; i++ )
                            for ( j = 0; j < I.mult[i]; j++ )
                            {
                                grad = e_grad_pot(&multi[i][j].C, ilens);
                                zbitstmp0[nimage] += grad.x * multi[i][j].dr;
                                zbitstmp0[nmult + nimage] += grad.y * multi[i][j].dr;
                                nimage++;
                            }
    
                        for( i = 0; i < narclet; i++ )
                        {
			    grad2 = e_grad2_pot(&arclet[i].C, ilens);
                            zbitstmp0[2 * nmult + i] += (grad2.a - grad2.c) * arclet[i].dr;
                            zbitstmp0[2 * nmult + narclet + i] += 2. * grad2.b * arclet[i].dr;
                        }
                    }

            // Indexes for the source positions
            nimage = 0;
            for ( i = 0; i < I.n_mult; i++ )
                for ( j = 0; j < I.mult[i]; j++ )
                {
                    zbitstmp1[G.nlens - G.nmsgrid + i][nimage] = 1.;
                    zbitstmp1[G.nlens - G.nmsgrid + I.n_mult + i][nmult + nimage] = 1.;
                    nimage++;
                }

            // Data
            nimage = 0;
            for ( i = 0; i < I.n_mult; i++ )
                for ( j = 0; j < I.mult[i]; j++ )
                {
                    Data[nimage] = multi[i][j].C.x - zbitstmp0[nimage];
                    Data[nmult + nimage] = multi[i][j].C.y - zbitstmp0[nmult + nimage];
                    Acc[nimage] = 1. / multi[i][j].E.a / multi[i][j].mu;  // cos(multi[i][j].E.theta);
                    Acc[nmult + nimage] = 1. / multi[i][j].E.b / multi[i][j].mu; // cos(multi[i][j].E.theta);;

                    ibitstmp[nimage] = nimage;
                    ibitstmp[nmult + nimage] = nmult + nimage;
                    nimage++;
                }

            for( i = 0; i < narclet; i++ )
            {    
                e = (arclet[i].E.a * arclet[i].E.a - arclet[i].E.b * arclet[i].E.b) / (arclet[i].E.a * arclet[i].E.a + arclet[i].E.b * arclet[i].E.b);
                Data[2 * nmult + i] = e * cos(2. * arclet[i].E.theta) - zbitstmp0[2 * nmult + i];  // gamma1
                Data[2 * nmult + narclet + i] = e * sin(2. * arclet[i].E.theta) - zbitstmp0[2 * nmult + narclet + i]; // gamma2
                Acc[2 * nmult + i] = 1. / sqrt(arclet[i].var1 + I.sig2ell);
                Acc[2 * nmult + narclet + i] = 1. / sqrt(arclet[i].var2 + I.sig2ell);
    
                ibitstmp[2 * nmult + i] = 2 * nmult + i;
                ibitstmp[2 * nmult + narclet + i] = 2 * nmult + narclet + i;
            }
    
            free(zbitstmp0);

            Common->Ndata       = Ndata;
            Common->Data        = Data;
            Common->Acc         = Acc;
           
            // UserBuild uses mock data
            for( i = 0; i < Common->ENSEMBLE; i++ ) 
                Objects[i].Mock = (double*)malloc(Ndata * sizeof(double));
    
        }
            
        // Number of Atoms free
        Common->MinAtoms    = 1;          // >= 1
        Common->MaxAtoms    = 0;          // >= MinAtoms, or 0 = infinity(not implemented)
        Common->Alpha       = -0.02 * (G.nlens - G.nmsgrid) - 2 * I.n_mult - 1;       // +ve for Poisson, -ve for geometric

        if( nDim < G.nlens )
        { 
	    NPRINTF(stderr,"Stating nDim<G.nlens\n ");
       
            // MassInf parameters
            Common->Ndim      = 1;     // Coordinate in the grid
            Common->Valency   = 1;        // On/off switch for MassInf/Grid optim (Default: off)
            Common->MassInf     = 1;
            if( I.n_mult > 0 && G.no_lens > 0 )
            {
                Common->Valency = 3;    // Add 2 valencies for the SL source positions X and Y 
                Common->Method      = -3;         // Remove lifeStory2 algorithm for conflict of atom valencies
                Common->MassInf     = 2;    // Positive/Negative prior on the flux
            }
            Common->ProbON      = 0.7;
            Common->FluxUnit0   = -10.;  // 1 for pmass, 10 for b0
            nDim = G.nlens - G.nmsgrid;  // To initialize statistics arrays err, tmpCube and avg
        }
        else
        {   
	    NPRINTF(stderr,"Stating nDim = G.nlens\n");
            // Normal optimization but with Natoms (see UserBuild)
            Common->Ndim      = nDim;     // Number of parameters of the optimized clumps
            Common->Valency   = 0;        // On/off switch for MassInf/Grid optim (Default: off)
        }
        NPRINTF(stderr,"Condition Check\n ");
    }
  
    else
    {   
	NPRINTF(stderr,"Stating G.nmsgrid = G.nlens\n ");
       
       // Number of Atoms = 1  Standard optimization
        Common->MinAtoms    = 1;          // >= 1
        Common->MaxAtoms    = 1;          // >= MinAtoms, or 0 = infinity(not implemented)
        Common->Alpha       = -1.;       // +ve for Poisson, -ve for geometric

        Common->Ndim      = nDim;     // Number of parameters of the optimized clumps
        Common->Valency   = 0;        // On/off switch for MassInf/Grid optim (Default: off)
  
 	// Entering pixelized source plane reconstruction
	// author: Soniya Sharma
   	if (M.iclean == 4)
	{
		NPRINTF(stderr,"Entering into cleanset = 4 loop in o_runbayes\n ");      
             	int Ndata = imFrame.nx*imFrame.ny;
             	NPRINTF(stderr,"NDATA = %d\n",Ndata);
		int ns= ps.nx*ps.ny;

		//   Ndata = 2 * Ndata;  //Each data point has two dimensions x and y 
		double *Data = (double *)malloc((unsigned int)(Ndata * sizeof(double)));
		double *Acc = (double *)malloc((unsigned int)(Ndata * sizeof(double)));
		extern double **imo,**wo;
		extern double **fluxmap;
		zbitstmp1 = alloc_square_double(ns, Ndata); //What do they denote?
		ibitstmp = (int *)calloc(Ndata, sizeof(int)); 

		//Data       
		for( j = 0; j < Ndata; j++ )
			ibitstmp[j] = j;  //Filling up ibits matrix 
		for(i=0;i<imFrame.nx;i++)
		{
			for(j=0;j<imFrame.ny;j++)
			{     
				Data[i*imFrame.ny+j] = imo[j][i];
           			if(wFrame.format != 0)
				        Acc[i*imFrame.ny+j]=sqrt(wo[j][i]);
				else 
					Acc[i*imFrame.ny+j]=1.  ;	 //sqrt(fabs(imo[j][i]));   
			}
		}   

		//      free_square_double(im, imFrame.ny); 
		for( i = 0; i < ns; i++ )
		{
			for( j = 0; j < Ndata; j++ )
			{
				zbitstmp1[i][j]=fluxmap[j][i];
			}
		}
         
		Common->Ndata       = Ndata;
		Common->Data        = Data;
		Common->Acc         = Acc;
           
		// UserBuild uses mock data
		for( i = 0; i < Common->ENSEMBLE; i++ ) 
			Objects[i].Mock = (double*)malloc(Ndata * sizeof(double));
    
		// Number of Atoms free
		Common->MinAtoms    = 1;          // >= 1
		Common->MaxAtoms    = 0;          // >= MinAtoms, or 0 = infinity(not implemented)
		Common->Alpha       = -0.02 * (ps.nx*ps.ny)- 1;        

		// MassInf parameters
		Common->Ndim      = 1;     // Coordinate in the grid
		Common->Valency   = 1;        // On/off switch for MassInf/Grid optim (Default: off)
		Common->MassInf     = 1;
		Common->ProbON      = 0.7; 
		Common->FluxUnit0   = -20.;  // 1 for pmass, 10 for b0
    
	}
 
    }
    // Initialize some statistics
    UserCommon = userCommon_init(nDim);
    Common->UserCommon = (void*)UserCommon;
    tmpCube = (double *)malloc((unsigned int) nDim * sizeof(double));

    // Handle CTRL-C to interrupt the optimisation but not lenstool
    signal(SIGINT, signalReset);
    optInterrupt = 0;
    // Initialize mutex for nchi2 counts
    init_mutex();
 
    //Run bayesys
    int code = BayeSys3(Common, Objects);
    printf("\nBayeSys3 return code %d\n", code);

    // Destroy mutex
    destroy_mutex();

    // Close the restart file
    if(M.restart !=0)
    {
       fclose(fh_restart);
    }

    // Reset the SIGINT signal to its default behavior
    signal(SIGINT, SIG_DFL);

    fprintf(stdout, "\n");

    /*Set the lens parameters that give the best model
     * and the Gaussian prior at 3sigma limits*/
    o_set_lens_bayes(-4, 3, 3.);

    NPRINTF( stderr, "log(Evidence)          = %lf\n", Common->Evidence );
    NPRINTF( stderr, "Information = -Entropy = %lf\n", Common->Information );
    NPRINTF( stderr, "Calls to chi2() function = %ld\n", UserCommon->Nchi2 );

    /*Free the structures*/
    //author: Soniya Sharma
    if (M.iclean == 4)
    {  
         free_square_double(zbitstmp1, ps.nx*ps.ny);
        free( ibitstmp );
        free( Common->Data );
        free( Common->Acc );
        free( Objects->Mock );
    }
    if( I.stat == 8 && G.nlens != G.nmsgrid )
    {
        free_square_double(zbitstmp1, G.nlens-G.nmsgrid);
        free( ibitstmp );
        free( Common->Data );
        free( Common->Acc );
        free( Objects->Mock );
    }
    free( Objects );
    userCommon_free(UserCommon);
    free( tmpCube );

    return( Common->Evidence );
}

/* Write the header of the bayes.dat file
 * Return the number of free parameters/dimensions.
 * If bayes pointer is provided, write the header at this location
 */
int bayesHeader(char *bayes)
{
    extern struct g_mode    M;
    extern struct g_grille  G;
    extern struct g_image   I;
    extern struct g_pixel    ps;
    extern struct g_pot     P[NPOTFILE];
    extern struct pot       lens[];
    extern struct z_lim     zlim[];
    extern struct z_lim     zalim;
    extern struct galaxie   multi[NFMAX][NIMAX];
    extern int block[][NPAMAX];
    extern int cblock[NPAMAX];
    extern int vfblock[NPAMAX];
    extern struct sigposStr sigposAs;
    int nDim = 0; //# of parameters
    int ipx;      //index of the current parameter in the Param
    int i;        //index of the clump in the lens[] global variable
    int k;        //index of the z_m_limit images to optimise
    int nimages;
    char limages[ZMBOUND][IDSIZE];

    FILE *fbayes;
    char name[60];

    if ( bayes == NULL )
    {
        bayes = (char *)malloc(60*getNParameters());

// Clean the results files
#ifdef DEBUG
        fbayes = fopen( "bayes.dbg.dat" , "w" );
#else
        fbayes = fopen( "bayes.dat" , "w" );
#endif
    }
    else
        fbayes = NULL;

    int pos = 0;
    pos += sprintf( bayes, "#Nsample\n");
    pos += sprintf( bayes+pos, "#ln(Lhood)\n");


// Number of lens parameters in the atom
    for ( i = 0; i < G.no_lens; i++ ){ 
        for ( ipx = CX; ipx <= PMASS; ipx++ ){
            if ( block[i][ipx] != 0 )
            { 
                nDim++;
                pos += sprintf( bayes+pos, "#%s : %s\n", lens[i].n, getParName(ipx, name, lens[i].type) );
            };
        };
        //Benjamin Beauchesne - Bspline modification
        for ( ipx = CX_PERT; ipx < NPAMAX; ipx++ ){
            if ( block[i][ipx] != 0 )
            {
                nDim++;
                pos += sprintf( bayes+pos, "#%s : %s\n", lens[i].n, getParName(ipx, name, lens[i].type) );
            };
        };
    };
    // multiscale grid clumps : code enters the loop below for the test case
    for ( i = G.nmsgrid; i < G.nlens; i++ )
    { 
        pos += sprintf( bayes+pos, "#%s : %s\n", lens[i].n, getParName(B0, name, lens[i].type) );
        if( block[i][B0] != 0 )
            nDim++;
    }
    //author: Soniya Sharma
    if (M.iclean == 4)
    {
    	for ( i = 0; i < (ps.nx*ps.ny); i++ )
		    pos += sprintf( bayes+pos, "#%d : %s\n", i, "flux" );
	    NPRINTF(stderr,"Entered bayesheader() in o_run_bayes\n"); 
	    nDim = ps.nx*ps.ny;
    }

    // source positions of multiple images with grid optimization
    if( G.nmsgrid != G.nlens && nDim < G.nlens - G.nmsgrid )
    {
        for ( i = 0; i < I.n_mult; i++ )
            pos += sprintf( bayes+pos, "#X src %s\n", multi[i][0].n );
        for ( i = 0; i < I.n_mult; i++ )
            pos += sprintf( bayes+pos, "#Y src %s\n", multi[i][0].n );
    }

    // source positions for image reconstruction
    if( M.iclean == 2 )
    {
        extern int sblock[NFMAX][NPAMAX];
        extern struct galaxie source[NFMAX];
        extern struct g_source  S;

        for ( i = 0; i < S.ns; i++ )
            for ( ipx = SCX; ipx <= SFLUX2; ipx++ )
                if ( sblock[i][ipx] != 0 )
                {
                    nDim++;
                    pos += sprintf( bayes+pos, "#src %s : %s\n", source[i].n, getParName(ipx, name, 0));
                }
    }

// Number of cosmological parameters in the atom
    for ( ipx = OMEGAM; ipx <= WA; ipx++ )
        if ( cblock[ipx] != 0 )
        {
            nDim++;
            pos += sprintf( bayes+pos, "#%s\n", getParName(ipx, name, 0) );
        }

// Number of redshifts to optimise
    for ( k = 0; k < I.nzlim; k++ )
        if ( zlim[k].bk != 0 )
        {
            nimages = splitzmlimit( zlim[k].n, limages );
            i = 0;
            while ( indexCmp( multi[i][0].n, limages[0] ) ) i++;
            pos += sprintf( bayes+pos, "#Redshift of %s\n", multi[i][0].n );
            nDim++;
        }

// redshift of the arclets
    if ( zalim.bk != 0 )
    {
        pos += sprintf( bayes+pos, "#Redshift of arclets\n");
        nDim++;
    }

// Velocity field parameters to optimise
    for ( ipx = VFCX; ipx <= VFSIGMA; ipx++ )
        if ( vfblock[ipx] != 0 )
        {
            nDim++;
            pos += sprintf( bayes+pos, "#%s\n", getParName(ipx, name, 0) );
        }


// Potfile parameters to optimise
    for ( i = 0; i < G.npot; i++ )
        if ( P[i].ftype != 0 )
        {
            if ( P[i].ircut != 0 )
            {
                nDim++;
                pos += sprintf( bayes+pos, "#Pot%d rcut (arcsec)\n", i);
            }
            if ( P[i].isigma != 0 )
            {
                nDim++;
                pos += sprintf( bayes+pos, "#Pot%d sigma (km/s)\n", i);
            }
            if ( P[i].islope != 0 )
            {
                nDim++;
                if ( P[i].ftype == 62 )
                    pos += sprintf( bayes+pos, "#Pot%d m200slope\n", i);
                else
                    pos += sprintf( bayes+pos, "#Pot%d rcut_slope\n", i);
            }
            if ( P[i].ivdslope != 0 )
            {
                nDim++;
                if ( P[i].ftype == 62 )
                    pos += sprintf( bayes+pos, "#Pot%d c200slope\n", i);
                else
                    pos += sprintf( bayes+pos, "#Pot%d sigma_slope\n", i);
            }
            if ( P[i].ivdscat != 0 )
            {
                nDim++;
                pos += sprintf( bayes+pos, "#Pot%d vdscatter\n", i);
            }
            if ( P[i].ircutscat != 0 )
            {
                nDim++;
                pos += sprintf( bayes+pos, "#Pot%d rcutscatter\n", i);
            }
            if ( P[i].ia != 0 )
            {
                nDim++;
                if ( P[i].ftype == 62 )
                    pos += sprintf( bayes+pos, "#Pot%d m200\n", i);
                else
                    pos += sprintf( bayes+pos, "#Pot%d a\n", i);
            }
            if ( P[i].ib != 0 )
            {
                nDim++;
                if ( P[i].ftype == 62 )
                    pos += sprintf( bayes+pos, "#Pot%d c200\n", i);
                else
                    pos += sprintf( bayes+pos, "#Pot%d b\n", i);
            }
        }

// The noise
    if ( sigposAs.bk != 0 )
    {
        for ( i = 0; i < I.n_mult; i++ )
            for ( k = 0; k < I.mult[i]; k++)
            {
                pos += sprintf( bayes+pos, "#SigposArsec %s (arcsec)\n", multi[i][k].n);
                nDim++;
            }
    }
    if ( I.dsigell != -1. )
    {
        nDim++;
        pos += sprintf( bayes+pos, "#Sigell (arcsec)\n");
    }

    //add Chi2 as last column
    pos += sprintf( bayes+pos, "#Chi2\n");
    
    
    if ( fbayes != NULL )
    {
        fprintf( fbayes, "%s", bayes);
        fclose(fbayes);
        free(bayes);
    }
    return(nDim);
}



static void signalReset(int param)
{
    signal(SIGINT, SIG_DFL);
    optInterrupt = 1;
    fprintf( stderr, "INFO: Optimisation interrupted by CTRL-C\r");
}

UserCommonStr *userCommon_init(int nDim)
{
    UserCommonStr *userCommon;
    userCommon = (UserCommonStr *)malloc(sizeof(UserCommonStr));

    userCommon->avg = (double *)malloc(nDim*sizeof(double));
    userCommon->err = (double *)malloc(nDim*sizeof(double));

    userCommon->Nsample = 0;
    userCommon->atoms   = 0.0;
    userCommon->Nchi2 = 0;
    userCommon->sum = 0.;
    userCommon->Ndim = nDim;

    int i;

    for ( i = 0; i < nDim; i++ )
    {
        userCommon->err[i] = 0.;
        userCommon->avg[i] = 0.;
    }

    
    return userCommon;
}

void userCommon_free(UserCommonStr *userCommon)
{
    free(userCommon->avg);
    free(userCommon->err);
    free(userCommon);
}
