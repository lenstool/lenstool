#include<stdio.h>
#include<math.h>
#include<fonction.h>
#include<dimension.h>
#include<structure.h>


/****************************************************************/
/*      nom:        o_set_source          */
/*      auteur:     Eric Jullo         */
/*      date:       3/10/2011            */
/*      place:      Marseille            */
/****************************************************************/

void  o_set_source(struct galaxie *source, int ipx, double val)
{

    switch (ipx)
    {
        case(SCX):
            source->C.x = val;
            break;
        case(SCY):
            source->C.y = val;
            break;
        case(SA):
            source->E.a = val;
            break;
        case(SEPS):
            source->eps = val;
            break;
        case(STHETA):
            source->E.theta = val;
            break;
        case(SINDEX):
            source->var1 = val;
            break;
        case(SFLUX):
            source->mag = val;
            break;
        case(SA2):
            source->E2.a = val;
            break;
        case(SEPS2):
            source->eps2 = val;
            break;
        case(STHETA2):
            source->E2.theta = val;
            break;
        case(SINDEX2):
            source->var2 = val;
            break;
        case(SFLUX2):
            source->mag2 = val;
            break;
    }
}

double  o_get_source(struct galaxie *source, int ipx)
{
    double val = 0;

    switch (ipx)
    {
        case(SCX):
            val = source->C.x;
            break;
        case(SCY):
            val = source->C.y;
            break;
        case(SA):
            val = source->E.a;
            break;
        case(SEPS):
            val = source->eps;
            break;            
        case(STHETA):
            val = source->E.theta;
            break;
        case(SINDEX):
            val = source->var1;
            break;
        case(SFLUX):
            val = source->mag;
            break;
        case(SA2):
            val = source->E2.a;
            break;
        case(SEPS2):
            val = source->eps2;
            break;            
        case(STHETA2):
            val = source->E2.theta;
            break;
        case(SINDEX2):
            val = source->var2;
            break;
        case(SFLUX2):
            val = source->mag2;
            break;
    }

    return val;
}
