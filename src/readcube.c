#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<string.h>
#include<assert.h>
#include<wcslib.h>
#include<fonction.h>
#include<constant.h>
#include<dimension.h>
#include<structure.h>
#include "lt.h"


/****************************************************************/
/*                 Program  : readcube              */
/*                 Version  : 24 may 2013            */
/*                 Location : Obs. Lyon             */
/*                 Auteur   : Johan Richard         */
/* 
 ****************************************************************
 *
 * Read a cube (eventually FITS) and extract the pixels that are inside
 * the contour P.
 *
 * Parameter
 *  - P contains the contour of the cube to be inversed
 *
 *  Return an array of double containing the cube[nb_lin=ny][nb_col=nx][nb_slices=nz]
 * of the P->pixfile FITS file.
 */
/*void Get_Wavelength(char *INFILE, struct WorldCoor *wcs, float *lamda){
    
    fitsfile *fptr;
    int  status;
    float del_lam, crv_lam, crp_lam, lam;
    
    char comment[FLEN_KEYWORD], command[150];
    
    status = 0;
    if(fits_open_file(&fptr, INFILE, READONLY, &status))
        return status;
    
    if(fits_read_key_flt(fptr, "CDELT3", &del_lam, comment, &status))
        return status;
    
    if(fits_read_key_flt(fptr, "CRVAL3", &crv_lam, comment, &status))
        return status;
    
    if(fits_read_key_flt(fptr, "CRPIX3", &crp_lam, comment, &status))
        return status;
    
    if(fits_close_file(fptr, &status))
        return status;
    
    lam = del_lam*(pixval-crv_lam) + crp_lam;
    *lamda = lam;
}*/

/*void pix2lam(struct WorldCoor *wcs, double zpix, double *zpos){
    
    double CRPIX, CRVAL, CDELT, lam;
    
    CRPIX = P->wcsinfo->crpix[2];
    CRVAL = P->wcsinfo->crval[2];
    CDELT = P->wcsinfo->cdelt[2];
    
    lam = CDELT*(zpix-CRPIX)+CRVAL;
    
    *zpos = lam;
}*/



double ***readcube(struct g_cube *P)
{
    const extern  struct  g_mode          M;
    double  x1, x2, y1, y2, z1, z2;
    double  xmin, xmax, ymin, ymax, lmin, lmax;

    int i, j, k;
    register    int ii; //,jj;
    FILE    *IN;

    int     nx, ny, nz;
    double  ***zf;
    char    line[50];
    char    type[8];
    char    mode[4];
    char    nature[8];
    char    comments[1024];
    double  zz[10]; //zmin,zmax,
    char    *header;
    int     status;

    IN = fopen(P->pixfile, "r");

    if ( IN != NULL )
    {
        NPRINTF(stderr, "READ: cube: %s - format: %d\n", P->pixfile, P->format);

        if ((P->format == 0) || (P->format == -1))
        {
/*            rewind(IN);
            if (P->header != 0)
                for (ii = 0; ii < P->header; flire(IN, line), ii++);
            if (P->format == 0)
                fscanf(IN, "%d%d", &P->ny, &P->nx);
            else
            {
                fscanf(IN, "%d%d%lf%lf%lf%lf", &P->ny, &P->nx, &x1, &x2, &y1, &y2);
                P->pixelx = (x2 - x1) / ((double)(P->nx - 1));
                P->pixely = (y2 - y1) / ((double)(P->ny - 1));
                P->xmin = x1;
                P->ymin = y1;
                P->xmax = x2;
                P->ymax = y2;
            };
            flire(IN, line);

            for (ii = 0; ii < P->column; fscanf(IN, "%lf", &zz[ii]), ii++);

            rewind(IN);

            if (P->header != 0)
                for (ii = 0; ii < P->header; flire(IN, line), ii++);

            fscanf(IN, "%d%d", &P->ny, &P->nx);
            flire(IN, line);

            zf = (double ***) alloc_square_double(P->ny, P->nx), P->nz;
            i = 0;
            do
            {
                j = 0;
                do
                {
                    for (ii = 0; ii < P->column; fscanf(IN, "%lf", &zz[ii]), ii++);

                    flire(IN, line);
                    zf[i][j] = zz[P->column-1];
                    j++;
                }
                while (j < P->nx);
                i++;
            }
            while (i < P->ny);
*/
        } /*end of if((P->format == 0)||(P->format == -1))*/

        else if ((P->format == 1) || (P->format == 2))
        {
            RAISE(E_PARFILE, "ERROR: ipx file format deprecated");
/*
            if (P->format == 1)
            {
                zf = (double **)rdf_ipxs(P->pixfile, &nx, &ny,
                                         type, mode, nature, comments);
                P->ny = nx;
                P->nx = ny;
                NPRINTF(stderr, "\tdim %d %d, %s %s %s\n\t%s\n", nx, ny,
                        type, mode, nature, comments);
            }
            else
            {
                zf = (double **)rdf_ipx(P->pixfile, &ny, &nx,
                                        type, mode, nature, comments, &xmin, &xmax, &ymin, &ymax);
                P->ny = ny;
                P->nx = nx;
                P->pixelx = (xmax - xmin) / ((double)(P->nx - 1));
                P->pixely = (ymax - ymin) / ((double)(P->ny - 1));
                P->xmin = xmin;
                P->ymin = ymin;
                P->xmax = xmax;
                P->ymax = ymax;

                NPRINTF(stderr, "\tdim %d %d, %lf %lf %lf %lf\n\t%s %s %s\n\t%s\n",
                        nx, ny, xmin, xmax, ymin, ymax, type, mode, nature, comments);
            };
*/
        } /*end of if ((P->format ==1)||(P->format ==2))*/
        else if (P->format == 3)
        {
            int nkey, nreject, nwcs, status;
            struct wcsprm **wcs;

            zf= (double ***)rdf_cube_fits(P->pixfile, &nx, &ny, &nz, &header, &lmin, &lmax);
            
            assert( strlen(header) % 80 == 0 );
            nkey = strlen(header) / 80;
            status = wcspih(header, nkey, 0, 2, &nreject, &nwcs, wcs);
            if ( status )
                RAISE(E_PARFILE, "ERROR: reading WCS information in %s\n", P->pixfile);

            // only keep the first WCS representation
            for ( ; nwcs > 1; nwcs-- )
                if ( wcsfree(wcs[nwcs-1]) )
                    RAISE(E_MEMORY, "ERROR: freeing memory allocated in wcs[%d]\n", nwcs-1); 

            if (nwcs == 1 && M.iref != 0)
            {
                P->wcsinfo = wcs[0]; // only consider the first coordinate representation
                /*Permute axis if CRTYPE1 is DEC*/

                int stat[2];
                double pixcrd[4], imgcrd[4], phi[2], theta[2], world[4];
                pixcrd[0] = 1.; pixcrd[1] = 1.;
                pixcrd[2] = nx; pixcrd[3] = ny;
                if ( status = wcsp2s(P->wcsinfo, 2, 2, pixcrd, imgcrd, phi, theta, world, stat) )
                    RAISE(E_RUNTIME, "ERROR: converting pixel to world coordinates. Error value %d\n", status);
 
                xmin = world[0]; ymin = world[1];
                xmax = world[2]; ymax = world[3];
                xmax -= xmax - xmin > 180 ? 360. : 0.;
                xmin -= xmin - xmax > 180 ? 360. : 0.;
            }
            else
            {
                NPRINTF(stderr, "WARN: No astrometrical data in %s\n", P->pixfile);
                P->wcsinfo = NULL;
                xmin = 1;
                xmax = nx;
                ymin = 1;
                ymax = ny;
                lmin = 1;
                lmax = nz;
            }

            NPRINTF(stderr, "INFO: %s absolute bounds in degrees (RA %lf:%lf, DEC %lf:%lf, LAMBDA %lf:%lf)\n",
                    P->pixfile, xmin, xmax, ymin, ymax, lmin, lmax);

            P->ny = ny;
            P->nx = nx;
            P->nz = nz;
            P->lmin = lmin;
            P->lmax = lmax;
            
            if (P->pixelx == 0. && P->pixely == 0. && M.iref != 0)
            { 
                /*Convert to relative position in arcsec*/
                P->xmin = xmin - M.ref_ra;
                P->xmin *= -3600. * cos(M.ref_dec * DTR);
                P->xmax = xmax - M.ref_ra;
                P->xmax *= -3600. * cos(M.ref_dec * DTR);
                P->ymin = ymin - M.ref_dec;
                P->ymin *= 3600.;
                P->ymax = ymax - M.ref_dec;
                P->ymax *= 3600.;

                P->pixelx = (P->xmax - P->xmin) / ((double)(P->nx - 1));
                P->pixely = (P->ymax - P->ymin) / ((double)(P->ny - 1));
            }
            else
            { 
                P->xmin = xmin - M.ref_ra;
                P->xmin *= -3600. * cos(M.ref_dec * DTR);
                P->ymin = ymin - M.ref_dec;
                P->ymin *= 3600.;
                P->xmax = P->xmin + P->pixelx * (double)(P->nx - 1);
                P->ymax = P->ymin + P->pixely * (double)(P->ny - 1);
            }
            if(P->pixelz == 0.)
            {
                P->pixelz = (P->lmax - P->lmin) / ((double)(P->nz - 1));
            }
            else
            {
                P->lmax = P->lmin + P->pixelz * (double)(P->nz - 1);
            }

            NPRINTF(stderr, "INFO: %s relative bounds in arcsec (RA %lf:%lf, DEC %lf:%lf, (LAMBDA %lf:%lf)\n",P->pixfile, P->xmin, P->xmax, P->ymin, P->ymax, lmin, lmax);
            
            NPRINTF(stderr, "INFO: Cube size (%d %d %d) Resolution (%lf %lf) (\"/pix) (%lf) (Angstroms/pix)\n",P->nx, P->ny, P->nz, P->pixelx, P->pixely, P->pixelz);
        }
        /*end of if (P->format==3)*/

        else
            RAISE(E_PARFILE, "FATAL ERROR: format %d not known\n", P->format);

    } /*end of if( IN != NULL )*/

    else
        RAISE(E_FILE, "ERROR: cube %s not found\n", P->pixfile);

    fclose(IN);

    if ((P->pixelx == 0.) || (P->pixely == 0.) || (P->pixelz == 0.) || (P->xmin == P->xmax) || (P->ymin == P->ymax) || (P->lmin == P->lmax))
        RAISE(E_PARFILE, "ERROR: cube is not scaled !\n");

    // Compute sum of pixedouble fluxtot;
    int NX, NY, NZ;
    NX = P->nx;
    NY = P->ny;
    NZ = P->nz;
    double fluxtot;
    fluxtot = 0.;
     for( j = 0; j < NY; j++ )
     for( i = 0; i < NX; i++ )
     for( k = 0; k < NZ; k++ )
     fluxtot = +zf[j][i][k];
     P->meanFlux = fluxtot / (1.* NX) / (1.* NY) / (1.* NZ);
    NPRINTF(stderr, "READ: done\n");
    P->array = zf;
    return(zf);

}
