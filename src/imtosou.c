#include <stdlib.h>
#include <math.h>
#include <constant.h>
#include <fonction.h>
#include <lt.h>
/*
*       nom:        imtosou
*       auteur:     Jean-Paul Kneib
*       creation date:  10/02/92
*       place:      Toulouse
* major modification
*   oct 94
*   IoA Cambridge
*
* aim: transform a collection of points of an image into the source plane
*
*/

void    imtosou(double zimage, char *sname )
{
//  register    int i,j,k;
//  int is,js,size[4];
    const extern  struct g_mode   M;
    extern  struct g_pixel  imFrame, ps;
    const extern  struct  pot lens[];
    double  dlsds;
//  struct  point   A,B;
    double  **im, **source;
    double  **erreur;
    int **imult;
    int j,k;
//  char    comment[1024];
    int npl;
    struct  pixlist *pl;    // list of points to send to source plane
    double  ra, dec, width, height;

    /*
    * compute  dlsds
    */

    dlsds = dratio(lens[0].z, zimage);

    /*
    * read image
    */

    im = (double **) readimage(&imFrame);
    /* Allocate the maximum number of points to send to source plane */
    pl = (struct pixlist*)malloc((unsigned int)
                                 imFrame.nx * imFrame.ny * sizeof(struct pixlist));
    
    /*  keep only the relevant points */
    if (imFrame.ncont == 0)
        s_pixlist(im, pl, &npl);
    else
        keep_cl(im, pl, &npl);

    /*
    * resize the pixel-size in the source plan and copy the wcs information
    */

    ps.pixelx = imFrame.pixelx / ps.ech;
    ps.pixely = imFrame.pixely / ps.ech;


    NPRINTF(stderr, "INFO: Resolution I(%.5lf %.5lf) S(%.5lf %.5lf) (\"/pix) \n",
            imFrame.pixelx, imFrame.pixely, ps.pixelx, ps.pixely);

    /*
    *  allocate square map
    */

    source = alloc_square_double(ps.ny, ps.nx);
    erreur = alloc_square_double(ps.ny, ps.nx);
    imult = alloc_square_int(ps.ny, ps.nx);

    /*
    * define the source box as the barycenter of the source image-center points
    */

    NPRINTF(stderr, "DO: Define source box around c_image barycenter\n");
    s_sourcebox(&ps, M.centerfile, dlsds, zimage);

    /*
    *  do the transform image -> source + compute the error
    */

    NPRINTF(stderr, "COMP: image(%d %d) -> source frame (%d %d)\n",
            imFrame.nx, imFrame.ny, ps.nx, ps.ny);

    if(M.iclean==1) do_itos(im, pl, npl, dlsds, zimage, source, erreur, imult);
    if(M.iclean==5) newdo_itos(im, dlsds, zimage, source, erreur, imult);

    /*
    *  save the results
    */
    
    // Compute the source image center and relative limits from ps.wcsinfo
    
    wcsfull(&ps, &ra, &dec, &width, &height);

    ps.xmin -= -3600.*cos(M.ref_dec * DTR) * (ra - M.ref_ra);
    ps.xmax -= -3600.*cos(M.ref_dec * DTR) * (ra - M.ref_ra);
    ps.ymin -= 3600.*(dec - M.ref_dec);
    ps.ymax -= 3600.*(dec - M.ref_dec);
    wrf_fits_abs(sname, source, ps.nx, ps.ny, ps.xmin, ps.xmax, ps.ymin, ps.ymax, ra, dec);
    wri_fits_abs("imult.fits", imult, ps.nx, ps.ny, ps.xmin, ps.xmax, ps.ymin, ps.ymax, ra, dec);
    wrf_fits_abs("erreur.fits", erreur, ps.nx, ps.ny, ps.xmin, ps.xmax, ps.ymin, ps.ymax, ra, dec);

    /*
    *  free the square maps
    */

    free_square_double(source, ps.ny);
    free_square_double(erreur, ps.ny);
    free_square_int(imult, ps.ny);

    free_square_double(im, imFrame.ny);

}

// Define cubetosou for cube reconstruction

void    cubetosou(double zimage, char *sname )
{
    //  register    int i,j,k;
    //  int is,js,size[4];
    const extern  struct g_mode   M;
    extern  struct g_pixel  imFrame, ps;
    extern  struct g_cube  cubeFrame;
    const extern  struct  pot lens[];
    double  dlsds;
    //  struct  point   A,B;
    double ***cubeo;
    double ***cube_sou=NULL;
    int ***cube_imult=NULL;
    double ***cube_erreur=NULL;
    double  **im, **source, **erreur;
    int **imult;
    double **ima_temp;
    int kk, k, j;
    //  char    comment[1024];
    int npl;
    struct  pixlist *pl;    // list of points to send to source plane
    double  ra, dec, width, height;
    /*
     * read cube
     */
     cubeo = (double ***) readcube(&cubeFrame);
    
    
   // cubeo = (double ***)alloc_cubic_double(cubeFrame.ny,cubeFrame.nx,cubeFrame.nz);
   
    ima_temp = (double **) alloc_square_double(cubeFrame.ny,cubeFrame.nx);
    cube_sou = (double ***)alloc_cubic_double(ps.ny,ps.nx,cubeFrame.nz);
    cube_erreur= (double ***)alloc_cubic_double(ps.ny,ps.nx,cubeFrame.nz);
    cube_imult = (int ***)alloc_cubic_int(ps.ny,ps.nx,cubeFrame.nz);
    
    /*
     * compute  dlsds
     */
    dlsds = dratio(lens[0].z, zimage);
    
     NPRINTF(stderr, "READ: cubeframe\n");
    /* Allocate the maximum number of points to send to source plane */
    pl = (struct pixlist*)malloc((unsigned int)
                                 cubeFrame.nx * cubeFrame.ny * sizeof(struct pixlist));
    
    /*
     *  allocate square map
     */
   
    source = alloc_square_double(ps.ny, ps.nx);
    erreur = alloc_square_double(ps.ny, ps.nx);
    imult = alloc_square_int(ps.ny, ps.nx);
    
    /*
     * resize the pixel-size in the source plan and copy the wcs information
     */
    ps.pixelx = cubeFrame.pixelx / ps.ech;
    ps.pixely = cubeFrame.pixely / ps.ech;
    
         /* over write paramters of imFrame from cubeFrame so that keep_cl can use it */
    imFrame.header=cubeFrame.header;
    imFrame.nx = cubeFrame.nx;
    imFrame.ny = cubeFrame.ny;
    imFrame.pixelx = cubeFrame.pixelx;
    imFrame.pixely = cubeFrame.pixely;
    imFrame.ymin = cubeFrame.ymin;
    imFrame.ymax = cubeFrame.ymax;
    imFrame.xmin = cubeFrame.xmin;
    imFrame.xmax = cubeFrame.xmax;
    imFrame.wcsinfo = cubeFrame.wcsinfo;
    imFrame.meanFlux = cubeFrame.meanFlux;
    
    
    NPRINTF(stderr, "INFO: Resolution I(%.5lf %.5lf) S(%.5lf %.5lf) (\"/pix) \n",
            cubeFrame.pixelx, cubeFrame.pixely, ps.pixelx, ps.pixely);
    
    
    
    /*
     * define the source box as the barycenter of the source image-center points
     */
    
    NPRINTF(stderr, "DO: Define source box around c_image barycenter\n");
    s_sourcebox(&ps, M.centerfile, dlsds, zimage);
    
    /*
     *  do the transform image -> source + compute the error
     */
    
    NPRINTF(stderr, "COMP: image(%d %d) -> source frame (%d %d)\n",
            cubeFrame.nx, cubeFrame.ny, ps.nx, ps.ny);
    
    for (kk=0  ; kk < cubeFrame.nz; kk ++)
    { 
	
	NPRINTF(stderr, "cubeFrame.nz= %d\n", kk); /* printing the lambda-frame for which source is being  */

      
	for (j = 0; j < cubeFrame.ny; j++)                /* reconstructed */
        {
       		for (k = 0; k < cubeFrame.nx; k++)
			ima_temp[j][k]=cubeo[j][k][kk];    /* writing 3d array into 2D array per wavelength frame */
        }
        
               /*  keep only the relevant points */
        if (imFrame.ncont == 0)
        	s_pixlist(ima_temp, pl, &npl);
        else
        	keep_cl(ima_temp, pl, &npl);
    
        do_itos(ima_temp, pl, npl, dlsds, zimage, source, erreur, imult);
        imFrame.wcsinfo = cubeFrame.wcsinfo;

        /*
         *  save the results
         */
        for (j = 0; j < ps.ny; j++)
        {
            for (k = 0; k < ps.nx; k++)
            {
                cube_sou[j][k][kk]=source[j][k];
                cube_erreur[j][k][kk]=erreur[j][k];
                cube_imult[j][k][kk]=imult[j][k];
                source[j][k]=0.0;
                erreur[j][k]=0.0;
                imult[j][k]=0;
            }
        }
      
    }
    // Compute the source image center and relative limits from ps.wcsinfo
    wcsfull(&ps, &ra, &dec, &width, &height);
    
    
    ps.xmin -= -3600.*cos(M.ref_dec * DTR) * (ra - M.ref_ra);
    ps.xmax -= -3600.*cos(M.ref_dec * DTR) * (ra - M.ref_ra);
    ps.ymin -= 3600.*(dec - M.ref_dec);
    ps.ymax -= 3600.*(dec - M.ref_dec);
    
    
   
    wrf_cube_fits_abs(sname,cube_sou,ps.nx,ps.ny,cubeFrame.nz,ps.xmin,ps.xmax,ps.ymin,ps.ymax,cubeFrame.lmin,cubeFrame.lmax,ra,dec);
    wrf_cube_fits_abs("erreur.fits",cube_erreur,ps.nx,ps.ny,cubeFrame.nz,ps.xmin,ps.xmax,ps.ymin,ps.ymax,cubeFrame.lmin,cubeFrame.lmax, ra, dec);
    wri_cube_fits("imult.fits",cube_imult,ps.nx,ps.ny,cubeFrame.nz,ps.xmin,ps.xmax,ps.ymin,ps.ymax,cubeFrame.lmin,cubeFrame.lmax);
    
    
    free_cubic_double(cubeo,cubeFrame.ny,cubeFrame.nx);
    free_cubic_double(cube_sou, ps.ny, ps.nx);
    free_cubic_double(cube_erreur, ps.ny, ps.nx);
    free_cubic_int(cube_imult, ps.ny, ps.nx);
    /*
     *  free the square maps
     */
    free_square_int(imult, ps.ny);
    free_square_double(source, ps.ny);
    free_square_double(erreur, ps.ny);
    free_square_double(ima_temp, cubeFrame.ny);
    
    // free_square_double(im, imFrame.ny);
    
}
