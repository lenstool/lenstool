#include <grille.h>

/****************************************************************/
/*      nom:        critic              */
/*      auteur:     Jean-Paul Kneib         */
/*      date:       10/02/92            */
/*      place:      Toulouse            */
/****************************************************************/

void    box_caustic(nx, ny, xmin, xmax, ymin, ymax)

int nx, ny;
double  *xmin, *xmax, *ymin, *ymax;

{
    const extern struct biline radial[], tangent[];
    const extern int nrline, ntline;
    register int i;
    double  dx, dy, d;


    *xmin = *xmax = radial[0].S.x;
    *ymin = *ymax = radial[0].S.y;

    for (i = 1; i < nrline; i++)
    {
        if (radial[i].S.x > *xmax)
            *xmax = radial[i].S.x;
        else if (radial[i].S.x < *xmin)
            *xmin = radial[i].S.x;
        if (radial[i].S.y > *ymax)
            *ymax = radial[i].S.y;
        else if (radial[i].S.y < *ymin)
            *ymin = radial[i].S.y;
    }
    for (i = 1; i < ntline; i++)
    {
        if (tangent[i].S.x > *xmax)
            *xmax = tangent[i].S.x;
        else if (tangent[i].S.x < *xmin)
            *xmin = tangent[i].S.x;
        if (tangent[i].S.y > *ymax)
            *ymax = tangent[i].S.y;
        else if (tangent[i].S.y < *ymin)
            *ymin = tangent[i].S.y;
    }

    dx = (*xmax) - (*xmin);
    dy = (*ymax) - (*ymin);

    if (dx / nx > dy / ny)
    {
        d = (dx * ny / nx - dy) / 2.;
        *ymin -= d;
        *ymax += d;
    }
    else
    {
        d = (dy * nx / ny - dx) / 2.;
        *xmin -= d;
        *xmax += d;
    }

}
