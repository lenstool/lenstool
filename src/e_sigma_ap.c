#include <gsl/gsl_spline.h>
#include <gsl/gsl_interp.h>
#include <gsl/gsl_integration.h>

#include <math.h>
#include "constant.h"
#include "structure.h"

/****************************************************************/
/*      nom:        e_sigma_ap            */
/*      auteur:     Eric Jullo          */
/*      date:       16/06/2022            */
/*      place:      Marseille            */
/****************************************************************/

/* Compute the correction factor needed for a dPIE potential
 * to relate the lenstool velocity dispersion \sigma_0 to the 
 * velocity dispersion measured in an aperture. 
 * 
 * Use the formula from Bergamini et al. 2019 (Eq C.16)
 * 
 */

// Global variables
gsl_interp_accel *accr;
gsl_spline *sp_cprp;
#define NRP 100
#define RMIN 0.001
#define RMAX 1.6  // Maximum R value [arcsec] in Bergamini et al. 2019, Fig C.3

double cp_int_r(double lnr, void *params)
{
    double a = ((double *)params)[0];
    double s = ((double *)params)[1];
    double rp = ((double *)params)[2];
    double p, r;
    r = exp(lnr);
    p = (s * atan(r/s) - a * atan(r/a)) / (1+r*r/a/a) / (1+r*r/s/s) * sqrt(r*r - rp*rp) / r;
   // printf("%lf %lf %le\n",r,rp,p);
    return p;
}

/* Initialize the spline for internal integral in dr (see Bergamini et al. 2019, Eq C.16)
 * a, s: core and cut radii in arcsec
 */
void cp_rp_init_spline(double a, double s)
{
    double cprp[NRP], rp[NRP];
    double integral, err;
    int i;
    size_t nit;
    gsl_integration_workspace * w = gsl_integration_workspace_alloc (1000);

    gsl_function F;
    F.function = &cp_int_r;
    double params[3];
    params[0] = a;
    params[1] = s;
    F.params = params;

    double rmax; int n_it;
    if ( RMAX > s )
    {
        rmax = RMAX; n_it = NRP-1; // skip the last iteration with integral = 0
        rp[NRP-1] = RMAX; cprp[NRP-1] = 0;
    }
    else
    { rmax = s; n_it = NRP; }

    for ( i = 0; i < n_it; i++ )
    {
        rp[i] = i * (RMAX - RMIN) / (NRP - 1) + RMIN;  // linear sampling [RMIN, RMAX] with NRP points
        params[2] = rp[i];
        gsl_integration_qag(&F,log(rp[i]),log(rmax),0,1e-6,1000,1,w,&integral,&err); // inf = rcut * 10
        cprp[i] = integral * rp[i];
    }
    gsl_integration_workspace_free(w);

    // Initialize the spline
    sp_cprp=gsl_spline_alloc(gsl_interp_cspline,NRP);
    accr=gsl_interp_accel_alloc();
    gsl_spline_init(sp_cprp,rp,cprp,NRP);
}

void cp_spline_free()
{
    gsl_interp_accel_free(accr);
    gsl_spline_free(sp_cprp);
}

double get_cp(double a, double s, double r)
{
    double integral;
    gsl_interp_accel *acc;

    double t0, p;
    t0 = 6. / PI * (a + s) / a / a / s; 
    p = sqrt(a*a + r*r) - a - sqrt(s*s + r*r) + s;
     
    cp_rp_init_spline(a, s);

    acc = gsl_interp_accel_alloc();
    integral = gsl_spline_eval_integ(sp_cprp, RMIN, r, acc);
    
    gsl_interp_accel_free(acc);
    cp_spline_free();
    return sqrt(integral * t0 / p);
}

double e_sigma_ap(struct pot *ilens, double r)
{
    double sigma_ap;
    if (ilens->type == 81 )
        sigma_ap = ilens->sigma * sqrt(3./2.) * get_cp(ilens->rc, ilens->rcut, r);
    else
    {
        fprintf(stderr, "WARN: sigma_ap correction term not computed for profile %d in potential %s\n", ilens->type, ilens->n);
        sigma_ap = ilens->sigma;
    }
    return sigma_ap;
}

