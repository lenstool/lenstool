#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<math.h>
#include<fonction.h>
#include<constant.h>
#include<dimension.h>
#include<structure.h>
#include<lt.h>
#include<errors.h>
#include<gsl/gsl_rng.h>

#ifdef _OPENMP
#include "omp.h"
#endif

#include "o_global.h"

static void readWeightMat(double *** weight, double *detCov);

/****************************************************************/
/*      nom:        global              */
/*      auteur:     Jean-Paul Kneib         */
/*      date:       10/02/92            */
/*      place:      Toulouse            */
/****************************************************************/

void  o_global()
{

    /*************  declaration de common et locale ****************/

    extern struct g_mode    M;
    extern struct g_grille  G;
    extern struct g_source  S;
    extern struct g_image   I;
    extern struct pot       lens[];


    double evidence;    // in the bayesian optimisation
    int  constraints, parameters; // for the computation of dof


    /**************************************************************/

    NPRINTF(stderr, "------- GLOBAL - Lens Optimisation - JPK, March 1994 ------\n");

    /* preparation de l'optimisation */
    readConstraints();

    /*-------------------------------------------------------*/
    /* optimisation */
    NPRINTF(stderr, "OPTIMISATION");
    if (( G.nmsgrid != G.nlens ) || (M.iclean == 4))
        NPRINTF(stderr, "(NON PARAM)");

    NPRINTF(stderr, " :\n");

    // set the evidence and compute the number of dof.
    evidence = 0;
    if ( M.inverse >= 3 ) NPRINTF( stderr, "Bayesys Rate : %lf \n", M.rate);

    constraints = getNConstraints();
    parameters = getNParameters();
    NPRINTF(stderr, "Number of contraints : %d\n", constraints);
    NPRINTF(stderr, "Number of free parameters : %d\n", parameters);
    if( constraints == 0 )
        RAISE(E_PARFILE, "ERROR: No constraints\n");

//    if ( parameters > constraints )
//    {
//        NPRINTF( stderr, "WARN : Too many free parameters.\n");
//      exit(-1);
//      return;
//    }
    
    // Start the optimisation engines
    if ( M.inverse == 3 || M.inverse == 4 )
        evidence = o_run_bayes();
    else if ( M.inverse == 5 )
     {
//        evidence = o_run_nest();
     }
//   else if (M.inverse == 6)
//     {
//	run_servermode();
//     }
    else
    {
        RAISE(E_PARFILE, "ERROR : inverse modes 1 & 2 are not supported anymore\n");

//        /* sauvegarde des parametres d'optimisations */
//
//        /* is the potentiel a spline? */
//        if (lens[0].type != 10)
//        {
//            for (i = 0; i < G.no_lens; i++)
//            {
//                lmin_s[i] = lmin[i];
//                lmax_s[i] = lmax[i];
//                prec_s[i] = prec[i];
//                for (j = 0; j < NPAMAX; j++)
//                    block_s[i][j] = block[i][j];
//            }
//        }
//        else
//        {
//            sm_p = (double **) alloc_square_double (G.nx, G.ny);
//            sm_p = map_p;
//        }
//
//        for (i = 0; i < I.nzlim; i++)
//            zlim_s[i] = zlim[i];
//
//        sigposAs_s = sigposAs;
//
//        /* selection/identification des parametres pour la carte de chi2 */
//        // if any block[ils][ipx] is < 0, set the ip.map global variable
//        o_set_map();
//        if ( ip.map > 0 ) NPRINTF( stderr, "map dimensions : %d \n", ip.map);
//
//        if (M.inverse == 1) /* direct fitting */
//        {
//            if (mc.optMC == 1)
//                o_run_mc();
//            else if (mc.optMC == 2)
//                o_run_mc0();
//            else if (zlim[0].opt == 2)
//                o_runz_mc();
//            else if (zlim[0].opt == 3)
//                o_runz_mc0();
//            else if (ip.map == 0)
//                o_run();
//            else if (ip.map == 1)
//                o_run1();
//            else if (ip.map == 2)
//                o_run2();
//        }
//        else if (M.inverse == 2)  /* potfile fitting */
//        {
//            if ((P.ircut > 1) && (P.isigma > 1))
//                o_runpot2();
//            else if (P.ircut > 1)
//                o_runpot1(1);
//            else if (P.isigma > 1)
//                o_runpot1(2);
//            else
//                o_run();
//        }
    }

    /*-------------------------------------------------------
     * Reset the lens parameters and print the optimisation results */
    printf("%f %f \n",lens[0].b0,lens[1].b0);
    /*Fill the chires.dat file*/
    o_chires("chires.dat");
    printf("%f %f \n",lens[0].b0,lens[1].b0);
    /*Reset the lens parameters*/
    set_res_par();
    printf("%f %f \n",lens[0].b0,lens[1].b0);
    /*Print the results of optimisation*/
    o_print_res(o_chi(), evidence);
    printf("%f %f \n",lens[0].b0,lens[1].b0);
//  NPRINTF(stderr,"chi: %.1lf (%.1lf,%.1lf)\n",chi0,chip,chil);

    /*-------------------------------------------------------*/
    /*  resultats  */

    if (lens[0].type != 10)
    {
        int    i;
        for (i = 0; i < G.no_lens; i++)
        {
            if (lens[i].type > 1)
                lens[i].sigma = sqrt(lens[i].b0 / 6. / pia_c2);
            else
                lens[i].sigma = sqrt(lens[i].b0 / 4. / pia_c2);

            lens[i].dlsds = dratio(lens[i].z, S.zs);
        }
    }

    if (I.stat != 0)
    {
        extern struct galaxie   arclet[];
        extern long int    narclet;
        o_stat(narclet, arclet);
    }

    o_global_free();
    NPRINTF(stderr, "------- Lens Optimisation Done -----------\n");
}

/*
 * Free all memory in o_global.readConstraints()
 */
void o_global_free()
{
    extern struct g_image   I;
    extern struct g_grille  G;
    extern struct g_mode    M;
    
    if ( I.srcfit != 0 )
        free(srcfit);

    if ( G.nlens != G.nmsgrid )
    {
        extern struct galaxie   arclet[];

        free(np_b0);
        if( I.n_mult > 0 )
        {
            free(multi[0][0].np_grad);  // root of the malloc
            free(multi[0][0].np_grad2a);
            free(multi[0][0].np_grad2b);
            free(multi[0][0].np_grad2c);
        }
        else
        {
            free(arclet[0].np_grad);
            free(arclet[0].np_grad2a);
            free(arclet[0].np_grad2b);
            free(arclet[0].np_grad2c);
        }
    }

    if (M.iclean != 0)
    {
        extern struct g_source  S;
        extern struct g_pixel   ps;
        extern struct g_pixel   imFrame;
        extern struct g_pixel   wFrame;
        extern struct g_cube    cubeFrame;
        extern double **map_axx, **map_ayy;
        int i;
        

        //  free the square maps
        if (M.iclean == 2 )
        {
	    const extern struct g_observ  O; 
            free_square_double(ero, imFrame.ny*O.bin);
	    free(imo_pl); // mod bclement shapemodel_contour
	    free(imo_pl_ext); // mod bclement shapemodel_contour_ext
	    free(imo_pl_dpl_ext); // mod bclement shapemodel_contour_ext
        }
        else if (M.iclean == 3 )
        {
            free_cubic_double(cubeo, cubeFrame.ny, cubeFrame.nx);
            free_cubic_double(ercubeo, cubeFrame.ny, cubeFrame.nx);
        }
	//author: Soniya Sharma
        else if (M.iclean == 4 )
        {
            free(sflux);
            free_square_double(ero, imFrame.ny);
            free_square_double(fluxmap, imFrame.ny*imFrame.nx);
        }
        else
        {
            free_square_double(soo, ps.ny);
            free_square_int(imuo, ps.ny);
        }
        
        free_square_double(imo, imFrame.ny);
        free_square_double(wo, wFrame.ny);
        

        int pot_nopt = 0;
        extern struct g_pot P[NPOTFILE];
        for (i = 0; i < G.npot; i++ )
        {
            pot_nopt += P[i].ircut;
            pot_nopt += P[i].isigma;
            pot_nopt += P[i].islope;
            pot_nopt += P[i].ivdslope;
            pot_nopt += P[i].ivdscat;
            pot_nopt += P[i].ircutscat;
            pot_nopt += P[i].ia ;
            pot_nopt += P[i].ib;
        }

        if (G.no_lens == 0 && pot_nopt == 0)
        {
            free_square_double(map_axx, imFrame.ny);
            free_square_double(map_ayy, imFrame.ny);
        } 

        // Reset source positions to absolute positions
        if( I.n_mult > 0 )
        {
            extern struct galaxie source[NFMAX];
            struct point Bs;  // list of source barycenters  
            struct point Ps[NIMAX];
            char str[IDSIZE], *pch;  // get image identifier
            int    j;

            for( i = 0; i < S.ns; i++ )
            {
                // check if source is attached to an image
                strcpy(str, source[i].n);
                pch = strtok(str, "_");
                pch = strtok(NULL, "_");
                if( pch == NULL )    break;  // no attachment to an image

                // look for the corresponding image family
                j = 0; 
                while ( indexCmp( pch, multi[j][0].n ) && j < I.n_mult ) j++;

                // add barycenter position
                if( j < I.n_mult )
                {
                    o_dpl(I.mult[j], multi[j], Ps, np_b0);
                    Ps[I.mult[j]] = Ps[0];
                    Bs = bcentlist(Ps, I.mult[j]);
                    source[i].C.x += Bs.x;
                    source[i].C.y += Bs.y;
                }
            }
        }

        // Reset source counter to zero
        //S.ns = 0;
    }
}

/* Read the different constraints to prepare the optimisation
 * according to the global variables set in the .par file.
 */
void readConstraints()
{
    extern struct g_mode    M;
    extern struct g_image   I;
    extern struct g_source  S;
    extern struct g_grille  G;
    extern struct pot       lens[];
    extern struct cline     cl[];
    extern struct sigposStr sigposAs;
    extern struct galaxie   multi[NFMAX][NIMAX];
    extern struct g_pixel   ps;
    extern struct galaxie   arclet[];
    extern long int    narclet;

    int  np;
    long int ntmult;
    int    i, j;
    FILE    *IN;

    if (M.iclean != 0)
    {
//        imo = (double **) o_invim(M.zclean, &drima, plo, &nplo);
        extern struct g_pixel imFrame;
        extern struct g_pixel   wFrame;
        extern struct g_cube    cubeFrame;
        extern struct galaxie source[NFMAX];
        extern double **map_axx, **map_ayy;
                //Read imframe
        if ( imFrame.format !=0 )
        { NPRINTF(stderr, "READ: imframe\n");
            imo = (double **) readimage(&imFrame);
        }
        //Read cubeframe
        if ( cubeFrame.format !=0 )
        { NPRINTF(stderr, "READ: cubeframe\n");
            cubeo = (double ***) readcube(&cubeFrame);
        }

        //Test if cubeFrame exists and iclean=1
        if(M.iclean==1 && imFrame.format==1 && cubeFrame.format==1)
        {
            fprintf(stderr, "WARNING: Both imframe and cubeframe provided. Only cubeframe will be used for reconstruction.\n ");
            
        }
        if(M.iclean==3)
        {
            cubeo = (double ***) readcube(&cubeFrame);
        }
//        for( i = 0; i < imFrame.nx; i++ )
//            for( j = 0; j < imFrame.ny; j++ )
//                if( imo[j][i] == 0. )
//                {
//                    fprintf(stderr, "ERROR: O value found in %s. Cannot compute chi2.\n", imFrame.pixfile);
//                    exit(-1);
//                }
        
//check for iclean = 4
        if (M.iclean==4)
        {
            
            precompsource();
            
            fprintf(stderr,"precomp generated\n");
            
        }
        
        if(wFrame.format != 0)
        {
            wo = (double **) readimage(&wFrame);
            
            
            // Test of positivity of wo 
            for( i = 0; i < wFrame.nx; i++ )
                for( j = 0; j < wFrame.ny; j++ )
                    if( wo[j][i] < 0. )
                        RAISE(E_RUNTIME, "ERROR: negative value found in %s.\n", wFrame.pixfile);
 
	    NPRINTF(stderr, "wFrame.nx= %d\t wFrame.ny=%d\n", wFrame.nx, wFrame.ny);

            // Test of dimension matching
            if ( wFrame.nx != imFrame.nx || wFrame.ny != imFrame.ny )
                RAISE(E_RUNTIME, "ERROR:  dimensions mismatch in %s and %s", wFrame.pixfile, imFrame.pixfile);

            // Pre-compute the loglikelihood normalization factor
            lhood_wo = 0.;
            for( i = 0; i < wFrame.nx; i++ )
                for( j = 0; j < wFrame.ny; j++ )
                    if( wo[j][i] > 0 )
                        lhood_wo -= log( 2.*PI*wo[j][i] );  
        }


        if ( M.iclean == 2 || M.iclean ==3 || M.iclean ==4)
        {

            if(M.iclean==2|| M.iclean ==4)
            {
		const extern struct g_observ  O; // mod bclement shapemodel_contour_ext
                // allocate temporary array to compute chi2 in o_chi()
                ero = (double **) alloc_square_double(imFrame.ny*O.bin, imFrame.nx*O.bin);
            }

	    // mod bclement shapemodel_contour
	    // Initialize and build the pixlist defining the contours
	    if(M.iclean == 2)
	    {
		const extern struct g_observ  O; // mod bclement shapemodel_contour_ext

		/* Allocate the maximum number of points to send to source plane */
		imo_pl = (struct pixlist*)malloc((unsigned int)
                                 imFrame.nx * imFrame.ny * sizeof(struct pixlist));

		/*  keep only the relevant points */
		if (imFrame.ncont == 0)
        {
            RAISE(E_PARFILE, "ERROR: the number of contours is %d (not defined). Set the keyword ncont in the parameter file.\n",imFrame.ncont);
        }
		else
		{
		    keep_cl(imo, imo_pl, &imo_npl);
		    // mod bclement shapemodel_contour_ext
		    if (O.setseeing)
		    {
			imo_pl_ext = (struct pixlist*)malloc((unsigned int)
                                 O.bin*O.bin*imFrame.nx * imFrame.ny * sizeof(struct pixlist));
			keep_cl_extended(imo_pl, imo_npl, imo_pl_ext, &imo_npl_ext);
		    }
		    else
		    {
                        imo_pl_ext = (struct pixlist*)malloc((unsigned int)
                                 imFrame.nx * imFrame.ny * sizeof(struct pixlist));
                        keep_cl(imo, imo_pl_ext, &imo_npl_ext);
		    }
		}

		// TODO : remove duplicate pixels in list when overlapping contours occur
		// This would cause a bug o_pixel_contour()

		// TODO possibly reallocate imo_pl given imo_npl 
		imo_pl = (struct pixlist*)realloc(imo_pl, (unsigned int)
                                 imo_npl * sizeof(struct pixlist));

		// mod bclement shapemodel_contour_ext
		imo_pl_ext = (struct pixlist*)realloc(imo_pl_ext, (unsigned int)
                                 imo_npl_ext * sizeof(struct pixlist));

		// redo the pre-computation of the loglikelihood normalization factor
		lhood_wo = 0.;
		int n;
		for( n = 0; n < imo_npl; n++ )
		{
		    j = imo_pl[n].ii;
		    i = imo_pl[n].jj;
                    if( wo[j][i] > 0 )
                        lhood_wo -= log( 2.*PI*wo[j][i] );  
		}

	    }

            if(M.iclean==3)
            {
              // allocate temporary array to compute chi2 in o_chi()
              ercubeo = (double ***) alloc_cubic_double(cubeFrame.ny, cubeFrame.nx, cubeFrame.nz);
            }

            int pot_nopt = 0;
            extern struct g_pot P[NPOTFILE];
            for (i = 0; i < G.npot; i++ )
            {
                pot_nopt += P[i].ircut;
                pot_nopt += P[i].isigma;
                pot_nopt += P[i].islope;
                pot_nopt += P[i].ivdslope;
                pot_nopt += P[i].ivdscat;
                pot_nopt += P[i].ircutscat;
                pot_nopt += P[i].ia ;
                pot_nopt += P[i].ib;
            }

            if( G.no_lens == 0 && pot_nopt == 0 )
            {
                //mod bclement shapemodel_contour_ext
                struct point pi, ps;
                double zs_ref = 100.;
                double dlsds_ref = dratio(lens[0].z, zs_ref);
		if(M.iclean == 2) // compute normalized dpl only inside extended contour
		{
		    imo_pl_dpl_ext = (struct pixlist*)malloc((unsigned int)
			      imo_npl_ext * sizeof(struct pixlist));
		    int n;
		    for(n = 0; n < imo_npl_ext; n++)
		    {
			pi.y = imo_pl_ext[n].i;
			pi.x = imo_pl_ext[n].j;
			e_dpl(&pi, dlsds_ref, zs_ref, &ps);
			imo_pl_dpl_ext[n].i = (pi.x - ps.x)/dlsds_ref;
			imo_pl_dpl_ext[n].j = (pi.y - ps.y)/dlsds_ref;
		    }
		}
		else
		{
		    map_axx = (double **) alloc_square_double(imFrame.ny, imFrame.nx);
		    map_ayy = (double **) alloc_square_double(imFrame.ny, imFrame.nx);
		    
		    for (i = 0; i < imFrame.ny; i++)
		    {
			pi.y = imFrame.ymin + i * imFrame.pixelx;
			for (j = 0; j < imFrame.nx; j++)
			{
			    pi.x = imFrame.xmin + j * imFrame.pixelx;
			    e_dpl(&pi, dlsds_ref, zs_ref, &ps);
			    map_axx[i][j] = pi.x - ps.x;
			    map_ayy[i][j] = pi.y - ps.y;
			}
		    }
		}
            }

            // read and prepare sources for o_chi()
            if( M.source > 0 )
            {
                long int istart = S.ns;
                f_shape(&S.ns, source, M.sourfile, M.source);  // with var1 & var2 parameters
                if( M.source == 2 )
                    for( i = istart; i < S.ns; i++ )
                        source[i].type = source[i].var1;

                pro_arclet(S.ns, source);  // compute eps = (a-b)/(a+b)
            }

            // prepare sources to have relative positions wrt barycenter
//            if( I.n_mult > 0 )
//            {
//                struct point Ps[NIMAX];
//                char str[IDSIZE], *pch;  // get image identifier
//
//                for( i = 0; i < S.ns; i++ )
//                {
//                    // check if source is attached to an image
//                    strcpy(str, source[i].n);
//                    pch = strtok(str, "_");
//                    pch = strtok(NULL, "_");
//                    if( pch == NULL )    break;  // no attachment to an image
//
//                    // look for the corresponding image family
//                    j = 0; 
//                    while ( indexCmp( pch, multi[j][0].n ) && j < I.n_mult ) j++;
//
//                    // set source position into relative coordinates
//                    if( j < I.n_mult )
//                        source[i].C.x = source[i].C.y = 0.;
//                }
//            }

            // prepare source redshifts
            for ( i = 0; i < S.ns; i++ )
            {
                if( source[i].z == 0 )
                {
                    source[i].z = M.zclean;
                    NPRINTF(stderr, "WARN: source %s redshift set to cleanset z=%lf\n", source[i].n, M.zclean);
                }
                dratio_gal(&source[i], lens[0].z);
            }
        }
        else
        {
            //  allocate square map
            soo = (double **) alloc_square_double(ps.ny, ps.nx);
            ero = (double **) alloc_square_double(ps.ny, ps.nx);
            imuo = (int **) alloc_square_int(ps.ny, ps.nx);
        }

    }

    if (I.shmap != 0)
    {
        nshmap = 0;
        f_shmap(&nshmap, shm, I.shfile);
        if (nshmap < 1)
            I.shmap = 0;

        I.dl0ssh = distcosmo2( lens[0].z , I.zsh );
        I.dossh = distcosmo1( I.zsh );
        I.drsh = I.dl0ssh / I.dossh;
    }

    if (I.stat != 0)
    {
        narclet = 0;

        // Test if the number of arclets is lower than NAMAX
        IN = fopen(I.arclet, "r");
        if ( IN == NULL )
            RAISE(E_FILE, "ERROR: File %s not found.\n", I.arclet );

        int nrows = (int)wc(IN); fclose(IN);
        if ( nrows > NAMAX )
            RAISE(E_PARFILE, "ERROR: Too many arclets in %s (%d). Max allowed %d\n", I.arclet, nrows, NAMAX);

        f_shape(&narclet, arclet, I.arclet, I.statmode);

        if (M.seeing > 0.)
            cor_seeing(narclet, arclet, M.seeing);

        if (narclet < 1)
            I.stat = 0;
        else
        {
            sort(narclet, arclet, comparer_z);
            zalim.opt = 0;
            gsl_rng *seed = gsl_rng_alloc(gsl_rng_taus);
            gsl_rng_set(seed, S.rand);
            gsl_ran_discrete_t *gsmail;
            if( S.distz == 2 && S.par1 != 0 )
                gsmail = smailpreproc();

            while( arclet[zalim.opt].z == 0 && zalim.opt < narclet ) 
            {
                if( I.zarclet > 0 )
                    arclet[zalim.opt].z = I.zarclet;
                else if( zalim.min > 0 )
                    arclet[zalim.opt].z = zalim.min;
                else if( S.distz == 0 )
                    arclet[zalim.opt].z = S.ns;
                else if( S.distz == 1 )
                    arclet[zalim.opt].z = S.zsmin;
                else if( S.distz == 2 && S.par1 != 0 )
                    arclet[zalim.opt].z = d_rndzsmail(seed, gsmail);

                if( arclet[zalim.opt].z > 0 )
                    zalim.opt++;
                else
                    RAISE(E_ZALIM_MISSING, "ERROR: Arclets with unknown redshift, and no redshift set in image section\n");
            }

            if( zalim.opt > 0 )
                FPRINTF(stdout, "INFO: Set %d arclets with unknown redshift\n", zalim.opt);
  
            for( i = 0; i < narclet; i++ )
                dratio_gal(&arclet[i], lens[0].z);

            pro_arclet(narclet, arclet);

            gsl_rng_free(seed);
            if( S.distz == 2 && S.par1 != 0 )
                gsl_ran_discrete_free(gsmail);
        }

        // Intrinsic ellipticity distribution width:
        I.sig2ell = I.sigell * I.sigell;

    }

    if (I.multmode != 0)
    {
        ntmult = 0;
        struct galaxie *mult = (struct galaxie *) malloc((unsigned long int) NFMAX * NIMAX * sizeof(struct galaxie));
        if ( mult == NULL )
            RAISE(E_MEMORY, "ERROR: mult[NFMAX*NIMAX] memory allocation failed.\n");

        f_shape(&ntmult, mult, I.multfile, I.multmode);
        if(I.multmode==1) 
        {
            for ( i = 0; i < ntmult; i++ )
                mult[i].var1 = mult[i].var2 = 1.;  //weight = 1
        }

        if (M.seeing > 0.)
            cor_seeing(ntmult, mult, M.seeing);

        if (ntmult < 1)
            I.multmode = 0;
        else
        {
            pro_arclet(ntmult, mult);
            o_prep_mult(ntmult, mult);
            dist_min();

            // Set the image position error in image plane
            for ( i = 0; i < I.n_mult; i++ )
                for ( j = 0; j < I.mult[i]; j++ )
                    I.sig2pos[i][j] = sigposAs.min * sigposAs.min;

            if( I.forme == 12)
                readWeightMat(&I.weight, &I.detCov);
        }
        free(mult);
    }
    
    //benjamin Beauchesne teste true source
     
    if (S.multmode != 0)
    {
        long int ntmult_s = 0; 
        struct galaxie *mult_s = (struct galaxie *) malloc((unsigned long int) NFMAX * NIMAX * sizeof(struct galaxie));
        if ( mult_s == NULL )
            RAISE(E_MEMORY, "ERROR: mult[NFMAX*NIMAX] memory allocation failed.\n");

        f_shape(&ntmult_s, mult_s, S.multfile, S.multmode);
        sort(ntmult_s, mult_s, comparer_z);
        for (i=0;i<ntmult_s;i++){
            S.multi_s[i]=mult_s[i].C;
        }
        //free(mult_s);
    }
    // Prepare Accelerated potentials
    if (( G.nmsgrid != G.nlens ) || (M.iclean == 4))
    {
        prep_non_param();
 
        // Error message to prevent deletion of imported data
        if( M.source != 0 && I.n_mult != 0 )
            RAISE(E_PARFILE, "ERROR: You cannot import a catalog of sources while optimizing with the grid.\n");
    }


    // Source plane fit of a brightness profile
    if (I.srcfit != 0 )
    {
        srcfit = (struct galaxie *)malloc((unsigned int)NSRCFIT * sizeof(struct galaxie));
        if ( srcfit == NULL )
            RAISE(E_MEMORY, "ERROR: srcfit memory allocation failed.\n");

        f_shape(&I.nsrcfit, srcfit, I.srcfitFile, 0);
        for (i = 0; i < I.nsrcfit; i++)
            dratio_gal(&srcfit[i], lens[0].z);
    }

    if (I.npcl != 0)
    {
        /* preparation de l'optimisation dans le cas de contraintes de ligne */
        /* critiques (cassures d'arcs) */
        np = 0;
        for (i = 0; i < I.npcl; i++)
        {
            if (cl[i].n != 0)
            {
                if (lens[0].z < cl[i].z)
                    cl[i].dl0s = distcosmo2(lens[0].z, cl[i].z);
                else
                    cl[i].dl0s = 0.;

                cl[i].dos = distcosmo1(cl[i].z);
                cl[i].dlsds = cl[i].dl0s / cl[i].dos;

                cl[np] = cl[i]; // stack all the CL contraints at the beginning of <cl> list
                np++;
            };
        }
        I.npcl = np;  // new number of CL constraints
        NPRINTF(stderr, "INF: critical points: %d\n", I.npcl);
    }
}


// Compute the number of constraints with multiple images
int getNConstraints()
{
    extern struct g_mode  M;
    extern struct g_image I;
    extern struct g_dyn   Dy;   //TV Oct 2011
    extern long int narclet;
    extern struct pixlist *imo_pl; // mod bclement shapemodel_contour
    extern double **wo; // mod bclement shapemodel_contour
    extern int imo_npl; // mod bclement shapemodel_contour
    int constraints, i;
    int opi;        // number of observable per images

    opi = 2;     // default only consider x,y
    if ( I.forme == 1 ) opi = 4;  // +shape
    if ( I.forme == 2 ) opi = 3;  // +flux only
    if ( I.forme == 3 ) opi = 5;  // +flux + shape

    constraints = 0;
    
   

    // Multiple images constraints
    for ( i = 0; i < I.n_mult; i ++)
        if ( I.mult[i] > 1 )
            constraints += opi * (I.mult[i] - 1);
        else  // for singly imaged galaxy
            constraints += opi; 

    // Sums up the critical lines
    constraints += 2 * I.npcl;

    if ( I.srcfit != 0 )
        constraints += 2 * I.nsrcfit;

    // arclets
    if ( I.stat > 0 )
        constraints += 2 * narclet;

    //dynamics                     // TV
    if(Dy.dynnumber == 1)
        constraints = constraints+1;
    
    if(Dy.dynnumber == 2)
        constraints = constraints+2;
    
    if(Dy.dynnumber == 3)
        constraints = constraints+1;
    
    if(Dy.dynnumber == 4)
        constraints = constraints+1;
	

    // FITS image reconstruction
    if ( M.iclean == 2 )
    {
        //extern struct g_pixel imFrame;
        //constraints += imFrame.nx * imFrame.ny;
	// mod bclement shapemodel_contour
	// Number of constraints is now the number 
	// of non-zero weight pixels inside the contours
	int n,j;
	for( n = 0; n < imo_npl; n++ )
	{
	    j = imo_pl[n].ii;
	    i = imo_pl[n].jj;
	    if( wo[j][i] > 0 )
		constraints++;
	}

    }

    //CHECK IF M.iclean = 4
    if ( M.iclean == 4 )
    {
        extern struct g_pixel imFrame;
        constraints += imFrame.nx * imFrame.ny;
        
    }
    return(constraints);
}


// Compute the number of free parameters
int getNParameters()
{
    extern  struct  g_grille    G;
    extern  struct  g_pot       P[NPOTFILE];
    extern  struct  g_image     I;
    extern  struct  g_source    S;
    extern struct g_mode  M;
    extern  int     block[][NPAMAX];
    extern  int     cblock[], sblock[NFMAX][NPAMAX];
    extern  struct  sigposStr sigposAs;
    extern struct g_pixel   ps;
    long int parameters, i, j;
    int ipx;

    parameters = 0;

    //check M.iclean =4 case first
    if (M.iclean == 4)
    {   //Initializing sflux
        parameters = ps.nx*ps.ny;
    }
    
    for ( i = 0; i < G.no_lens; i ++ ){
        for ( ipx = CX; ipx <= PMASS; ipx ++ ){
            if ( block[i][ipx] != 0 ){
                parameters++;
            }
        }
        //Benjamin Beauchesne - Bspline modification
        for ( ipx = CX_PERT; ipx < NPAMAX; ipx ++ ){
            if ( block[i][ipx] != 0 ){
                parameters++;
            }
        }
    };
    // multiscale grid clumps
    parameters += G.nlens - G.nmsgrid;

    // source parameters
    for ( i = 0; i < S.ns; i++ )
        for (ipx = SCX; ipx <= SFLUX2; ipx++ )
            if ( sblock[i][ipx] != 0 )
                parameters++;

    // cosmological parameters
    for ( ipx = OMEGAM; ipx <= WA; ipx++ )
        if ( cblock[ipx] ) parameters++;

    // redshift optimization
    for ( i = 0; i < I.nzlim; i++ )
        if( zlim[i].bk > 0 )
            parameters ++;

    // source fitting
    if ( I.srcfit != 0 )
    {
        if ( !strcmp(I.srcfitMethod, "LINFIT") )
            parameters += 2;
    }

    for ( i = 0; i < G.npot; i++ )
    {
        struct g_pot *pot = &P[i];

        if ( pot->ftype && pot->ircut != 0 ) parameters++;
        if ( pot->ftype && pot->isigma != 0 ) parameters++;
        if ( pot->ftype && pot->islope != 0 ) parameters++;
        if ( pot->ftype && pot->ivdslope != 0 ) parameters++;
        if ( pot->ftype && pot->ivdscat != 0 ) parameters++;
        if ( pot->ftype && pot->ircutscat != 0 ) parameters++;
        if ( pot->ftype && pot->ia != 0 ) parameters++;
        if ( pot->ftype && pot->ib != 0 ) parameters++;
    }

    if ( sigposAs.bk != 0 )
    {
        for ( i = 0; i < I.n_mult; i++ )
            for ( j = 0; j < I.mult[i]; j++ )
                parameters++;
    }
    if ( I.dsigell != -1. ) parameters++;
    return(parameters);
}


/* Initialise the np_grad and np_grad2 global variables
 * used after in e_grad() and e_grad2().
 *
 * These variables are used to speed up the computation
 * of the gradients for the many identical potentials
 * involved in the non parametric model.
 *
 */
void prep_non_param()
{
    extern struct g_image  I;
    extern struct g_grille G;
    extern struct pot      lens[];
    extern long int        narclet;
    extern struct galaxie  arclet[];
    extern struct pot      lmax[];
    extern struct g_pixel ps,imFrame;
    extern struct g_mode M;

    struct galaxie *image;
    long int i, j, k, l, n, nimages;
    struct point *pGrad;
    double  *pGrad2a, *pGrad2b, *pGrad2c, *tmp;
    double dls, oldz;
    struct matrix grad2;
    long int gcount, lcount;  // global and local counters
    struct ellipse ampli;

    //author: Soniya Sharma
   if (M.iclean == 4 )
   { 
       fprintf(stderr,"entered cleanset = 4 loop in prep_non_param\n");
       n = ps.nx*ps.ny;
       sflux = (double *) calloc((unsigned) n, sizeof(double));
       double **source;
       ps.header=  imFrame.header;
       ps.format = imFrame.format; //Editing the format since its default value is 0
       // NPRINTF(stderr,"ps->format = %d",ps.format);
       //  source = (double **) readimage(&ps);
       for(i=0;i<ps.nx;i++)  //Initializing sflux
       {
          for(j=0;j<ps.ny;j++)
             sflux[i*ps.ny+j] = 0.;
       }
    }
    else
    {
       // Number of clumps
       n = G.nlens - G.nmsgrid;

       // Create a global array for the lens.b0
       np_b0 = (double *) calloc((unsigned) n, sizeof(double));

       // set all lens[*].b0 so that all lens[*].pmass=1
       for ( i = 0; i < G.nlens - G.nmsgrid; i++ )
       {
          np_b0[i] = lens[i + G.nmsgrid].b0;
          lens[i + G.nmsgrid].b0 = 1.;
//        lens[i + G.nmsgrid].b0 = 0.;
//        for( j = 0; j < G.nlens - G.nmsgrid; j++ )
//            lens[i + G.nmsgrid].b0 += G.invmat[i][j];
       }

       // Initialise np_grad and np_grad2 arrays
       nimages = narclet; 
       for ( i = 0; i < I.n_mult; i++ )
           nimages += I.mult[i];

       pGrad = (struct point *) malloc((unsigned int) n * nimages * sizeof(struct point));
       pGrad2a = (double *) malloc((unsigned int) n * nimages * sizeof(double));
       pGrad2b = (double *) malloc((unsigned int) n * nimages * sizeof(double));
       pGrad2c = (double *) malloc((unsigned int) n * nimages * sizeof(double));
       tmp = (double *) malloc((unsigned int) n * nimages * sizeof(double));

       // Check memory allocation
       if( pGrad == NULL || pGrad2a == NULL || pGrad2b == NULL || pGrad2c == NULL || tmp == NULL )
            RAISE(E_MEMORY, "ERROR in prep_non_param() during memory allocation\n");

       nimages = 0;
       for ( i = 0; i < I.n_mult; i++ )
           for ( j = 0; j < I.mult[i]; j++ )
           {
              image = &multi[i][j];

              image->np_grad = pGrad + nimages * n;
              image->np_grad2a = pGrad2a + nimages * n;
              image->np_grad2b = pGrad2b + nimages * n;
              image->np_grad2c = pGrad2c + nimages * n;
              nimages++;

              oldz = lens[0].z; dls = image->dl0s;
              for ( k = G.nmsgrid; k < G.nlens; k++ )
              {
                if( lens[k].z != oldz )
                {
                    dls  = distcosmo2(lens[k].z, image->z);
                    oldz = lens[k].z;
                }

                // dls/ds multiplication is done in o_dpl.c for e_grad_pot()
                image->np_grad[k-G.nmsgrid] = e_grad_pot(&image->C, k);

                grad2 = e_grad2_pot(&image->C, k); 
                image->np_grad2a[k - G.nmsgrid] = grad2.a * dls;
                image->np_grad2b[k - G.nmsgrid] = grad2.b * dls;
                image->np_grad2c[k - G.nmsgrid] = grad2.c * dls;
              }
           }

       // For SL: Compute amplification due to the fixed potentials
       if( I.n_mult > 0 )
       {
          extern struct g_pot P[NPOTFILE];
          struct matrix *amatinv; // correction factor for the error, which is not exactly in the image plane if there are fixed potentials
          int nimage = 0;
          long int ilens;
          for ( i = 0; i < I.n_mult; i++ )
             nimage += I.mult[i];
    
          amatinv = (struct matrix *)calloc((unsigned int)nimage, sizeof(struct matrix));
    
             // Contribution from the fixed potentials
          for( ilens = G.no_lens; ilens < G.nplens[0]; ilens++ )
          {
            nimage = 0;
            for ( i = 0; i < I.n_mult; i++ )
                for ( j = 0; j < I.mult[i]; j++ )
                {
                    grad2 = e_grad2_pot(&multi[i][j].C, ilens);
                    amatinv[nimage].a += grad2.a * multi[i][j].dr;
                    amatinv[nimage].b += grad2.b * multi[i][j].dr;
                    amatinv[nimage].c += grad2.c * multi[i][j].dr;
                    nimage++;
                }
          } 
        
          // Contribution from the potfile, if it is fixed 
          for( k = 0; k < G.npot; k++ )
            if( P[k].isigma == 0 && P[k].ircut == 0 )
                for( ilens = G.nplens[k]; ilens < G.nplens[k+1]; ilens++ )
                {
                    nimage = 0;
                    for ( i = 0; i < I.n_mult; i++ )
                        for ( j = 0; j < I.mult[i]; j++ )
                        {
                            grad2 = e_grad2_pot(&multi[i][j].C, ilens);
                            amatinv[nimage].a += grad2.a * multi[i][j].dr;
                            amatinv[nimage].b += grad2.b * multi[i][j].dr;
                            amatinv[nimage].c += grad2.c * multi[i][j].dr;
                            nimage++;
                        }
                }
        
        
          // Compute resulting amplification
          nimage = 0;
          for ( i = 0; i < I.n_mult; i++ )
            for ( j = 0; j < I.mult[i]; j++ )
            {
                multi[i][j].mu = sqrt(fabs((1. - amatinv[nimage].a) * (1. - amatinv[nimage].c) - amatinv[nimage].b * amatinv[nimage].b));
                nimage++;
            }
        
          free(amatinv);

       }

       //
       // Process arclets with OPENMP
       //
       gcount = lcount = 0; // count number of processed arclets (big/small loop)

//#pragma omp parallel default(shared) firstprivate(pGrad,pGrad2a,pGrad2b,pGrad2c) private(i,image,k,lcount,grad2)
       {
//#pragma omp for nowait
          for ( i = 0; i < narclet; i++ )
          {
            lcount++;
            if ( lcount == 200 )
            {
//#pragma omp critical
                {
                    gcount = gcount + lcount;
                    printf("INFO: prepare lens profiles for arclet %ld/%ld\r", gcount, narclet);
                }

                lcount = 0;
            }

            image = &arclet[i];

            image->np_grad = pGrad + nimages * n;
            image->np_grad2a = pGrad2a + nimages * n;
            image->np_grad2b = pGrad2b + nimages * n;
            image->np_grad2c = pGrad2c + nimages * n;
            nimages++;

            oldz = lens[0].z; dls = image->dl0s;
            for ( k = G.nmsgrid; k < G.nlens; k++ )
            {
                if( lens[k].z != oldz )
                {
                    dls  = distcosmo2(lens[k].z, image->z);
                    oldz = lens[k].z;
                }

                image->np_grad[k-G.nmsgrid] = e_grad_pot(&image->C, k);
                image->np_grad[k-G.nmsgrid].x *= image->dr;
                image->np_grad[k-G.nmsgrid].y *= image->dr;
                grad2 = e_grad2_pot(&image->C, k);
                image->np_grad2a[k-G.nmsgrid] = grad2.a * dls;
                image->np_grad2b[k-G.nmsgrid] = grad2.b * dls;
                image->np_grad2c[k-G.nmsgrid] = grad2.c * dls;
            }
          }
       }
       if( narclet > 0 )
          printf("INFO: prepare lens profiles for arclet %ld/%ld\n", narclet, narclet);

    // restore lens[i].b0 values
       for ( i = G.nmsgrid; i < G.nlens; i++ )
          lens[i].b0 = np_b0[i-G.nmsgrid];

    // clean allocated data not used afterwards
       free(tmp);
     }
}

/* Swap 2 columns of a square matrix from i1 to i2, starting from 0 index.
 * args:
 *  - weight: the 2D array
 *  - n: the dimension of the array
 *  - i1, i2: the indexes to swap
 *  - axis : 0 or 1. The axis along which to perform the swap
 */ 
static void swap(double **weight, int n, int i1, int i2, int axis)
{
    if( axis==1 )
    {
        double *tmp;
        tmp = weight[i2];
        weight[i2] = weight[i1];
        weight[i1] = tmp;
    }
    else if( axis == 0 )
    {
        double tmp;
        int i;
        for(i=0; i<n; i++)
        {
            tmp = weight[i][i2];
            weight[i][i2] = weight[i][i1];
            weight[i][i1] = tmp;
        }
    }
}

// Read a FITS file containing the weight matrix
// Checks that the name of the images match the names declared in 
// the image file, and reorder the matrix rows and columns if needed
// Return the determinant of the Covariance matrix, for Likelihood normalization
static void readWeightMat(double *** weight_ext, double *detCov)
{
    extern struct g_image I;
    int i, j, nx, ny, nullity;
    char *header, *id, *pch;
    double *p, result, **tmp, **weight;

    weight = rdf_fits("weight.fits", &nx, &ny, &header);
    if( nx != ny )
        RAISE(E_RUNTIME, "ERROR: weight.fits matrix is not square\n");

    // Grab image IDs from the header and reorder weight array
    int niw = 0;
    pch = header;
    while( pch[0] != 0 && niw < NIMAX)
    {
        if( !strncmp(pch, "IMG", 3) )
        {
            // format id into a \0 terminated string
            id = pch + 11; 
            i=0; while(i < IDSIZE && id[i] != ' ' && id[i] != '\'') i++;
            id[i] = 0;
            
            // Search id in list of multiple images
            int nim = 0;
            for( i = 0; i < I.n_mult; i++ )
                for( j = 0; j < I.mult[i]; j++ )
                {
                    if( !strcmp(id, multi[i][j].n) && niw!=nim)
                    {
                        swap(weight, nx, 2*niw, 2*nim, 0); // dx
                        swap(weight, nx, 2*niw, 2*nim, 1);
                        swap(weight, nx, 2*niw+1, 2*nim+1, 0); // dy
                        swap(weight, nx, 2*niw+1, 2*nim+1, 1);
                    }
                    nim++;
                }
            niw++;
        }
        pch += 80;
    }

    // Compute the weight determinant
    p = (double *) calloc(nx, sizeof(double));
    tmp = (double **) malloc(nx * ny * sizeof(double));
    memcpy( tmp, weight, nx * ny * sizeof(double));

    cholesky(tmp, nx, p);

    result = 1.;
    for( i = 0; i < nx; i++ )
        result *= p[i] * p[i];

    *detCov = 1. / result; 
    *weight_ext = weight;

    free(tmp);
    free(p);
    free(header);
}
