#include<fonction.h>
#include<structure.h>

/****************************************************************/
/*      nom:        e_dpl               */
/*      auteur:     Jean-Paul Kneib         */
/*      date:       10/02/92            */
/*      place:      Toulouse            */
/****************************************************************/

/* Return the position of the source from the position of an image
 *
 * Global variables used :
 * - in e_grad() : G, lens, lens_table
 */
void e_dpl(const struct point *gi, double dlsds, double zs, struct point *gs)
{
    struct point Grad;

    Grad = e_grad(gi, dlsds, zs);
    gs->x = gi->x - dlsds * Grad.x;
    gs->y = gi->y - dlsds * Grad.y;
}

/* Return the position of the source from the position of an image
 * This function cannot be used during optimization because 
 * it calls with e_grad_gal() with NULL second argument.
 *
 * Global variables used :
 * - in e_grad_gal() : G, lens, lens_table
 */
void e_dpl_gal(struct galaxie *image, struct point *gs)
{
    struct point Grad;

    Grad = e_grad_gal(image, NULL);
    gs->x = image->C.x - image->dr * Grad.x;
    gs->y = image->C.y - image->dr * Grad.y;
}
