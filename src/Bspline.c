#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<math.h>
#include<fonction.h>
#include<constant.h>
#include<dimension.h>
#include<structure.h>
#include<lt.h>
#include<errors.h>
#include <errno.h>
#include <gsl/gsl_complex.h>
#include <gsl/gsl_complex_math.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_monte.h>
#include <gsl/gsl_monte_plain.h>
#include <gsl/gsl_monte_miser.h>
#include <gsl/gsl_integration.h>
#include <gsl/gsl_sf_trig.h>
#include <complex.h>


#define max(a,b) (a>=b?a:b)


/* Benjamin Beauchesne 18/11/19
*
* Some function use to compute and represent Bspline surface:
*
* --> Compute knot vector from control points
* --> compute value of a surface represented by control points
* --> It's derivative also
*
*/

// More accurate sum
static double NeumaierSum(double *input,int N){
    int i; 
    double sum = 0.0;
    double c = 0.0 ;
    double t=0.0;                      // A running compensation for lost low-order bits.
    for (i =N;i>=0;i--){ 
        t = sum + input[i];
        if (fabs(sum) >= fabs(input[i])){
            c += (sum - t) + input[i]; // If sum is bigger, low-order digits of input[i] are lost.
        }
        else{
            c += (input[i] - t) + sum; // Else low-order digits of sum are lost.
        };
        sum = t;
    }
    return sum + c;
}

static double arithmetic_mean(int n, double U[n]){

    // Initializing variables

    double sum=0.0;

    int k;

    // Core of the function
    
    
    
    for (k=0;k<n;k++){
        
        sum=sum+U[k];

    };
    
    sum=sum/n;

    return sum;
}

/*
* Flatten array V in array W
*
*
*/



static int flatten_array(int n, int m, double **V, double W[n*m]){

    // Initializing variables

    int k;
    int l;

    // Core of the function

    for (k=0;k<n;k++){

        for (l=0;l<m;l++){

            W[k*n+l]=V[k][l]; 
        };

    };

    return 0;

}



/* Give the minimum values of an array U
* of size n
*/



static double min_array(int n, double U[n]){

    // Initializing variables

    double min=U[0];

    int k;

    // Core of the function


    for (k=0;k<=n;k++){

        if (min>=U[k]) min=U[k];

    };

    return min;

}




/* Give the maximum values of an array U
* of size n
*/



static double max_array(int n, double U[n]){

    // Initializing variables

    double max=U[0];

    int k;

    // Core of the function


    for (k=0;k<=n;k++){

        if (max<=U[k]) max=U[k];

    };

    return max;

}

/* From coordinnate x of a set of control points P,
* return the associated knot vector
*
*
* n: number of controle point on the x-axis
* m: number of controle point on the y-axis
* p: degree of Bspline surface in x-direction
* P: set of coordinate x of controle point polygon
* U: knot vector --> output
*/


void knotx_from_ctrlpts(struct Bspline2D *Bspline, double **P){

    // Initializing variables
    

    // initialize from structure

    int n=Bspline->nx;
    int m=Bspline->ny;
    int p=Bspline->degx;

    double P_flat[n*m]; // use to compute it's minimum and maximum
    
    double Mean_line[n]; // use to average lines along all columns
    double line[m];
    double temp[p];

    int l,r,k;
    double min_P,max_P;

    // Core of the function
    if (Bspline->kx==NULL)   Bspline->kx=alloc_vector_double(n+Bspline->degx+1);
    // averaging along each columns
    
    for (l=0;l<n;l++){
        for (r=0;r<m;r++){

            line[r]=P[l][r];

        };

        Mean_line[l]=arithmetic_mean(m,line);


    };
    
    flatten_array(n,m,P,P_flat);
    max_P=max_array(n*m,P_flat);
    min_P=min_array(n*m,P_flat); 
    for (k=0;k<=p;k++){

        Bspline->kx[k]=min_P;
        Bspline->kx[n+p-k]=max_P;
    };

    for (k=1;k<=n-p-1;k++){

        for (l=0;l<p;l++){

            temp[l]=Mean_line[k+l];
        };
 
        Bspline->kx[k+p]=arithmetic_mean(p,temp);
 
    }; 
     
   
    

}



/* From coordinnate y of a set of control points P,
* return the associated knot vector
*
*
* n: number of controle point on the x-axis
* m: number of controle point on the y-axis
* p: degree of Bspline surface in y-direction
* P: set of coordinate y of controle point polygon
* U: knot vector 
*/


void knoty_from_ctrlpts(struct Bspline2D *Bspline, double **P){
    
    // Initializing variables

    // initialize from structure

    int n=Bspline->ny;
    int m=Bspline->nx;
    int p=Bspline->degy;



    double P_flat[n*m]; // use to compute it's minimum and maximum
    
    double Mean_line[m]; // use to average lines along all columns
    double line[n];
    double temp[p];

    int l,r,k;
    double min_P,max_P;

    // Core of the function
    if (Bspline->ky==NULL) Bspline->ky=alloc_vector_double(m+Bspline->degy+1);
    // averaging along each columns

    for (l=0;l<m;l++){

        for (r=0;r<n;r++){

            line[r]=P[r][l];

        };

        Mean_line[l]=arithmetic_mean(n,line);

    };

    flatten_array(n,m,P,P_flat);
    max_P=max_array(n*m,P_flat);
    min_P=min_array(n*m,P_flat);

    for (k=0;k<=p+1;k++){

        Bspline->ky[k]=min_P;
        Bspline->ky[n+p-k]=max_P;

    };
 
    for (k=1;k<=n-p-1;k++){

        for (l=0;l<p;l++){

            temp[l]=Mean_line[k+l];

        };
        Bspline->ky[k+p]=arithmetic_mean(p,temp);

    };    
    

}





/* For a knot vector U, find the index of the knot which
* is just before the value u. U is sorted so it find it 
* by comparing the value of the element U until it find 
* the first one which is higher.
* n : number of control points in the U-direction
* p : degree of Bsplines
* u : value in the U-direction
* U : Knot vector in the U-direction
*/
static int index_knot_linear(int n, int p, double u, double U[n+p+1] ){
    //int n, int p, double u, double U[n+p+1] 
    // Initializing variables

    int indice=p+1;

    // Core of the function


    while ((indice<n)&&(U[indice]<=u)){

        indice=indice+1;

    };

    indice=indice-1; // At the end of the loop U[indice]>u=>U[indice-1]


    return indice;


}

/* Initialize knot vector from a uniform x-coordinate of controle points
*
* size_sq : size of unitary lattice
* n : n*n number of variable control points
* degx : x degree
* degy : y degree
*/


void knotx_init(struct Bspline2D *Bspline){

    

    int k,l;
    int nx,ny; 
    double size_sq=Bspline->size_sq;
    double x_c=Bspline->x_c;
    double **P;
         
    nx=Bspline->nx;
    ny=Bspline->ny;
    P=alloc_square_double(nx,ny);
 
    
    for (k=0;k<nx;k++){
        for (l=0;l<ny;l++){
            P[k][l]=size_sq*(double)k - size_sq*(double)(nx-1)/2.0+x_c;
        };
    };

    knotx_from_ctrlpts(Bspline,P);
    free_square_double(P,nx);
    

}


/* Initialize knot vector from a uniform y-coordinate of controle points
*
* size_sq : size of unitary lattice
* n : n*n number of variable control points
* degx : x degree
* degy : y degree
*/

void knoty_init(struct Bspline2D *Bspline){


            

    int k,l;
    int nx,ny;
    double size_sq=Bspline->size_sq;
    double y_c=Bspline->y_c;
    double **P;
   
 

    
    nx=Bspline->nx;
    ny=Bspline->ny;
    P=alloc_square_double(nx,ny);

    for (k=0;k<nx;k++){
        for (l=0;l<ny;l++){
            P[k][l]=size_sq*(double)l - size_sq*(double)(ny-1)/2.0+y_c;
        };
    };


    knoty_from_ctrlpts(Bspline,P);
    free_square_double(P,nx); 
 
}


/* Initialize array of controle points to 0.0
*
* n : n*n number of variable control points
* degx : x degree
* degy : y degree
*/

double **init_ctrl_pts(struct Bspline2D *Bspline){
    //int n, int degx, int degy
    double **P;

    int nx,ny;
    int k,l;
    nx=Bspline->nx;
    ny=Bspline->ny;

    P=alloc_square_double(nx,ny);

    
    for (k=0;k<nx;k++){
        for (l=0;l<ny;l++){
            P[k][l]=0.0;//10.0*(float)k+10.0*(float)l;
        };
    };
    //printf("%d\n",nx);
    return P;
}

/* Initialize array of controle points to val
*
* n : n*n number of variable control points
* degx : x degree
* degy : y degree
*/

double **init_ctrl_pts_prec(struct Bspline2D *Bspline,double val){
    //int n, int degx, int degy
    double **P;

    int n;
    int k,l;
    n=Bspline->n;

    P=alloc_square_double(n,n);

    
    for (k=0;k<n;k++){
        for (l=0;l<n;l++){
            P[k][l]=val;//10.0*(float)k+10.0*(float)l;
        };
    };
    //printf("%d\n",nx);
    return P;
}

/* Initialize array of controle points for lmax
* and lmin. Error distribution will be centered
* on the initial value of the coefficient instead
* of zero.
* n : n*n number of variable control points
* degx : x degree
* degy : y degree
*/

double **init_ctrl_pts_lim(struct Bspline2D *Bspline,struct Bspline2D *Bspline_coeff,int *block,double val,int a,double c,int d){
    //int n, int degx, int degy
    double **P;
    //extern int block[NLMAX][NPAMAX];
    extern struct g_mode    M;
    int n;
    int k,l;
    n=Bspline->n;

    P=alloc_square_double(n,n);
    if (a==0){ // do the thing for lmin
        for (k=0;k<n;k++){
            for (l=0;l<n;l++){
                if (block[CY_PERT+1+l*Bspline->n+k]==3){
                    P[k][l]=Bspline_coeff->Pz[k+Bspline->degx+1][l+Bspline->degy+1]+val;//10.0*(float)k+10.0*(float)l;
                }
                if (block[CY_PERT+1+l*Bspline->n+k]==1){
                    P[k][l]=Bspline_coeff->Pz[k+Bspline->degx+1][l+Bspline->degy+1]-val;//10.0*(float)k+10.0*(float)l;
                }
                //if ((k==0) &&(l==0)) printf("%f %f %f %d\n",P[k][l],val,Bspline_coeff->Pz[k+Bspline->degx+1][l+Bspline->degy+1],block[CY_PERT+l*Bspline->n+k]);
            };
        };
    }

    if (a==1){ // do the thing for lmax
        for (k=0;k<n;k++){
            for (l=0;l<n;l++){
                if (block[CY_PERT+1+l*Bspline->n+k]==3){
                    P[k][l]=val;//10.0*(float)k+10.0*(float)l;
                }
                if (block[CY_PERT+1+l*Bspline->n+k]==1){
                    P[k][l]=Bspline_coeff->Pz[k+Bspline->degx+1][l+Bspline->degy+1]+val;//10.0*(float)k+10.0*(float)l;
                }
                //if ((k==0) &&(l==0)) printf("%f %f %f %d\n",P[k][l],val,Bspline_coeff->Pz[k+Bspline->degx+1][l+Bspline->degy+1],block[CY_PERT+l*Bspline->n+k]);
            };
        };
    }
    
    //printf("%d\n",nx);
    return P;
}




/* Update non zeros controles points from the list
* of coefficient coeff
*
* Bspline : structure containing all needed variables for Bspline surface
* coeff[m] : list of coefficient to update (m=Bspline.n^2)
*/

int update_ctrl_pts(struct Bspline2D *Bspline,int n,double *coeff){


    int k,l;
    int nx,ny;

    nx=(Bspline->n+2*(Bspline->degx+1));
    ny=(Bspline->n+2*(Bspline->degy+1));

    for (k=Bspline->degx+1;k<(nx-Bspline->degx-1);k++){
        for (l=Bspline->degy+1;l<(ny-Bspline->degx-1);l++){
            printf("%f\n",coeff[k*Bspline->n+l]);
            Bspline->Pz[k][l]=coeff[k*Bspline->n+l];
        };
    };
    for (k=0;k<nx;k++){
        for (l=0;l<ny;l++){
            printf("%f\n",Bspline->Pz[k][l]);
        };
    };
    return 0;

}



/* Get coefficient values from a text files
* filename -> name of the text file
* Bspline -> structure containing the Bpsline surface parameters
* i -> i th line containing the Bspline Coefficient
* Output: Bspline.Pz
*/
int get_coeff_from_file(char *filename, struct Bspline2D *Bspline, int i){

    FILE *file;

    int l,k,n,j,m;
    int p,pint;
    int nx,ny;
    char *line=NULL;
    char *word=NULL;
    extern struct g_mode    M;
    size_t line_buf_size = 0;

    nx=(Bspline->n+2*(Bspline->degx+1));
    ny=(Bspline->n+2*(Bspline->degy+1));


    n=Bspline->n;
    file=fopen(filename,"r");
    if (file==NULL)
        RAISE(E_PARFILE, "ERROR: Empty Bspline Coefficient File\n");
 
    if ((getline(&line,&line_buf_size, file)) == -1)
        RAISE(E_PARFILE, "ERROR: Not Enough Line in your Bspline Coefficient File\n");

    /* 
    for (j=0;j<=i;j++){
        if ((getline(&line,&line_buf_size, file)) == -1) {
            NPRINTF(stderr,"ERROR: Not Enough Line in your Bspline Coefficient File\n");
            exit(-1);
        }
        //printf("caribou %s\n",line);
    }
    */
    i=j=0;
    p=0;
    //word="0.0"
    for (k=Bspline->degx+1;k<(nx-Bspline->degx-1);k++){
        for (l=Bspline->degy+1;l<(ny-Bspline->degx-1);l++){
            if ((l==Bspline->degy+1) && (k==Bspline->degx+1)){
                word=strtok(line," ");
            }
            else {
                word=strtok(NULL," ");
            }
            //printf("%s\n",word);
            p++;
            if (word==NULL) {
                RAISE(E_PARFILE, "ERROR: Not Enough Bspline coefficient\n");
                p--;
            }
            sscanf(word,"%lf",&Bspline->Pz[l][k]);
        };
    };
    
    if ((p<Bspline->n*Bspline->n) || (p>Bspline->n*Bspline->n))
        RAISE(E_PARFILE, "ERROR: Wrong number of coefficient for Bspline Perturbation of potential %d\n \
                          There are %d coefficient instead of %d\n",i,p,Bspline->n*Bspline->n);
    
    //free(word); 
    free(line);
    fclose(file);

    return 0;


}




/* Implementation of de Boor algorithm to compute value of a Bspline curve
*
* k: index where lies u
* u: float where you want to evaluate the Bspline curves
* U: knot vector
* P: array of controle point
* p: degree of the curve
* n: number of controle point
*/



static void deboor_algorithms(int p, int k, double u, double *U, double *P,double *s){

    // Initializing variables


    int j,r;
    double alpha;
    double d[p+1]; // will contains the value we want to evaluate


    for (j=0;j<=p;j++){
        d[j]=P[j+k-p];
        //printf("%f \n",d[j]);
    };
    
    // Core of the function
    for (r=1;r<=p;r++){
        for (j=p;j>r-1;j--){
            //printf("boucle \n");
            alpha=(u-U[j+k-p])/(U[j+1+k-r]-U[j+k-p]);
            d[j]=(1.0-alpha)*d[j-1]+alpha*d[j];
        };
    };
    *s=d[p];

}

/* Apply two times the de Boor algorithm to compute a point of surface of Bspline
*
* n : number of control points in u-direction
* p : degree of Bspline in u-direction
* U : knot_vector in U-direction
* m : number of control points in v-direction
* q : degree of Bspline in v-direction
* V : knot vector in v-direction
* ctrl_pts : array of control points
* u : float where you want to evaluate the surface
* v ; same
*
*
*
*/

// https://pages.mtu.edu/~shene/COURSES/cs3621/NOTES/surface/bspline-de-boor.html

double Bspline_surface_point(const struct Bspline2D *Bspline, double u,double v){

    //int n, int p, double U[n+p+1],int m,int q, double V[m+q+1],double **ctrl_pts, double u,double v
    // Initializing variables

    int n,p,m,q;

    n=Bspline->nx;
    m=Bspline->ny;
    p=Bspline->degx;
    q=Bspline->degy;

    int vspan, uspan;
    int k,l,l_p,k_p;


    double S;  // will contains the value of the surface
    double Pu[p+1]; // will contains a intermediate set of control points along u-direction
    double Pv[q+1]; // will contains a intermediate set of control points along v-direction
    double kx_red[2*p+1];
    double ky_red[2*q+1];
    //printf("Bspline_surface_point P[3][3]=%e  Ux[10]=%e Uy[10]=%e %d  %d\n",Bspline->Pz[3][3],Bspline->kx[28],Bspline->ky[28],n,m); 
    //printf("Here 1");
    // Core of the function
    if ((u<Bspline->kx[p])||(u>Bspline->kx[n]) || (v<Bspline->ky[q])||(v>Bspline->ky[m])){
	    //printf("Here %f<%f<%f  %f<%f<%f\n",Bspline->kx[p],u,Bspline->kx[n],Bspline->kx[q],v,Bspline->kx[m]);
	    S=0.0;
    }

    else {
        // Find where u and v lies in knot vector
        uspan=index_knot_linear(n,p,u,Bspline->kx);
        vspan=index_knot_linear(m,q,v,Bspline->ky);

        
        // Apply de Boor algorithm
        k_p=0;
        for (k=uspan-p;k<=uspan;k++){
            l_p=0;
	        for (l=vspan-q;l<=vspan;l++)
            {
                Pv[l_p]=Bspline->Pz[k][l];
                l_p++;
            }
            for (l=0;l<2*q+1;l++){
                ky_red[l]=Bspline->ky[l+vspan-q];
            }
            deboor_algorithms(q,q,v,ky_red,Pv,&Pu[k_p]);
            k_p++;

        };
        for (l=0;l<2*p+1;l++){
            kx_red[l]=Bspline->kx[l+uspan-p];
        }
        deboor_algorithms(p,p,u,kx_red,Pu,&S);
    };
   
    return S;

}

/* Define the new set of control points Q which represent the derivative of 
* the Bspline surface. see ref below.
*
* P : array of control points
* U : knot vector in the u-direction
* V : knot vector in the v-direction
* n : number of control points in the u-direction
* m : number of control points in the v-direction
* axes : if axes==0 derivative along the u-direction et axes==1 for the v-direction
* p : degree of Bspline in the u-direction
* q : degree of Bspline in the v-direction
* Q : array of new control points (Output)
*/


// https://pages.mtu.edu/~shene/COURSES/cs3621/NOTES/spline/B-spline/bspline-derv.html

double **controle_point_derive(int n, int p, int m, int q,double **P,double *U,double *V,int axes){

    // int n, int p, int m, int q,double **P,double *U,double *V,int axes
    // Initializing variables

    int k,l;
    double **Q;



    // Core of the function

    //printf("%f \n",U[0]);
    //printf("%f \n",U[1]);
    // derivative along u-direction
    
    if (axes==0) {
        Q=alloc_square_double(n-1,m);
        for (k=0;k<(n-1);k++){
            for (l=0;l<m;l++){
                if ((k<=(p-1)) || (k>=(n-p-1))){
                    Q[k][l]=0.0; // because Pz==0.0
                }
                
                else if(k==(n-p-2)){
                    Q[k][l]=-p/(U[k+p+1]-U[k+1])*P[k][l];
                }
                else if(k==p){
                    Q[k][l]=p/(U[k+p+1]-U[k+1])*(P[k+1][l]);
                }
                
                else { 
                    Q[k][l]=p/(U[k+p+1]-U[k+1])*(P[k+1][l]-P[k][l]);
                };
            };
        };

    };

    // derivative along v-direction
    if (axes==1) {
        Q=alloc_square_double(n,m-1);
        for (k=0;k<n;k++){
            for (l=0;l<(m-1);l++){
                //printf("(V[k+q+1]-V[k+1])=%f \n",(V[l+q+1]-V[l+1]));
                
                if ((l<=(q-1)) || (l>=(m-q-1))){
                    Q[k][l]=0.0; // because Pz==0.0
                }
                else if (l==(m-q-2)) {
                    Q[k][l]=-q/(V[l+q+1]-V[l+1])*(P[k][l]);
                }
                else if (l==q)   {
                    Q[k][l]=q/(V[l+q+1]-V[l+1])*(P[k][l+1]);
                }
                else { 
                    Q[k][l]=q/(V[l+q+1]-V[l+1])*(P[k][l+1]-P[k][l]);
                };

            };
        };


    };
 
    return Q;
}


/* allocate space for table work1 and copy the value contained
* in Tocopy in it.
*
* work1 : table to be assigned
* n: number of lines in Tocopy
* m: number of columns in Tocopy
* Tocopy : table to be copied
*/



static double **alloc_and_copy_table(int n, int m, double **Tocopy){

    // Initializing variables

    int k,l;
    double **work1;


    // Core of the function

    work1=alloc_square_double(n,m);
    
    for (k=0;k<n;k++){
        for (l=0;l<m;l++) work1[k][l]=Tocopy[k][l];
    };
    
    return work1;
}


/* Same as alloc_and_copy_table but for a vector
*/


static double *alloc_and_copy_vector(int n, double *Tocopy){

    // Initializing variables

    int l;
    double *work1;


    // Core of the function

    work1=alloc_vector_double(n);

    //work1[0]=0.0; 
    for (l=0;l<n;l++) work1[l]=Tocopy[l];
    
    return work1;
}



/* Compute the derivative nx times in the u-direction and ny times in the v-direction for 
* the surface of Bspline at the position (u,v) using de Boor algorithm
*
* n : number of control points in u-direction
* p : degree of Bspline in u-direction
* U : knot_vector in U-direction
* m : number of control points in v-direction
* q : degree of Bspline in v-direction
* V : knot vector in v-direction
* ctrl_pts : array of control points
* u : float where you want to evaluate the surface
* v : same
* nx : number of derivative in the u-direction
* ny : number of derivative in the v-direction
*/



double Bspline_surface_point_der(struct Bspline2D *Bspline, double u,double v,int nx,int ny){


    // int n, int p, double U[n+p+1],int m,int q, double V[m+q+1],double **ctrl_pts, double u,double v,int nx,int ny


    // Initializing variables
    int n,p,m,q;
    n=Bspline->nx;
    m=Bspline->ny;
    p=Bspline->degx;
    q=Bspline->degy;

    double **work1=NULL;
    double **work2=NULL;
    double *knot_cut1=NULL;
    double *knot_cut2=NULL;
    double *U_cut=NULL;
    double *V_cut=NULL;
    int k,l;
    double Pu[n-nx];
    double Pv[m-ny];
    int uspan,vspan;
    double S=0.0;
    // Core of the function
     
    if ((u<Bspline->kx[p])||(u>Bspline->kx[n]) || (v<Bspline->ky[q])||(v>Bspline->ky[m])) S=0.0;

    else {
        // alloc_and_copy_table(work1,n,m,ctrl_pts);
        work1=alloc_square_double(n,m);
        
        for (k=0;k<n;k++){
            for (l=0;l<m;l++) work1[k][l]=Bspline->Pz[k][l];
        };
        
            
        knot_cut2=alloc_and_copy_vector(n+p+1,Bspline->kx);
        
        // compute the new control point of the derivative

        for (k=0;k<nx;k++){

            // allocate space

            knot_cut1=alloc_vector_double(n+p+1-2*(k+1));


            // find the new control points

            work2=controle_point_derive(n-k,p-k,m,q,work1,knot_cut2,Bspline->ky,0);

            // De-allocate and exchange work1 and work2. The same for the knot vector
            
            free_square_double(work1,n-k-1);
            
            work1=alloc_and_copy_table(n-k-1,m,work2);
            
            free_square_double(work2,n-k-1);
            
            for (l=0;l<n+p+1-2*(k+1);l++) knot_cut1[l]=knot_cut2[l+1];
            
            free(knot_cut2);
            knot_cut2=alloc_vector_double(n+p+1-2*k);
            for (l=0;l<n+p+1-2*(k+1);l++) knot_cut2[l]=knot_cut1[l];
            free(knot_cut1);        

        };
        
        U_cut=alloc_and_copy_vector(n+p+1-2*nx,knot_cut2);
        free(knot_cut2);
        knot_cut2=alloc_and_copy_vector(m+q+1,Bspline->ky);
           
        for (k=0;k<ny;k++){

            // allocate space 
            knot_cut1=alloc_vector_double(m+q+1-2*(k+1));

            // find the new control points
            work2=controle_point_derive(n-nx,p-nx,m-k,q-k,work1,U_cut,knot_cut2,1);

            // De-allocate and exchange work1 and work2. The same for the knot vector
           
            
            free_square_double(work1,n-nx);
            work1=alloc_and_copy_table(n-nx,m-k-1,work2);
            free_square_double(work2,n-nx);
            

            for (l=0;l<m+q+1-2*(k+1);l++) knot_cut1[l]=knot_cut2[l+1];
            free(knot_cut2);

            knot_cut2=alloc_vector_double(m+q+1-2*(k+1));
            for (l=0;l<m+q+1-2*(k+1);l++) knot_cut2[l]=knot_cut1[l];
            free(knot_cut1);
            
        };
        
        V_cut=alloc_and_copy_vector(m+q+1-2*ny,knot_cut2);
            
        
        // Find where u and v lies in knot vector
        uspan=index_knot_linear(n-nx,p-nx,u,U_cut);
        vspan=index_knot_linear(m-ny,q-ny,v,V_cut);
        
        
        // Apply de Boor algorithm
        for (k=0;k<(n-nx);k++){
            for (l=0;l<(m-ny);l++){ 
                Pv[l]=work1[k][l];
                //printf("work1[%d][%d]=%f n-nx=%d m-my=%d\n",k,l,work1[k][l],n-nx,m-ny);
            };
            deboor_algorithms(q-ny,vspan,v,V_cut,Pv,&Pu[k]);
            //printf("Pu[k]=%f Pv[k]=%f \n",Pu[k],Pv[k]);

        };
        
        deboor_algorithms(p-nx,uspan,u,U_cut,Pu,&S);
        free_square_double(work1,n-nx);
        free(knot_cut2);
        free(U_cut);
        free(V_cut);
        
    
    };
    
    return S;
}


/* Derivate a Bspline surface by defining it a Bspline2D_red structures
* It computes the derivative in both axis
* Bspline -> contains the initial Bspline information
* Bsplinedev -> contains the information about derivatives of Bspline
* nx and ny: number of derivation along each axis
*/

void derivate_control_points(struct Bspline2D *Bspline,struct Bspline2D_red *Bsplinedev ,int nx, int ny){

    int n,p,m,q;
    n=Bspline->nx;
    m=Bspline->ny;
    p=Bspline->degx;
    q=Bspline->degy;
    double **work1=NULL;
    double **work2=NULL;
    double *knot_cut1=NULL;
    double *knot_cut2=NULL;
    double *U_cut=NULL;
    int k,l;



    work1=alloc_square_double(n,m);
        
    for (k=0;k<n;k++){
        for (l=0;l<m;l++) work1[k][l]=Bspline->Pz[k][l];
    };
        
            
    knot_cut2=alloc_and_copy_vector(n+p+1,Bspline->kx);
        
    // compute the new control point of the derivative

    for (k=0;k<nx;k++){

        // allocate space

        knot_cut1=alloc_vector_double(n+p+1-2*(k+1));


        // find the new control points

        work2=controle_point_derive(n-k,p-k,m,q,work1,knot_cut2,Bspline->ky,0);

        // De-allocate and exchange work1 and work2. The same for the knot vector
            
        free_square_double(work1,n-k-1);
            
        work1=alloc_and_copy_table(n-k-1,m,work2);
            
        free_square_double(work2,n-k-1);
            
        for (l=0;l<n+p+1-2*(k+1);l++) knot_cut1[l]=knot_cut2[l+1];
            
        free(knot_cut2);
        knot_cut2=alloc_vector_double(n+p+1-2*k);
        for (l=0;l<n+p+1-2*(k+1);l++) knot_cut2[l]=knot_cut1[l];
        free(knot_cut1);        

    };
        
    U_cut=alloc_and_copy_vector(n+p+1-2*nx,knot_cut2);
    free(knot_cut2);
    knot_cut2=alloc_and_copy_vector(m+q+1,Bspline->ky);
           
    for (k=0;k<ny;k++){

        // allocate space 
        knot_cut1=alloc_vector_double(m+q+1-2*(k+1));

        // find the new control points
        work2=controle_point_derive(n-nx,p-nx,m-k,q-k,work1,U_cut,knot_cut2,1);

        // De-allocate and exchange work1 and work2. The same for the knot vector
           
            
        free_square_double(work1,n-nx);
        work1=alloc_and_copy_table(n-nx,m-k-1,work2);
        free_square_double(work2,n-nx);
            

       for (l=0;l<m+q+1-2*(k+1);l++) knot_cut1[l]=knot_cut2[l+1];
       free(knot_cut2);

       knot_cut2=alloc_vector_double(m+q+1-2*(k+1));
       for (l=0;l<m+q+1-2*(k+1);l++) knot_cut2[l]=knot_cut1[l];
       free(knot_cut1);
            
    };
    Bsplinedev->kx=U_cut;
    Bsplinedev->ky=knot_cut2;
    Bsplinedev->Pz=work1;
    Bsplinedev->degx=p-nx;
    Bsplinedev->degy=q-ny;
    Bsplinedev->nx=n-nx;
    Bsplinedev->ny=m-ny; 
    
}


// free a  Bspline2D_red structure

static void free_bspline_red(struct Bspline2D_red *Bspline){

    if (Bspline->Pz!=NULL){
        free_square_double(Bspline->Pz,Bspline->nx);      
    }
    if (Bspline->kx!=NULL){
        free(Bspline->kx);
    }
    if (Bspline->ky!=NULL){
        free(Bspline->ky);
    }

}


// Initiate all the derivative needed for convergence and shear fields.

void init_derivate_Bspline(struct Bspline2D *Bspline){

     free_bspline_red(&Bspline->der10);
     free_bspline_red(&Bspline->der01);
     free_bspline_red(&Bspline->der11);
     free_bspline_red(&Bspline->der20);
     free_bspline_red(&Bspline->der02);
     derivate_control_points(Bspline,&Bspline->der10,1,0);
     derivate_control_points(Bspline,&Bspline->der01,0,1);
     derivate_control_points(Bspline,&Bspline->der20,2,0);
     derivate_control_points(Bspline,&Bspline->der11,1,1);
     derivate_control_points(Bspline,&Bspline->der02,0,2);


    
}


/* update set of control points Q which represent the derivative of 
* the Bspline surface. see ref below.
*
* P : array of control points
* U : knot vector in the u-direction
* V : knot vector in the v-direction
* n : number of control points in the u-direction
* m : number of control points in the v-direction
* axes : if axes==0 derivative along the u-direction et axes==1 for the v-direction
* p : degree of Bspline in the u-direction
* q : degree of Bspline in the v-direction
* Q : array of new control points (Output)
*/


// https://pages.mtu.edu/~shene/COURSES/cs3621/NOTES/spline/B-spline/bspline-derv.html

static void update_controle_point_derive(int n, int p, int m, int q,double **P,double **Q,double *U,double *V,int axes){

    // int n, int p, int m, int q,double **P,double *U,double *V,int axes
    // Initializing variables

    int k,l;


    // Core of the function

    //printf("%f \n",U[0]);
    //printf("%f \n",U[1]);
    // derivative along u-direction
    
    if (axes==0) {
        for (k=p;k<(n-p-1);k++){
            for (l=0;l<m;l++){
                Q[k][l]=p/(U[k+p+1]-U[k+1])*(P[k+1][l]-P[k][l]);
                
            };
        };

    };

    // derivative along v-direction
    if (axes==1) {
        for (k=0;k<n;k++){
            for (l=q;l<(m-q-1);l++){
                //printf("(V[k+q+1]-V[k+1])=%f \n",(V[l+q+1]-V[l+1]));
                Q[k][l]=q/(V[l+q+1]-V[l+1])*(P[k][l+1]-P[k][l]);
                

            };
        };


    };
}


// modify the knot vector of the derivative of the Bspline surfaces
// Bspline -> all infos about surface
// Bsplineder -> infos about the derivative nx times in x-axis and ny times in y-axis

static void update_knot_der(struct Bspline2D *Bspline, struct Bspline2D_red *Bsplineder,int nx, int ny){

     int l;

     for (l=0;l<(Bsplineder->nx+Bsplineder->degx+1);l++){
         Bsplineder->kx[l]=Bspline->kx[l+nx];
     }
     for (l=0;l<(Bsplineder->ny+Bsplineder->degy+1);l++){
         Bsplineder->ky[l]=Bspline->ky[l+ny];
     }

}

// Same as init_derivate_Bspline but only updating the values
void update_derivate_Bspline(struct Bspline2D *Bspline){

   int n,m,p,q;

    n=Bspline->nx;
    m=Bspline->ny;
    p=Bspline->degx;
    q=Bspline->degy;
    update_knot_der(Bspline,&Bspline->der10,1,0);
    update_knot_der(Bspline,&Bspline->der01,0,1);
    update_knot_der(Bspline,&Bspline->der11,1,1);
    update_knot_der(Bspline,&Bspline->der02,0,2);
    update_knot_der(Bspline,&Bspline->der20,2,0);
    update_controle_point_derive(n,p,m,q,Bspline->Pz,Bspline->der10.Pz,Bspline->kx,Bspline->ky,0);
    update_controle_point_derive(n,p,m,q,Bspline->Pz,Bspline->der01.Pz,Bspline->kx,Bspline->ky,1);
    update_controle_point_derive(n-1,p-1,m,q,Bspline->der10.Pz,Bspline->der20.Pz,Bspline->der10.kx,Bspline->der10.ky,0);
    update_controle_point_derive(n,p,m-1,q-1,Bspline->der01.Pz,Bspline->der02.Pz,Bspline->der01.kx,Bspline->der01.ky,1);
    update_controle_point_derive(n,p,m-1,q-1,Bspline->der01.Pz,Bspline->der11.Pz,Bspline->der01.kx,Bspline->der01.ky,0);   
}


// sign function
static int sign(int x)
{
    // assumes 32-bit int/unsigned
    x|=x>>1;
    return ((unsigned)-x >> 31) - ((unsigned)x >> 31);
}


/* Apply two times the de Boor algorithm to compute a point of surface of Bspline
*
* n : number of control points in u-direction
* p : degree of Bspline in u-direction
* U : knot_vector in U-direction
* m : number of control points in v-direction
* q : degree of Bspline in v-direction
* V : knot vector in v-direction
* ctrl_pts : array of control points
* u : float where you want to evaluate the surface
* v ; same
*
*
*
*/

// https://pages.mtu.edu/~shene/COURSES/cs3621/NOTES/surface/bspline-de-boor.html

double Bspline_surface_point_red(const struct Bspline2D_red *Bspline, double u,double v){

    //int n, int p, double U[n+p+1],int m,int q, double V[m+q+1],double **ctrl_pts, double u,double v
    // Initializing variables

    int n,p,m,q;

    n=Bspline->nx;
    m=Bspline->ny;
    p=Bspline->degx;
    q=Bspline->degy;

    int vspan, uspan;
    int k,l;
    int ipx;


    double S;  // will contains the value of the surface
    double Pu[n]; // will contains a intermediate set of control points along u-direction
    double Pv[m]; // will contains a intermediate set of control points along v-direction



    // Core of the function
    if ((u<Bspline->kx[p])||(u>Bspline->kx[n]) || (v<Bspline->ky[q])||(v>Bspline->ky[m])) S=0.0;

    else {
        // Find where u and v lies in knot vector
        uspan=index_knot_linear(n,p,u,Bspline->kx);
        vspan=index_knot_linear(m,q,v,Bspline->ky);

        // It's more efficient to apply deboor algorithm on the 
        // axes of lower degree in first. So we use a switch 
        // based on the different cases we will faced
        // Apply de Boor algorithm
        
        ipx=sign(p-q);
        switch(ipx){

            case(1):
                for (k=uspan-p;k<=uspan;k++){
	            for (l=vspan-q;l<=vspan;l++) 
		    {	
			  Pv[l]=Bspline->Pz[k][l];
		    }

                    deboor_algorithms(q,vspan,v,Bspline->ky,Pv,&Pu[k]);

                };

                deboor_algorithms(p,uspan,u,Bspline->kx,Pu,&S);
                break;
            
            case(-1):
                for (k=vspan-q;k<=vspan;k++){
	            for (l=uspan-p;l<=uspan;l++) 
		    {	
			  Pu[l]=Bspline->Pz[l][k];
		    }

                    deboor_algorithms(p,uspan,u,Bspline->kx,Pu,&Pv[k]);

                };

                deboor_algorithms(q,vspan,v,Bspline->ky,Pv,&S);
                break;
            
            case(0):
                for (k=uspan-p;k<=uspan;k++){
	            for (l=vspan-q;l<=vspan;l++) 
		    {	
			  Pv[l]=Bspline->Pz[k][l];
		    }

                    deboor_algorithms(q,vspan,v,Bspline->ky,Pv,&Pu[k]);

                };

                deboor_algorithms(p,uspan,u,Bspline->kx,Pu,&S);
                break;

        }; 

    };
   
    return S;

}

// Some functions use with Bspline potential defined on the convergence
// too slow for the optimisation an present some issues due to the use 
// of a ft



static double ft_basis_Bspline(struct point p){
    
    return pow(gsl_sf_sinc(p.x/(2.0)),4)*pow(gsl_sf_sinc(p.y/(2.0)),4);
}



static double ft_basis_dplx(struct point p){


    return -ft_basis_Bspline(p)*p.x/(p.x*p.x+p.y*p.y);
}


static double ft_basis_psixx(struct point p){


    return ft_basis_Bspline(p)*p.x*p.x/(p.x*p.x+p.y*p.y);
}

static double ft_basis_psixy(struct point p){


    return ft_basis_Bspline(p)*p.x*p.y/(p.x*p.x+p.y*p.y);
}

static double ft_basis_dply(struct point p){


    return -ft_basis_Bspline(p)*p.y/(p.x*p.x+p.y*p.y);
}

static double ft_basis_poten(struct point p){


    return -ft_basis_Bspline(p)/(p.x*p.x+p.y*p.y);
}





//TODO à modifier légèrment --> déjà un peu modifier
void dftcor(double w, double delta, double a, double b, double endpts[],double *corre, double *corim, double *corfac){
    double a0i,a0r,a1i,a1r,a2i,a2r,a3i,a3r,arg,c,cl,cr,s,sl,sr,t;
    double t2,t4,t6;
    double cth,ctth,spth2,sth,sth4i,stth,th,th2,th4,tmth2,tth4i;
    extern struct g_mode    M;

    th=w*delta;
    if (a >= b || th < 0.0e0 || th > 3.1416e0)  NPRINTF(stderr,"Bad arguments to compute the fourier transform, you must reduce the field of view\n");
    if (fabs(th) < 5.0e-2) { //Use series.
        t=th;
        t2=t*t;
        t4=t2*t2;
        t6=t4*t2;
        *corfac=1.0-(11.0/720.0)*t4+(23.0/15120.0)*t6;
        a0r=(-2.0/3.0)+t2/45.0+(103.0/15120.0)*t4-(169.0/226800.0)*t6;
        a1r=(7.0/24.0)-(7.0/180.0)*t2+(5.0/3456.0)*t4-(7.0/259200.0)*t6;
        a2r=(-1.0/6.0)+t2/45.0-(5.0/6048.0)*t4+t6/64800.0;
        a3r=(1.0/24.0)-t2/180.0+(5.0/24192.0)*t4-t6/259200.0;
        a0i=t*(2.0/45.0+(2.0/105.0)*t2-(8.0/2835.0)*t4+(86.0/467775.0)*t6);
        a1i=t*(7.0/72.0-t2/168.0+(11.0/72576.0)*t4-(13.0/5987520.0)*t6);
        a2i=t*(-7.0/90.0+t2/210.0-(11.0/90720.0)*t4+(13.0/7484400.0)*t6);
        a3i=t*(7.0/360.0-t2/840.0+(11.0/362880.0)*t4-(13.0/29937600.0)*t6);
    }
    else { //Use trigonometric formulas in double precision.
        cth=cos(th);
        sth=sin(th);
        ctth=cth*cth-sth*sth;
        stth=2.0e0*sth*cth;
        th2=th*th;
        th4=th2*th2;
        tmth2=3.0e0-th2;
        spth2=6.0e0+th2;
        sth4i=1.0/(6.0e0*th4);
        tth4i=2.0e0*sth4i;
        *corfac=tth4i*spth2*(3.0e0-4.0e0*cth+ctth);
        a0r=sth4i*(-42.0e0+5.0e0*th2+spth2*(8.0e0*cth-ctth));
        a0i=sth4i*(th*(-12.0e0+6.0e0*th2)+spth2*stth);
        a1r=sth4i*(14.0e0*tmth2-7.0e0*spth2*cth);
        a1i=sth4i*(30.0e0*th-5.0e0*spth2*sth);
        a2r=tth4i*(-4.0e0*tmth2+2.0e0*spth2*cth);
        a2i=tth4i*(-12.0e0*th+2.0e0*spth2*sth);
        a3r=sth4i*(2.0e0*tmth2-spth2*cth);
        a3i=sth4i*(6.0e0*th-spth2*sth);
    }
    cl=a0r*endpts[0]+a1r*endpts[1]+a2r*endpts[2]+a3r*endpts[3];
    sl=a0i*endpts[0]+a1i*endpts[1]+a2i*endpts[2]+a3i*endpts[3];
    cr=a0r*endpts[7]+a1r*endpts[6]+a2r*endpts[5]+a3r*endpts[4];
    sr = -a0i*endpts[7]-a1i*endpts[6]-a2i*endpts[5]-a3i*endpts[4];
    *corre=cl+cr-sr;
    *corim=sl+cr+sr;
}



void inite_power_spectrum(struct Bspline2D *Bspline,double Fs,int N){

    Bspline->der11.Pz=alloc_square_double(N+1,N+1); // pot
    Bspline->der10.Pz=alloc_square_double(N+1,N+1); //dplx
    Bspline->der01.Pz=alloc_square_double(N+1,N+1); //dply
    Bspline->der20.Pz=alloc_square_double(N+1,N+1); // psi/xx
    Bspline->der02.Pz=alloc_square_double(N+1,N+1); // psi/xy

    int k,l;
    double kx[N+1],ky[N+1];
    //double fact=(2*Fs/N)*(2*Fs/N);
    struct point pf;

    for (k=0;k<=N;k++) kx[k]=ky[k]=k*2.0*Fs/(double)N - Fs;
    for (k=0;k<=N;k++){
        for (l=0;l<=N;l++){
            pf.x=2.0*kx[k];
            pf.y=2.0*ky[l];
            Bspline->der11.Pz[k][l]=ft_basis_poten(pf);
            Bspline->der10.Pz[k][l]=ft_basis_dplx(pf);
            Bspline->der01.Pz[k][l]=ft_basis_dply(pf);
            Bspline->der20.Pz[k][l]=ft_basis_psixx(pf);
            Bspline->der02.Pz[k][l]=ft_basis_psixy(pf);
        }
    }
    Bspline->der11.Pz[N/2][N/2]=0.0;
    Bspline->der10.Pz[N/2][N/2]=0.0;
    Bspline->der01.Pz[N/2][N/2]=0.0;
    Bspline->der20.Pz[N/2][N/2]=0.0;
    Bspline->der02.Pz[N/2][N/2]=0.0;
    //P[N/2][N/2]=0.0; already change
}






// discrete cosinus transform
static void ctf( int N,struct point *p, double Fs,const struct Bspline2D_red *Bspline, double **P){

    int k,l;
    double kx[N+1],ky[N+1];
    double a;
    //double fact=5.312209862*(1.0/(double)N)*(1.0/(double)N); // normalisation
    //struct point pf;

    for (k=0;k<=N;k++) kx[k]=ky[k]=k*2.0*Fs/(double)N - Fs;
    /*
    for (k=0;k<=N;k++){
        for (l=0;l<=N;l++){
            a=gsl_sf_cos(2.0*PI*(p->x*kx[k]+p->y*ky[l]));
            P[k][l]=Bspline->Pz[k][l]*a;
            //printf("%1.16e \n",P[k][l]);
        }
    }
    */
    
    for (k=N;k>=0;k--){
        for (l=N/2-1;l>=0;l--){
            a=cos(2.0*PI*(p->y*kx[k]+p->x*ky[l]));
            P[k][l]=Bspline->Pz[k][l]*a;
            P[-k+N][-l+N]=Bspline->Pz[-k+N][-l+N]*a;
        }
    }
    
    //P[N/2][N/2]=0.0; already change
}


// discrete sinus transform
static void stf( int N,struct point *p, double Fs,const struct Bspline2D_red *Bspline, double **P){


    int k,l;
    double a;
    double kx[N+1],ky[N+1];

    //double fact=(2*Fs/N)*(2*Fs/N);
    //struct point pf;

    for (k=0;k<=N;k++) kx[k]=ky[k]=k*2.0*Fs/(double)N - Fs;
    for (k=N;k>=0;k--){
        for (l=N/2-1;l>=0;l--){
            a=sin(2.0*PI*(p->x*kx[k]+p->y*ky[l]));
            P[k][l]=Bspline->Pz[k][l]*a;
            P[-k+N][-l+N]=-Bspline->Pz[-k+N][-l+N]*a;
        }
    }
    //P[N/2][N/2]=0.0; already change
}


// when ft purely real and even (potential)
static double ft_approx_cos(struct point *p ,int N,double Fs,const struct Bspline2D_red *Bspline){

    int k,l;
    double sum=0.0;
    double endpts[8],seconde_table[N+1];
    double corre,corim,corfac;
    double corre1,corim1,corfac1;
    double **P;
    double fact=4.1205218237*(1.0/(double)N)*(1.0/(double)N);



     P=alloc_square_double(N+1,N+1);
     ctf(N,p,Fs,Bspline,P);
     for (k=0;k<=N;k++){
         /*
         endpts[0]=P[k][0];
         endpts[1]=P[k][1];
         endpts[2]=P[k][2];
         endpts[3]=P[k][3];
         endpts[4]=P[k][N-3];
         endpts[5]=P[k][N-2];
         endpts[6]=P[k][N-1];
         endpts[7]=P[k][N]; 
         */
         //dftcor(fabs(p->x),2.0*Fs/(double)N, -Fs,Fs,endpts,&corre,&corim,&corfac); 
         sum=0.0;//NeumaierSum(P[k],N+1);
         for (l=N;l>=0;l--) sum+=P[k][l];
         //printf("%1.16e sum1\n",sum);
         seconde_table[k]=sum;//corfac*sum+corre;
         //printf("%1.16e sk\n",seconde_table[k]);
     }
     
     free_square_double(P,N+1);
     /*
     endpts[0]=seconde_table[0];
     endpts[1]=seconde_table[1];
     endpts[2]=seconde_table[2];
     endpts[3]=seconde_table[3];
     endpts[4]=seconde_table[N-3];
     endpts[5]=seconde_table[N-2];
     endpts[6]=seconde_table[N-1];
     endpts[7]=seconde_table[N];
     */
     sum=0.0;//NeumaierSum(seconde_table,N+1);
     for (l=N;l>=0;l--) sum+=seconde_table[l];
     //printf("%1.16e \n",sum);
     //dftcor(fabs(p->y),2.0*Fs/(double)N, -Fs,Fs,endpts,&corre,&corim,&corfac);
     //printf("%1.16e %1.16e \n",corfac*sum+corre,corfac);
     return fact*sum;//fact*(corfac*sum+corre);
}


// when ft purely imaginary and odd (dplx and dply)
static double ft_approx_sin(struct point *p ,int N, double Fs,const struct Bspline2D_red *Bspline){

    int k,l;
    double sum=0.0;
    double endpts[8],seconde_table[N+1];
    double corre,corim,corfac;
    double **P;
    double fact=4.1205218237*(1.0/(double)N)*(1.0/(double)N);



     P=alloc_square_double(N+1,N+1);
     stf(N,p,Fs,Bspline,P);
     for (k=0;k<=N;k++){
         /*
         endpts[0]=P[k][0];
         endpts[1]=P[k][1];
         endpts[2]=P[k][2];
         endpts[3]=P[k][3];
         endpts[4]=P[k][N-4];
         endpts[5]=P[k][N-3];
         endpts[6]=P[k][N-2];
         endpts[7]=P[k][N-1];
         */
         //dftcor(fabs(p->x),2.0*Fs/(double)N, -Fs,Fs,endpts,&corre,&corim,&corfac);
         sum=0.0;//NeumaierSum(P[k],N+1);
         for (l=0;l<=N;l++) sum+=P[k][l];
         seconde_table[k]=sum;//corfac*sum+corim;
     }
     free_square_double(P,N+1);
     /*
     endpts[0]=seconde_table[0];
     endpts[1]=seconde_table[1];
     endpts[2]=seconde_table[2];
     endpts[3]=seconde_table[3];
     endpts[4]=seconde_table[N-4];
     endpts[5]=seconde_table[N-3];
     endpts[6]=seconde_table[N-2];
     endpts[7]=seconde_table[N-1];
     */
     sum=0.0;//NeumaierSum(seconde_table,N+1);
     for (l=0;l<=N;l++) sum+=seconde_table[l];
     //dftcor(fabs(p->y),2.0*Fs/(double)N, -Fs,Fs,endpts,&corre,&corim,&corfac);
     return fact*sum;//fact*(corfac*sum+corim);
}




double poten_from_kappa(const struct Bspline2D *Bspline ,struct point *point,int n){


     

     double poten=0.0; 
     int k,l,m,t;
     struct point p;
     double Fs=1.0;

     for (k=0;k<Bspline->n;k++){
         for (l=0;l<Bspline->n;l++){
             m=Bspline->degx+1+(int)((Bspline->degx+1)/2.0);
             t=Bspline->degy+1+(int)((Bspline->degy+1)/2.0);
             p.x=(point->x-Bspline->kx[k+m])/((Bspline->size_sq));
             p.y=(point->y-Bspline->ky[l+t])/((Bspline->size_sq));
             //p.x=(point->x)/(Bspline->size_sq);
             //p.y=(point->y)/(Bspline->size_sq);
             poten+=Bspline->Pz[k+Bspline->degx+1][l+Bspline->degy+1]*ft_approx_cos(&p ,n,Fs,&Bspline->der11);
         }
     }

     return poten;

}


struct matrix seconde_derivative_from_kappa(const struct Bspline2D *Bspline,struct point *point,int n){


    struct matrix seconde_deriv;
    int k,l,m,t;
    double kappa;
    double Fs=1.0;
    struct point p;
    

    seconde_deriv.a=seconde_deriv.b=seconde_deriv.c=seconde_deriv.d=0.0;
    kappa=Bspline_surface_point(Bspline, point->x,point->y);
    for (k=0;k<Bspline->n;k++){
         for (l=0;l<Bspline->n;l++){
             m=Bspline->degx+1+(int)((Bspline->degx+1)/2.0);
             t=Bspline->degy+1+(int)((Bspline->degy+1)/2.0);
             p.x=(point->x-Bspline->kx[k+m])/((Bspline->size_sq));
             p.y=(point->y-Bspline->ky[l+t])/((Bspline->size_sq));
             seconde_deriv.a+=Bspline->Pz[k+Bspline->degx+1][l+Bspline->degy+1]*ft_approx_cos(&p ,n,Fs,&Bspline->der20);
             seconde_deriv.b+=Bspline->Pz[k+Bspline->degx+1][l+Bspline->degy+1]*ft_approx_cos(&p ,n,Fs,&Bspline->der02);
         }
    }
    seconde_deriv.d=seconde_deriv.d;
    seconde_deriv.c=2*kappa-seconde_deriv.a;


    return seconde_deriv;


}


struct point dpl_from_kappa(const struct Bspline2D *Bspline ,struct point *point,int n){


    
     struct point dpl;
     struct point p;
     int k,l,t,m;//,r;
     extern  struct  g_mode   M;

     
     //size_t calls = 30000;
     double Fs=1.0;

     dpl.x=dpl.y=0.0;
          
     //p.x=(point->x)/(Bspline->size_sq);
     //p.y=(point->y)/(Bspline->size_sq);
     //dpl.x=ft_approx_cos(&ft_basis_dplx,&p ,n,Fs,&Bspline->der10);
     //printf("%1.16e %f %f\n",ft_basis_Bspline(p),p.x,p.y);
     //dpl.y=ft_approx_sin(&ft_basis_dply,&p ,n,Fs);
     
     for (k=0;k<Bspline->n;k++){
         for (l=0;l<Bspline->n;l++){
             m=Bspline->degx+1+(int)((Bspline->degx+1)/2.0);
             t=Bspline->degy+1+(int)((Bspline->degy+1)/2.0);
             p.x=(point->x-Bspline->kx[k+m])/((Bspline->size_sq));
             p.y=(point->y-Bspline->ky[l+t])/((Bspline->size_sq));
             //printf("%f %f \n",p.x,p.y);
             dpl.x+=Bspline->Pz[k+Bspline->degx+1][l+Bspline->degy+1]*ft_approx_sin(&p ,n,Fs,&Bspline->der10);
             dpl.y+=Bspline->Pz[k+Bspline->degx+1][l+Bspline->degy+1]*ft_approx_sin(&p ,n,Fs,&Bspline->der01);
         }
     }
     
     return dpl;

}

