#include<stdio.h>
#include<math.h>
#include<fonction.h>
#include<constant.h>
#include<dimension.h>
#include<structure.h>

/*
JPK - OMP - 20 Mai 1995

comp the chi2 of images using a source object
NOT USED IN LENSTOOL
*/

double	comp_chi_os(im,tim,nx,ny,xmin,xmax,ymin,ymax,source,nbs)

double	**im,**tim;
int	nx,ny,nbs;
double	xmin,xmax,ymin,ymax;
struct	galaxie	source[];

{
extern struct g_observ O;

register	int	i,j;
double	sigim=0.001;
double	merrit;


for(i=0;i<nx;i++)
for(j=0;j<ny;j++)
	tim[i][j]=0.;
cp_im(tim,nx,ny,xmin,xmax,ymin,ymax,source,nbs);
cv_cpsf(tim,nx,ny,xmin,xmax,ymin,ymax,O.seeing);

/* compute  merrit function of the difference of the test and observed images*/
merrit = cp_errdiff(tim,im,nx,ny,sigim);	/* sigim is the image noise */

fprintf(stderr,"Merrit: %.3lf %.3lf  %.3lf %.3lf %.3lf\n",
		source[0].C.x,source[0].C.y,
		source[0].E.a,source[0].E.b,merrit);

return(merrit);
}
