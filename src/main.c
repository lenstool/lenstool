#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<math.h>
#include<fonction.h>
#include<constant.h>
#include<dimension.h>
#include<structure.h>

/****************************************************************/
/*      nom:        LENSTOOL            */
/*      auteur:     Jean-Paul Kneib         */
/*      date:       10/02/92            */
/*      place:      Toulouse            */
/****************************************************************/


int  main(int argc, char *argv[])
{

    /*************  declaration de common et locale ****************/

    extern struct g_mode    M;
    extern struct g_grille  G;
    extern struct galaxie   source[NFMAX];
    extern struct galaxie   image[NFMAX][NIMAX];
    extern struct pot       lens[NLMAX];
    extern struct g_cosmo   C;
    extern struct g_cube    cubeFrame;
    extern struct g_pixel   imFrame, ps;
    extern struct galaxie  cimage[NFMAX];
    extern struct g_cline  CL;
    extern struct g_source S;
    int noedit, init;
    double  chi2, lhood0;
    char    infile[80];
    long int ni, ncistart;

   
    /*************  Verification du format de la commande  ****************/
    if ( argc == 2 && !strcmp(argv[1], "-v") )
    {
        printf("%s\n", VERSION);
        exit(1);
    }
    if ( argc != 2 && argc != 3 )
    {
        fprintf(stderr, "\nUnexpected number of arguments\n");
        fprintf(stderr, "\nUSAGE:\n");
        fprintf(stderr, "lenstool  input_file [-n]\n\n");
        exit(-1);
    }
    if ( argc == 2 || argc == 3 )
        strcpy (infile, argv[1]);

    if ( strstr( infile, ".par" ) == NULL )
        strcat( infile, ".par" );

    noedit = 0;
    if ( argc == 3 && !strcmp(argv[2], "-n") )
        noedit = 1;

    /************** Read the .par file and initialise potentials ***********/
    init_cosmoratio(0, NULL);
    init = init_grille(infile, "para.out", noedit);
    
    if ( noedit == 0 && M.verbose > 0 )
        copyright();
    

    /************** Print common information *******************************/
    if ( M.iref != 0 )
        NPRINTF(stderr, "Reference coordinates WCS  : %lf, %lf\n",
                M.ref_ra, M.ref_dec);
    NPRINTF(stderr, "Conversion Factor @ z = %.3lf, 1 arcsec == %.3lf kpc\n",
            lens[0].z, d0 / C.h*distcosmo1(lens[0].z));

    /*************** Creation d'un grille polaire ou cartesienne ***********/
    if ( G.pol != 0 )
        gridp();
    else
        grid();

    /*************** Checks parameters and constraints *********************/
    checkpar();

    /*************** Optimisation globale **********************************/
    
    if ( M.inverse != 0 )
        o_global();

    tracepot();

    if ( M.ichi2 != 0 )
    {
        int error;
        NPRINTF(stdout, "INFO: compute chires.dat\n");
        readConstraints();
        o_chires("chires.dat");
        error = o_chi_lhood0(&chi2, &lhood0, NULL);
        printf("INFO: chi2 %lf  Lhood %lf\n", chi2, -0.5 * ( chi2 + lhood0 )  );
        o_global_free();
    }

    if ( M.radial != 0 )
        g_radial(M.radial, M.zradial, M.theta);
    
    /************** Calcul des lignes critiques et caustiques ***************/
    if ( CL.nplan > 0 )
    {
        // if there is more than 1 clump
        if ( G.nlens > 0 )
        {
            criticnew(1);
	    w_critic();

        };
        

        // if there is only 1 clump
        if ( G.nlens == 1 )
            critic_an();

        /* calcul des zones d'images multiples */
        if ( CL.zone != 0 )
	{
  	    cl_connectdots(1);
            zonemult();
	}
    }

    if ( M.marker != 0 )
    {
        NPRINTF(stderr, "COMP: inversion des markers \n");
        i_marker(M.markfile, M.zmarker);
    }

    /* Plan I pixelise renvoyer dans le plan S - morphologie de la source */
    if ( M.iclean == 1 && cubeFrame.format !=0 )
    { NPRINTF(stderr, "READ1: cubeframe\n");
        cubetosou(M.zclean, ps.pixfile);
    }
    else if (((M.iclean == 1)||(M.iclean==5)) && imFrame.format !=0)
        { NPRINTF(stderr, "READ1: imframe\n");
            imtosou(M.zclean, ps.pixfile);}
//  else if ( M.iclean == 2 && M.inverse == 0 )
//        opt_source();
    else if ( M.iclean == 3 )
        cleanlens(M.zclean);

    if (( M.pixel ==2) && (ps.pixfile[0] != 0))
        pixelsource(M.npixel, M.pixelfile);

    /* antecedant d'une grille source ou d'une grille image */
    if ( M.grille != 0 )
        g_grid(M.grille, M.ngrille, M.zgrille);

    /* grille du amplification (keyword ampli)*/
    if ( M.iampli != 0 )
        g_ampli(M.iampli, M.nampli, M.zampli, M.amplifile);

    /* grille du potential */
    if ( M.ipoten != 0 )
        g_poten(M.ipoten, M.npoten, M.zpoten, M.potenfile);

    /* grille du mass */
    if ( M.imass != 0 )
        g_mass(M.imass, M.nmass, M.zmass, S.zs, M.massfile);

    /* grille du dpl */
    if ( M.idpl != 0 )
        g_dpl(M.idpl, M.ndpl, M.zdpl, M.dplxfile, M.dplyfile);

    /* grille du curv */
    if ( M.icurv != 0 )
        g_curv(M.icurv, M.ncurv, M.zcurv, M.cxxfile, M.cxyfile, M.cyyfile);

    /* grille du shear */
    if ( M.ishear != 0 )
        g_shear(M.ishear, M.nshear, M.zshear, M.shearfile);

    /* grille du time-delay */
    if ( M.itime != 0 )
        g_time(M.itime, M.ntime, M.ztime, M.timefile);

    /* grille du shear_field */
    if ( M.ishearf != 0 )
        g_shearf(M.ishearf, M.zshearf, M.shearffile, M.nshearf);

    /* amplification field grid */
    if ( M.iamplif != 0 )
        g_amplif(M.iamplif, M.zamplif, M.ampliffile);

    /* grille des proprietes */
    if ( M.prop != 0 )
    {
        g_prop(M.nprop, M.zprop);
        w_prop(M.nprop, M.propfile);
    }

    /* local study of images */
    if ( M.local != 0 )
        local(M.local, M.localfile);

    /* set des sources */
    if ( M.source != 0 || M.image != 0 || S.ns > 0 )
    {
        s_source();

        NPRINTF(stderr, "COMP: images of the sources\n");
        if (M.pixel == 1)
	{
	    if(strcmp(ps.pixfile, "")) 
		    e_pixel(M.npixel, M.pixelfile, ps.pixfile, source);
	    else 
                    e_pixel(M.npixel, M.pixelfile, "source.fits", source);
	}

        if (M.pixel < 2)
        {
            e_lensing(source, image);
            w_sicat(image, source);
            classer(image, cimage, &ni, &ncistart);
            distor(cimage, ni);
            ecrire_r(0, ni, cimage, "sort.dat", 0);
            ecrire_r(0, ni, cimage, "image.all", 2);
            ecrire_r(ncistart, ni, cimage, "image.dat", 0);
        }
       	if (M.cube > 0)
	{
		printf("%s\n","if_cube");
	        e_cube(M.npixel, M.nslices, M.cubefile, "sourcecube.fits",source);
		printf("%s\n","end_if_cube");
	}
    }

    /* Compute from an image file only the source file */
    if ( M.sof > 0 )
    {
        NPRINTF(stderr, "COMP: sources only from an image file\n");
        s_sof();
    }

    /* study of images */
    if ( M.study != 0 )
    {
        NPRINTF(stderr, "ESS: elipticity source study %d %.2lf %s %d\n",
                M.study, M.seeing, M.studyfile, M.fake);
        study_pg(M.study, M.seeing, M.studyfile, M.fake);
    }

    if ( M.icorshear != 0 )
        cor_shear();
    
    return 0;
}
