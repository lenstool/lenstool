#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<fonction.h>
#include<constant.h>
#include<dimension.h>
#include<structure.h>
#include<string.h>



static double hypo(double x, double y, long int i);
static struct point e_grad_np(const struct galaxie *image, double *np_b0);

/****************************************************************/
/*      nom:        e_grad              */
/*      auteur:     Jean-Paul Kneib         */
/*      date:       10/02/92            */
/*      place:      Toulouse            */
/****************************************************************
 * Return the gradient of the projected lens potential.
 *
 * !!! You have to multiply by dlsds to obtain the true gradient
 *
 * Global variables used (ALL global variables is constant here) : 
 * - lens, G
 * - in hypo() : lens
 * - in nfwg_dpl() : lens_table
 */
struct point e_grad_pot(const struct point *pi, long int i)
{
    const extern  struct  g_mode      M;
    const extern struct g_grille G;
    const extern struct pot      lens[];
    const struct pot *ilens = &lens[i];
    double X, Y, R;
    double t, u, q, t05, t10, t15, z;
    double za, zb, zas, zbc, dpl_rad;
    struct point P, Q, Grad, g, pg, dlenspotent, dweight; //,Qnfw
    struct polar QP;
    complex zis, zis_cut, ztot, ztot_cut;
    double tell;
     double pa,ps, weight, weightx, weighty,lenspotent, lenspotentx, lenspotenty, skew, skewangle, rslope, newelipticity;
     double newx,newx_x,newx_y,newx_xx,newx_yy,newx_xy,newy,newy_x,newy_y,newy_xx,newy_yy,newy_xy;

    double xw,yw,xpixel,ypixel;
    int offscl;

    g.x = g.y = 0.;

    // shortcut for N atoms
    if (ilens->b0 == 0. && ilens->type != 14 && ilens->type !=17 && ilens->type !=814)
        return g;
   
    /*positionning at the potential center*/
    P.x = pi->x - ilens->C.x;
    P.y = pi->y - ilens->C.y;

    /*rotation to the potential axes*/
    //Q = rotation(P, ilens->theta);
    Q = rotation_opt(P, ilens->costheta, ilens->sintheta); //factorized rotation (Benjamin Clement)

    switch (ilens->type)
    {
        case(1):
            u = sqrt(hypo(Q.x, Q.y, i));
            g.x = ilens->b0 * (1 - ilens->epot) * Q.x / u;
            g.y = ilens->b0 * (1 + ilens->epot) * Q.y / u;
            break;
        case(-1):
            QP = polxy(Q);
            z = sqrt(2.*ilens->epot * 3.);
            za = z / sqrt(1 + ilens->epot * 3.);
            zb = z / sqrt(1 - ilens->epot * 3.);
            zas = -za * cos(QP.theta);
            zbc = zb * sin(QP.theta);
            pg.x = ilens->b0 / z * ( -cos(QP.theta) * asin(zas)
                                     + sin(QP.theta) * asinh(zbc) );
            pg.y = ilens->b0 / z * (sin(QP.theta) * asin(zas)
                                    + cos(QP.theta) * asinh(zbc) );
            g = rotation(pg, -QP.theta);
            break;
        case(-2):
            zis = csiemd(Q.x, Q.y, ilens->epot, ilens->b0);
            zis_cut = ci05(Q.x, Q.y, ilens->epot, ilens->rcut);
            g.x = zis.re - ilens->b0 * zis_cut.re;
            g.y = zis.im - ilens->b0 * zis_cut.im;
            break;

        case(3): // power law with core radius
            z = ilens->rc * ilens->rc;
            t = 2 * ilens->alpha * ilens->b0 / ilens->rc;
            u = pow(1. + (1 - ilens->epot) * Q.x * Q.x / z + (1 + ilens->epot) * Q.y * Q.y / z,
                    ilens->alpha - 1.);
            g.x = t * (1 - ilens->epot) * Q.x * u;
            g.y = t * (1 + ilens->epot) * Q.y * u;
            break;
        case(4):
            /* ici l'ellipticite est celle du potentiel */
            z = ilens->rc * ilens->rc;
            X = Q.x * Q.x / z;
            Y = Q.y * Q.y / z;
            R = X + Y;
            t = ilens->b0 / ilens->rc / sqrt(1. + R);
            g.x = t * Q.x * (1 - ilens->epot / 2. / (1. + R) * (2. + X + 3.*Y));
            g.y = t * Q.y * (1 + ilens->epot / 2. / (1. + R) * (2. + 3.*X + Y));
            break;
        case(41):
            /* same as 4 but with a troncated potentiel
            */
            z = ilens->rc * ilens->rc;
            X = Q.x * Q.x / z;
            Y = Q.y * Q.y / z;
            R = X + Y;
            if (sqrt(R) < ilens->rcut)
            {
                t = ilens->b0 / ilens->rc / sqrt(1. + R);
                g.x = t * Q.x * (1 - ilens->epot / 2. / (1. + R) * (2. + X + 3.*Y));
                g.y = t * Q.y * (1 + ilens->epot / 2. / (1. + R) * (2. + 3.*X + Y));
                /* cut */
                t = 2.*ilens->psicut;
                g.x -= t * Q.x;
                g.y -= t * Q.y;
            }
            else
            {
                u = Q.x * Q.x + Q.y * Q.y;
                g.x = ilens->psimcut * Q.x / u;
                g.y = ilens->psimcut * Q.y / u;
            }
            break;
        case(5):
            QP = polxy(Q);
            z = ilens->rc * ilens->rc;
            R = QP.r * QP.r / z;
            t = ilens->b0 * ilens->rc / 2. / QP.r;
            pg.x = t * (log(1. + R)
                        - ilens->epot * cos(2.*QP.theta) * (1. / (1. + R) - log(1. + R) / R));
            pg.y = ilens->epot * t * sin(2.*QP.theta) * (log(1. + R) / R - 1.);
            g = rotation(pg, -QP.theta);
            break;
        case(6):
            QP = polxy(Q);
            z = ilens->rc * ilens->rc;
            R = QP.r * QP.r / z;
            t = ilens->b0 / ilens->rc * QP.r;
            pg.x = 2.*ilens->alpha * t * pow(1. + R, ilens->alpha - 1.) +
                   2.*ilens->epot * t * (1. + (1. - ilens->beta) * R) /
                   pow(1. + R, 1. + ilens->beta) * cos(2.*QP.theta);
            pg.y = -2.*ilens->epot * t / pow(1. + R, ilens->beta) * sin(2.*QP.theta);
            g = rotation(pg, -QP.theta);
            break;
        case(7): /* point mass */
            if (ilens->b0 != 0.)
            {
                u = Q.x * Q.x + Q.y * Q.y;
                z = (G.dx * G.dx + G.dy * G.dy) / 4.;
                /* if(u>z) */
                if ( (fabs(Q.x) > G.dx / 2.) || (fabs(Q.y) > G.dy / 2.))
                {
                    g.x = ilens->b0 * Q.x / u;
                    g.y = ilens->b0 * Q.y / u;
                }
                else
                {
                    g.x = ilens->b0 * Q.x / z;
                    g.y = ilens->b0 * Q.y / z;
                }
            }
            else
                g.x = g.y = 0.;
            break;
        case(8): /* PIEMD kovner  I0.5 */
            zis = ci05(Q.x, Q.y, ilens->epot, ilens->rc);
            g.x = ilens->b0 * zis.re;
            g.y = ilens->b0 * zis.im;
            break;

        case(81): //PIEMD Kassiola & Kovner,1993 I0.5c-I0.5cut
            if ( ilens->epot > 2E-4 )
            {
                t05 = ilens->b0 * ilens->rcut / (ilens->rcut - ilens->rc);
                //zis = ci05(Q.x, Q.y, ilens->epot, ilens->rc);
                //zis_cut = ci05(Q.x, Q.y, ilens->epot, ilens->rcut);
                //g.x = t05 * (zis.re - zis_cut.re);
                //g.y = t05 * (zis.im - zis_cut.im);
		zis = ci05f(Q.x, Q.y, ilens->epot, ilens->rc, ilens->rcut);
		g.x = t05 * zis.re;
		g.y = t05 * zis.im;
            }
            else if ((u = Q.x * Q.x + Q.y * Q.y) > 0. )
            {
                // Circular dPIE Elliasdottir 2007 Eq A23 slighly modified for t05
                X = ilens->rc;
                Y = ilens->rcut;
                t05  = sqrt(u + X * X) - X - sqrt(u + Y * Y) + Y;  // Faster and equiv to Elliasdottir (see Golse PhD)
                t05 *= ilens->b0 * Y / (Y - X) / u; // 1/u because t05/sqrt(u) and normalised Q/sqrt(u)
                g.x = t05 * Q.x;
                g.y = t05 * Q.y;
            }
            else
            {
                g.x = 0.;
                g.y = 0.;
            }
            break;
         case(812): /*skewed piemd with a weight function see P. Taylor et al. 2016 or 2017 for more details*/ 
/*skew is given by alpha*/
/* rc_slope is skew angle in radians*/
/*alpha is skew*/
/*beta is radal dependence parameter*/
/*scale radius is rc so don't optimise for the core radius when using this potential*/
	    if ( ilens->epot > 2E-25 )
            {
               // function needed to compute lensing potential and first derivative for piemd 
               t05 = ilens->b0 * ilens->rcut / (ilens->rcut - ilens->rc);
                zis = ci05(Q.x, Q.y, ilens->epot, ilens->rc);
                zis_cut = ci05(Q.x, Q.y, ilens->epot, ilens->rcut);
		pa = pi05(Q.x, Q.y, ilens->epot, ilens->rc, ilens->b0);
                ps = pi05(Q.x, Q.y, ilens->epot, ilens->rcut, ilens->b0);
                //weight function and first derivatives 
		weight = f_weight(P.x, P.y, ilens->alpha, ilens->beta, ilens->rc, ilens->rcslope);
		weightx = f_weightx(P.x, P.y, ilens->alpha, ilens->beta, ilens->rc, ilens->rcslope);
		weighty = f_weighty(P.x, P.y, ilens->alpha, ilens->beta, ilens->rc, ilens->rcslope);
                // lens potential and first derivatives
		lenspotent = t05 * (pa - ps);
		g.x = t05 * (zis.re - zis_cut.re);
		g.y = t05 * (zis.im - zis_cut.im);
                // rotate back to the shared coordinate system so that the skew is independent of the lens orientation. We explicitly do not rotate back later as is the case for most other potentials. Do a search for 812 to see where this happens. 
		g = rotation_opt(g, ilens->costheta, -ilens->sintheta);
                //calculate the first derivatives using the product rule
		Grad.x = g.x * weight + lenspotent * weightx;       
		Grad.y = g.y * weight + lenspotent * weighty;
            }
         //this is the same code as in the previous if statement, but is needed since the piemd is not defined for epsilon = 0, so we make it really reall small instead.   
         else if ((u = Q.x * Q.x + Q.y * Q.y) > 0. )
            {
		newelipticity =.0000001 ;  
             	t05 = ilens->b0 * ilens->rcut / (ilens->rcut - ilens->rc);
                zis = ci05(Q.x, Q.y, newelipticity, ilens->rc);
                zis_cut = ci05(Q.x, Q.y, newelipticity, ilens->rcut);
		pa = pi05(Q.x, Q.y, newelipticity, ilens->rc, ilens->b0);
                ps = pi05(Q.x, Q.y, newelipticity, ilens->rcut, ilens->b0);
		weight = f_weight(P.x, P.y, ilens->alpha, ilens->beta, ilens->rc, ilens->rcslope);
		weightx = f_weightx(P.x, P.y, ilens->alpha, ilens->beta, ilens->rc,ilens->rcslope);
		weighty = f_weighty(P.x, P.y, ilens->alpha, ilens->beta, ilens->rc, ilens->rcslope);
		lenspotent = t05 * (pa - ps);
		g.x = t05 * (zis.re - zis_cut.re);
		g.y = t05 * (zis.im - zis_cut.im);
		g = rotation_opt(g, ilens->costheta, -ilens->sintheta);                
		Grad.x = g.x * weight + lenspotent * weightx;               
		Grad.y = g.y * weight + lenspotent * weighty;
            }
            else
            {
                Grad.x = 0.;
                Grad.y = 0.;
            }
            break; 
	 case(813): /*PISMD skewed potential see P.Taylor 2016 or 2017 for more details */
/*alpha is skew*/
/*beta is skew angle in radians*/
	    if ( ilens->epot > 2E-25 )
            {
                //compute the first derivative of piemd
                t05 = ilens->b0 * ilens->rcut / (ilens->rcut - ilens->rc);
                zis = ci05(Q.x, Q.y, ilens->epot, ilens->rc);
                zis_cut = ci05(Q.x, Q.y, ilens->epot, ilens->rcut);
		g.x = t05 * (zis.re - zis_cut.re);
		g.y = t05 * (zis.im - zis_cut.im);
		//rotate back to shared coordinate system so skew does not depend on the orientation of the lens. We explicitly don't rotate back later
                g = rotation_opt(g, ilens->costheta, -ilens->sintheta);
		//compute first derivatives of the new coordinates coordinates that we are transforming to.
                newx_x  = f_newx_x(P.x, P.y, ilens->alpha, ilens->beta);
		newx_y  = f_newx_y(P.x, P.y, ilens->alpha, ilens->beta);
		newy_x  = f_newy_x(P.x, P.y, ilens->alpha, ilens->beta);
		newy_y  = f_newy_y(P.x, P.y, ilens->alpha, ilens->beta);
                //use the chain rule to ro compute the gradient
		Grad.x = g.x * newx_x + g.y * newy_x;
		Grad.y = g.x * newx_y + g.y * newy_y;
		
	    }
            //this else if is to account for the fact piemd is not defined for epsilon = 0, so we run the same code as above but with a really small epsilon 
            else if ((u = Q.x * Q.x + Q.y * Q.y) > 0. )
            {
		newelipticity =.0000001 ;  
             	t05 = ilens->b0 * ilens->rcut / (ilens->rcut - ilens->rc);
                zis = ci05(Q.x, Q.y, newelipticity, ilens->rc);
                zis_cut = ci05(Q.x, Q.y, newelipticity, ilens->rcut);
		g.x = t05 * (zis.re - zis_cut.re);
		g.y = t05 * (zis.im - zis_cut.im);
		g = rotation_opt(g, ilens->costheta, -ilens->sintheta);
		newx_x  = f_newx_x(P.x, P.y, ilens->alpha, ilens->beta);
		newx_y  = f_newx_y(P.x, P.y, ilens->alpha, ilens->beta);
		newy_x  = f_newy_x(P.x, P.y, ilens->alpha, ilens->beta);
		newy_y  = f_newy_y(P.x, P.y, ilens->alpha, ilens->beta);
		Grad.x = g.x * newx_x + g.y * newy_x;
		Grad.y = g.x * newx_y + g.y * newy_y;
            }
            else
            {
                Grad.x = 0.;
                Grad.y = 0.;
            }
            break;
        //Benjamin Beauchesne - Bspline modification
        case(814): // PIEMD with a Bspline perturbation
            /*
            // Compute initial PIEMD
            if ( ilens->epot > 2E-4 )
            {
                t05 = ilens->b0 * ilens->rcut / (ilens->rcut - ilens->rc);
                //zis = ci05(Q.x, Q.y, ilens->epot, ilens->rc);
                //zis_cut = ci05(Q.x, Q.y, ilens->epot, ilens->rcut);
                //g.x = t05 * (zis.re - zis_cut.re);
                //g.y = t05 * (zis.im - zis_cut.im);
		zis = ci05f(Q.x, Q.y, ilens->epot, ilens->rc, ilens->rcut);
		g.x = t05 * zis.re;
		g.y = t05 * zis.im;
            }
            else if ((u = Q.x * Q.x + Q.y * Q.y) > 0. )
            {
                // Circular dPIE Elliasdottir 2007 Eq A23 slighly modified for t05
                X = ilens->rc;
                Y = ilens->rcut;
                t05  = sqrt(u + X * X) - X - sqrt(u + Y * Y) + Y;  // Faster and equiv to Elliasdottir (see Golse PhD)
                t05 *= ilens->b0 * Y / (Y - X) / u; // 1/u because t05/sqrt(u) and normalised Q/sqrt(u)
                g.x = t05 * Q.x;
                g.y = t05 * Q.y;
            }
            else
            {
                g.x = 0.;
                g.y = 0.;
            }
            
            */
            // Compute Bspline perturbation
            //double tmp1, tmp2;
            g.x=Bspline_surface_point_red(&ilens->Bspline->der10,Q.x,Q.y);
            g.y=Bspline_surface_point_red(&ilens->Bspline->der01,Q.x,Q.y);
            //printf("%f %f %f %f %f %f %f \n",ilens->b0,ilens->rcut,ilens->rc, ilens->epot, ilens->emass,ilens->Bspline->x_c,ilens->Bspline->y_c); 
            break;

        case(815): // PIEMD with a Bspline perturbation

            // Compute initial PIEMD
            if ( ilens->epot > 2E-4 )
            {
                t05 = ilens->b0 * ilens->rcut / (ilens->rcut - ilens->rc);
                //zis = ci05(Q.x, Q.y, ilens->epot, ilens->rc);
                //zis_cut = ci05(Q.x, Q.y, ilens->epot, ilens->rcut);
                //g.x = t05 * (zis.re - zis_cut.re);
                //g.y = t05 * (zis.im - zis_cut.im);
		zis = ci05f(Q.x, Q.y, ilens->epot, ilens->rc, ilens->rcut);
		g.x = t05 * zis.re;
		g.y = t05 * zis.im;
            }
            else if ((u = Q.x * Q.x + Q.y * Q.y) > 0. )
            {
                // Circular dPIE Elliasdottir 2007 Eq A23 slighly modified for t05
                X = ilens->rc;
                Y = ilens->rcut;
                t05  = sqrt(u + X * X) - X - sqrt(u + Y * Y) + Y;  // Faster and equiv to Elliasdottir (see Golse PhD)
                t05 *= ilens->b0 * Y / (Y - X) / u; // 1/u because t05/sqrt(u) and normalised Q/sqrt(u)
                g.x = t05 * Q.x;
                g.y = t05 * Q.y;
            }
            else
            {
                g.x = 0.;
                g.y = 0.;
            }


            // Compute Bspline perturbation
            //double tmp1, tmp2;
            /*
            g.x+=Bspline_surface_point_red(&ilens->Bspline->der10,Q.x,Q.y);
            g.y+=Bspline_surface_point_red(&ilens->Bspline->der01,Q.x,Q.y);
            */ 
            struct point point=dpl_from_kappa(ilens->Bspline ,&Q,128);
            g.x+=point.x;
            g.y+=point.y;
            //printf("%f %f \n",Q.x,Q.y);
            break;

        case(82): /* PIEMD kovner with shallow central slope I0.5c+I0.5cut*/

            t05 = ilens->b0 * (ilens->rcut + ilens->rc) / ilens->rcut;
            zis = ci05(Q.x, Q.y, ilens->epot, ilens->rc);
            zis_cut = ci05(Q.x, Q.y, ilens->epot, ilens->rcut);
            g.x = t05 * (zis.re + zis_cut.re);
            g.y = t05 * (zis.im + zis_cut.im);
            break;

        case(83): /* EMD kovner 3/2: I1.5c*/

            ztot = ci15(Q.x, Q.y, ilens->epot, ilens->rc, ilens->b0);
            g.x = ztot.re;
            g.y = ztot.im;
            break;

        case(84): /* EMD kovner isotherme I0.5c-I0.5cut + I1.5c */

            t05 = ilens->b0 * ilens->rcut / (ilens->rcut - ilens->rc);
            t15 = ilens->rcut / ilens->rc;
            zis = ci05(Q.x, Q.y, ilens->epot, ilens->rc);
            zis_cut = ci05(Q.x, Q.y, ilens->epot, ilens->rcut);
            ztot = ci15(Q.x, Q.y, ilens->epot, ilens->rc, ilens->b0);
            q = ilens->alpha;
            g.x = q * t15 * ztot.re + (1 - q) * t05 * (zis.re - zis_cut.re);
            g.y = q * t15 * ztot.im + (1 - q) * t05 * (zis.im - zis_cut.im);
            break;

        case(85): /* EMD kovner 1: I1c*/

            ztot = ci10(Q.x, Q.y, ilens->epot, ilens->rc, ilens->b0);
            g.x = ztot.re;
            g.y = ztot.im;
            break;

        case(86): /* EMD kovner 1: I1c-I1cut*/

            t10 = ilens->rc / (1. - ilens->rc * ilens->rc / ilens->rcut / ilens->rcut);
            ztot = ci10(Q.x, Q.y, ilens->epot, ilens->rc, ilens->b0);
            ztot_cut = ci10(Q.x, Q.y, ilens->epot, ilens->rcut, ilens->b0);
            g.x = t10 * (ztot.re - ztot_cut.re);
            g.y = t10 * (ztot.im - ztot_cut.im);
            break;

        case(87): /* EMD kovner  I1c-I1cut +I0.5c-I0.5cut */

            t05 = ilens->b0 / (1. - ilens->rc / ilens->rcut);
            t10 = ilens->rc / (1. - ilens->rc * ilens->rc / ilens->rcut / ilens->rcut);
            zis = ci05(Q.x, Q.y, ilens->epot, ilens->rc);
            zis_cut = ci05(Q.x, Q.y, ilens->epot, ilens->rcut);
            ztot = ci10(Q.x, Q.y, ilens->epot, ilens->rc, ilens->b0);
            ztot_cut = ci10(Q.x, Q.y, ilens->epot, ilens->rcut, ilens->b0);
            q = ilens->alpha;
            g.x = (1 - q) * t05 * (zis.re - zis_cut.re) + q * t10 * (ztot.re - ztot_cut.re);
            g.y = (1 - q) * t05 * (zis.im - zis_cut.im) + q * t10 * (ztot.im - ztot_cut.im);
            break;

        case(88): /* EMD kovner  I1c-I1cut +I1.5cut */

            t10 = ilens->rc / (1. - ilens->rc * ilens->rc / ilens->rcut / ilens->rcut);
            t15 = ilens->rcut / ilens->rc;
            zis_cut = ci15(Q.x, Q.y, ilens->epot, ilens->rcut, ilens->b0);
            ztot = ci10(Q.x, Q.y, ilens->epot, ilens->rc, ilens->b0);
            ztot_cut = ci10(Q.x, Q.y, ilens->epot, ilens->rcut, ilens->b0);
            q = ilens->alpha;
            g.x = (1 - q) * t15 * zis_cut.re + q * t10 * (ztot.re - ztot_cut.re);
            g.y = (1 - q) * t15 * zis_cut.im + q * t10 * (ztot.im - ztot_cut.im);
            break;

        case(89): /* EMD kovner  I1c-I1cut +I0.5c-I0.5cut */

            t05 = ilens->b0 / (1. - 1. / ilens->beta);
            t10 = ilens->rc / (1. - 1. / ilens->beta / ilens->beta);
            zis = ci05(Q.x, Q.y, ilens->epot, ilens->rc);
            zis_cut = ci05(Q.x, Q.y, ilens->epot, ilens->rc * ilens->beta);
            ztot = ci10(Q.x, Q.y, ilens->epot, ilens->rc, ilens->b0);
            ztot_cut = ci10(Q.x, Q.y, ilens->epot, ilens->rc * ilens->beta, ilens->b0);
            q = ilens->alpha;
            g.x = q * t05 * (zis.re - zis_cut.re) + (1 - q) * t10 * (ztot.re - ztot_cut.re);
            g.y = q * t05 * (zis.im - zis_cut.im) + (1 - q) * t10 * (ztot.im - ztot_cut.im);
            break;


        case(9): /* plan masse */
            t = ilens->b0;
            g.x = t * Q.x;
            g.y = t * Q.y;
            break;

        case(10): /* spline */
            Grad = sp_grad(*pi);
            break;

        case(11): /* 1/r4 mass */
            z = ilens->rc * ilens->rc;
            t = 2.*ilens->b0 / ilens->rc;
            R = 1. + (Q.x * Q.x + Q.y * Q.y) / z;
            g.x = t * Q.x / R;
            g.y = t * Q.y / R;
            break;

        case(12): /* NFW */
            /*     Qnfw.x=Q.x-ilens->rc*ilens->emass/(1.-ilens->emass);
              u=sqrt(Qnfw.x*Qnfw.x+Q.y*Q.y);
              g.x=nfw_dpl(u,ilens->rc,ilens->b0/2.)*Qnfw.x/u;
              g.y=nfw_dpl(u,ilens->rc,ilens->b0/2.)*Q.y/u;
                  Qnfw.x=Q.x+ilens->rc*ilens->emass/(1.-ilens->emass);
              u=sqrt(Qnfw.x*Qnfw.x+Q.y*Q.y);
              g.x+=nfw_dpl(u,ilens->rc,ilens->b0/2.)*Qnfw.x/u;
              g.y+=nfw_dpl(u,ilens->rc,ilens->b0/2.)*Q.y/u;
             */
            /*
             u=sqrt(Q.x*Q.x+Q.y*Q.y);
             g.x=nfw_dpl(u,ilens->rc,ilens->b0)*Q.x/u;
             g.y=nfw_dpl(u,ilens->rc,ilens->b0)*Q.y/u;
            */
            u = sqrt( (1. - ilens->emass) * Q.x * Q.x + (1. + ilens->emass) * Q.y * Q.y );
            if ( u == 0. )
            {
                g.x = g.y = 0.;
                return g;
            }
            if (ilens->alpha == 0.)
            {
                dpl_rad = nfw_dpl(u, ilens->rc, ilens->b0); // circular potential
                g.x = dpl_rad * (1. - ilens->emass) * Q.x / u;        // corrections to elliptical potential
                g.y = dpl_rad * (1. + ilens->emass) * Q.y / u;
            }
            else
            {
                dpl_rad = nfwg_dpl(u, ilens->rc, ilens->b0, ilens->alpha);
                g.x = dpl_rad * (1. - ilens->emass) * Q.x / u;
                g.y = dpl_rad * (1. + ilens->emass) * Q.y / u;
            }
            break;
        case(121): // NFW triaxial
            tell = elli_tri(ilens);
    		u = sqrt( (1.-tell)*Q.x*Q.x+(1.+tell)*Q.y*Q.y );
    		if( u == 0. ) // central point
    		{
    			g.x = g.y = 0.;
    			return g;
    		}	
    		if (ilens->alpha == 0.) // standard NFW
    		{
        		dpl_rad=nfw_dpl(u,ilens->rc,ilens->b0);	// circular potential
        		g.x=dpl_rad*(1.-tell)*Q.x/u;		// corrections to elliptical potential
        		g.y=dpl_rad*(1.+tell)*Q.y/u;
    		}
    		else  // generalized NFW
                RAISE(E_PARFILE, "ERROR: Triaxial generalized NFW not yet implemented\n");
            break;
        case(122): // NFW + PM
            u = sqrt( (1. - ilens->emass) * Q.x * Q.x + (1. + ilens->emass) * Q.y * Q.y );
            if ( u == 0. )
            {
                g.x = g.y = 0.;
                return g;
            }
            if (ilens->alpha == 0.)
            {
                dpl_rad = nfw_dpl(u, ilens->rc, ilens->b0); // circular potential
                g.x = dpl_rad * (1. - ilens->emass) * Q.x / u;        // corrections to elliptical potential
                g.y = dpl_rad * (1. + ilens->emass) * Q.y / u;
            }
            else
            {
                dpl_rad = nfwg_dpl(u, ilens->rc, ilens->b0, ilens->alpha);
                g.x = dpl_rad * (1. - ilens->emass) * Q.x / u;
                g.y = dpl_rad * (1. + ilens->emass) * Q.y / u;
            }
            break;
        case(13): // Sersic
            u = sqrt( (1. - ilens->emass) * Q.x * Q.x + (1. + ilens->emass) * Q.y * Q.y );
            dpl_rad = sersic_dpl(u, ilens->rc, ilens->alpha, ilens->b0); // circular potential
            g.x = dpl_rad * (1. - ilens->emass) * Q.x / u;        // corrections to elliptical potential
            g.y = dpl_rad * (1. + ilens->emass) * Q.y / u;
            break;
        case(14): // local shear and kappa
//            u = sqrt(P.x * P.x + P.y * P.y);
//            Grad.x = ilens->emass * P.x / u;
//            Grad.y = ilens->emass * P.y / u;
            //Equations from Massimo Meneghetti http://www.ita.uni-heidelberg.de/~massimo/sub/Lectures/gl_all.pdf (3.76,3.80,3.81,3.83,3.84)
            g.x= (ilens->emass+lens[i].pmass)*Q.x; //lens[i].masse is kappa
            g.y= (-ilens->emass+lens[i].pmass)*Q.y; //lens[i].masse is kappa
            break;
        case(15): // Einasto model
	    u=sqrt( (1.-ilens->emass)*Q.x*Q.x+(1.+ilens->emass)*Q.y*Q.y );
	    dpl_rad=einasto_alpha(u,ilens->rc,ilens->alpha, ilens->pmass,ilens->b0);
	    g.x=dpl_rad*(1.-ilens->emass)*Q.x/u;
	    g.y=dpl_rad*(1.+ilens->emass)*Q.y/u;
	    break;
        case(16): // Hernquist model
            u = sqrt( (1. - ilens->emass) * Q.x * Q.x + (1. + ilens->emass) * Q.y * Q.y );
            dpl_rad = hern_dpl(u, ilens->rc, ilens->b0); // circular potential
            g.x = dpl_rad * (1. - ilens->emass) * Q.x / u;        // corrections to elliptical potential
            g.y = dpl_rad * (1. + ilens->emass) * Q.y / u;
            break;
    
        case(17): ; // external displacement maps

            double world[2], imgcrd[2], pixcrd[2];
            double phi[1], theta[1];
            int stat[1], status;
            world[0] = Q.x / (-3600.) / cos(M.ref_dec * DTR) + M.ref_ra;
            world[1] = Q.y / 3600. + M.ref_dec;
            if (wcss2p(ilens->kappamap->wcsinfo, 1, 2, world, phi, theta, imgcrd, pixcrd, stat)==0)
            {
                g.x=bilinear(ilens->dplxmap->array,pixcrd[0],pixcrd[1],ilens->dplxmap->nx,ilens->dplxmap->ny);
                g.y=bilinear(ilens->dplymap->array,pixcrd[0],pixcrd[1],ilens->dplymap->nx,ilens->dplymap->ny);
            }
            else
            {
                g.x=0.0;
                g.y=0.0;
            }
            break;

        default:
            RAISE(E_PARFILE, "ERROR: profil type of clump %s unknown : %d\n", ilens->n, ilens->type);
            break;
    };


        if (ilens->type != 10 && ilens->type != 812 && ilens->type != 813)
        //Grad = rotation(g, -ilens->theta);
	Grad = rotation_opt(g, ilens->costheta, -ilens->sintheta); //factorized rotation (Benjamin Clement)

    return Grad;
}

struct point e_grad(const struct point *pi, double dlsds, double zs)
{
    const extern struct g_grille G;
    const extern struct pot lens[];
    struct point Grad, grad;
    long int i;

    double z[3];  // [z_(k-2), z_(k-1), z_(k)]
    z[0] = 0.; z[1] = lens[0].z; z[2] = e_grad_getNextz(0, zs);
    struct point beta[3]; // [b_(k-2), b_(k-1), b_(k)]
    beta[0] = *pi;
    beta[1] = *pi;

    grad.x = 0.;
    grad.y = 0.;

    /* for each lens*/
    for (i = 0; i < G.nlens; i++)
    {
        if( lens[i].z >= zs )
            continue;

        if( lens[i].z > z[1] )
        {
            e_grad_computeSourcePositions(grad, beta, z);
            z[0] = z[1];
            z[1] = lens[i].z;
            z[2] = e_grad_getNextz(i, zs);
            grad.x = grad.y = 0.; // reinitialize grad for the next lens plane
        }

        Grad = e_grad_pot(&beta[1], i);
        grad.x += Grad.x;
        grad.y += Grad.y;
    };  /*end of for each lens*/

    e_grad_computeSourcePositions(grad, beta, z);
    grad.x = (pi->x - beta[1].x)/dlsds;  // normalize to put back in the
    grad.y = (pi->y - beta[1].y)/dlsds;  // old convention
    return (grad);
}

/* Return the gradient at the position of an image contained in the
 * multi[ifamille][iimage] global variable.
 * Args: 
 *  - image: galaxie structure with mandatory attributes C, grad, z, dr
 *  - np_b0: array of vdisp for the multiscale grid clumps [NULL]
 */
struct point e_grad_gal(struct galaxie *image, double *np_b0)
{
    const extern struct g_grille  G;
    const extern struct g_pot     P[NPOTFILE];
    const extern struct pot       lens[];
    const extern struct sigposStr sigposAs;

    struct point grad, Grad;
    double dx, dy, u;
    int skip_clump; // if 1, skip the gradient computation for a clump
    long int i;
    int j;


    grad.x = grad.y = 0;
    double z[3];  // [z_(k-2), z_(k-1), z_(k)]
    z[0] = 0.; z[1] = lens[0].z; z[2] = e_grad_getNextz(0, image->z);
    struct point beta[3]; // [b_(k-2), b_(k-1), b_(k)]
    beta[0] = image->C;
    beta[1] = image->C;

    // get the potential of the optimised clumps
    for ( i = 0; i < G.no_lens; i++ )
    {
        if( lens[i].z >= image->z )
            continue;

        if( lens[i].z > z[1] )
        {
            e_grad_computeSourcePositions(grad, beta, z);
            z[0] = z[1];
            z[1] = lens[i].z;
            z[2] = e_grad_getNextz(i, image->z); 
            grad.x = grad.y = 0.; // reinitialize grad for the next lens plane
        }

        Grad = e_grad_pot(&beta[1], i);
        grad.x += Grad.x;
        grad.y += Grad.y;
    }
 
    // if not already computed, or multiplane lensing
    // then compute the gradient of the not optimised clumps
    if ( image->grad.x == image->grad.y || z[2] != image->z )
    {
        image->grad.x = 0.;
        image->grad.y = 1e-10;  // to block images that skip all pots

        // Potentials not optimized but defined individually
        for ( i = G.no_lens; i < G.nplens[0]; i++ )
        {
            if( lens[i].z > image->z )
                continue;

            if( lens[i].z > z[1] )
            {
                e_grad_computeSourcePositions(grad, beta, z);
                z[0] = z[1];
                z[1] = lens[i].z;
                z[2] = e_grad_getNextz(i, image->z); 
                grad.x = grad.y = 0.; // reinitialize grad for the next lens plane
            }

            Grad = e_grad_pot(&beta[1], i);
            grad.x += Grad.x;
            grad.y += Grad.y;
            image->grad.x += Grad.x;
            image->grad.y += Grad.y;  // igrad only works for single plane lensing
        }

        // Potentials in potfiles
        for ( j = 0; j < G.npot; j++ )
            for( i = G.nplens[j]; i < G.nplens[j+1]; i++ )
            {
                if ( P[j].select == 1 )
                {
                    // test if the deflexion produced by this clump is detectable
                    // assuming a simple SIS potential in first approx
                    dx = image->C.x - lens[i].C.x;
                    dy = image->C.y - lens[i].C.y;
                    u = sqrt(dx * dx + dy * dy);
                    dx = lens[i].b0 * dx / u * image->dr;
                    dy = lens[i].b0 * dy / u * image->dr;
                    if ( dx*dx + dy*dy > sigposAs.max*sigposAs.max ) continue;
                }
                else if( P[j].select == 2 )
                {
                    // test on the distance to the image in arcsec
                    dx = image->C.x - lens[i].C.x;
                    dy = image->C.y - lens[i].C.y;
                    u = sqrt(dx * dx + dy * dy);
                    if ( u > 10 * lens[i].rc ) continue;
                }
              	else if( P[j].select > 2 )
                {
                    // test on the distance to the image in arcsec
                    dx = image->C.x - lens[i].C.x;
                    dy = image->C.y - lens[i].C.y;
                    u = sqrt(dx * dx + dy * dy);
                    if ( u > P[j].select ) continue;
                }

                if( lens[i].z >= image->z )
                    continue;

                if( lens[i].z > z[1] )
                {
                    e_grad_computeSourcePositions(grad, beta, z);
                    z[0] = z[1];
                    z[1] = lens[i].z;
                    z[2] = e_grad_getNextz(i, image->z); 
                    grad.x = grad.y = 0.; // reinitialize grad for the next lens plane
                }    

                Grad = e_grad_pot(&beta[1], i);
                grad.x += Grad.x;
                grad.y += Grad.y;
                image->grad.x += Grad.x;
                image->grad.y += Grad.y;
            }
    }
    else
    {
        grad.x += image->grad.x;
        grad.y += image->grad.y;
    }

    // and eventually those of the multiscale grid
    // TODO: Extend the multiple mode to the multiscale grid
    if ( image->np_grad != NULL )
    {
        if( image->C.x != beta[1].x && image->C.y != beta[1].y )
            RAISE(E_PARFILE, "ERROR: multiscale mode does not support multiplane lensing\n");

        Grad = e_grad_np(image, np_b0);
        grad.x += Grad.x;
        grad.y += Grad.y;
    }

    e_grad_computeSourcePositions(grad, beta, z);
    grad.x = (image->C.x - beta[1].x)/image->dr;  // normalize to put back in the
    grad.y = (image->C.y - beta[1].y)/image->dr;  // old convention
    return grad;
}

/* Return the gradient of all the potentials, when optimising
 * in non-parametric mode.
 */
static struct point e_grad_np(const struct galaxie *image, double *np_b0_thread)
{
    const extern struct g_grille  G;
    const extern double *np_b0;
    const double *np_b0_local;

    struct point grad, *pgrad;
    long int k, n;
    double b0, gx, gy;

    gx = gy = 0.;
    if( np_b0_thread != NULL )
        np_b0_local = np_b0_thread;
    else
        np_b0_local = np_b0;

    n = G.nlens - G.nmsgrid;
    for ( k = 0; k < n; k++ )
    {
        pgrad = &image->np_grad[k];
        b0 = np_b0_local[k];

        gx += b0 * pgrad->x;
        gy += b0 * pgrad->y;
    }

    grad.x = gx;
    grad.y = gy;
    return grad;
}

/***********************************************************
 * Global variables used :
 * - lens
 */
static double hypo(double x, double y, long int i)
{
    const extern struct pot lens[];
    return( (1 - lens[i].epot)*x*x + (1 + lens[i].epot)*y*y );
}

/* Return the next redshift in the list of lenses, 
 * or the redshift of the source otherwise
 * Args:
 *  - i: the id from which to start the search
 *  - zs: the redshift of the source
 */
double e_grad_getNextz(long int i, double zs)
{
    const extern struct pot lens[];
    const extern struct g_grille G;
    double z;
    z = lens[i].z;
    while (lens[i].z == z && i < G.nlens) i++;
    if(i < G.nlens && lens[i].z < zs) 
        z = lens[i].z;
    else
        z = zs;
    return z;
}

/* Use Equation 15 from Hilbert et al. 2009 to compute
 * the source position on the next lens plane, or
 * at the source redshift otherwise.
 * Shift the values of the beta array by 1 step 
 * Args:
 *  - grad: deflection vector computed with the lenses in the current lens plane
 *  - beta: array of source positions [b_(k-2), b_(k-1), b_(k)]
 *  - z: array of lens plane (or eventually source) redshifts [z_(k-2), z_(k-1), z_(k)]
 */
void e_grad_computeSourcePositions(struct point grad, struct point *beta, double *z)
{
    const extern struct pot lens[];
    double d1, d2, d3, d4, d5;
    d1 = distcosmo2(0,z[1]);  // DOL
    d2 = distcosmo2(0,z[2]);  // DOS
    d3 = distcosmo2(z[0],z[2]);   //DOS
    d4 = distcosmo2(z[0],z[1]);   //DOL
    d5 = distcosmo2(z[1],z[2]);   //DLS
    beta[2].x = (1. - d1/d2*d3/d4)*beta[0].x    // 1-DOL/DOS*DOS/DOL
        + d1/d2*d3/d4*beta[1].x      // DOL/DOS*DOS/DOL
        - d5/d2*grad.x;         // DLS/DOS   --> beta=pi-DLS/DOS*grad
    beta[2].y = (1. - d1/d2*d3/d4)*beta[0].y
        + d1/d2*d3/d4*beta[1].y
        - d5/d2*grad.y;
    
    beta[0] = beta[1];
    beta[1] = beta[2];
    

}
