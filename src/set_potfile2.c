#include<stdio.h>
#include<math.h>
#include<fonction.h>
#include<constant.h>
#include<dimension.h>
#include<structure.h>

/****************************************************************/
/*                 Program  : grille				*/
/*                 Version  : 1 mai 1992			*/
/*                 Location : Obs. Toulouse			*/
/*                 Auteur   : jean-paul				*/
/****************************************************************/

int set_potfile2(int nplens)
{
	extern struct   g_mode M;
	extern struct	pot	lens[];
	extern struct	g_cosmo	C;
	extern struct	g_pot	Q;
	
	int	il;
	double	aa,bb;
	double  GG=10.867;
	char	line[128];
	FILE	*POT;
	
	il=nplens;
	
	NPRINTF(stderr,"\n> reading:%s",Q.potfile);
	
	/* clump type file */
	
	if(Q.ftype==2)
	{
	POT=fopen(Q.potfile,"r");
	if( POT != NULL )
	        while((fscanf(POT,"%d%lf%lf%lf%lf%lf%lf%lf%lf",
	                &lens[il].type,&lens[il].C.x,&lens[il].C.y,
			&lens[il].emass,&lens[il].theta,&lens[il].rckpc,
			&lens[il].rcutkpc,&lens[il].sigma,&lens[il].z
	                ))!=-1)
	                        {
				lens[il].epot=lens[il].emass=0.;
				lens[il].theta*=DTR;
	                        flire(POT,line);
				il++;
	                        };
	fclose(POT);
	}
	
	/* photometric type file */
	
	else if(Q.ftype==1)
	{
	POT=fopen(Q.potfile,"r");
	if( POT != NULL )
	        while((fscanf(POT,"%s%lf%lf%lf%lf%lf%lf%lf",
	                lens[il].n,&lens[il].C.x,&lens[il].C.y,
			&aa,&bb,&lens[il].theta,&lens[il].mag,&lens[il].lum))!=-1)
	                        {
				lens[il].type=Q.type;
				lens[il].z=Q.zlens;
				lens[il].emass=(aa*aa-bb*bb)/(aa*aa+bb*bb);
				lens[il].rckpc=Q.corekpc*
					pow(10.,(Q.mag0-lens[il].mag)/1.25/4.);
				lens[il].rcutkpc=Q.cutkpc1*
					pow(10.,(Q.mag0-lens[il].mag)/1.25/Q.slope1);
				lens[il].rcut=
				     lens[il].rcutkpc /(d0/C.h*distcosmo1(lens[il].z));
				lens[il].sigma=Q.sigma1*
					pow(10.,(Q.mag0-lens[il].mag)/2.5/4.);
				lens[il].theta*=DTR;
	/* the 1.5 factor come from the fact that we used 6PI\sigma^2 instead of 
	 4PI\sigma^2 - still don't understand why we need the 2.5 factor */
				if (lens[il].lum!=0)
				    lens[il].mtol=2.5* 1.5*M_PI/GG*
					(lens[il].sigma/1000)*(lens[il].sigma/1000)*
	                                lens[il].rcutkpc/lens[il].lum;
	                        flire(POT,line);
	
				il++;
	                        };
	fclose(POT);
	}
	
	/*
	return the number of clump read in the potfile -----------------------------
	*/
	
	NPRINTF(stderr," %d\n",il-nplens);
	
	return(il-nplens);
	
}
