#include<stdio.h>
#include<math.h>
#include<fonction.h>
#include<constant.h>
#include<dimension.h>
#include<structure.h>

/****************************************************************/
/*		nom:		pro_arclet			*/
/*		auteur:		Jean-Paul Kneib			*/
/*		date:		10/02/92			*/
/*		place:		Toulouse			*/
/****************************************************************/
/* FOR MULTIPLE IMAGES */
/* Compute the (q, dr, tau, dis, eps) parameters of a list of arclets.
 * Set the grad2 variable to NULL
 * 
 * Parameters :
 * - n : number of arclets
 * - gal : list of arclets
 * 
 * Global variables used :
 * - lens
 * - in dratio() : C
 */

void    pro_arclet_m(int n,struct galaxie gal[NAMAX])
{
	const extern	struct	pot	lens[];
	register int		i;
	double				qi;
	
	for(i=0;i<n;i++)
	{
		qi=gal[i].q=gal[i].E.b/gal[i].E.a;
	
		if (gal[i].z!=0.)
		    gal[i].dr=dratio(lens[0].z,gal[i].z);
	
		if (qi>0.)
		{
			gal[i].tau=.5*(1./qi-qi);
			gal[i].dis=.5*(1./qi+qi);
			gal[i].eps=(1.-qi)/(1.+qi);
		}
		
		// initialise the grad2 matrix to identity matrix
		gal[i].grad2.a = gal[i].grad2.c = 1;
		gal[i].grad.x = gal[i].grad.y = 0;
	
	};
}
