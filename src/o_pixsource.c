#include<string.h>
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<math.h>
#include<fonction.h>
#include<constant.h>
#include<dimension.h>
#include<structure.h>
#include<lt.h>
/****************************************************************/
/*      nom:        o_pixsource              */
/*      auteur:     Johan Richard         */
/*      date:       02/01/14            */
/*      place:      Lyon                */
/****************************************************************/

void precompsource(){
/****************************************************************/
/*      author:     Soniya Sharma         */
/****************************************************************/
	FILE *fp;
	struct point Pi, Ps;
	const extern  struct  pot lens[];
	const extern struct g_source  S;
	extern struct g_mode M; // M.iclean
	const extern struct g_observ  O;
	extern struct g_pixel imFrame,ps,PSF;
	extern double **fluxmap;
	double **tmpimage,xx ;
	extern   int **nimage;
	const extern struct g_frame   F;  

	xx = (double)imFrame.ech; 

	//Setting the ps parameters
	double dlsds = dratio(lens[0].z, M.zclean);
	if( strcmp(M.centerfile, "") )
	{
	    ps.pixelx = imFrame.pixelx / ps.ech;
	    ps.pixely = imFrame.pixelx/ ps.ech;
	    s_sourcebox(&ps, M.centerfile, dlsds, M.zclean);
	} 
	
	int ni=imFrame.nx*imFrame.ny;
	int ns=ps.nx*ps.ny;
	double stepx,stepxs,stepy,stepys;
	fluxmap=(double **)alloc_square_double(ni,ns);
	   
	double sizeratio=imFrame.pixelx/ps.pixelx; // pixel size ratio between image and source plane
	double xs,ys;
	
	tmpimage=(double **)alloc_square_double(imFrame.ny,imFrame.nx);
	
	int is,js,i,j,ii,jj;
	double z,rbfsig;
	stepx=(imFrame.xmax-imFrame.xmin)/(imFrame.nx-1);
	stepy=(imFrame.ymax-imFrame.ymin)/(imFrame.ny-1);
	stepxs=(ps.xmax-ps.xmin)/(ps.nx-1);
	stepys=(ps.ymax-ps.ymin)/(ps.ny-1);
	
	rbfsig = (double)(O.rbfsig/2.355) ;       //Setting the sigma of gaussian radial basis functions
	
	for (i=0;i<ni;i++)
	    for(j=0;j<ns;j++)
	        fluxmap[i][j]=0.0;

	xs= ps.xmin;
	for(is=0;is<ps.nx;is++)
	{ 
		ys=ps.ymin;
		for(js=0;js<ps.ny;js++)
		{  
			Pi.x= imFrame.xmin - stepx/2 * (1.- 1./xx);
			for (i=0;i<imFrame.nx;i++)
				for(j=0;j<imFrame.ny;j++)
					tmpimage[j][i]=0.0;
			for(i=0;i<imFrame.nx;i++)
			{
				for (ii = 0; ii < imFrame.ech; ii++)
				{ 
					Pi.y=imFrame.ymin - stepy/2 * (1.- 1./xx); 
					for(j=0;j<imFrame.ny;j++)
					{
						for (jj = 0; jj < imFrame.ech; jj++)
						{
							e_dpl(&Pi, dlsds,S.zs, &Ps);
							if (O.setrbfsig)
							{
								z= ((Ps.x-xs)*(Ps.x-xs)/stepxs/stepxs)+((Ps.y-ys)*(Ps.y-ys)/stepys/stepys);
								tmpimage[j][i]+=(1.0/(2*3.1415927*rbfsig*rbfsig))*exp(-z/(2.0*rbfsig*rbfsig));    
							}
							else 
							{
								z= fmax(fabs(Ps.x-xs)/stepxs,fabs(Ps.y-ys)/stepys);
	        							if(z<0.5)
									tmpimage[j][i]+=1.0;
	                                     		}
	                
	                            			Pi.y +=stepy/xx;
						}
					}
					Pi.x +=stepx/xx;
				}
			}  
		      	/*  convolve tmpimage with PSF */
			if (O.setseeing)
			d_seeing_omp(tmpimage, imFrame.nx, imFrame.ny, imFrame.pixelx);

			/* store tmpimage in flux matrix */
			for(i=0;i<imFrame.nx;i++)
			{ 
				for(j=0;j<imFrame.ny;j++)
				{ 
					if(M.flux!=0) tmpimage[j][i] = tmpimage[j][i]*((double)ps.ech*ps.ech);	
					fluxmap[i*imFrame.ny+j][is*ps.ny+js]=tmpimage[j][i]/xx/xx;
				}
			}
			ys+=stepys;
		}
		xs+=stepxs;
	}
	free_square_double(tmpimage,imFrame.ny);
}

void testpixel(char *imname,char *sname,double *sflux,double **simu)
{
/****************************************************************/
/*      author:     Soniya Sharma         */
/****************************************************************/
	const extern struct g_observ  O;
	extern struct g_mode    M;
	extern struct g_pixel   ps,imFrame;
	extern double **fluxmap;
	double xmin, ymin, xmax, ymax, dx, dy;
	double **source;
	double f, scale,xx;
	int nx, ny;
	FILE *fp;
	int i,j,k;
	extern   int **nimage;
	double  ra, dec, width, height;
	xmin = imFrame.xmin;
	xmax = imFrame.xmax;
	ymin = imFrame.ymin;
	ymax = imFrame.ymax;

	dx = xmax - xmin;
	dy = ymax - ymin;
	nx = imFrame.nx;// default: assign np to nx and adjust ny
	scale = dx / (nx-1);
	ny =  round(dy / scale + 1.);  // to prevent trunc(ny) behaviour

	dx = (nx - 1) * scale;
	dy = (ny - 1) * scale;
	xmax = xmin + dx;
	ymax = ymin + dy;
	int ni=imFrame.nx*imFrame.ny;
	int ns=ps.nx*ps.ny;

	NPRINTF(stderr, "\timage (%d,%d) s=%.3lf [%.3lf,%.3lf] [%.3lf,%.3lf]\n",nx, ny, scale, xmin, xmax, ymin, ymax);

	source = (double **) alloc_square_double(ps.ny, ps.nx);
	for(i=0;i<ps.nx;i++)
		for(j=0;j<ps.ny;j++)
			source[j][i]=sflux[i*ps.ny+j];	

	if (O.setrbfsig)   
		d_rbf(source, ps.nx, ps.ny, 1.);

	for(i=0;i<imFrame.nx;i++) 
		for(j=0;j<imFrame.ny;j++)			
		{
			simu[j][i]=0.0;
			for(k=0;k<ns;k++) 		 
				simu[j][i]+=fluxmap[i*ny+j][k]*sflux[k];
		}

	wrf_fits_abs(imname, simu, nx, ny, xmin, xmax, ymin, ymax, M.ref_ra, M.ref_dec);

	wrf_fits_abs(sname, source, ps.nx, ps.ny, ps.xmin, ps.xmax, ps.ymin, ps.ymax, M.ref_ra, M.ref_dec);

	free_square_double(source, ps.ny);
}
