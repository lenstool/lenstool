#include<stdio.h>
#include<math.h>
#include<fonction.h>
#include<constant.h>
#include<dimension.h>
#include<structure.h>

/****************************************************************/
/*      nom:        checkpar            */
/*      auteur:     Johan Richard         */
/*      date:       25/06/19            */
/*      place:      Lyon                */
/*                              */
/*
 * Check the consistency of parameters and constraints in the .par
 *
 * Sources with sigx + eps => sigy adjusted 
 *
 ****************************************************************
 */

void checkpar()
{
    const extern struct pot      lens[];
    const extern struct g_grille G;
    const extern struct g_mode   M;
    extern struct g_source  S;
    extern struct galaxie source[NFMAX];
    extern  int             sblock[NFMAX][NPAMAX];
    extern int              block[NLMAX][NPAMAX];

    FILE *OUT;
    int i;
    double aa, bb, Cx, Cy;

    //Double check galaxy sources
    for (i=0; i<S.ns;i++)
    {  
	    // ellipticity has been defined in shapemodel
	    if(source[i].eps>=0)
	    {
	            // size has been defined in shapemodel
		    if(source[i].E.a>0)
		    {
	                  // three parameters sigx,sigy,eps defined in shapemodel: sigy ignored
			  if(source[i].E.b>0)
                               fprintf(stderr, "WARNING: source %d sig_y parameter readjusted according to eps\n", i);
                          source[i].E.b = source[i].E.a * (1. - source[i].eps) / (1. + source[i].eps);
		    }
		    else
                RAISE(E_PARFILE, "ERROR: in source %d eps parameter is set without sigx\n", i);
	    }
	    // ellipticity has not been defined in shapemodel
	    else
	    {
	            // sig_x and sig_y defined in shapemodel: compute eps, forbid optimisation
		    if((source[i].E.a>0)&&(source[i].E.b>0))
		    {
		          source[i].eps = (source[i].E.a - source[i].E.b) / (source[i].E.a + source[i].E.b);
			  //forbid optimisation HERE
                          fprintf(stderr, "WARNING: ellipticity for source %d not defined. Source shape optimisation forbidden\n", i);
			  sblock[i][SA]=0;
			  sblock[i][SB]=0;
			  sblock[i][SEPS]=0;
			  sblock[i][STHETA]=0;
		    }
		    else if(source[i].E.a>0) // only sig_x defined in shapemodel
		    {
		          fprintf(stderr, "WARNING: ellipticity for source %d not defined. Assuming 0\n", i);
                          source[i].eps = 0;
			  source[i].E.b=source[i].E.a;
	            }
	    }
	    if((source[i].eps2>0)||(source[i].E2.a>0))
	    {
		    if(source[i].type!=6)
                RAISE(E_PARFILE, "ERROR: wrong type (should be 6) for 2nd Sersic profile in source %d\n", i);
 
		    if(source[i].eps2>=0)
	            {
	                  // second size has been defined in shapemodel
		          if(source[i].E2.a>0)
                               source[i].E2.b = source[i].E2.a * (1. - source[i].eps2) / (1. + source[i].eps2);
			  else
                RAISE(E_PARFILE, "ERROR: 2nd size not defined in source %d type 6\n", i);

		    }
		    else // only size defined 
	            {
		          fprintf(stderr, "WARNING: second ellipticity for source %d not defined. Assuming 0\n", i);
                          source[i].eps2 = 0;
			  source[i].E2.b=source[i].E2.a;
		    }
	    }
    }

    // Check that the number of clumps with optimized parameters is consistent with nlens_opt
    int p;
    int n_opt;
    n_opt = 0;
    for(i = 0 ; i < G.nlens; i++)
    {
	for(p = 0 ; p < NPAMAX; p++)
	{
	    if(block[i][p] > 0)
	    {
		n_opt++;
		break;
	    }
	}
    }  

    if(n_opt != G.no_lens)
        RAISE(E_PARFILE, "ERROR: number of clumps with optimized parameters = %d but nlens_opt = %ld\n", n_opt, G.no_lens);

    //Double check potentials
    for (i = 0; i < G.nlens; i++)
    {
        if (lens[i].type == 17)
        {
            if ( lens[i].gamma1map==0 || lens[i].gamma2map==0 || lens[i].kappamap==0 )
            {
                if ( lens[i].gamma1map==0 && lens[i].gamma2map==0 && lens[i].kappamap==0 )
                    fprintf(stderr, "WARNING: No convergence / shear file provided for potential %d. Magnifications / Shapes will not be correctly computed.\n", i+1);
                else
                    RAISE(E_PARFILE, "ERROR: Missing convergence or shear file for potential %d. \n", i+1);

            }
            if ( lens[i].dplxmap==0 || lens[i].dplymap==0 )
            {
                if ( lens[i].dplxmap==0 && lens[i].dplymap==0 )
                    fprintf(stderr, "WARNING: No displacement file provided for potential %d. Predicted positions / displacements will not be correctly computed.\n", i+1);
                else
                    RAISE(E_PARFILE, "ERROR: Missing displacement file for potential %d. \n", i+1);
        }
        if ( lens[i].potenmap==0 )
            fprintf(stderr, "WARNING: No potential map provided for potential %d. Time delays will not be correctly computed.\n", i+1);
        }
    }
}
