#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<fonction.h>
#include<constant.h>
#include<dimension.h>
#include<structure.h>
#include<lt.h>

static int  readlist(struct point I[NPOINT], char file[NIMAX]);

/*
*                 Program  : keep_cl
*                 Version  : oct 94
*                 Location : IoA Cambridge
*                 Auteur   : jean-paul
*
* Extract the points of image that are inside the polygonal contour pl
* and save the result in the imFrame.outfile FITS file in double format.
* This FITS file is 0 ouside of the imFrame.ncont polygonal contours.
*
* The polygonal contours are read in the files imFrame.contfile[k].
* k varying from 0 to imFrame.ncont. Each contour can contain one of
* the image of the multiple images system.
*
* imFrame is a global variable.
*
* Parameters :
*  - image is the original full image of the field
*
* Return :
*  - pl.{i=row,j=col} is the list of the pixels inside the contour
*  - npl is the number of pixels inside the polygonal contour
*  - imFrame.outfile in FITS file format is written on disk
*
*/

void    keep_cl(double **image, struct pixlist *pl, int *npl)
{
    extern  struct  g_mode      M;
    extern struct   g_pixel     imFrame;

    int k, kk, ii, jj, l;
    double  **clean;

//  int *size;
    struct  point   P, I[NPOINT];
    int np;

    int cxmin, cxmax, cymin, cymax, xpixel, ypixel;

    double *world, *imgcrd, *pixcrd, *phi, *theta;
    int *stat;

    /*clean is in [nb_row][nb_col] format*/
    clean = (double **) alloc_square_double(imFrame.ny, imFrame.nx);

    NPRINTF(stderr, "COMP: clean image (%d,%d)[%.5lf,%.5lf]\n",
            imFrame.nx, imFrame.ny, imFrame.pixelx, imFrame.pixely);

    kk = 0;
    for (k = 0; k < imFrame.ncont; k++)
    {
        np = readlist(I, imFrame.contfile[k]);
        NPRINTF(stderr, "KEEP: contour %d #%s# %d points \n", k + 1, imFrame.contfile[k], np);
        /*Limit the search region for the contour in the image*/
        if (imFrame.wcsinfo != NULL)
        {
            cxmin = imFrame.nx;
            cxmax = 0;
            cymin = imFrame.ny;
            cymax = 0;

            pixcrd = (double*)malloc(np*2*sizeof(double));
            imgcrd = (double*)malloc(np*2*sizeof(double));
            world = (double*)malloc(np*2*sizeof(double));
            phi = (double*)malloc(np*sizeof(double));
            theta = (double*)malloc(np*sizeof(double));
            stat = (int*)malloc(np*sizeof(int));

            for (ii = 0; ii < np; ii++)
            {
		        // points in contfile[k] are in relative arcsec
                world[2*ii] = I[ii].x / (-3600.) / cos(M.ref_dec * DTR) + M.ref_ra;
                world[2*ii+1] = I[ii].y / 3600. + M.ref_dec;
            }

            wcss2p(imFrame.wcsinfo, np, 2, world, phi, theta, imgcrd, pixcrd, stat);

            for (ii = 0; ii < np; ii++)
            {
                xpixel = (int)pixcrd[2*ii];
                ypixel = (int)pixcrd[2*ii+1];
                if (cxmin > xpixel)     cxmin = xpixel;
                if (cxmax < xpixel)     cxmax = xpixel;
                if (cymin > ypixel)     cymin = ypixel;
                if (cymax < ypixel)     cymax = ypixel;
            } /*end of for each point of the contour*/

            free(imgcrd); free(pixcrd); free(world);
            free(phi); free(theta); free(stat);
        }
        else /*imFrame.wcsinfo is NULL*/
        {
            /*TODO*/
            cxmin = 0;
            cymin = 0;
            cxmax = imFrame.nx;
            cymax = imFrame.ny;
        }

        // List pixels in the region in relative world coordinates
        int ncoord = (cymax - cymin) * (cxmax - cxmin);
        world = (double*)malloc(ncoord*2*sizeof(double));
        if (imFrame.wcsinfo != NULL)
        {
            pixcrd = (double*)malloc(ncoord*2*sizeof(double));
            imgcrd = (double*)malloc(ncoord*2*sizeof(double));
            phi = (double*)malloc(ncoord*sizeof(double));
            theta = (double*)malloc(ncoord*sizeof(double));
            stat = (int*)malloc(ncoord*sizeof(int));

            l = 0;
            for (ii = cymin; ii < cymax; ii++)
                for (jj = cxmin; jj < cxmax; jj++)
                {
                    pixcrd[2*l] = jj + 1;
                    pixcrd[2*l+1] = ii + 1;
                    l++;
                }
            wcsp2s(imFrame.wcsinfo, ncoord, 2, pixcrd, imgcrd, phi, theta, world, stat);
            free(pixcrd); free(imgcrd);
            free(phi); free(theta); free(stat);

            for (ii = 0; ii < ncoord; ii++)
            {
                world[2*ii] = (world[2*ii] - M.ref_ra) * (-3600.) * cos(M.ref_dec * DTR);
                world[2*ii+1] = (world[2*ii+1] - M.ref_dec) * 3600.;
            }
        }
        else
        {
            l = 0;
            for (ii = cymin; ii < cymax; ii++)
                for (jj = cxmin; jj < cxmax; jj++)
                {
                    world[2*l] = imFrame.ymin + imFrame.pixely * ii;
                    world[2*l+1] = imFrame.xmin + imFrame.pixelx * jj;
                    l++;
                }
        }

        // Search pixels that are inside the contour
        l = 0;
        for (ii = cymin; ii < cymax; ii++)
            for (jj = cxmin; jj < cxmax; jj++)
            {
                P.x = world[2*l]; P.y = world[2*l+1]; l++;
                if (inconvexe(P, np, I) > 0)
                {
                    pl[kk].i = P.y;
                    pl[kk].j = P.x;
                    pl[kk].ii = ii; // mod bclement shapemodel_contour
                    pl[kk].jj = jj; // mod bclement shapemodel_contour
                    pl[kk++].flux = image[ii][jj];
                    clean[ii][jj] = image[ii][jj];
                }
            }
        free(world);
    }
    NPRINTF(stderr, "KEEP: %d pixel\n", kk);
    *npl = kk;

//    if( imFrame.wcsinfo == NULL )
        wrf_fits(imFrame.outfile, clean, imFrame.nx, imFrame.ny, imFrame.xmin, imFrame.xmax, imFrame.ymin, imFrame.ymax);
//    else
//    {
//        double ra, dec, width, height;
//        wcsfull(imFrame.wcsinfo, &ra, &dec, &width, &height);
//        wrf_fits_abs(imFrame.outfile, clean, imFrame.nx, imFrame.ny, 
//              imFrame.xmin, imFrame.xmax, imFrame.ymin, imFrame.ymax, ra, dec);
//    }
}




/*
*                 Program  : s_pixlist
*                 Version  : oct 94
*                 Location : IoA Cambridge
*                 Auteur   : jean-paul
*/

void    s_pixlist(double **image, struct pixlist *pl, int *npl)
{
    extern  struct  g_mode          M;
    extern struct   g_pixel imFrame;
    register    int kk, ii, jj;

    kk = 0;
    for (ii = 0; ii < imFrame.ny; ii++)
        for (jj = 0; jj < imFrame.nx; jj++)
            if (image[ii][jj] != 0.)
            {
                pl[kk].i = ii;
                pl[kk++].j = jj;
            }

    NPRINTF(stderr, "KEEP: %d pixel\n", kk);
    *npl = kk;
}

/****************************************************************
 * In the cleanlens mode, read the contour file
 * Return
 *  - I the array of point filled with the contour
 *  - k the number of elements in I
 */

static int  readlist(struct point I[NPOINT], char file[NIMAX])
{
    extern  struct  g_mode          M;
    int n, k = 0;
    int stop = 0;
    FILE    *IN;

    IN = fopen(file, "r");
    if ( IN != NULL )
        while (stop != -1)
        {
            stop = fscanf(IN, "%d%lf%lf\n", &n, &I[k].x, &I[k].y);

            if (stop == 3)
                k++;
            else if (stop > 0)
                RAISE(E_PARFILE, "ERROR: %s is not in the right format\n", file);
        }
    else
        RAISE(E_FILE, "ERROR: Opening the contour file %s\n", file);

    fclose(IN);
    return(k);
}

/****************************************************************
 * In the cleanlens mode 2, grow the contour inside which the profile
 * will be computed according to the psf
 * Input
 *  - the pixlist containing the points inside the contours 
 *    provided by the user
 * Return
 *  - pl_ext the pixlist of points in the extended contour
 *  - npl_ext the number of elements in pl_ext
 */

void    keep_cl_extended(struct pixlist *pl, int npl, struct pixlist *pl_ext, int *npl_ext)
{
    extern  struct  g_mode      M;
    extern struct g_observ   O;
    
    extern struct   g_pixel     imFrame;
    int n, j, k;
    double **tmp;

    // allocate temporary array to compute chi2 in o_chi()
    tmp = (double **) alloc_square_double(imFrame.ny, imFrame.nx);

    // set to 1 the pixels that are inside the contour
    for(n = 0; n < npl; n++)
    {
	j = pl[n].ii; 
	k = pl[n].jj; 
	tmp[j][k] = 1.0;
    }

    // convolve array with psf
    d_seeing_omp(tmp, imFrame.nx, imFrame.ny, imFrame.pixelx);

    // debug output image
    //char *outfile_ext = "tmp.fits";
    // wrf_fits(outfile_ext, tmp, imFrame.nx, imFrame.ny, imFrame.xmin, imFrame.xmax, imFrame.ymin, imFrame.ymax);
    //exit(-1);
    
    // get pixels that are above threshold in the convolved array
    const double ZERO_TH = 1e-10;
    int l, kk;
    double xpixel, ypixel, xw, yw;
    struct  point   P;

    double *world, *imgcrd, *pixcrd, *phi, *theta;
    int *stat, ncoord;

    ncoord = imFrame.nx*O.bin*imFrame.ny*O.bin;
    world = (double*)malloc(ncoord*2*sizeof(double));
    if (imFrame.wcsinfo != NULL)
    {
        pixcrd = (double*)malloc(ncoord*2*sizeof(double));
        imgcrd = (double*)malloc(ncoord*2*sizeof(double));
        phi = (double*)malloc(ncoord*sizeof(double));
        theta = (double*)malloc(ncoord*sizeof(double));
        stat = (int*)malloc(ncoord*sizeof(int));

        l = 0;
        for (j = 0; j < imFrame.ny*O.bin; j++)
            for (k = 0; k < imFrame.nx*O.bin; k++)
		        if(tmp[(int)j/O.bin][(int)k/O.bin] > ZERO_TH)
                {
                    pixcrd[2*l] = (k+0.5*(1-O.bin))/O.bin+1.0;
                    pixcrd[2*l+1] = (j+0.5*(1-O.bin))/O.bin+1.0;
                    l++;
                }
        wcsp2s(imFrame.wcsinfo, l, 2, pixcrd, phi, theta, imgcrd, world, stat);
        free(imgcrd);
        free(phi); free(theta); free(stat);
        world = realloc(world, l*2*sizeof(double));
        ncoord = l;

        for (l = 0; l < ncoord; l++)
        {
            world[2*l] = (world[2*l] - M.ref_ra) * (-3600.) * cos(M.ref_dec * DTR);
            world[2*l+1] = (world[2*l+1] - M.ref_dec) * 3600.;
        }
    }
    else /* TODO: take this case into account if necessary */
        RAISE(E_PARFILE, "ERROR: no wcs in input image %s\n",imFrame.pixfile);

    kk = 0; l = 0;
	for (j = 0; j < imFrame.ny*O.bin; j++)
	    for (k = 0; k < imFrame.nx*O.bin; k++)
		    if(tmp[(int)j/O.bin][(int)k/O.bin] > ZERO_TH)
		    {
                pl_ext[kk].i = world[2*l+1];
                pl_ext[kk].j = world[2*l];
                pl_ext[kk].ii = j;
                pl_ext[kk].jj = k;
		        kk++; l++;
		    }
    free(world);

    NPRINTF(stderr, "KEEP: %d pixels in extended contour\n", kk);
    *npl_ext = kk;

    free_square_double(tmp, imFrame.ny);
    
}
