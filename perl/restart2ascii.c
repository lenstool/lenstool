#include<stdlib.h>
#include<stdio.h>
/* Extract ASCII version of the restart.dat binary file into
 * - lhood_restart.dat: the ln(lhood) values calculated
 * - error_restart.dat: the values of the error flags for each calculation
 */ 
int main(int argc, char **argv)
{
    FILE *ifh;
    FILE *ofhl, *ofhe;
    double lhood;
    int error, valid;
    printf("Read file restart.dat\n");
    ifh = fopen("restart.dat", "rb");
    ofhl = fopen("lhood_restart.dat", "w");
    ofhe = fopen("error_restart.dat", "w");
    while(fread(&valid, sizeof(int), 1, ifh))
    {
        if(valid)
        {
            fread(&lhood, sizeof(double), 1, ifh);
            fread(&error, sizeof(int), 1, ifh);
            fprintf(ofhl, "%lf\n", lhood);
            fprintf(ofhe, "%d\n", error);
//            fwrite(&lhood, sizeof(double), 1, ofhl);
//            fwrite(&error, sizeof(int), 1, ofhe);
        }
    }
    fclose(ifh);
    fclose(ofhl);
    fclose(ofhe);
    return 0;
}
