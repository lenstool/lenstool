import numpy as np
import matplotlib
matplotlib.use('Agg')
import corner 

labels = []
with open('bayes.dat','r') as fh:
	line = fh.readline()
	while '#' in line:
		if not np.any([s in line for s in ['Nsample','ln(Lhood)','Chi2']]):
			labels.append(line[1:].strip())
		line = fh.readline()
cat = np.loadtxt('bayes.dat',usecols=np.arange(1,2+len(labels),1))
cat = cat[cat[:,0]>-1455,1:]
for i, n in enumerate(labels):
	print('%s %.4f %.4f'%(n, np.median(cat[:,i]),np.std(cat[:,i])))


fig = corner.corner(cat, labels=labels)
fig.savefig('corner.png')
