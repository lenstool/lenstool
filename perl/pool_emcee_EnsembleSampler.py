#!/usr/bin/env python
# Example run emcee EnsembleSampler  with lenstool
# see http://dan.iel.fm/emcee/current/api/#the-affine-invariant-ensemble-sampler

import numpy as np
import emcee
import multiprocessing
from multiprocessing import Pool
import configparser
import signal
import sys, os
import lenstool

# Parameter file
if len(sys.argv) != 2 :
    sys.exit("Missing configuration file (e.g. pool_emcee.cfg)");
config = configparser.ConfigParser(inline_comment_prefixes=(';',))
config.read(sys.argv[1])

nwalkers = config.getint("emcee", "nwalkers") # number of walkers 
nburnin  = config.getint("emcee", "nburnin")   # number of iterations for Burn-in phase
nsample  = config.getint("emcee", "nsample")   # number of iterations for Sample phase
nthin    = config.getint("emcee", "nthin")     # take each nthin from sample during Sample phase

global_parfile  = config.get("lenstool", "parfile")
nthreads        = config.getint("lenstool","nthreads")
npools        = config.getint("lenstool","npools")

# Check files
if not os.path.exists(global_parfile):
    print(f"File {global_parfile} does not exist")
    sys.exit(1)

# First, define the probability distribution that we would like to sample.
def lnprob(x):
    if not np.all((x>=0)&(x<=1)):
        return -np.inf

    value = model.rescaleCube(x, return_rescaled=False)
    if value != 1:
        return -np.inf

    error, chi2, lhood0 = model.o_chi_lhood0()
    if error != 0 or chi2 == 0:
        return -np.inf

    prob = -0.5*(chi2 + lhood0)
#    print(f"[{os.getpid()}] {prob} {chi2} {lhood0}") # {x[:1]}")
    return prob

# Use a flat prior
def logp(x):
    return 0.0

# Launch lenstool. Once for the main script, and one subprocess per pool.map instance
os.environ['OMP_NUM_THREADS'] = str(nthreads)
model = lenstool.Lenstool(global_parfile)
model.readConstraints()

# Ignore SIGINT in subprocesses
def sigint_ignore(x,y):
    print(f"[{os.getpid()}] SIGINT ignored in pool and lenstool subprocesses")

signal.signal(signal.SIGINT, sigint_ignore)

# All pool import instances stop here
# Only main script goes beyond
if __name__ == '__main__':

    #function for create random initial positions which have non "-np.inf" fn
    #ndim - ndim
    #nreq - number of points to generate
    #fun_map - map function (can bee mpipool)
    #fn   - lnprob
    def make_noninf_uniform(ndim, nreq, fun_map, fn):
        rez   = np.zeros((nreq, ndim))
        ngood = 0
        print(f"[{os.getpid()}] Create initial points: 0/{nreq}")
        while ngood < nreq:
            ngen = nreq - ngood
            p0 = np.random.uniform(low=0, high=1.0, size=(ngen, ndim))
            l0 = list(fun_map(fn, p0))
            for idx, l in enumerate(l0):
                if l != -np.inf :
                    rez[ngood] = p0[idx]
                    ngood += 1
                    print(f"\r[{os.getpid()}] Create initial points: {ngood}/{nreq}", end='')
        print(f"\n[{os.getpid()}] Done")
        return rez

    def signal_handler_stop_burnin(x,y):
        global stop_burnin
        stop_burnin = True
        print(f"[{os.getpid()}] SIGINT detected. Interrupt burnin after next iteration. CTRL-C again to exit")
        signal.signal(signal.SIGINT, signal_handler_exit)

    def signal_handler_exit(x,y):
        print(f"[{os.getpid()}] SIGINT detected. Close pool and exit")
        pool.close()
        pool.join()
        sys.exit(0)

    multiprocessing.set_start_method('spawn')

    model.write_bayes_header()

    ndim = model.get_NParameters()

    with Pool(npools) as pool:

        # Set signal of the main process only. Pool subprocesses have been forked at this point
        stop_burnin = False
        signal.signal(signal.SIGINT, signal_handler_stop_burnin)
        
        # Choose an initial set of positions for the walkers.
        p0 = make_noninf_uniform(ndim, nwalkers, pool.map, lnprob)
        assert np.all((p0>0) & (p0<1))

        p0 = p0.reshape((nwalkers, ndim))

        # Initialize the sampler with the chosen specs.
        sampler = emcee.EnsembleSampler(nwalkers, ndim, lnprob, pool = pool, moves=[(emcee.moves.DEMove(), 0.8), (emcee.moves.DESnookerMove(), 0.2)])

        # BURN-IN phase
        index = 0
        autocorr = np.empty(nburnin)
        old_tau = np.inf
        tol = 50 # minimum number of autocorrelation times needed to trust the estimate 
                 # https://github.com/dfm/emcee/blob/93bc45ed4e28d9ce82e0308790414872677c85cd/src/emcee/autocorr.py#L49
        
        print(f"[{os.getpid()}] Press CTRL-C to interrupt burnin")
        for state in sampler.sample(p0, iterations=nburnin, progress=True):

            # Write status
            if sampler.iteration % nthin:
                continue

            tau = sampler.get_autocorr_time(quiet=True, tol=tol)
            autocorr[index] = np.mean(tau)
            index += 1

            converged = tau * tol < sampler.iteration
            converged &= np.abs(old_tau - tau) / tau < 0.01
            nconverged = np.count_nonzero(converged)
            if nconverged > 0.8 * ndim: # criteria before jumping to sampling
                break
            old_tau = tau

            print(f"[{os.getpid()}] burning: ", sampler.iteration)
            print(f"[{os.getpid()}] Mean acceptance fraction:", np.mean(sampler.acceptance_fraction))
            print(f"[{os.getpid()}] min/max autocorrelation time: %.2f / %.2f steps"%(np.min(tau),np.max(tau)))
            print(f"[{os.getpid()}] Converged parameters:", nconverged)
            print(f"[{os.getpid()}] min/max prob=", np.amin(state.log_prob), np.amax(state.log_prob)) 

            # ask server to write in burnin.dat
#            print(f"[{os.getpid()}] Ask server to write in burnin.dat")
#            for i, p in enumerate(prob):
#                if np.isfinite(p):
#                    model.write_burnin_line(iteration, p, pos[i])

            if stop_burnin:
                break
        
        print("\n")
        print(f"[{os.getpid()}] Start sampling:")
        
        # Reset the chain to remove the burn-in samples.
        sampler.reset()
        
        # Sample phase
        tol = int(np.min(tau))
        niterations = nsample * tol // nwalkers
        for state in sampler.sample(state, iterations=niterations, progress=True):

            # Write status
            if sampler.iteration % tol:
                continue

            prob = state.log_prob
            for i, p in enumerate(prob):
                coord = state.coords[i]
                if np.all((coord>0)&(coord<1)):
                    model.write_bayes_line(sampler.iteration, p, coord)

            tau = sampler.get_autocorr_time(quiet=True, tol=tol)

            print(f"[{os.getpid()}] sampling: ", sampler.iteration)
            print(f"[{os.getpid()}] Mean acceptance fraction:", np.mean(sampler.acceptance_fraction))
            print(f"[{os.getpid()}] min/max autocorrelation time: %.2f / %.2f steps"%(np.min(tau),np.max(tau)))
            print(f"[{os.getpid()}] min/max prob=", np.amin(prob), np.amax(prob)) 
            
        pool.close()
        pool.join()
