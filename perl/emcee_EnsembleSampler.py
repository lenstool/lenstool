#!/usr/bin/env python
# Example run emcee EnsembleSampler  with lenstool
# see http://dan.iel.fm/emcee/current/api/#the-affine-invariant-ensemble-sampler

# Parameters
nwalkers = 100 # number of walkers

nburnin  = 300  # number of iterations for Burn-in phase
nsample  = 200  # number of iterations for Sample phase
nthin    = 100    # write each nthin from sample during Sample phase

url      =  "ipc:///tmp/sm_lenstool.test"

#                                                                 
#                                                                 

import numpy as np
import emcee
import zmq
import sm_client


sm = sm_client.client(url)
ndim = sm.req_ndim();
print("INFO: Number of free parameters : ", ndim)

sm.req_init_files();

# First, define the probability distribution that we would like to sample.
# Use a flat prior
def lnprob(x):
    lnp =  sm.req_calc_lnprob(x);
    return lnp


# Choose an initial set of positions for the walkers.
print(f"Choose initial values for the {nwalkers} walkers")
p0 = np.random.uniform(low=0, high=1.0, size=(nwalkers, ndim))
for i in range(nwalkers):
    p0[i] = np.random.rand(ndim)
    while (lnprob(p0[i]) == -np.inf):
        p0[i] = np.random.rand(ndim)


# Initialize the sampler with the chosen specs.
sampler = emcee.EnsembleSampler(nwalkers, ndim, lnprob, moves=[(emcee.moves.DEMove(), 0.8), (emcee.moves.DESnookerMove(), 0.2)])

# BURN-IN phase
iteration = 1
for pos, prob, state in sampler.sample(p0, iterations=nburnin//nthin, store=True, thin_by=nthin):
    print("burning: ", iteration*nthin)
    print("Mean acceptance fraction:", np.mean(sampler.acceptance_fraction))
    print("Autocorrelation time: {0:.2f} steps".format(emcee.autocorr.integrated_time(prob)[0]))
    print("min/max prob=", np.amin(prob), np.amax(prob))

    
    # ask server to write in burnin.dat
    print("Ask server to write in burnin.dat")
    for i, p in enumerate(prob):
        sm.req_write_burnin(iteration, p, pos[i])
    iteration = iteration + 1

print("\n")
print("Start sampling:")

# Reset the chain to remove the burn-in samples.
sampler.reset()


# Sample phase
iteration = 1
for pos, prob, state in sampler.sample(pos, prob, state, iterations=nsample//nthin, store=True, thin_by=nthin):
    print("sampling: ", iteration*nthin)
    print("Mean acceptance fraction:", np.mean(sampler.acceptance_fraction))
    print("Autocorrelation time: {0:.2f} steps".format(emcee.autocorr.integrated_time(prob)[0]))
    print("min/max prob=", np.amin(prob), np.amax(prob))
        
    # ask server to write in bayes.dat 
    print("Ask server to write in bayes.dat")
    for i, p in enumerate(prob):
        sm.req_write_bayes(iteration, p, pos[i])
    iteration = iteration + 1
