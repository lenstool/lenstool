% \input macro.tex
\chapter{How to run the \lenstool program}

% ------------------------------------------------------------------------------

\section{Foreword}

This program was born in the ``Laboratoire d'Astrophysique de Toulouse'' during the
course of my Ph.D. Thesis (Kneib 1993). Since then, it has grown a lot with the
great help of Yannick Mellier, and the uses of some numerical subroutines of
Henri Bonnet, Karim Bouyouceff and Jorgen Maeland. More recently, Eric Jullo
has, among other things,
implemented the Bayesian optimisation scheme using the BayeSys MCMC sampler of
John Skilling.\\

I would be most grateful to anybody using this program if they would  send me
their comments and criticisms, and also report any bugs. I will then correct the
program and make it better.\\

My e-mail address is: \textit{jean-paul.kneib@oamp.fr}.

% ------------------------------------------------------------------------------

\section{Compilation and link}

The program is compiled and linked with the \textbf{configure} and \textbf{make}
commands.
It is linked to three libraries :
\begin{description}
\setlength{\itemsep}{-15pt}
\item[libwcs.a]: the Image World Coordinates System Library developed by the 
                 Smithsonian astrophysical Observatory\\
\item[libcfitsio.a]: the library that deals with fits file format developed at 
                     HEASARC. The CFITSIO library version must be at least
                     version 2.510\\
\item[libpgplot.a]: the library, developed by Tim Pearson, that enables the 
                    plots of the Bayesian results\\
\end{description}

Those libraries can be downloaded at : \\
-- http://tdc-www.harvard.edu/software/wcstools\\
-- http://heasarc.gsfc.nasa.gov/docs/software/fitsio/fitsio.html\\
-- ftp://ftp.astro.caltech.edu/pub/pgplot\\

To compile this package, just follow the standard procedure :
\begin{enumerate}
\item `cd' to the directory containing the package

\item Type `./configure' to configure the package for your system.

\item Type `make' to compile the package.
\end{enumerate}

If configure does not find the \textbf{cfitsio}, the \textbf{wcs} or the
\textbf{pgplot} libraries in the standard libraries, it will ask you for them.
You then have to enter the absolute path to these libraries.\\ 
For example:
\begin{verbatim}
 /home/user/cfitsio
 /home/user/wcstool-3.7.2
 /home/user/pgplot
\end{verbatim}
You may also (depending on the system) need to set the environment variables 
\verb+$CFITSIO_DIR+, \verb+$WCS_DIR+, and \verb+$PGPLOT_DIR+ to be the top
level directory for each package.

Once compiled, the binary file is called \textbf{lenstool} and is located in the
\verb+./src+ directory. You can then copy it to your \verb+~/bin+ directory.\\

The \textbf{lenstool\_tab} binary executable is located in the
\verb+./table_src+ directory. You can use it to produce a \verb+lenstool_tab+
binary file used by the NFWg profile. There is an example in the
\verb+./examples_table+ directory.\\

I added also 2 tools: \textbf{Histogram} and \textbf{Histogram2D}, from the 
McAdam package of Phil Marshall. You can use them to visualise the MCMC samples
contained in  the \verb+bayes.dat+ file. Once compiled, they are located in the
\verb+./utils+ directory.\\

The \textsc{perl} visualization tools to display the \lenstool results in  DS9
are in the \verb+./perl+ directory. To use them, you must define the
\verb+LENSTOOL_DIR+ environment variable in your \verb+.tcshrc+ or
\verb+.bashrc+ file and add this \verb+perl+ directory to your \verb+PATH+
environment variable : \\ With the tcsh or csh shell : 
\begin{verbatim}
  setenv LENSTOOL_DIR /home/user/lenstool-6.5
  setenv PATH ${PATH}:${LENSTOOL_DIR}/perl:${LENSTOOL_DIR}/utils
\end{verbatim}
With the bash shell :
\begin{verbatim}
  export LENSTOOL_DIR=/home/user/lenstool-6.5
  export PATH=${PATH}:${LENSTOOL_DIR}/perl:${LENSTOOL_DIR}/utils
\end{verbatim}

The distribution has been tested on :
\begin{itemize}
\item Linux Fedora Core 3, Suse
\item Mac Tiger 10.4
\item Mac OS X
\end{itemize}
\newpage
\section{Run}

\lenstool is run easily by typing the command:\\

\texttt{lenstool} \textit{filename}\\

The \textit{filename} is the basename of the inputfile named
"\textit{filename}.par". \\

The first step of the \lenstool program is to edit the inputfile using
the ``vi'' editor, so that the user can change one or many parameters of the 
inputfile.\\

The following steps correspond to what have been asked in the inputfile.\\

The lens tool program creates different files: data files that usually
have the extension ".dat" and a parameter file "para.out" listing the
values read from  the inputfile (this is a good way to check
if the program has understood the inputfile).

% ------------------------------------------------------------------------------
%---------------------------------------------------------------------------

\section{Troubleshooting/FAQ}

%---------------------------------------------------------------------------
\textbf{I got the following error when I launch Histogram or Histogram2D:}\\

\textit{./Histogram: error while loading shared libraries: libpgplot.so: cannot
open shared object file: No such file or directory}\\

This means that your \textbf{LD\_LIBRARY\_PATH} environment variable doesn't
point to the directory containing the libpgplot.so file. You may set the
variable with this bash command: 
\begin{verbatim}
export LD_LIBRARY_PATH=<directory containing libpgplot.so>
\end{verbatim}
or in the csh shell:
\begin{verbatim}
setenv LD_LIBRARY_PATH <directory containing libpgplot.so> 
\end{verbatim}

%---------------------------------------------------------------------------
\textbf{I got the following error when I compile Histogram and Histogram2D
tools:}\\ 
\textit{ld: Undefined symbols: .objc\_class\_name\_AQTAdapter\ldots}\\

Change the file \verb+utils/Makefile.am+ in the following way:
\begin{verbatim}
Histogram_LDFLAGS =  -L/scisoft/i386/Packages/pgplot-5.2.2/ -L/usr/X11R6/lib 
-Wl,-framework -Wl,Foundation -lpgplot -lgfortran -laquaterm -lX11 -lpng 
-lcc_dynamic 
Histogram_LDADD =
\end{verbatim}
Then, run \verb+make+ again in the \verb+utils+ directory. 
Make sure you are compiling with either the 
\texttt{g77} or \texttt{f77} compiler! \\
On MAC OS X 10.5.2 Leopard, \verb+-lcc_dynamic+ option is not
required.\\

%---------------------------------------------------------------------------
\textbf{I got the following error when I compile Histogram and
Histogram tools:}

\noindent
\textit{Error on line 638: Declaration error for x: adjustable dimension on
non-argument\\}
\textit{Error on line 638: wr\_ardecls:  nonconstant array size\\ }

You are probably using the f2c compiler. This is not supported by
\lenstool! The solution is to change to the g77 compiler\ldots\\

%---------------------------------------------------------------------------
\textbf{I want to compile with the \emph{icc} compiler}\\

Define the \verb+CC+ environment variable as follow before running \verb+./configure+ \\
in csh shell : \verb+setenv CC icc+ \\
or in bash shell : \verb+export CC=icc+ \\

The same for the \textsc{fortran} compiler : \\
in csh shell : \verb+setenv F77 g77+ \\
or in bash shell : \verb+export CC=g77+ \\ 

\textbf{I got an autoconf version error when I run the ./configure script}

In the distribution, I provide a configure.in script, which is used to
build the ./configure script. You can rebuild the ./configure script
by typing the command : \\
\verb+aclocal+ \\
\verb+autoconf+ \\
