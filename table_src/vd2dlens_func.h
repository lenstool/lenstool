extern double menclj(double mtol, double cdl, double r_j, double reval);
extern double angdist(double z1, double z2, double omm, double oml, double lilh);
extern double rhocrit(double lilh, double omegam, double omegal, double z);
extern double mencd(double r);
extern double sigRj_dm(double r);
extern double sigRj_lum(double reval,double lum,double mtol,double r_j,double ranis);
extern double nsigPj(double t);
extern double sigseeing(double sigPj[], double rad[], int n, double radcent,double cing, double lum, double r_s, double r_j, double mtol,int lumchoice);
extern double intjaffe(double lum, double dmscale, double hmscale, double x);
extern double intseeing(double radcent,double cing, double lum, double r_s, double r_j, double mtol,int lumchoice);
extern double sigbinning(double sigseeing[],double rad[],double intseeing[],int n, double xmin, double xmax, double slitsize);
extern double rhohern(double mtol, double lum, double r_h, double reval);
extern double intenshern(double lum, double hmscale, double s);
extern double sigRh(double rhocrit, double rhosc, double rsc, double zbeta, double r_h,double ranis, double mtol,double lum, double reval);
extern double nsigPh(double reval, double r_j, double ranis, double lum, double r_s, double rhocrit, double rhosc, double zbeta, double mtol);
extern double sigcrit(double Ds, double Dl, double Dls);
