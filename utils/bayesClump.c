#include<stdio.h>
#include<signal.h>
#include<stdlib.h>
#include<math.h>
#include<string.h>
#include <float.h>
#include "dimension.h"
#include "structure.h"
#include "constant.h" 
#include "fonction.h"

/******************************************************************************
 * Name: bayesCl.c
 * Authors: JR
 * Date: 03/08/16
 * 
 * Analyse a bayes.dat file and for each line, compute the critical lines specified
 * in the cline section.
 *
 * syntax : bayesCl <.par> 
 *
 * Input : The bayes.dat file in the current directory is used.
 * Output in tmp/ directory
 *
 * Existing files are skipped.
 *
 ******************************************************************************/

typedef void (*sighandler_t)(int);
static void signalReset();
int optInterrupt;
extern struct  g_mode  M;
extern struct g_grille  G;
extern struct pot       lens[NLMAX];
extern struct biline    radial[NMAX], tangent[NMAX];
extern int    nrline, ntline;


int main( int argc, char** argv )
{
    extern struct g_cosmo C;
	char fname[50]; // <map><ival>.fits
    long int iVal, nVal;  // size of array
    double **array;
    int nParam;
    FILE    *CLUMP;
	int i,j;
	double *index;  // list of bayes.dat lines
	int    seed;   // random seed
	int tmp;
        double q, a, b;

	// Check the arguments
	if( argc < 2 || 
	    strstr( argv[1], ".par" ) == NULL )
	{
		fprintf(stderr, "Syntax : bayesClump <.par>\n");
		return -1;
	}

	// Read the .par file
	init_grille( argv[1], "para.out", 1);



	// Read catalog of multiple images 
	readConstraints();

	// Initialise the grid
	if( G.pol != 0 )
		gridp();
	else
		grid();
	
	// Switch to silent mode
	M.verbose = 0;

	//if( G.nmsgrid != G.nlens ) 
	//	prep_non_param();
	
	// Read the bayes.dat file
	array = readBayesModels(&nParam, &nVal);
	if( array == NULL )
	{
		fprintf(stderr, "ERROR: bayes.dat file not found\n");
		return -1; 
	}

	// Create the ./tmp directory
	i = system("mkdir -p tmp");
	
	// Prepare the index list
	index = (double *) malloc((unsigned) nVal*sizeof(double));
	for( i = 0 ; i < nVal ; i++ ) index[i]=i;
	seed = -2; 

	// Handle CTRL-C to interrupt the optimisation but not lenstool
	signal(SIGINT, signalReset);
	optInterrupt = 0;

	// Loop over each line
	for( i = 0; i < nVal && !optInterrupt && i < 2000; i++ )
	{

		// Randomly draw a line from index array 
		tmp = (int) floor(d_random(&seed) * (nVal - i));
		iVal = index[i+tmp];
		// and swap the indexes in the index list
		index[i+tmp] = index[i]; 		
		
		// Set the lens parameters from <array>
		setBayesModel( iVal, nVal, array );
                sprintf( fname, "tmp/clump%ld.dat",iVal);

                printf("INFO: Compute file %d/%ld : %s [CTRL-C to interrupt]\n",i+1, nVal,fname);
    
		fflush(stdout);
		CLUMP = fopen( fname, "r" );
		if( CLUMP == NULL ) 
		{
			CLUMP = fopen( fname, "w");
			fprintf( CLUMP, "busy\n" );
			fclose(CLUMP);
    	                CLUMP = fopen(fname, "w");

                        if ( M.iref != 2 )
                           fprintf(CLUMP, "#REFERENCE 3 %.7lf %.7lf\n", M.ref_ra, M.ref_dec);
                        else
                           fprintf(CLUMP, "#REFERENCE 2 %.7lf %.7lf\n", M.ref_ra, M.ref_dec);

                        for (i = 0; i < G.nlens; i++)
                        {
                           q = sqrt((1 - lens[i].emass) / (1 + lens[i].emass));
                           if ( lens[i].rc > 5 && lens[i].rcut != DBL_MAX )
                               b = lens[i].rc;
                           else
                               b = lens[i].rcut;

                           a = b / q;
                           lens[i].rcutkpc=lens[i].rcut* d0 / C.h * distcosmo1(lens[i].z);
                           if ( lens[i].rcut != DBL_MAX )
                               q = lens[i].rcutkpc;
                           else
                               q = 0;
                           fprintf(CLUMP,"%2d %7.2lf %7.2lf %6.2lf %6.2lf %7.3lf %7.2lf %7.3lf %7.3lf %6.1lf %5.3lf %6.2lf %s %d\n", lens[i].type, lens[i].C.x, lens[i].C.y, a, b, lens[i].theta*RTD, lens[i].emass, lens[i].rckpc, q, lens[i].sigma, lens[i].z, lens[i].mag, lens[i].n, i);

                        }
                        fclose(CLUMP);
                }
                else
                        fclose(CLUMP);


        }
	printf( "\n" ); 	
	free( array );  	
	return 0;       	
}

static void signalReset()
{
	signal(SIGINT, SIG_DFL);
	optInterrupt = 1;
	printf( "\nINFO: Optimisation interrupted by CTRL-C\r");
}
