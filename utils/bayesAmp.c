#include<stdio.h>
#include<signal.h>
#include<stdlib.h>
#include<string.h>
#include<math.h>
#include "dimension.h"
#include "structure.h"
#include "constant.h" 
#include "fonction.h"

/******************************************************************************
 * Name: bayesAmp.c
 * Authors: EJ
 * Date: 30/10/07
 * Date: 21/09/14  Add image mode amplification
 * 
 * Analyse a bayes.dat file and print the absolute value of the amplification 
 * for each optimised system or image.
 *
 * syntax : bayesAmp <.par> 
 *
 * The bayes.dat file in the current directory is used.
 * Output : none
 *
 ******************************************************************************/
typedef void (*sighandler_t)(int);
static void signalReset();
int optInterrupt;

void help_msg()
{
	fprintf(stderr, "Syntax : bayesAmp [-i] <.par>\n");
    fprintf(stderr, "\t-i\n\t\tcompute the amplification per image (default per system)\n");
	exit(-1);
}

int main( int argc, char** argv )
{
    extern struct g_image I;
    extern struct galaxie multi[NFMAX][NIMAX];
    extern struct z_lim   zlim[NFMAX];
	double **array; // contains the bayes.dat data
	double amp;
	int nParam;  // size of array
	long int iVal, nVal; 
    int i, j;
    int im_mode; 
    char *pch;

	// Check the arguments
	if( argc < 2 )
        help_msg();

    im_mode = 0;  // default per system amplification
    while( argv[1][0] == '-' )
    {
		pch = argv[1];
		if( pch[1] == 'i' )
            im_mode = 1;
		for( i = 1; i < argc-1 ; i++ )
			argv[i]=argv[i+1];
		argc--;
    }

	if( strstr(argv[1], ".par") == NULL )
        help_msg();

	// Read the .par file
	init_grille( argv[1], "para.out", 1);
	
	// Read the image file
	readConstraints();

	// Print header with redshifts
	printf( "#Nsample\n" );
    if( im_mode == 1 )
    	for( i = 0; i < I.n_mult; i++ )
            for( j = 0; j < I.mult[i]; j++ )
        	{
                if( zlim[i].bk != 0 ) multi[i][j].z = 0.;
        		printf( "#Amplification of %s : %f\n", 
        			multi[i][j].n, multi[i][j].z );
        	}
    else
    	for( i = 0; i < I.n_mult; i++ )
    	{
            if( zlim[i].bk != 0 ) multi[i][0].z = 0.;
    		printf( "#Amplification of %s : %f\n", 
    			multi[i][0].n, multi[i][0].z );
    	}

	// Read the bayes.dat file
	array = readBayesModels(&nParam, &nVal);
	if( array == NULL )
	{
		fprintf(stderr, "ERROR: bayes.dat file not found\n");
		return -1; 
	}

	// Handle CTRL-C to interrupt the optimisation but not lenstool
	signal(SIGINT, signalReset);
	optInterrupt = 0;

	// Loop over each line
    if( im_mode == 1)
    	for( iVal = 0; iVal < nVal && !optInterrupt; iVal++ )
    	{
    		// Set the lens parameters from <array>
    		setBayesModel( iVal, nVal, array );
    
    		printf( "%ld ", iVal );
    		// Print the averaged amplification for each system
    		for( i = 0; i < I.n_mult; i++ )
    		{
    			for( j = 0; j < I.mult[i]; j++ )
                {
    				amp = fabs(1./e_amp(&multi[i][j].C,multi[i][j].dl0s,multi[i][j].dos,multi[i][j].z));
    			    printf( "%f ", amp );
                }
    
    		}
    		printf("\n");
    	}
    else
    	for( iVal = 0; iVal < nVal && !optInterrupt; iVal++ )
    	{
    		// Set the lens parameters from <array>
    		setBayesModel( iVal, nVal, array );
    
    		printf( "%ld ", iVal );
    		// Print the averaged amplification for each system
    		for( i = 0; i < I.n_mult; i++ )
    		{
    			amp = 0.;
    			for( j = 0; j < I.mult[i]; j++ )
    				amp += fabs(1./e_amp(&multi[i][j].C,multi[i][j].dl0s,multi[i][j].dos,multi[i][j].z));
    
    			amp /= I.mult[i];
    			printf( "%f ", amp );
    		}
    		printf("\n");
    	}
	free( array );
	return 0;
}


static void signalReset()
{
	signal(SIGINT, SIG_DFL);
	optInterrupt = 1;
	printf( "\nINFO: Optimisation interrupted by CTRL-C\r");
}
