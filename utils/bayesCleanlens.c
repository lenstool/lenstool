#include<stdio.h>
#include<signal.h>
#include<stdlib.h>
#include<math.h>
#include<string.h>
#include "dimension.h"
#include "structure.h"
#include "constant.h" 
#include "fonction.h"

/******************************************************************************
 * Name: bayesCleanlens.c
 * Authors: JR
 * Date: 03/08/16
 * 
 * Analyse a bayes.dat file and for each line, compute the source reconstruction specified
 * in the cleanlens section.
 *
 * syntax : bayesCleanlens <.par> 
 *
 * Input : The bayes.dat file in the current directory is used.
 * Output in tmp/ directory
 *
 * Existing files are skipped.
 *
 ******************************************************************************/

typedef void (*sighandler_t)(int);
static void signalReset();
int optInterrupt;
struct  g_mode  M;
struct  biline  radial[NMAX], tangent[NMAX];
int      nrline, ntline;
struct point gsource_global[NGGMAX][NGGMAX]; //for cleanset 5


int main( int argc, char** argv )
{
    extern struct g_grille G;
    extern struct g_pixel  imFrame, ps;
	char fname[50]; // <map><ival>.fits
    long int iVal, nVal;  // size of array
    double **array;
    int nParam;
    FILE *pFile;
	int i,j;
	double *index;  // list of bayes.dat lines
	int    seed;   // random seed
	int tmp;

	// Check the arguments
	if( argc < 2 || 
	    strstr( argv[1], ".par" ) == NULL )
	{
		fprintf(stderr, "Syntax : bayesCl <.par>\n");
		return -1;
	}

	// Read the .par file
	init_grille( argv[1], "para.out", 1);



	// Read catalog of multiple images 
	readConstraints();

	// Initialise the grid
	if( G.pol != 0 )
		gridp();
	else
		grid();
	
	// Switch to silent mode
	M.verbose = 0;

	//if( G.nmsgrid != G.nlens ) 
	//	prep_non_param();
	
	// Read the bayes.dat file
	array = readBayesModels(&nParam, &nVal);
	if( array == NULL )
	{
		fprintf(stderr, "ERROR: bayes.dat file not found\n");
		return -1; 
	}

	// Create the ./tmp directory
	i = system("mkdir -p tmp");
	
	// Prepare the index list
	index = (double *) malloc((unsigned) nVal*sizeof(double));
	for( i = 0 ; i < nVal ; i++ ) index[i]=i;
	seed = -2; 

	// Handle CTRL-C to interrupt the optimisation but not lenstool
	signal(SIGINT, signalReset);
	optInterrupt = 0;

        ps.pixfile[strlen(ps.pixfile)-5]=0;

        if ( M.iclean == 1 && imFrame.format !=0)
        {
 	// Loop over each line
           for( i = 0; i < nVal && !optInterrupt && i < 2000; i++ )
	   {

		// Randomly draw a line from index array 
		tmp = (int) floor(d_random(&seed) * (nVal - i));
		iVal = index[i+tmp];
		// and swap the indexes in the index list
		index[i+tmp] = index[i]; 		
		
		// Set the lens parameters from <array>
		setBayesModel( iVal, nVal, array );

                sprintf( fname, "tmp/%s%ld.fits",ps.pixfile,iVal);

    	        FILE    *OUT;

                printf("INFO: Compute file %d/%ld : %s [CTRL-C to interrupt]\n",i+1, nVal,fname);
    
		fflush(stdout);
		pFile = fopen( fname, "r" );
		if( pFile == NULL ) 
		{
			pFile = fopen( fname, "w");
			fprintf( pFile, "busy\n" );
			fclose(pFile);
                        imtosou(M.zclean, fname);
		}
		else
			fclose(pFile);


            }

        }
        printf( "\n" ); 	
        free( array );  	
        return 0;       	
}

static void signalReset()
{
	signal(SIGINT, SIG_DFL);
	optInterrupt = 1;
	printf( "\nINFO: Optimisation interrupted by CTRL-C\r");
}
