#include<stdio.h>
#include<signal.h>
#include<stdlib.h>
#include<string.h>
#include<math.h>
#include "dimension.h"
#include "structure.h"
#include "constant.h"
#include "fonction.h"

/******************************************************************************
 * Name: bayesCov.c
 * Authors: EJ
 * Date: 03/16/10
 *
 * Analyse a bayes.dat file and for each model, compute the distance between
 * the barycenter and the predicted source, and write the (dx,dy) vector for
 * for each image in bayesCov.dat. This file can then be used to compute
 * a covariance matrix. 
 *
 * syntax : bayesCov <.par>
 *
 * The bayes.dat file in the current directory is used.
 * Output : bayesCov.dat
 *
 ******************************************************************************/

typedef void (*sighandler_t)(int);
static void signalReset();
int optInterrupt;

void help_msg()
{
    fprintf(stderr, "Syntax : bayesCov <.par>\n");
    exit(0);
}

/* Print a line of bayesGrad.dat
 */
void printLine(FILE *bayes, double *vartotx, double *vartoty)
{
    extern struct g_image I;
    extern struct galaxie multi[NFMAX][NIMAX];
    int i, j;
    double dx, dy, dx0;
    double MA, MB, MC, MD;
    struct point Ps[NIMAX], Bs;

    // Compute amplification matrix per image
    amplif_mat();

    // for each system
    for ( i = 0 ; i < I.n_mult; i++ )
    {
        o_dpl(I.mult[i], multi[i], Ps);

        /*Find the barycenter position of the computed sources*/
        if (I.forme == 4 || I.forme == 6)
            Bs = weight_baryc(Ps, multi[i], I.mult[i]);
	else if ( I.forme == -2 ) /* barycenter weighted by amplification */
	    Bs=weight_baryc_amp(Ps,multi[i],I.mult[i] , i); //i==n_familly
        else
            Bs = bcentlist(Ps, I.mult[i]);  /* barycentre */

        // for each image in a system
        for ( j = 0 ; j < I.mult[i] ; j++ )
        {
            dx = Bs.x - Ps[j].x;
            dy = Bs.y - Ps[j].y;

            // Convert error to image plane
            MA = amplifi_mat[i][j].a;
            MB = amplifi_mat[i][j].b;
            MC = amplifi_mat[i][j].c;
            MD = amplifi_mat[i][j].d;
            dx0 = dx;
            dx = MA * dx0 + MD * dy;
            dy = MB * dx0 + MC * dy;
            *vartotx += dx * dx;
            *vartoty += dy * dy;

            fprintf(bayes, " %lf %lf", dx, dy);
        }
    }

    fprintf( bayes, "\n" );
}

int main( int argc, char** argv )
{
    double **array; // contains the bayes.dat data
    int nParam, i, j, nimages;  // size of array
    long int    iVal, nVal;
    FILE   *bayes;
    double *index;  // list of bayes.dat lines
    double vartotx, vartoty;
    int    seed;   // random seed
    char   name[20];
    int tmp;

    // Check the arguments
    if ( argc < 1 )
        help_msg();

    if ( strstr(argv[1], ".par") == NULL )
        help_msg;

    // Read the .par file
    init_grille( argv[1], "para.out", 1);

    // Read constraints
    readConstraints();

    if ( G.nmsgrid != G.nlens )
    {
        prep_non_param();
    }

    // Read the bayes.dat file
    array = readBayesModels(&nParam, &nVal);
    if ( array == NULL )
    {
        fprintf(stderr, "ERROR: bayes.dat file not found\n");
        return -1;
    }

    // Write the header of the bayesGrad.dat file
    bayes = fopen( "bayesCov.dat", "w" );

    fprintf( bayes, "#Nsample\n");
    fprintf( bayes, "#Chi2\n");

    nimages = 0;
    for ( i = 0 ; i < I.n_mult ; i++ )
        for ( j = 0; j < I.mult[i]; j++ )
        {
            sprintf( name, "#dx of image %s\n", multi[i][j].n );
            fprintf( bayes, name );
            sprintf( name, "#dy of image %s\n", multi[i][j].n );
            fprintf( bayes, name );
            nimages ++;
        }

    // Handle CTRL-C to interrupt the optimisation but not lenstool
    signal(SIGINT, signalReset);
    optInterrupt = 0;

    // Loop over each line
    vartotx = vartoty = 0.;
    for ( iVal = 0; iVal < nVal && !optInterrupt; iVal++ )
    {
        // Set the lens parameters from <array>
        setBayesModel( iVal, nVal, array );

        fprintf( bayes, "%d %lf", iVal, array[0][iVal] );
        printLine(bayes, &vartotx, &vartoty);
        printf( "INFO: Compute the bayesCov.dat file %d/%d  RMS_dx %f RMS_dy %f[CTRL-C to interrupt]\r", iVal + 1, nVal, sqrt(vartotx/(iVal+1)/nimages), sqrt(vartoty/(iVal+1)/nimages));
        fflush(stdout);
    }
    printf( "\n" );
    fclose(bayes);
    free( array );
    return 0;
}

static void signalReset()
{
    signal(SIGINT, SIG_DFL);
    optInterrupt = 1;
    printf( "\nINFO: Optimisation interrupted by CTRL-C\r");
}
