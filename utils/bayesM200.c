#include<stdio.h>
#include<signal.h>
#include<stdlib.h>
#include<string.h>
#include<math.h>
#include<sys/stat.h>
#include "dimension.h"
#include "structure.h"
#include "constant.h"
#include "fonction.h"
#include <gsl/gsl_interp.h>
#include <gsl/gsl_spline.h>
#include <gsl/gsl_deriv.h>
#include <gsl/gsl_min.h>
#include <gsl/gsl_roots.h>
#include <gsl/gsl_integration.h>

/******************************************************************************
 * Name: bayesM200.c
 * Authors: EJ
 * Date: 23/03/2015
 *
 * Analyse a bayes.dat file and for each model, compute the mass m200 of each 
 * clump. 
 * 
 * syntax : bayesM200 <.par>
 *
 * The bayes.dat file in the current directory is used.
 * Output : bayesM200dat
 *
 ******************************************************************************/

typedef void (*sighandler_t)(int);
static void signalReset();
int optInterrupt;

long int g_ilens; // lens to use in f_piemd
double g_rho_m;     // 200 times critical matter density at the cluster redshift
double g_arc2kpc, g_dl;  // convert from arcsec to kpc

/* Structure for parameters of brent minimisation methods
 */ 
struct brent_trad_par
{
    double kap1;
    long int ilens;
};

void help_msg()
{
	fprintf(stderr, "Syntax : bayesM200 [OPTIONS] <.par>\n");
    fprintf(stderr, "Available OPTIONS:\n");
    fprintf(stderr, " -m 1: compute tidal mass (dPIE) with Tormen+99 analytic expression, 2: compute total mass (dPIE), 3: compute tidal mass (dPIE) with 3D density Springel+08 method, otherwise: compute m200.\n");
    fprintf(stderr, " -i <potential id min>,<potential id max> : range of potentials to compute. Id min and Id max are the names of potentials. Potentials before Id min are considered part of the main halo\n");
    fprintf(stderr, " -p profile.dat : ASCII file containing the profile of the cluster smooth component for tidal mass calculation. Format: x_kpc m_msun\n");
    fprintf(stderr, " -c <x>,<y> : Center of the smooth component profile in arcsec relative to mod.par reference position\n");
    exit(-1);
}



/* Return the mass inside a circular aperture (Valid of dPIE only)
 * param r : aperture radius in kpc
 * return the mass in Msun
 */ 
double maper(double r, long int ilens)
{
    extern struct g_cosmo C;
    extern struct pot     lens[NLMAX];
    double maper;
    double r_arcsec;
    double rc, rcut, sig;
    //r_arcsec = r / g_arc2kpc;
    //struct point p, grad;
    //p.x = r_arcsec + lens[ilens].C.x;
    //p.y = lens[ilens].C.y;
    //grad = e_grad_pot(&p, ilens);
    //maper = grad.x/r_arcsec;  // kappa_bar(<r)
    //maper = maper * 1E12 * cH0_4piG * C.h / g_dl;  // sigma_bar(<r) in Msun/kpc^2
    //maper = maper * PI * r * r;  // in Msun
    switch(lens[ilens].type)
    { 
        case(16): // hernquist
            rc = lens[ilens].rc * g_arc2kpc; // in kpc
            maper = 6.*lens[ilens].sigma*lens[ilens].sigma/PI/8.*INVG*rc*rc/g_dl/D0Mpc*C.h/1E6;
            //maper = lens[ilens].b0/8./PI/PI*INVG*rc*rc*vol*vol/g_dl/D0Mpc*C.h/180./3600.*PI/1E6;
            maper *= r/(r+rc);  // Hernquist1990 Eq 3
            maper *= r/(r+rc);
            break;
        default:
            sig = lens[ilens].sigma;
            rc = lens[ilens].rc * g_arc2kpc;
            rcut = lens[ilens].rcut * g_arc2kpc;
            maper = PI*sig*sig*INVG/1E3*rcut*(1.-(sqrt(rcut*rcut+r*r)-sqrt(rc*rc+r*r))/(rcut-rc));
            break;
    }
    return maper;
}

/* Return the total mass of a dPIE profil
 *  param ilens: the id of the potential
 */
double mtot(long int ilens)
{
    extern struct pot lens[NLMAX];
    double mtot;
    double rc, rcut, sig;
    sig = lens[ilens].sigma;
    rc = lens[ilens].rc * g_arc2kpc;
    rcut = lens[ilens].rcut * g_arc2kpc;
    mtot = PI*sig*sig*INVG/1E3*rcut*rcut/(rcut+rc);
    return mtot;
}


/* Function for bisection root finding that gives the
 * difference between global variable rho_m and the
 * 3D density rho = Maper(r) / Vol(r),
 * where vol(r) is the volume of a sphere of radius r in kpc.
 */ 
double f_maper(double r)
{
    double vol_r = 4.*PI*r*r*r/3.;  // in kpc^3
    double fc = fabs(maper(r,g_ilens)/vol_r - g_rho_m);
    return fc;
}

/* Return the radius (in kpc) within which the average density is 200 times the 
 * critical matter density at the cluster redshift
 */
double rho_radius(long int ilens)
{
    double r_aper;
    g_ilens = ilens; r_aper = 0;
    brent(10., 1000., 5000., f_maper, 0.001, &r_aper);

    if(r_aper > 5000. || r_aper < 10.) 
        printf("WARNING: lens[%ld] estimated r200 out of bounds (r200=%lf)\n",ilens,r_aper);
    return r_aper;
}

struct curve
{
    gsl_interp_accel *acc;
    gsl_spline *f_spline;
    gsl_spline *logf_spline;
    gsl_spline *dlfdlr_spline;
    double xcent, ycent;
};

/* Function that returns the interpolation of the profile at a radius
 */ 
double f_f(double x, void *params)
{
    return gsl_spline_eval(((struct curve *)params)->f_spline, x, ((struct curve*)params)->acc);
}

/* Function that returns the interpolation of the log of the profile at the log of a radius
 */ 
double f_logf(double logx, void *params)
{
    return gsl_spline_eval(((struct curve *)params)->logf_spline, logx, ((struct curve*)params)->acc);
}

/* Function that returns the interpolation of the dln(profile)/dln(r) at a radius
 */ 
double f_dlfdlr(double logx, void *params)
{
    return gsl_spline_eval(((struct curve *)params)->dlfdlr_spline, logx, ((struct curve*)params)->acc);
}

/* Read a curve profile from an ASCII catalog in format "x_i y_i"
 * with x_i in kpc. It contains the number of elements on the first line.
 * Return a pointer to an initialized curve structure
 */
struct curve* init_curve(char *filename, double xcent, double ycent)
{
    int N, i; 
    char *line;
    FILE *fh;
    fh = fopen(filename, "r");
    line = malloc(256*sizeof(char));
    getline(&line,(size_t*)&N,fh);
    while(line[0]=='#') getline(&line,(size_t*)&N,fh);
    i = sscanf(line, "%d", &N);
    free(line);
    if(i == 0)
    {
        fprintf(stderr, "ERROR reading %s\n", filename);
        fclose(fh);
        exit(0);
    } else 
        fprintf(stderr, "INFO reading %d lines from %s\n",N, filename);

    double *a_x = malloc(N*sizeof(double));
    double *a_y = malloc(N*sizeof(double));
    double *logx = malloc(N*sizeof(double));
    double *logy = malloc(N*sizeof(double));
    double x, y;
    for(i=0; i<N; i++)
    {
        fscanf(fh, "%lf %lf\n", &x, &y);
        a_x[i] = x;
        a_y[i] = y;
        logx[i] = log(x);
        logy[i] = log(y);
    }
    fclose(fh);

    struct curve *mycurve = malloc(sizeof(struct curve));
    mycurve->acc = gsl_interp_accel_alloc();
    mycurve->f_spline = gsl_spline_alloc(gsl_interp_cspline,N);
    gsl_spline_init(mycurve->f_spline, a_x, a_y, N);

    mycurve->logf_spline = gsl_spline_alloc(gsl_interp_cspline,N);
    gsl_spline_init(mycurve->logf_spline, logx, logy, N);
    
    // Create derivative dln(profile)/dln(r)
    gsl_function F;
    F.function = &f_logf;
    F.params = mycurve;
    double *dlfdlr = malloc(sizeof(double)*(N-2));
    double abserr;
    for(i=0;i<N-2; i++)
    {
        gsl_deriv_central(&F, logx[i+1], 1e-8, &dlfdlr[i], &abserr);
        logx[i] = logx[i+1];
    }


    mycurve->dlfdlr_spline = gsl_spline_alloc(gsl_interp_cspline,N-2);
    gsl_spline_init(mycurve->dlfdlr_spline, logx, dlfdlr, N-2);

    mycurve->xcent = xcent;
    mycurve->ycent = ycent;
    free(a_x);
    free(a_y);
    free(logx);
    free(logy);
    return mycurve;
}

/* Free a curve structure
 * Return 1 if everything is properly freed.
 */
int free_curve(struct curve * mycurve)
{
    gsl_spline_free(mycurve->f_spline);
    gsl_spline_free(mycurve->logf_spline);
    gsl_spline_free(mycurve->dlfdlr_spline);
    gsl_interp_accel_free(mycurve->acc);
    free(mycurve);
    return 1;
}

/* Return the tidal radius of a subhalo (in kpc)
 * Arguments:
 *  - ilens: long int
 *       Index of the subhalo
 *  - mycurve: struct curve
 *       Initialized mass profile of the smooth component of the cluster
 */
double tradius_analytic(long int ilens, struct curve *mycurve)
{
    extern struct pot lens[NLMAX];
    int i;
    double msub, rsub, rt;
    double dx = lens[ilens].C.x - mycurve->xcent;
    double dy = lens[ilens].C.y - mycurve->ycent;
    rsub = sqrt(dx*dx + dy*dy) * g_arc2kpc;

    if(log(rsub) < mycurve->dlfdlr_spline->interp->xmin || log(rsub) > mycurve->dlfdlr_spline->interp->xmax)
        return -1;

    double mvir = exp(f_logf(log(rsub), mycurve));
    double dlfdlr = f_dlfdlr(log(rsub), mycurve);
    if( dlfdlr > 2 )
        return -1;

    // Iterate from r200 to find rt
    rt = rho_radius(ilens);  // r200 in kpc
    for(i=0;i<6;i++)
    {
        msub = maper(rt, ilens);
        rt = rsub * pow(msub / (2. - dlfdlr) / mvir, 1./3.);
    }

    return rt;
}

/* Functions to be minimized 
 * Compute the mean density of a lens in radius r and compare to a density coming from params
 * Args:
 *  - r: radius in arcsec
 *  - params: pointer to a brent_trad_par structure
 * Returns: 
 *  - positive value when the subhalo mean density is greater than 2% of the host mean density
 */ 
double f_root_trad_dens(double r, void *params)
{
    double rho_sub; 
    long int ilens = ((struct brent_trad_par *)params)->ilens;
    double kap1 = ((struct brent_trad_par *)params)->kap1; //mean density of host at subhalo position
    rho_sub = maper(r, ilens) / r/r/r;

    return rho_sub/kap1 - 0.02; // rho_sub/rho_loc = 0.02 Springel+08 Acquarius Fig14
}

/* Return the tidal radius of a subhalo (in kpc) based in the 3D mean density of the host and subhalos
 * Arguments:
 *  - ilens: long int
 *       Index of the subhalo
 *  - mycurve: struct curve
 *       Initialized mass profile of the smooth component of the cluster
 */
double tradius_density(long int ilens, struct curve *mycurve)
{
    extern struct pot lens[NLMAX];
    int i;
    int status;
    double rsub, rt, rho_sub;
    double dx = lens[ilens].C.x - mycurve->xcent;
    double dy = lens[ilens].C.y - mycurve->ycent;
    rsub = sqrt(dx*dx + dy*dy) * g_arc2kpc;

    if(log(rsub) < mycurve->dlfdlr_spline->interp->xmin || log(rsub) > mycurve->dlfdlr_spline->interp->xmax)
        return -1;

    double rho_host = exp(f_logf(log(rsub), mycurve)) / (rsub*rsub*rsub);


    // Iterate from r200 to find rt
    rt = rho_radius(ilens);  // r200 in kpc
    rho_sub = maper(rt, ilens)/rt/rt/rt; // rho_r200
 
    // Find the radius rt at which mean_kap(<rt) - kap1 = 0, using Brent minimization
    const gsl_root_fsolver_type *T = gsl_root_fsolver_bisection;
    gsl_root_fsolver *s = gsl_root_fsolver_alloc(T);
    gsl_function F;
    F.function = &f_root_trad_dens;
    struct brent_trad_par intg_par;
    intg_par.kap1 = rho_host;
    intg_par.ilens = ilens;
    F.params = &intg_par;

    double a, b;
    a = 1.;
    b = 1000.;
    status = gsl_root_fsolver_set(s, &F, a, b);
    int iter = 0;
    do
    {
        iter++;
        status = gsl_root_fsolver_iterate(s);
        a = gsl_root_fsolver_x_lower(s);
        b = gsl_root_fsolver_x_upper(s);        
        status = gsl_root_test_interval(a, b, 0., 0.01); // check 1% relative precision on root
    }
    while(status==GSL_CONTINUE && iter < 30);
    a = gsl_root_fsolver_root (s);
    gsl_root_fsolver_free(s);

    return a;
}


/* Functions to be minimized 
 * Compute the mean convergence inside radius r 
 * Args:
 *  - r: radius in arcsec
 *  - params: pointer to a brent_trad_par structure
 * Returns: 
 *  - the difference between the mean convergence inside r, and the main halo
 *  convergence at the subhalo position.
 */ 
double f_brent_trad(double r, void *params)
{
    extern struct pot lens[NLMAX];
    double kap_sub; 
    long int ilens = ((struct brent_trad_par *)params)->ilens;
    double kap1 = ((struct brent_trad_par *)params)->kap1;
    double emass = lens[ilens].emass;
    double epot = lens[ilens].epot;
    struct point p, grad;
    lens[ilens].emass = 0.;
    lens[ilens].epot = 0.;
    p.x = r + lens[ilens].C.x;
    p.y = lens[ilens].C.y;
    grad = e_grad_pot(&p, ilens);
    lens[ilens].emass = emass;
    lens[ilens].epot = epot;
    kap_sub = grad.x / r;

    return fabs(kap_sub - kap1);
}

/* Compute the tidal radius defined as the radius rt from the subhalo center
 * at which the mean density within rt is equal to the density of the main
 * halo (Tormen, Diaferio & Syer, 1998)
 * Arguments:
 *  - ilens: long int
 *           index of the subhalo
 *  - ilensmin: long int
 *           index of the first subhalo in the lens[] list. Used to compute
 *           the contribution of the main halo (clumps between ilens=0 and ilens=ilensmin-1)
 * Returns:
 *  - the tidal radius in kpc
 */ 
double tradius(long int ilens, long int ilensmin)
{
    extern struct g_cosmo C;
    extern struct pot     lens[NLMAX];
    // Kappa due to the main halo at the subhalo position
    double kap1 = 0;
    struct matrix grad2;
    long int i;
    int status;
    struct point p;
    for(i=0; i<ilensmin; i++)
    {
        grad2 = e_grad2_pot(&lens[ilens].C, i);
        kap1 += 0.5 * (grad2.a + grad2.c);
    }
    kap1 = kap1 * 1E12 * cH0_4piG * C.h / g_dl;  // sigma in Msun/kpc^2

    // Find the radius rt at which mean_kap(<rt) - kap1 = 0, using Brent minimization
    const gsl_min_fminimizer_type *T = gsl_min_fminimizer_brent;
    gsl_min_fminimizer *s = gsl_min_fminimizer_alloc(T);
    gsl_function F;
    F.function = &f_brent_trad;
    struct brent_trad_par intg_par;
    intg_par.kap1 = kap1;
    intg_par.ilens = ilens;
    F.params = &intg_par;

    double m, a, b;
    m = 50. / g_arc2kpc;   // radius boundaries in arcsec
    a = 1. / g_arc2kpc;
    b = 1000. / g_arc2kpc;
    gsl_set_error_handler_off();
    status = gsl_min_fminimizer_set(s, &F, m, a, b);
    if( status == GSL_EINVAL )
        return -1;
    gsl_set_error_handler(NULL);
    int iter = 0;
    do
    {
        iter++;
        status = gsl_min_fminimizer_iterate(s);
        m = gsl_min_fminimizer_x_minimum(s);
        a = gsl_min_fminimizer_x_lower(s);
        b = gsl_min_fminimizer_x_upper(s);        
        status = gsl_min_test_interval (a, b, 0.001, 0.0);
    }
    while(status==GSL_CONTINUE && iter < 30);
    gsl_min_fminimizer_free(s);

    return m * g_arc2kpc;
}

/* Compute the luminosity from magnitude and redshift
 * Assumes mag is in K band
 */ 
double get_lum(long int ilens, struct curve *mycurve)
{
    extern struct g_cosmo C;
    extern struct pot     lens[NLMAX];
    double msun=3.39;  // Smith et al. 2005 (K band)
    double dlum=dlumcosmo1(lens[ilens].z)*D0Mpc/C.h;  
    double kcorr = f_f(lens[ilens].z, mycurve);
    double mabs = lens[ilens].mag + kcorr - 5.*log(dlum)/log(10.)-25.;
    double lum = exp(log(10.)*(0.4*(msun-mabs)));
    return lum;
}

/* Print a line of bayesSubhalos.dat
 * Arguments:
 *  - bayes: FILE pointer
 *           pointer to the open file in which to print the line
 *  - ilensmin, ilensmax: long int
 *           indexes to the lens[i] array of lenstool clumps
 *  - mode: if 1 then compute tidal mass, otherwise compute m200
 *  - mycurve: pointer to a curve structure
 *           initialized density profile of the smooth halo component to compute the masses in the 
 *           subhaloes tidal radius.
 */
void printLine(FILE *bayes, long int ilensmin, long int ilensmax, int mode, struct curve *mycurve, struct curve *mycurve_kcorr)
{
    extern struct g_grille G;
    extern struct pot      lens[NLMAX];
    long int  k;
    double mass, lum, ml;
    double r_aper;

    for (k = 0; k < G.nlens; k++)
    {
        mass = 0.; r_aper = -1.;
        lum = ml = 0.;
        if (k < ilensmin || k > ilensmax)
            continue;

        switch (mode)
        {
            case 1:
                if( mycurve == NULL )
                    r_aper = tradius(k, ilensmin);
                else
                    r_aper = tradius_analytic(k, mycurve);

                if( r_aper != -1 )
                    mass = maper(r_aper, k);
                break;
            case 2:
                mass = mtot(k);
                break;
            case 3:
                if( mycurve != NULL )
                    r_aper = tradius_density(k, mycurve);
                
                if( r_aper != -1 )
                    mass = maper(r_aper, k);
                break;
            default:
                // M200 
                r_aper = rho_radius(k);
                mass = maper(r_aper, k);
        }
        fprintf( bayes, "%.4le %.4lf ", mass, r_aper);
        if( mycurve_kcorr != NULL )
        {
            lum = get_lum(k, mycurve_kcorr);
            ml = mass/lum;
            fprintf( bayes, "%.4le %.4lf ", lum, ml);
        }
    }
    fprintf( bayes, "\n" );
}

int main( int argc, char** argv )
{
    extern struct g_grille G;
    extern struct g_cosmo  C;
    extern struct pot      lens[NLMAX];
    double **array; // contains the bayes.dat data
    int nParam;  // size of array
    long int    nVal, iVal, i, k;
    FILE   *bayes;
    double *index;  // list of bayes.dat lines
    int    seed;   // random seed
    int tmp;
    int mode = 0;  // m200 mode (0), or tidal mass (1), or total mass (2)
    long int ilensmin = -1;  // subhalos lens id
    long int ilensmax = NLMAX;
    char filename[160]; // filename of the smooth component ASCII file
    double x_cent = 0; // position of the smooth component positions in relative 
    double y_cent = 0; // coordinates
    char id_lens[2*IDSIZE];
    id_lens[0] = 0;
    char *pch;

    // Initialize variables
    filename[0] = 0;

    // Check the arguments
    if ( argc < 2 )
        help_msg();

    // Parse options
    while(argc > 2)
    {
        if( !strcmp(argv[1], "-m" ) )
            sscanf(argv[2], "%d", &mode);

        if( !strcmp(argv[1], "-i" ) )
            strcpy(id_lens, argv[2]);
    
        if( !strcmp(argv[1], "-p") )
            strcpy(filename, argv[2]);
    
        // center of the smooth component in arcsec 
        // relative to mod.par reference position
        if( !strcmp(argv[1], "-c") )
            sscanf(argv[2], "%lf,%lf", &x_cent, &y_cent);

        // shift arguments
        for( i = 1; i < argc-1 ; i++ )
        	argv[i]=argv[i+2];
           argc-=2;
    }

    if (mode == 3 && strlen(filename) == 0)
    {
        fprintf(stderr, "ERROR: mode 3 tidal radius requires a density profile");
        exit(1);
    }

    if ( strstr(argv[1], ".par") == NULL )
        help_msg();

    // Read the .par file
    init_grille( argv[1], "para.out", 1);

    // look for lens id in lens[] array
    pch = strtok(id_lens, ",");
    if( pch != NULL )
    {
        i = 0; while( i < G.nlens && strcmp(pch, lens[i].n))  i++;
        ilensmin = i;
        pch = strtok(NULL, " ");
        if( pch != NULL )
        {
            while( i < G.nlens && strcmp(pch, lens[i].n))  i++;
            ilensmax = i;
        }
	else
	    ilensmax = G.nlens;	

        if (ilensmin == -1)
        {
            printf("ERROR: -i option format not recognized (%s)\n", argv[2]);
            exit(1);
        }
    }

    // Read constraints
    readConstraints();

    if ( G.nmsgrid != G.nlens )
    {
        prep_non_param();
    }

    // Read the bayes.dat file
    array = readBayesModels(&nParam, &nVal);
    if ( array == NULL )
    {
        fprintf(stderr, "ERROR: bayes.dat file not found\n");
        return -1;
    }
 
    // Compute the critical density
    double rho_c = RHO_C0 /100./100. * C.H0 * C.H0; // in Msun/Mpc^3
    double om_z = C.omegaM*pow(1.+lens[0].z,3)*chiz(lens[0].z)*chiz(lens[0].z);
    g_rho_m = 200. * C.omegaM/om_z * rho_c /1E9; // in Msun/kpc^3
    g_dl = distcosmo1(lens[0].z);
    g_arc2kpc = d0 / C.h * g_dl;

    // Read the smooth component profile and initialize curve structure
    struct stat buffer;
    struct curve *mycurve_profile = NULL;
    if(strlen(filename) > 0 && stat(filename, &buffer)==0)
        mycurve_profile = init_curve(filename, x_cent, y_cent);

    // Eventually reads a local kcorrection table
    struct curve *mycurve_kcorr = NULL;
    if(stat("kcorr_E_kband.dat", &buffer)==0)
    {
        printf("INFO Read kcorrection file kcorr_E_band.dat\n");
        mycurve_kcorr = init_curve("kcorr_E_kband.dat", 0, 0);
    }

    // Write the header of the bayesM200.dat file
    if(mode == 1 || mode == 3)
        bayes = fopen( "bayesTidal.dat", "w" );
    else
        bayes = fopen( "bayesM200.dat", "w" );

    fprintf( bayes, "#Nsample\n");
    fprintf( bayes, "#Chi2\n");

    for (k = 0; k < G.nlens; k++)
        if (k >= ilensmin && k <= ilensmax)
        {
            fprintf( bayes, "#Potential mass %s\n#Potential radius %s\n", lens[k].n, lens[k].n);
            if(mycurve_kcorr!=NULL)
                fprintf( bayes, "#Potential luminosity %s\n#Potential M/L %s\n", lens[k].n, lens[k].n);
        }


    // Prepare the index list
    index = (double *) malloc((unsigned) nVal * sizeof(double));
    for ( i = 0 ; i < nVal ; i++ ) index[i] = i;
    seed = -2;

    // Handle CTRL-C to interrupt the optimisation but not lenstool
    signal(SIGINT, signalReset);
    optInterrupt = 0;

    // Loop over each line
    for ( i = 0; i < nVal && !optInterrupt; i++ )
    {
        // Randomly draw a line from index array
//        tmp = (int) floor(d_random(&seed) * (nVal - i));
//        iVal = index[i + tmp];
        // and swap the indexes in the index list
//        index[tmp] = i;
        iVal = i;

        // Set the lens parameters from <array>
        setBayesModel( iVal, nVal, array );
        if(mode==1)
            printf( "INFO: Compute the bayesTidal.dat file %ld/%ld  [CTRL-C to interrupt]\r", i + 1, nVal);
        else
            printf( "INFO: Compute the bayesM200.dat file %ld/%ld  [CTRL-C to interrupt]\r", i + 1, nVal);

        fflush(stdout);

        fprintf( bayes, "%ld %lf ", iVal, array[0][iVal] );
        printLine(bayes, ilensmin, ilensmax, mode, mycurve_profile, mycurve_kcorr);
        fflush(bayes);
    }
    printf( "\n" );
    fclose(bayes);
    if(mycurve_profile != NULL) free_curve(mycurve_profile);
    if(mycurve_kcorr != NULL) free_curve(mycurve_kcorr);
    free( array );
    return 0;
}

static void signalReset()
{
    signal(SIGINT, SIG_DFL);
    optInterrupt = 1;
    printf( "\nINFO: Optimisation interrupted by CTRL-C\r");
}
