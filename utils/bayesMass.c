#include<stdio.h>
#include<signal.h>
#include<stdlib.h>
#include<math.h>
#include<string.h>
#include "dimension.h"
#include "structure.h"
#include "constant.h" 
#include "fonction.h"

/******************************************************************************
 * Name: bayesMass.c
 * Authors: EJ
 * Date: 12/09/07
 * 
 * Analyse a bayes.dat file and compute a mass.fits file for each line. 
 * If the mass keyword exists in the .par file, it is used, otherwise a 
 *
 * mass 3 200 1.2 mass.fits
 *
 * line is inserted in the .par file.
 *
 * syntax : bayesMass <.par> 
 *
 * Input : The bayes.dat file in the current directory is used.
 * Output : tmp/mass??.fits
 *
 ******************************************************************************/
typedef void (*sighandler_t)(int);
static void signalReset();
int optInterrupt;

int main( int argc, char** argv )
{
	double **array; // contains the bayes.dat data
	int nParam, nVal;  // size of array
	int iVal;
	char fname[20]; // mass<ival>.fits
	FILE *pFile;
	int i;
	double *index;  // list of bayes.dat lines
	int    seed;   // random seed
	int tmp;

	// Check the arguments
	if( argc < 2 || 
	    strstr( argv[1], ".par" ) == NULL )
	{
		fprintf(stderr, "Syntax : bayesMass <.par>\n");
		return -1;
	}

	// Read the .par file
	init_grille( argv[1], "para.out", 1);

	// Check for the presence of the mass keyword
	if( M.imass == 0 || M.imass == 5 )
	{
		M.imass = 3;
		M.nmass = 200;
		M.zmass = 1.2;
	}
	
	// Read catalog of multiple images 
	readConstraints();

	// Initialise the grid
	if( G.pol != 0 )
		gridp();
	else
		grid();
	
	// Switch to silent mode
	M.verbose = 0;

	if( G.nmsgrid != G.nlens ) 
	{
		prep_non_param();
	}
	
	// Read the bayes.dat file
	array = readBayesModels(&nParam, &nVal);
	if( array == NULL )
	{
		fprintf(stderr, "ERROR: bayes.dat file not found\n");
		return -1; 
	}

	// Create the ./tmp directory
	i = system("mkdir -p tmp");
	
	// Prepare the index list
	index = (double *) malloc((unsigned) nVal*sizeof(double));
	for( i = 0 ; i < nVal ; i++ ) index[i]=i;
	seed = -2; 

	// Handle CTRL-C to interrupt the optimisation but not lenstool
	signal(SIGINT, signalReset);
	optInterrupt = 0;

	// Loop over each line
	for( i = 0; i < nVal && !optInterrupt; i++ )
	{
		// Randomly draw a line from index array 
		tmp = (int) floor(d_random(&seed) * (nVal - i));
		iVal = index[i+tmp];
		// and swap the indexes in the index list
		index[i+tmp] = i; 		
		
		// Set the name of the mass file
		sprintf( fname, "tmp/mass%d.fits", iVal );
		
		// Set the lens parameters from <array>
		setBayesModel( iVal, nParam, nVal, array );
		printf( "INFO: Compute file %d/%d  : %s [CTRL-C to interrupt]\r", i+1, nVal,fname);
		fflush(stdout);
	
		// skip this file if it already exists
		pFile = fopen( fname, "r" );
		
		if( pFile == NULL ) 
		{
			// Compute the mass map and save it
			g_mass( M.imass, M.nmass, M.zmass, fname );
		}
		else
			fclose(pFile);

	}
	printf( "\n" );
	free( array );
	return 0;
}


static void signalReset()
{
	signal(SIGINT, SIG_DFL);
	optInterrupt = 1;
	printf( "\nINFO: Optimisation interrupted by CTRL-C\r");
}
