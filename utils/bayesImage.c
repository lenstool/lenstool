#include<stdio.h>
#include<signal.h>
#include<stdlib.h>
#include<string.h>
#include<math.h>
#include "dimension.h"
#include "structure.h"
#include "constant.h" 
#include "fonction.h"
#include "lt.h"

/******************************************************************************
 * Name: bayesImage.c
 * Authors: EJ
 * Date: 12/09/07
 * 
 * Analyse a bayes.dat file and compute a image.all file for each line. 
 *
 * syntax : bayesImage <.par> 
 *
 * The bayes.dat file in the current directory is used.
 * Output : image??.all
 *
 ******************************************************************************/
typedef void (*sighandler_t)(int);
static void signalReset();
static int bc_mode();
static int image_mode();
void bayesimage_distor(struct galaxie *image, char *fname, long int ni);
static void reset_grad_image();
int optInterrupt;
struct point gsource_global[NGGMAX][NGGMAX]; //(for e_lensing)

void help_msg()
{
  fprintf(stderr, "Syntax : bayesImage [OPTION] <.par> <method>\n");
  fprintf(stderr, "Available OPTIONS:\n");
  fprintf(stderr, " -b : compute images of system barycenter [default]\n");
  fprintf(stderr, " -i : compute counter images, image per image\n");
  fprintf(stderr, "Available method:\n");
  fprintf(stderr, " all :  take all models [default]\n");
  fprintf(stderr, " best : take best chi2 model only\n");
  exit(-1);
}

int main( int argc, char** argv )
{
  extern struct g_mode   M;
  extern struct g_grille G;
  extern struct g_source S;
  extern struct g_image  I;
  extern struct galaxie  cimage[NFMAX];
  extern struct galaxie  source[NFMAX], image[NFMAX][NIMAX];
  double **array; // contains the bayes.dat data
  int nParam;  // size of array
  long int iVal, nVal;
  char fname[128]; // image<ival>.all
  char fnamesrc[128]; // source<ival>.dat
  char fnamedist[128]; // dist<ival>.dat
  int mode;  // lens barycenter (0) or lens image per image (1) 
  char *pch;
  int ni, status, i;
  int    method;
  
  // Check the arguments
  if( argc < 2 )
    help_msg();
  
  mode = 0;  // image of barycenter
  while( argv[1][0] == '-' )
    {
      pch = argv[1];
      if( pch[1] ==  'b' )
	mode = 0;
      if( pch[1] == 'i'  )
	mode = 1;

      for( i = 1; i < argc-1 ; i++ )
	argv[i]=argv[i+1];
      argc--;
    }	
  
  if( strstr( argv[1], ".par" ) == NULL )
    help_msg();
  
  // Default method : all
  method = 0;
  if( argc == 3 )
    {
      if( !strcmp( argv[2], "best" ) ) method = -4;
    }

  // Read the .par file
  init_grille( argv[1], "para.out", 1);
  
  // image per image computation reads runmode/image keyword
  if( mode == 1 && M.imafile[0] == '\0' )
    {
      fprintf(stderr, "ERROR: keyword runmode/image is not defined\n");
      exit(-1);
    }
  
  // Read catalog of multiple images 
  readConstraints();
  
  // Initialise the grid
  if( G.pol != 0 )
    gridp();
  else
    grid();
  
  // Switch to silent mode
  M.verbose = 0;
  M.image = 1; // to cheat the e_lensing print out
  
  if( G.nmsgrid != G.nlens ) 
    {
      prep_non_param();
    }
  
  // Read the bayes.dat file
  array = readBayesModels(&nParam, &nVal);
  if( array == NULL )
    {
      fprintf(stderr, "ERROR: bayes.dat file not found\n");
      return -1; 
    }
  
  // Create the ./tmp directory
  status = system("mkdir -p images");
  
  // Handle CTRL-C to interrupt the optimisation but not lenstool
  signal(SIGINT, signalReset);
  optInterrupt = 0;
  if(method == -4)
    {
      	// Set the best model in memory
	  setBayesModel( method, nVal, array );
	  printf( "INFO: Compute the Image.all file for the best model[CTRL-C to interrupt]\r");
	  fflush(stdout);
	  
	  if( mode == 0 )
	    ni = bc_mode();
	  else
	    ni = image_mode();
	  
	  // write image array
	  sprintf( fname, "images/image_best.all");
	  sprintf( fnamesrc, "images/source_best.dat");
	  ecrire_r(0,ni,cimage,fname,2);
	  ecrire_r(0, S.ns, source, fnamesrc, 2); 
    }
  else
    {
      // Loop over each line
      for( iVal = 0; iVal < nVal && !optInterrupt; iVal++ )
	{
	  // Set the lens parameters from <array>
	  setBayesModel( iVal, nVal, array );
	  printf( "INFO: Compute the Image.all file %ld/%ld  [CTRL-C to interrupt]\r", iVal+1, nVal);
	  fflush(stdout);
	  
	  if( mode == 0 )
	    ni = bc_mode();
	  else
	    ni = image_mode();
	  
	  // write image array
	  sprintf( fname, "images/image%ld.all", iVal );
	  sprintf( fnamesrc, "images/source%ld.dat", iVal);
	  sprintf( fnamedist, "images/dist%ld.dat", iVal);
	  ecrire_r(0,ni,cimage,fname,2);
	  ecrire_r(0, S.ns, source, fnamesrc, 2); 
	  bayesimage_distor(cimage,fnamedist, ni); 

	  // reset the number of sources for each line of bayes.dat (BCLEMENT - 01/18/2017)
	  ni = 0;
	  S.ns = 0;

	  // reset image structure grad/grad2 fields (BCLEMENT - 01/20/2017)
	  reset_grad_image();
	}
    }
  printf( "\n" );
  free( array );
  return 0;
  
}
static int bc_mode()
{
    extern struct g_image  I;
    extern struct g_source S;
    extern struct galaxie  multi[NFMAX][NIMAX];
    extern struct galaxie  cimage[NFMAX];
    extern struct galaxie  source[NFMAX], image[NFMAX][NIMAX];
	struct point Bs;
	struct point Ps[NIMAX];
    int i, j;
    int ni;

	for( i = 0 ; i < I.n_mult ; i++ )
	{
		S.ns = i;

		// arclet --> source list
		e_unlens(I.mult[i],multi[i], &S.ns, source);

		for( j = 0; j < I.mult[i]; j++ )
		{
			multi[i][j].grad.x=multi[i][j].grad.y=0.;
			multi[i][j].grad2.a=multi[i][j].grad2.c=0.;
		}
		
		o_dpl(I.mult[i], multi[i], Ps, NULL);

		/*Find the barycenter position of the computed sources*/
		if (I.forme == 4 || I.forme == 6 )
			Bs=weight_baryc(Ps,multi[i],I.mult[i] , i); //i==n_familly
		else if ( I.forme == -2 ) /* barycenter weighted by amplification */
		    	Bs=weight_baryc_amp(Ps,multi[i],I.mult[i] , i); //i==n_familly
		else
			Bs=bcentlist(Ps,I.mult[i]);  /* barycentre */

		source[i].C = Bs;
		source[i].n[strlen(source[i].n)-1] = 0;
	}
	S.ns = I.n_mult;

       
	// source list --> image array
	e_lensing(source,image);

	// Count the number of images
	ni = 0;
	for( i = 0 ; i < S.ns ; i++ )
		for( j = 0 ; j < NIMAX && strcmp(image[i][j].n,"") ; j++ )
			cimage[ni++]=image[i][j];

    return(ni);
}

static int image_mode()
{
    extern struct galaxie  cimage[NFMAX];
    extern struct galaxie  source[NFMAX], image[NFMAX][NIMAX];
    long int ni, ncistart;

    s_source();
    e_lensing(source, image);
    classer(image, cimage, &ni, &ncistart);
    return(ni);
}


static void signalReset()
{
	signal(SIGINT, SIG_DFL);
	optInterrupt = 1;
	printf( "\nINFO: Optimisation interrupted by CTRL-C\r");
}

/*
 * reset the image.grad(2) and cimage.grad(2) parameters for the optimisation.
 * Author: BCLEMENT - 01/20/2017
 */
static void reset_grad_image()
{
    extern struct galaxie  cimage[NFMAX];
    extern struct galaxie  image[NFMAX][NIMAX];
    long int i;
    long int j;

    // reset the grad and grad2 image/cimage temporary structure for optimisation
    for ( i = 0; i < NFMAX; i++ )
    {
        for ( j = 0; j < NIMAX; j++ )
        {
	    // fix bayesImage bug
            image[i][j].grad2.a = 0;
            image[i][j].grad2.c = 0;
            image[i][j].grad.x = 0;
            image[i][j].grad.y = 0;
        }
    }
}

void    bayesimage_distor(struct galaxie *image, char *fname, long int ni)
{
    extern struct g_mode M;
    long int     i;
    double  t0, r, R, E;
    FILE    *OUT;
    struct ellipse amp;
    char   id0[IDSIZE];

    char   parity1[NAMAX], parity2[NAMAX];
    struct point pref;  // point of REFERENCE in runmode

    for (i = 0; i < ni; i++)
    {
        r = image[i].E.a / image[i].E.b;
        image[i].q = 1. / r;
        R = r * r;
        E = image[i].eps = (R - 1.) / (R + 1.);
        image[i].tau = (R - 1.) / 2. / r;
        image[i].time = e_time_gal(&image[i]);
        amp = e_unmag_gal(&image[i]);
        image[i].A = 1 / fabs(amp.a * amp.b);
        parity1[i] = amp.a > 0 ? '+' : '-';
        parity2[i] = amp.b > 0 ? '+' : '-';
    }

    NPRINTF(stderr, "COMP: %s file ",fname);
    NPRINTF(stderr, "\n");

    if (M.sort == 1)
        sort(ni, image, comparer_tau);
    else if (M.sort == 2)
        sort(ni, image, comparer_pos);

    OUT = fopen(fname, "w");

    fprintf(OUT, "#REFERENCE 3 %.7f %.7f\n", M.ref_ra, M.ref_dec);
    fprintf(OUT, "   #ID       X         Y         R       EPS       TAU       AMP       ");
    fprintf(OUT, "DMAG  TIME[days]  DTIME    PARITY\n");

    t0 = image[0].time; strcpy(id0, image[0].n);
    pref.x = pref.y = 0.;
    for (i = 0; i < ni; i++)
    {
        if ( indexCmp(image[i].n, id0) )
        {
            t0 = image[i].time; strcpy(id0, image[i].n);
        }

        fprintf(OUT, "%6s  %8.3lf  %8.3lf  %8.3lf  %8.3lf  %8.3lf  %8.3lf  ",
                image[i].n, image[i].C.x, image[i].C.y, dist(image[i].C, pref),
                image[i].eps, image[i].tau, image[i].A);
        fprintf(OUT, "%8.2lf  %8.1lf  %8.1lf    %1c%1c\n",
                -2.5*log10(1 / image[i].A),
                image[i].time, image[i].time - t0, parity1[i], parity2[i]);
    };

    fclose(OUT);
}

