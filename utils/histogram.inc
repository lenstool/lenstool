c==============================================================================
c
c  Include file for the histogram.f and histogram2D.f file
c
c EJ 8/2007
c==============================================================================
c
c Number of parameters in bayes.dat file
	integer NFS
	parameter(NFS=400)

c Number of character in 1 line of bayes.dat file
	integer REC_SIZE
	parameter(REC_SIZE=4096)
