import os
import numpy
import lenstool

model = lenstool.Lenstool('test_galaxies.par')
assert model.get_nparams() == 4

cube = [0.5,] * model.get_nparams()
assert model.rescaleCube(cube) == 1
assert numpy.allclose(cube, [10550.0, 505.0, 0.5612544252782918, 150.0])

if os.path.exists('my_chires.dat'): os.unlink('my_chires.dat')
model.o_chires('my_chires.dat')
assert os.path.exists('my_chires.dat')

if os.path.exists('bayes.dat'): os.unlink('bayes.dat')
if os.path.exists('bayes.dbg.dat'): os.unlink('bayes.dbg.dat')
model.write_bayes_header()
assert os.path.exists(model._bayes_name)
with open(model._bayes_name, 'r') as fh:
    lines = fh.readlines()
assert len(lines) == model.get_nparams() + 3
model.close_bayes()

if os.path.exists('bayes.dat'): os.unlink('bayes.dat')
if os.path.exists('bayes.dbg.dat'): os.unlink('bayes.dbg.dat')
model.write_bayes_header()
cube = [0.5,] * model.get_nparams()
model.rescaleCube(cube)
model.write_burnin_line(1, -50)
model.write_bayes_line(1, -50)
model.close_bayes()
with open(model._burnin_name, 'r') as fh:
    lines = fh.readlines()
values = [float(v) for v in lines[-1].split()]
assert len(values) == model.get_nparams() + 3
assert all([abs(values[2+i] - cube[i]) < 1E-5 for i in range(model.get_nparams())])

with open(model._bayes_name, 'r') as fh:
    lines = fh.readlines()
values = [float(v) for v in lines[-1].split()]
assert len(values) == model.get_nparams() + 3
assert all([abs(values[2+i] - cube[i]) < 1E-5 for i in range(model.get_nparams())])

model.readConstraints()
error, chi2, lhood0 = model.o_chi_lhood0()
assert error == 0
assert numpy.isfinite(chi2)
assert numpy.isfinite(lhood0)
