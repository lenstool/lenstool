import lenstool
from lenstool.potentials import sis
lt = lenstool.Lenstool()
lt.add_lens(sis(0,0,0.3,400))
print(lt.lens[0])
tangent, radial = lt.criticnew(zs=1.2)
print(lt.ntline, lt.nrline)
print(tangent, radial)
print(radial[0])

from lenstool.pcl import create_lines, parse_cline
import numpy as np
array = np.array(
            [(obj.i, obj.I.x, obj.I.y, obj.S.x, obj.S.y) for obj in radial],
            dtype = [('lid','int'), ('xi','float'), ('yi','float'), ('xs','float'), ('ys','float')]
    )
#critic = create_lines(array['lid'], array['xi'], array['yi'])
critic, caustic = parse_cline(radial)
print(critic)

#caustic = create_lines(array['lid'], array['xs'], array['ys'])
#print(critic)
