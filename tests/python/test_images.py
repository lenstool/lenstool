import lenstool, os
from astropy.table import Table
from lenstool.potentials import sis

lt = lenstool.Lenstool()
lt.add_lens(sis(0.,0.,0.3,800))
lt.C.omegaM = 0.3
lt.C.omegaX = 0.7
#lt.C.H0 = 70
lt.C.wX = -1.0
lt.F.xmin, lt.F.xmax, lt.F.ymin, lt.F.ymax = -30, 30, -30, 30
lt.reset_grid()
lt.M.verbose = 1

tab = Table(rows=[(b'1a',0,0,1.,1.,0,2.0,20),], names=['n','x','y','a','b','theta','z','mag'], dtype=['str',*['float',]*7])
lt.set_sources(tab)
print(lt.get_sources())
lt.e_lensing()
print(lt.get_images())

tab = Table(rows=[(b'1a',10.,-9,1.,1.,0,2.0,20),], names=['n','x','y','a','b','theta','z','mag'], dtype=['str',*['float',]*7])
lt.set_sources(tab)
print(lt.get_sources())
lt.e_lensing()
print(lt.get_images())

