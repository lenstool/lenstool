import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import dblquad

def func(r, rp, a, s):
    return (s*np.arctan(r/s) - a*np.arctan(r/a))/(1+r*r/a/a)/(1+r*r/s/s)*np.sqrt(r*r-rp*rp)/r/r * rp

import ctypes
lib = ctypes.CDLL('e_sigma_ap.so')
get_cp = lib.get_cp
get_cp.argtypes = [ctypes.c_double, ctypes.c_double, ctypes.c_double]
get_cp.restype = ctypes.c_double

nr = 30
a = 0.01
r = np.linspace(0.2,1.6,nr)
colors = ['m','g','r']
leg = []
for j, a in enumerate([1e-5, 1e-2, 5e-2]):
    for s in [100, 20, 10, 5]:
        cp = []
    #    cp_python = []
        for i in range(nr):
            cp.append(get_cp(a,s,r[i]))
    #        if s == 100 and i%2==0:
    #            t0 = 6 / np.pi * (a + s) / a / a / s
    #            p = np.sqrt(a*a + r[i]**2) - a - np.sqrt(s*s + r[i]**2) + s
    #            integral = dblquad(func, 0.001, r[i], lambda x: x, lambda x: 10*s, args=(a,s))[0]
    #            cp_python.append(np.sqrt(integral * t0 / p))
    #            print(r[i], cp[-1], cp_python[-1])
    #    if s==100:
    #        plt.plot(r[::2], cp_python)
    
        l, = plt.plot(r, cp, c=colors[j])
    leg.append(l)
plt.axhline(np.sqrt(3./2.), ls='--', c='k')
plt.xlabel('R [arcsec]')
plt.ylabel(r'$c_p$')
plt.legend(leg, [r'$r_{core} = %.2le$'%a for a in [1e-5,1e-2,5e-2]],loc='upper center', ncol=3, frameon=False, fontsize='x-small')
plt.ylim((1.050,1.240))
plt.savefig('sigma_ap.png')
