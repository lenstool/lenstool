import lenstool
from lenstool.potentials import nfw
from astropy.table import Table

lt = lenstool.Lenstool()
lt.set_cosmology(70, 0.3, 0.7, -1)
lt.set_field(50)
lt.set_grid(1000,1)

tab = Table(names=['n','x','y','a','b','theta','z','mag'],dtype=['str',*['float',]*7]) 
tab.add_row(['a1',1,1,1,1,0,3,21])
lt.set_sources(tab)

lt.set_lenses([nfw(0.,0.,0,0,0.3,m200=1e15,c200=6),])
assert lt.lens[0].sigma > 0
lt.e_lensing()
print(lt.get_images())

lt.set_lenses([nfw(0.,0.,0,0,0.3,r200=420,c200=6),])
assert lt.lens[0].sigma > 0
lt.e_lensing()
print(lt.get_images())

lt.set_lenses([nfw(0.,0.,0,0,0.3,rs=70.,c200=6),])
assert lt.lens[0].sigma > 0
lt.e_lensing()
print(lt.get_images())

lt.set_lenses([nfw(0.,0.,0,0,0.3,rs=70.,m200=1e15),])
assert lt.lens[0].sigma > 0
lt.e_lensing()
print(lt.get_images())

lt.set_lenses([nfw(0.,0.,0,0,0.3,rs=70.,r200=420.),])
assert lt.lens[0].sigma > 0
lt.e_lensing()
print(lt.get_images())
