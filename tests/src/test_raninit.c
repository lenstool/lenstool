#include <stdio.h>
#include "random.h"

int main(int argc, char** argv)
{
    int CALLvalue = 0;
    int seed = 19;
    //int seed = 13179;
    //int seed = 1865579;
    Rand_t Rand;
    CALLvalue =  RanInit(Rand, seed);
    printf("input  / output seed : %d / %d\n", CALLvalue, seed);
    printf("Rand values %d %d %d %d\n", Rand[0], Rand[1], Rand[2], Rand[3]);
    return 0;
}
