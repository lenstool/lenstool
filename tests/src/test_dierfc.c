#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <gsl/gsl_rng.h>
#include "constant.h"
#include "fonction.h"

// Test the dierfc() function from o_rescale.c
double dierfc(double y);
void t_dierfc()
{
#define SQRTTWO 1.414213562
    long int nsamp = 200000;
    long int i;
    double rnd;
    double var, mu, val;

    gsl_rng * r = gsl_rng_alloc (gsl_rng_taus);
    gsl_rng_set (r, 4321);

    var = mu = 0.; 
    for( i = 0; i < nsamp; i++ ) 
    {
        rnd = gsl_rng_uniform (r);
        val = SQRTTWO * dierfc(2 * (1. - rnd));
        mu += val;
        var += val*val;
    }
    gsl_rng_free (r);

    mu /= nsamp;
    var = var/nsamp - mu*mu;
    assert(mu < 1E-4);
    assert(fabs(var - 1.) < 1E-2);
}

void t_omega_M_z()
{
    extern struct g_cosmo C;
    C.omegaM = 0.31;
    C.omegaX = 0.69;
    C.kcourb = 0.;
    C.wX = -1.;
    C.wa = 0.;
    C.h = 100./h0;
    C.model = 1;

    assert(fabs(omega_M_z(0.3) -0.496743) < 1E-6);
}

void t_deltaVF()
{
    extern struct g_cosmo C;
    C.omegaM = 0.31;
    C.omegaX = 0.69;
    C.kcourb = 0.;
    C.wX = -1.;
    C.wa = 0.;
    C.h = 100./h0;
    C.model = 1;

    assert(fabs(deltaVF(0.5) - 137.112) < 1E-3);
}

int main(int argc, char** argv)
{
    printf("test dierfc..."); fflush(stdout);
    t_dierfc(); printf("ok\n");

    printf("test omega_M_z..."); fflush(stdout);
    t_omega_M_z(); printf("ok\n");

    printf("test deltaVF..."); fflush(stdout);
    t_omega_M_z(); printf("ok\n");
}

