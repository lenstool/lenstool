#include<stdio.h>
#include<string.h>
#include "dimension.h"
#include "structure.h"

double get_cp(double a, double s, double r);

int main(int argc, char** argv)
{
    extern struct pot lens[NLMAX];
    double cp, r;
    strcpy(lens[0].n, "1");
    lens[0].type = 81;
    lens[0].rc = 0.01;
    lens[0].rcut = 100;
    lens[0].sigma = 200;
    r = 0.4;
    cp = get_cp(lens[0].rc, lens[0].rcut, r);
    printf("cp(%lf,%lf,%lf) %lf\n", lens[0].rc, lens[0].rcut, r, cp);
    r = 1.6;
    cp = get_cp(lens[0].rc, lens[0].rcut, r);
    printf("cp(%lf,%lf,%lf) %lf\n", lens[0].rc, lens[0].rcut, r, cp);
}
