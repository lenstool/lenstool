from ctypes import *
import ctypes.util
import os
import sys
from . import structure
import signal

# Search in LD_LIBRARY_PATH (unix), DYLD_LIBRARY_PATH (osx) or 
# other standard places. Also appends prefix and suffix according to system
# https://docs.python.org/3/library/html#finding-shared-libraries
lib_file = ctypes.util.find_library('lenstool')

_lib = CDLL(lib_file)

_lib.get_version.argtypes = [c_char_p]
_lib.get_version.restype = None
p = create_string_buffer(10) 
_lib.get_version(p)
__version__ = p.value.decode('ascii')
del p

_lib.init_grille.argtypes = [c_char_p, c_char_p, c_int]
_lib.init_grille.restype = c_int


_lib.init_cosmoratio.argtypes = [c_int, POINTER(structure.cpoint)]
_lib.init_cosmoratio.restype = None

_lib.checkpar.argtypes = None
_lib.checkpar.restype = None

_lib.grid.argtypes = None
_lib.grid.restype = None

_lib.readConstraints.argtypes = []
_lib.readConstraints.restype = None

_lib.getNParameters.argtypes = []
_lib.getNParameters.restype = c_int

square_double_t = POINTER(POINTER(c_double))
_lib.free_square_double.argtypes = [square_double_t, c_int]
_lib.free_square_double.restype = None

_lib.alloc_square_double.argtypes = [c_int, c_int]
_lib.alloc_square_double.restype = square_double_t

_lib.readBayesModels.argtypes = [POINTER(c_int), POINTER(c_long)]
_lib.readBayesModels.restype = square_double_t

_lib.setBayesModel.argtypes = [c_long, c_long, square_double_t]
_lib.setBayesModel.restypes = None

_lib.o_chires.argtypes = [c_char_p]
_lib.o_chires.restype = None

_lib.o_chi_lhood0.argtypes = [POINTER(c_double), POINTER(c_double), POINTER(c_double)]
_lib.o_chi_lhood0.restype = c_int

_lib.rescaleCube_1Atom.argtypes = [POINTER(c_double), c_int]
_lib.rescaleCube_1Atom.restype = c_int

_lib.bayesHeader.argtypes = None
_lib.bayesHeader.restype = c_int

_lib.write_bayes_line.argtype = [c_void_p, c_double, c_double, c_long, c_void_p]
_lib.write_bayes_line.restype = None

_lib.open_mcmc_files.argtype = [c_double, c_char_p]
_lib.open_mcmc_files.restype = c_void_p

_lib.close_mcmc_files.argtype = [c_void_p, c_void_p]
_lib.close_mcmc_files.restype = None

_lib.userCommon_init.argstype = [c_int]
_lib.userCommon_init.restype = c_void_p

_lib.userCommon_free.argstype = [c_void_p]
_lib.userCommon_free.restype = None

_lib.o_get_lens.argstype = [c_int, c_int]
_lib.o_get_lens.restype = c_double

_lib.o_set_lens.argstype = [c_int, c_int, c_double]
_lib.o_set_lens.restype = None

_lib.getParName.argstype = [c_int, c_char_p, c_int]
_lib.getParName.restype = c_char_p

_lib.s_source.argstype = None
_lib.s_source.restype = None

_lib.computeKmap.argstype = [c_int, c_int, c_double, c_double, square_double_t, c_double, c_double]
_lib.computeKmap.restype = None

_lib.dratio.argstype = [c_double, c_double]
_lib.dratio.restype = c_double

_lib.distcosmo1.argstype = [c_double,]
_lib.distcosmo1.restype = c_double

_lib.distcosmo2.argstype = [c_double, c_double]
_lib.distcosmo2.restype = c_double

_lib.g_ampli_implane.argstype = [c_int, square_double_t, c_int, c_double]
_lib.g_ampli_implane.restype = None

_lib.g_dpl_tmp.argstype = [c_int, c_double, square_double_t, square_double_t]
_lib.g_dpl_tmp.restype = None

_lib.o_pixel.argstype = [
    square_double_t, c_int, c_int, c_double, 
    c_double, c_double, c_double, c_double, 
    POINTER(c_int), square_double_t, square_double_t  # POINTER(c_int) is for the pointer on struct galaxie *source
]
_lib.o_pixel.restype = None

_lib.o_pixel_source.argstype = [
    square_double_t, c_int, c_int, 
    c_double, c_double, c_double, c_double, 
    c_int, POINTER(c_int)
]
_lib.o_pixel_source.restype = None

_lib.o_print_res.argstype = [c_double, c_double]
_lib.o_print_res.restype = None

from .lenstool import Lenstool

DBL_MAX = ctypes.c_double.in_dll(_lib, "g_DBL_MAX").value

def handler(signum, frame):
    err_msg = (ctypes.c_char*500).in_dll(lenstool._lib, "error_string")
    err_no = ctypes.c_int.in_dll(lenstool._lib, "error_number").value
    raise RuntimeError(err_no, err_msg.value)
signal.signal(signal.SIGTERM, handler)

