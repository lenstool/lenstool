import ctypes
from ctypes import POINTER
import os.path
import numpy
from . import _lib
from . import structure, dimension
from . import constant
import astropy.wcs
from astropy.io import fits
import logging

def sdarray_ndarray(array, nrows, ncols):
    """ Convert Lenstool square_double array to numpy.ndarray. The memory content of the
        input sd_array is freed upon the call to this function.

        Parameters
        ----------
            - array         Object of ctypes type square_double_t
            - nrows, ncols  Size of the first and second dimensions

        Returns
        -------
            Array of type numpy.ndarray with shape (nrows, ncols)
    """
    arr = numpy.array([[array[i][j] for j in range(nrows)] for i in range(ncols)])
    _lib.free_square_double(array, ncols)
    return arr

class Lenstool():
    """
    A lenstool instance initialized with a paramater file, and providing
    some high-level functions of the C version.

    The simplest way to initialize a Lenstool object is to load a standard
    lenstool parameter file

        >>> lenstool = lenstool.Lenstool('test_galaxies.par')

    Make sure to have all the file dependencies mentioned in the parameter
    file available in your current directory.

    Note that a Lenstool instance is not thread-safe. The global variables
    initialized in memory by the underlying C library will be shared between
    multiple Lenstool instances.

    Parameters:
        infile:     The path to the parameter file
    """

    def __init__(self, infile=None):
        self._constraints_read = False
        self._chains = None
        self._fburnin = None
        self._fbayes = None
        self._userCommon = None
        self._modelset = False

        self.init_cosmoratio()
        if infile:
            if not os.path.exists(infile):
                raise RuntimeError(f"Input file {infile} not found in current directory")
            if os.path.basename(infile) != infile:
                raise Warning("Not loading input file from current directory")
            _lib.init_grille(bytes(infile, "ascii"), bytes("/dev/null", "ascii"), 1)
        else:
            _lib.set_default()

        _lib.checkpar()
        _lib.grid()
        self._nparams = self.get_NParameters()

        if self.M.image != 0 or self.M.source != 0:
            _lib.s_source()

            if self.M.pixel < 2:
                _lib.e_lensing(_lib.source, _lib.image)

    def __del__(self):
        if self._chains:
            _lib.free_square_double(self._chains, self._nparams)

        if self._fburnin or self._fbayes:
            self.close_bayes()

    ################################################################################
    # Retrieve global variables
    ################################################################################
    @property
    def M(self):
        """ Return the M global variable, with the runmode content """
        M = ctypes.cast(_lib.M, ctypes.POINTER(structure.g_mode))[0]
        return M

    @property
    def G(self):
        """ Return the G structure """
        G = ctypes.cast(_lib.G, ctypes.POINTER(structure.g_grille))[0]
        return G

    @property
    def S(self):
        """ Return the S structure """
        S = ctypes.cast(_lib.S, ctypes.POINTER(structure.g_source))[0]
        return S

    @property
    def I(self):
        """ Return the I structure """
        I = ctypes.cast(_lib.I, ctypes.POINTER(structure.g_image))[0]
        return I

    @property
    def F(self):
        """ Return the F structure """
        F = ctypes.cast(_lib.F, ctypes.POINTER(structure.g_frame))[0]
        return F

    @property
    def CL(self):
        """ Return the CL structure """
        CL = ctypes.cast(_lib.CL, ctypes.POINTER(structure.g_cline))[0]
        return CL

    @property
    def C(self):
        """ Return the C structure """
        C = ctypes.cast(_lib.C, ctypes.POINTER(structure.g_cosmo))[0]
        return C

    @property
    def ps(self):
        """ Return the ps structure """
        ps = ctypes.cast(_lib.ps, ctypes.POINTER(structure.g_pixel))[0]
        return ps

    @property
    def lens(self):
        """ Return the lens array """
        t_lens = structure.pot * dimension.NLMAX
        lens = ctypes.cast(_lib.lens, ctypes.POINTER(t_lens))[0]
        return lens

    @property
    def image(self):
        """ Return the image array global variable """
        t_image = (structure.galaxie * dimension.NIMAX) * dimension.NFMAX
        image = ctypes.cast(_lib.image, ctypes.POINTER(t_image))[0]
        return image

    @property
    def source(self):
        """ Return the source array global variable """
        t_source = structure.galaxie * dimension.NFMAX
        source = ctypes.cast(_lib.source, ctypes.POINTER(t_source))[0]
        return source

    @property
    def arclet(self):
        """ Return the arclet array global variable """
        t_arclet = structure.galaxie * dimension.NAMAX
        arclet = ctypes.cast(_lib.arclet, ctypes.POINTER(t_arclet))[0]
        return arclet

    @property
    def narclet(self):
        """ Return the narclet global variable """
        narclet = ctypes.c_long.in_dll(_lib, "narclet").value
        return narclet

    @property
    def multi(self):
        """ Return the multi array global variable """
        t_multi = (structure.galaxie * dimension.NIMAX) * dimension.NFMAX
        multi = ctypes.cast(_lib.multi, ctypes.POINTER(t_multi))[0]
        return multi


    @property
    def ntline(self):
        """ Return the ntline global variable """
        ntline = ctypes.c_int.in_dll(_lib, "ntline").value
        return ntline

    @property
    def nrline(self):
        """ Return the nrline global variable """
        nrline = ctypes.c_int.in_dll(_lib, "nrline").value
        return nrline

    ################################################################################
    # Wrapper fo lenstool C functions
    ################################################################################
    def readBayesModels(self):
        """ Read the content of the bayes.dat found in the current directory """
        n_params = ctypes.c_int(0)
        n_vals = ctypes.c_long(0)
        self._chains = _lib.readBayesModels(n_params, n_vals)
        if n_params.value != self._nparams + 2:  # +2 because ln(Lhood0) and chi2 extra lines in bayes.dat
            raise RuntimeError("Model and bayes.dat number of free parameters mismatch ({0} != {1})".format(self._nparams, n_params.value))
        self._nvals = n_vals.value

    def bayesHeader(self):
        """ Return the name of the parameters in bayes.dat """
        bayes = b' '*60*self.get_NParameters()
        _lib.bayesHeader(bayes)
        names = [s.lstrip('#') for s in bayes.decode('ascii').split('\n')[:-1]] 
        return names

    def get_chains(self, return_ndarray=True):
        """ Return the content of the bayes.dat. If return_ndarray is True, the
            returned numpy.ndarray contains a copy of the internal memory content.

        Parameters
        ----------
            return_ndarray    Return a numpy.ndarray if True, otherwise return the
                              content in the internal ctype square_double_t type.
        """

        if not self._chains:
            logging.debug('Read bayes.dat')
            self.readBayesModels()

        if return_ndarray:
            np_arr = sdarray_ndarray(self._chains, self._nvals, self._nparams + 2)
            self._chains = None
            return np_arr.T, self.bayesHeader()[1:] # remove Nsample column because it doesn't get read by readBayesModel()
        else:
            return self._chains, self._nparams, self._nvals

    def get_NParameters(self):
        """ Return the number of free parameters """
        return _lib.getNParameters()

    def o_chi_lhood0(self):
        """ Return the chi2 and likelihood corresponding to the model and the constraints
        previously stored in memory.

        Returns
        -------
            error, chi, lhood0   The error value (0 is good, 1 is bad model), the chi2 and the normalization likelihood term
        """
        chi = ctypes.c_double(0)
        lhood0 = ctypes.c_double(0)
        np_b0 = ctypes.POINTER(ctypes.c_double)() # NULL pointer
        error = _lib.o_chi_lhood0(chi, lhood0, np_b0)
        return error, chi.value, lhood0.value

    def readConstraints(self):
        """ Read in memory the constraints files (multiple images, arclets,
            etc.) mentioned in the input parameter file.
        """
        _lib.readConstraints()
        self._constraints_read = True

    def setBayesModel(self, method=-4):
        """ Select a model from the bayes.dat samples read with readBayesModels() function.

        Parameters:
            method     Specify the row from the bayes.dat from which a chires.dat file should be calculated. Special values are -4 to use the maximum likelihood row (default).
         """
        if not self._constraints_read:
            readConstraints()

        if not self._chains:
            readBayesModels()

        _lib.setBayesModel(method, self._nvals, self._chains)
        self._modelset = True

    def o_chires(self, filename='chires.dat'):
        """ Write a chires.dat file for the model stored in memory """
        if not self._modelset:
            raise RuntimeError("Model not set in memory. Use setBayesModel() before calling this function")

        _lib.o_chires(bytes(filename,"ascii"))

    def rescaleCube(self, cube, return_rescaled=True):
        """ Rescale the cube values to their corresponding parameter values in
            Lenstool.

            Parameters
                cube[nparams]    The array containing the parameters for all the potentials. Cube values must
                                 follow uniform distributions in the range [0,1].

                return_rescaled  If True, cube values are replaced in-place by their rescaled values.

            Returns
                Return 1 if the rescaled parameters are meaningfull in the
                parameter space, 0 otherwise.
        """
        if len(cube) != self._nparams:
            raise ValueError(f"Cube size mismatch ({0} != {1})".format(len(cube), self._nparams))

        if not all([(v>=0) & (v<=1) for v in cube]):
            raise ValueError("Cube value not in range [0, 1]", cube)

        array = (ctypes.c_double * len(cube))(*cube)
        valid = _lib.rescaleCube_1Atom(array, len(cube))

        self._modelset = True

        if return_rescaled:
            return valid, [v for v in array] # convert ctypes.Array to list 
        else:
            return valid

    def write_bayes_header(self):
        """ Write the header of the bayes.dat file. Open the burnin.dat and bayes.dat files on the disk.

            Return the number of free parameters/dimensions
        """
        if self._fburnin or self._fbayes:
            raise RuntimeError("bayes.dat or burnin.dat files already open. Close them first with close_bayes().")

        nparams = _lib.bayesHeader(0)
        _suffix = b' '*13  # allocate enough memory

        # Need to cast here because by default ctypes converts returned c_void_p into class <int>
        # which is then truncated to 32 bits when passed to close_bayes(), and results in segfault
        self._fburnin = ctypes.cast(_lib.open_mcmc_files(ctypes.c_double(0), _suffix), ctypes.c_void_p)
        self._fbayes = ctypes.cast(_lib.open_mcmc_files(ctypes.c_double(1), _suffix), ctypes.c_void_p)
        _suffix = _suffix.split(b'\x00')[0].decode()
        self._burnin_name = "burnin." + _suffix
        self._bayes_name = "bayes." + _suffix
        self._userCommon = ctypes.cast(_lib.userCommon_init(self._nparams), ctypes.c_void_p)
        return nparams

    def write_burnin_line(self, iteration, likelihood=None, coord=None):
        """ Write a line in the burnin.dat file with the parameter currently set in memory.

            Call o_chi_lhood() function is likelihood is not set.
        """
        if self._fburnin is None:
            raise RuntimeError(f"{self._burnin_name} file not open. Use write_bayes_header() function first.")

        if not coord is None:
            if self.rescaleCube(coord) == 0:
                raise RuntimeError("Invalid model with rescaled cube", cube)

        if likelihood is None:
            error, chi2, lhood0 = self.o_chi_lhood0()
            likelihood = -0.5 * (chi2 + lhood0)

        _lib.write_bayes_line(self._fburnin, ctypes.c_double(likelihood), ctypes.c_double(0),
                              iteration, self._userCommon)

    def write_bayes_line(self, iteration, likelihood=None, coord=None):
        """ Write a line in the bayes.dat file with the parameter currently set in memory.

            Call o_chi_lhood() function is likelihood is not set.
        """
        if self._fbayes is None:
            raise RuntimeError(f"{self._bayes_name} file not open. Use write_bayes_header() function first.")

        if not coord is None:
            if self.rescaleCube(coord) == 0:
                raise RuntimeError("Invalid model with rescaled cube", cube)

        if likelihood is None:
            error, chi2, lhood0 = self.o_chi_lhood0()
            likelihood = -0.5 * (chi2 + lhood0)

        _lib.write_bayes_line(self._fbayes, ctypes.c_double(likelihood), ctypes.c_double(0),
                              iteration, self._userCommon)

    def close_bayes(self):
        """ Close the burnin.dat and bayes.dat files """
        if self._fburnin:
            _lib.close_mcmc_files(self._fbayes, self._fburnin)
            self._fburnin = None
            self._fbayes = None

        if self._userCommon:
            _lib.userCommon_free(self._userCommon)
            self._userCommon = None

    def e_lensing(self):
        """ Run e_lensing() function and return the resulting array of images """
        _lib.e_lensing(_lib.source, _lib.image)
        return self.image

    def create_wcs(self, nx, ny , xmin, xmax, ymin, ymax, ra, dec):
        """ Return a astropy.wcs.WCS object based on boundary, size, and reference center """
        hdr = fits.Header()
        hdr['EQUINOX'] = 2000.0
        hdr['CTYPE1'] = 'RA---TAN'
        hdr['CRVAL1'] = ra
        hdr['CRPIX1'] = -(nx - 1)*xmin/(xmax-xmin) + 1  # nx-1 because if xmin=0, xmax=3, nx=4
        hdr['CDELT1'] = -(xmax-xmin)/(nx-1)/3600        # then delta=(xmax - xmin) / (nx-1)
        hdr['CUNIT1'] = "deg"
        hdr['CTYPE2'] = 'DEC--TAN'
        hdr['CRVAL2'] = dec
        hdr['CRPIX2'] = -(ny - 1)*ymin/(ymax-ymin) + 1
        hdr['CDELT2'] = (ymax-ymin)/(ny-1)/3600
        hdr['CUNIT2'] = "deg"
        return astropy.wcs.WCS(hdr)

    def create_field_wcs(self, nx, ny):
        """ Return a astropy.wcs.WCS object based on field boundary and reference coordinates in F and M global variables """
        return self.create_wcs(nx, ny, self.F.xmin, self.F.xmax, self.F.ymin, self.F.ymax, self.M.ref_ra, self.M.ref_dec)

    def e_pixel_image(self, np):
        """ Compute and return the image of the arcs and arclets in the image plane """
        dx = self.F.xmax - self.F.xmin
        dy = self.F.ymax - self.F.ymin
        nx = np    # default: assign np to nx and adjust ny
        scale = dx / (nx-1)
        ny = int(dy / scale) + 1

        dx = (nx - 1) * scale
        dy = (ny - 1) * scale
        xmax = self.F.xmin + dx
        ymax = self.F.ymin + dy
        xmin, ymin = self.F.xmin, self.F.ymin

        logging.info(
            "image (%d,%d) s=%.3lf [%.3lf,%.3lf] [%.3lf,%.3lf]\n"%(
            nx, ny, scale, xmin, xmax, ymin, ymax)
        )

        image = _lib.alloc_square_double(ny, nx)
        null = ctypes.POINTER(ctypes.c_int)()
        _lib.o_pixel(
            image, nx, ny, ctypes.c_double(scale), 
            ctypes.c_double(xmin), ctypes.c_double(xmax), ctypes.c_double(ymin), ctypes.c_double(ymax), 
            _lib.source, null, null
        )
        image = sdarray_ndarray(image, ny, nx)

        return image, self.create_wcs(nx, ny, xmin, xmax, ymin, ymax, self.M.ref_ra, self.M.ref_dec)

    def e_pixel_source(self, np):
        """ Compute and return the image of the arcs and arclets in the source plane """
        dx = self.ps.xmax - self.ps.xmin
        dy = self.ps.ymax - self.ps.ymin
        self.ps.pixelx = dx / (self.ps.nx-1)
        self.ps.pixely = dy / (self.ps.ny-1)

        logging.info(
            "source (%d,%d) s=(%.3lf,%.3lf) [%.3lf,%.3lf] [%.3lf,%.3lf]\n"%(
            self.ps.nx, self.ps.ny, self.ps.pixelx, self.ps.pixely, self.ps.xmin, self.ps.xmax, self.ps.ymin, self.ps.ymax)
        )

        source = _lib.alloc_square_double(self.ps.ny, self.ps.nx)
        _lib.o_pixel_source(
            source, self.ps.nx, self.ps.ny,
            ctypes.c_double(self.ps.xmin), ctypes.c_double(self.ps.pixelx), 
            ctypes.c_double(self.ps.ymin), ctypes.c_double(self.ps.pixely), 
            self.S.ns, _lib.source
        )
        source = sdarray_ndarray(source, self.ps.ny, self.ps.nx)

        return source, self.create_wcs(
            self.ps.nx, self.ps.ny, 
            self.ps.xmin, self.ps.xmax, self.ps.ymin, self.ps.ymax, 
            self.M.ref_ra, self.M.ref_dec
        )

    def g_mass(self, imass, np, zl, zs):
        """ Compute density maps """
        F = self.F
        nx = np
        ny = int(nx / (F.xmax - F.xmin) * (F.ymax - F.ymin))
        dx = (F.xmax - F.xmin) / (nx - 1)
        dy = (F.ymax - F.ymin) / (ny - 1)

        mass = _lib.alloc_square_double(ny, nx)
        _lib.computeKmap(
                ny, nx, 
                ctypes.c_double(dx),
                ctypes.c_double(dy), 
                mass,
                ctypes.c_double(zl), 
                ctypes.c_double(zs)
        )

        mass = sdarray_ndarray(mass, ny, nx)
        dlsds = _lib.dratio(ctypes.c_double(zl), ctypes.c_double(zs))
        dl = _lib.distcosmo1(ctypes.c_double(zl))
        if imass == 2:
             mass *= constant.cH6piG * self.C.h / dl / dlsds  # in g/cm^2
        if imass == 3:
            mass *= constant.MCRIT12 / self.C.h * dx * dy * dl / dlsds  # in  10^12 M_sol/pixel
        if imass == 4:
            mass *= constant.cH0_4piG * self.C.h / dl / dlsds  # in 10^12 M_sol/kpc^2

        return mass, self.create_field_wcs(nx, ny)

    def g_ampli(self, iamp, np, z):
        """ Compute amplification maps """
        ampli = _lib.alloc_square_double(np, np)
        _lib.g_ampli_implane(iamp, ampli, np, ctypes.c_double(z))
        ampli = sdarray_ndarray(ampli, np, np)
        return ampli, self.create_field_wcs(np, np)

    def g_dpl(self, np, z):
        """ Compute the deflexion fields alpha_x and alpha_y """
        alpha_x = _lib.alloc_square_double(np, np)
        alpha_y = _lib.alloc_square_double(np, np)
        _lib.g_dpl_tmp(np, ctypes.c_double(z), alpha_x, alpha_y)
        alpha_x = sdarray_ndarray(alpha_x, np, np)
        alpha_y = sdarray_ndarray(alpha_y, np, np)
        return alpha_x, alpha_y, self.create_field_wcs(np, np)

    def get_sources(self):
        """ Return a astropy.table.Table object containing a copy of the source catalog """
        tab = astropy.table.Table(
                names=['n', 'x', 'y', 'a', 'b', 'theta', 'z', 'mag'],
                dtype=['str', 'float', 'float', 'float', 'float', 'float', 'float', 'float']
        )

        for i in range(self.S.ns):
            source = self.source[i]
            tab.add_row([
                source.n,
                source.C.x,
                source.C.y,
                source.E.a,
                source.E.b,
                numpy.degrees(source.E.theta),
                source.z,
                source.mag
            ])
        return tab

    def set_sources(self, tab):
        """ Initialise a set of sources based on a astropy.table.Table object """
        if len(tab) > dimension.NFMAX:
            raise ValueError(f"Too many sources in table {len(tab)}. Maximum value = {dimension.NFMAX}")

        for i, row in enumerate(tab):
            source = self.source[i] # prevent from creating source object at every value assignment
            source.n = bytes(row['n'], "ascii")
            source.C.x = row['x']
            source.C.y = row['y']
            source.E.a = row['a']
            source.E.b = row['b']
            source.E.theta = numpy.radians(row['theta'])
            source.z = row['z']
            source.mag = row['mag']
            source.dl0s = _lib.distcosmo2(ctypes.c_double(self.lens[0].z), ctypes.c_double(source.z))
            source.dos = _lib.distcosmo1(ctypes.c_double(source.z))
            source.dr = source.dl0s / source.dos
            source.grad2.a = source.grad2.c = 1
            source.grad.x = source.grad.y = 0
            source.np_grad = None
            source.np_grad2a = source.np_grad2b = source.np_grad2c = None
            source.type = 3
            if row['a'] == 0 and row['b'] == 0:
                source.c = b's'
            else:
                source.c = b'g'
            source.I0 = 50
        self.S.ns = len(tab)

    def get_images(self):
        """ Return a astropy.table.Table object containing a copy of the image catalog """
        tab = astropy.table.Table(
                names=['n', 'x', 'y', 'a', 'b', 'theta', 'z', 'mag'],
                dtype=['str', 'float', 'float', 'float', 'float', 'float', 'float', 'float']
        )

        for i in range(self.S.ns):
            for j in range(dimension.NIMAX):
                image = self.image[i][j]
                if image.n != b'':
                    tab.add_row([
                        image.n,
                        image.C.x,
                        image.C.y,
                        image.E.a,
                        image.E.b,
                        numpy.degrees(image.E.theta),
                        image.z,
                        image.mag
                    ])
                else:
                    break
        return tab

    def get_lenses(self):
        """ Return a astropy.table.Table object containing a copy of the lens array """
        tab = astropy.table.Table(
                names=['type', 'n', 'x', 'y', 'z'],
                dtype=['int', 'str', 'float', 'float', 'float']
        )

        for i in range(self.G.nlens):
            lens = self.lens[i]
            tab.add_row([
                lens.type,
                lens.n,
                lens.C.x,
                lens.C.y,
                lens.z
            ])
        return tab

    def add_lens(self, pot):
        """ Add a new lens to the internal set of potentials. """
        if pot.costheta == 0 and pot.sintheta == 0:
            pot.costheta, pot.sintheta = 1.0, 0.0
        if pot.n == b'':
            pot.n = bytes('%d'%(self.G.no_lens+1), 'ascii')

        self.lens[self.G.nlens] = pot
        self.G.nlens += 1
        if self.G.nmsgrid == -1:
            self.G.nmsgrid = self.G.nlens
        for i in range(self.G.npot):
            self.G.nplens[i] += 1
        self.G.nplens[self.G.npot] = self.G.nmsgrid  # TODO: add support for potfile 
        _lib.set_lens()
        return self.G.nlens

    def set_lenses(self, tab):
        """ Set the lenses in the model from an array of potentials """
        if len(tab) > dimension.NLMAX:
            raise ValueError(f"Too many lenses in array {len(tab)}. Maximum value = {dimension.NLMAX}")

        self.G.nlens = 0
        self.G.nmsgrid = -1
        self.G.npot = 0

        for pot in tab:
            self.add_lens(pot)

        return self.G.nlens

    def criticnew(self, zs=None, xmin=None, ymin=None, xmax=None, ymax=None):
        """ Compute critical and caustic lines and return two arrays of `structure.biline` for the tangential
            and radial critic and caustic lines. These can be parsed using `lenstool.pcl.parse_cline()` function.

            If zs is None, self.S.zs is used.
            If one of xmin, xmax, ymin, ymax is None, corresponding field limit is taken from self.F

            Modified internal variables: CL.cz[0], CL.nplan, F.xmin, F.xmax, F.ymin, F.ymax, tangent, radial

            Return
            ------
            tangent, radial : arrays of `structure.biline`, each for critic and caustic lines
        """
        if zs:
            self.CL.cz[0] = zs
        else:
            self.CL.cz[0] = self.S.zs

        old_f_bounds = [self.F.xmin, self.F.xmax, self.F.ymin, self.F.ymax]
        if xmin:
            self.F.xmin = xmin
        if xmax:
            self.F.xmax = xmax
        if ymin:
            self.F.ymin = ymin
        if ymax:
            self.F.ymax = ymax
        self.CL.nplan = 1

        _lib.criticnew(ctypes.c_int(1)) # verbose mode 1
        t_bilines = structure.biline * dimension.NMAX
        tangent = t_bilines.in_dll(_lib, "tangent")[:self.ntline]
        radial = t_bilines.in_dll(_lib, "radial")[:self.nrline]

        self.F.xmin = old_f_bounds[0]
        self.F.xmax = old_f_bounds[1]
        self.F.ymin = old_f_bounds[2]
        self.F.ymax = old_f_bounds[3]

        return tangent, radial

    def reset_grid(self):
        """ Call this function after modifying self.F """
        _lib.grid()

    def init_cosmoratio(self, cp0=None):
        """ Initialize the internal cosmological distance look-up-table 

        Parameters
        ----------
        cp0: list of tuples
            Each list element should contain a tuple (z1, z2, angular_diameter_distance(z1, z2))

        """
        if isinstance(cp0, list):
            array = (structure.cpoint * len(cp0))()
            for i, cp in enumerate(cp0):
                array[i].z1 = ctypes.c_double(cp[0])
                array[i].z2 = ctypes.c_double(cp[1])
                array[i].d = ctypes.c_double(cp[2] * self.C.h / constant.D0Mpc)
            _lib.init_cosmoratio(len(cp0), array)
        else:
            _lib.init_cosmoratio(0, None)

    def set_cosmology(self, H0, omegaM, omegaX, wX=-1, wa=0, kcourb=0, model=1):
        """ Initialize internal cosmology variables
        
        Parameters
        ----------
        model
            Several models are implemented (see cosmoratio.c). model=1 is Lambda CDM model.
        """
        self.C.H0 = H0
        self.C.omegaM = omegaM
        self.C.omegaX = omegaX
        self.C.kcourb = kcourb
        self.C.wX = wX
        self.C.wa = wa
        self.C.model = model
        self.C.h = self.C.H0 / constant.h0

    def set_field(self, field, reset_grid=True):
        """ Set the field section (internal F global variable) 
    
        Parameter
        ---------
        field : list or scalar
            If a list, set the xmin, xmax, ymin, ymax value. If a scalar, set the dmax value centered on the lens
        """
        if numpy.isscalar(field):
            self.F.rmax = field * numpy.sqrt(2.)
            self.F.xmin, self.F.xmax = -field, field
            self.F.ymin, self.F.ymax = -field, field
        else:
            self.F.xmin, self.F.xmax = field[0], field[1]
            self.F.ymin, self.F.ymax = field[2], field[3]
            self.F.rmax = max(numpy.abs(field)) * numpy.sqrt(2.)

        if reset_grid:
            self.reset_grid() 

    def get_field(self, field):
        """ Return the contient of the field section (internal F global variable) """
        return self.F.xmin, self.F.xmax, self.F.ymin, self.F.ymax

    def set_grid(self, number, polar, dx=5, dy=5):
        """ Initialize the grid for searching multiple images """

        if number > dimension.NGGMAX:
            raise ValueError(f"number {number} too large. Maximum value = {dimension.NGGMAX}")

        self.G.ngrid = 2*(number//2) if number >= 10 else 10
        self.G.pol = polar
        self.G.dx = dx
        self.G.dy = dy
        _lib.grid()

    def o_print_res(self):
        """ Write best.par and bestopt.par """
        error, chi2, lhood0 = self.o_chi_lhood0()
        _lib.o_print_res(ctypes.c_double(chi2), 0)
