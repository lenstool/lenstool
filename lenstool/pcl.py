#!/usr/bin/env python
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.collections import LineCollection
import os.path
import argparse

def define_options():
    parser = argparse.ArgumentParser(
            description="This script display critical and caustic lines."
        )
    parser.add_argument('--ext', action='store_true', 
        help="Only display external critical and caustic lines.")
    parser.add_argument('--int', action='store_true', 
        help="Only display internal critical and caustic lines.")
    parser.add_argument('--critic', action='store_true', 
        help="Only display critical lines.")
    parser.add_argument('--caustic', action='store_true', 
        help="Only display caustic lines.")
    return parser
    
def parse_header(line):
    """ Parse the #REFERENCE line of a Lenstool data file.

    Return
    ------
        iref, ra, dec : tuple with WCS centering information
    """
    iref, ra, dec = line.split(" ")[1:]
    iref = int(iref)
    if iref != 0 and iref != 3:
        raise RuntimeError("Unknown header #REFERENCE format", line)

    return iref, float(ra), float(dec)

def convert_radec(x, y, iref, ra, dec):
    """ Convert coordinates to WCS world coordinates 

    Parameters
    ----------
        x, y: numpy.ndarray
            Coordinates to convert. They can be either in absolute world coordinates, or in arcsec coordinates relative to `ra`, `dec` reference position.
        iref: int
            WCS flag. 0: x,y are in absolute coordinates, 3: x,y are in relative coordinates

    Return
    ------
        x, y: numpy.ndarray
            If `iref` = 3, converted absolute coordinates, otherwise input (x,y) unchanged.
    """
    if iref == 3:
        x = x / (-3600*np.cos(np.radians(dec))) + ra
        y = y / 3600 + dec
    return x, y


def create_lines(lid, x, y):
    """ Return lines associated to a list of positions.
        A line is drawn between 2 successive rows with the same index `lid`.
    """
    split_indexes = np.where(np.diff(lid) != 0)[0] + 1
    x = np.array_split(x, split_indexes)
    y = np.array_split(y, split_indexes)
    return [[[xx[0],yy[0]],[xx[1],yy[1]]] for xx, yy in zip(x, y)]

def parse_cline_file(filename, return_critic=True, return_caustic=True):
    """ Parse ce.dat or ci.dat and return critic and caustic polygons """
    if not os.path.exists(filename):
        raise RuntimeError(f"File {filename} doesn't exist")

    critic, caustic = None, None

    with open(filename, 'r') as fh:
        iref, ra, dec  = parse_header(fh.readline())
        try:
            lid, xi, yi, xs, ys = np.loadtxt(fh, unpack=True)
        except:
            return None, None

        xi, yi = convert_radec(xi, yi, iref, ra, dec)
        xs, ys = convert_radec(xs, ys, iref, ra, dec)
        if return_critic:
            critic = LineCollection(create_lines(lid, xi, yi), color='r')

        if return_caustic:
            caustic = LineCollection(create_lines(lid, xs, ys), color='y')

    return critic, caustic

def parse_cline(bilines, ra=0, dec=0):
    """ Convert an array of `structure.biline` objects to a LineCollection
        for the critical and caustic lines to be used with `Axes.add_collection()`.
    """
    array = np.array(
            [(obj.i, obj.I.x, obj.I.y, obj.S.x, obj.S.y) for obj in bilines],
            dtype = [('lid','int'), ('xi','float'), ('yi','float'), ('xs','float'), ('ys','float')]
    )
    array['xi'], array['yi'] = convert_radec(array['xi'], array['yi'], 3, ra, dec)
    array['xs'], array['ys'] = convert_radec(array['xs'], array['ys'], 3, ra, dec)
    critic = LineCollection(create_lines(array['lid'], array['xi'], array['yi']), color='r')
    caustic = LineCollection(create_lines(array['lid'], array['xs'], array['ys']), color='y')
    return critic, caustic

if __name__ == "__main__":
    parser = define_options()
    args = parser.parse_args()

    critic_int, critic_ext, caustic_int, caustic_ext = None, None, None, None

    if not args.ext:
        critic_int, caustic_int = parse_cline_file(
            "ci.dat",
            return_critic = not args.caustic,
            return_caustic = not args.critic
        )

    if not args.int:
        critic_ext, caustic_ext = parse_cline_file(
            "ce.dat",
            return_critic = not args.caustic,
            return_caustic = not args.critic
        )

    fig, ax = plt.subplots(1)
    for poly in [critic_int, critic_ext, caustic_int, caustic_ext]:
        if poly:
            ax.add_collection(poly)
    ax.autoscale_view()
    plt.show()
