import ctypes
from ctypes import *
from .dimension import *

class Structure(ctypes.Structure):
    def __str__(self):
        return ', '.join([f'{f[0]} {getattr(self, f[0])}' for f in self._fields_])

class point(Structure):
    _fields_ = [
        ("x", c_double),
        ("y", c_double)
    ]

class cpoint(Structure):
    _fields_ = [
        ("next", c_void_p),
        ("prev", c_void_p),
        ("z1", c_double),
        ("z2", c_double),
        ("d", c_double)
    ]

class ellipse(Structure):
    _fields_ = [
        ("a", c_double),
        ("b", c_double),
        ("theta", c_double)
    ]

class matrix(Structure):
    _fields_ = [
        ("a", c_double),
        ("b", c_double),
        ("c", c_double),
        ("d", c_double)
    ]

class biline(Structure):
    _fields_ = [
        ("i", c_int),
        ("I", point),
        ("S", point)
    ]


class Bspline2D_red(Structure):
     _fields_ = [
        ("degx", c_int),
        ("degy", c_int),
        ("nx", c_int),
        ("ny", c_int),
        ("Pz",POINTER(POINTER(c_double))),
        ("kx",POINTER(c_double)),
        ("ky",POINTER(c_double))
    ]



class Bspline2D(Structure):
    _fields_ = [
        ("degx", c_int),
        ("degy", c_int),
        ("n", c_int),
        ("nx", c_int),
        ("ny", c_int),
        ("Pz",POINTER(POINTER(c_double))),
        ("der10", Bspline2D_red),
        ("der01", Bspline2D_red),
        ("der20", Bspline2D_red),
        ("der11", Bspline2D_red),
        ("der02", Bspline2D_red),
        ("kx",POINTER(c_double)),
        ("ky",POINTER(c_double)),
        ("x_c",c_double),
        ("y_c",c_double),
        ("size_sq",c_double),
        ("lim",c_double),
        ("coeff_file", c_char * FILENAME_SIZE),
    ]

class g_pixel(Structure):
    _fields_ = [
        ("column", c_int),
        ("ech", c_int),
        ("format", c_int),
        ("pixfile", c_char * FILENAME_SIZE),
        ("ncont", c_int),
        ("outfile", c_char * FILENAME_SIZE),
        ("contfile", (c_char * FILENAME_SIZE) * 10),
        ("pixelx", c_double),
        ("pixely", c_double),
        ("header", c_int),
        ("xmin", c_double),
        ("xmax", c_double),
        ("ymin", c_double),
        ("ymax", c_double),
        ("nx", c_int),
        ("ny", c_int),
        ("meanFlux", c_double),
        ("wcsprm", c_void_p),  # TODO: implement the wcsinfo pointer
        ("array", POINTER(POINTER(c_double)))
    ]

class g_source(Structure):
    _fields_ = [
        ("multi_s", point * NFMAX),
        ("multfile", c_char * FILENAME_SIZE),
        ("n_mult", c_int),
        ("multmode", c_int),
        ("mult", c_int * NFMAX),
        ("grid", c_int),
        ("rand", c_int),
        ("ns", c_long),
        ("distz", c_int),
        ("emax", c_double),
        ("zs", c_double),
        ("zsmin", c_double),
        ("zsmax", c_double),
        ("par1", c_double),
        ("par2", c_double),
        ("par3", c_double),
        ("taille", c_double),
        ("lfalpha", c_double),
        ("lfm_star", c_double),
        ("lfm_min", c_double),
        ("lfm_max", c_double)
    ]

class g_image(Structure):
    _fields_ = [
        ("random", c_int),
        ("nzlim", c_int),
        ("npcl", c_int),
        ("shmap", c_int),
        ("zsh", c_double),
        ("dl0ssh", c_double),
        ("dossh", c_double),
        ("drsh", c_double),
        ("shfile", c_char * FILENAME_SIZE),
        ("stat", c_int),
        ("statmode", c_int),
        ("arclet", c_char * FILENAME_SIZE),
        ("nza", c_char * 10),
        ("zarclet", c_double),
        ("c_drarclet", c_double),
        ("sigell", c_double),
        ("dsigell ", c_double),
        ("sig2ell", c_double),
        ("srcfit    ", c_int),
        ("nsrcfit   ", c_long),
        ("srcfitFile", c_char * FILENAME_SIZE),
        ("srcfitMethod", c_char * 50),
        ("forme", c_int),
        ("n_mult", c_int),
        ("multmode", c_int),
        ("mult_abs", c_int),
        ("mult", c_int * NFMAX),
        ("multfile", c_char * FILENAME_SIZE),
        ("sig2pos", (c_double * NIMAX) * NFMAX),
        ("weight", POINTER(POINTER(c_double))),
        ("detCov", c_double),
        ("sig2amp", c_double),
        ("Dmag", c_double),
        ("adjust", c_int),
        ("Afile", c_char * FILENAME_SIZE),
        ("Anx", c_int),
        ("Any", c_int),
        ("Abin", c_int),
        ("Anfilt", c_int),
        ("Anpixseeing", c_int),
        ("Apixel", c_double),
        ("Aseeing", c_double),
        ("Axmin", c_double),
        ("Axmax", c_double),
        ("Aymin", c_double),
        ("Aymax", c_double),
        ("Amean", c_double),
        ("Adisp", c_double),
        ("newton ", c_int)
    ]

class g_grille(Structure):
    _fields_ = [
        ("ngrid", c_int),
        ("pol", c_int),
        ("nlens", c_long),
        ("nplens", c_long * (NPOTFILE+1)),
        ("npot", c_int),
        ("nlens_crit", c_long),
        ("no_lens", c_long),
        ("nmsgrid", c_long),
        ("splinefile", c_char * FILENAME_SIZE),
        ("xmin", c_double),
        ("xmax", c_double),
        ("ymin", c_double),
        ("ymax", c_double),
        ("dx", c_double),
        ("dy", c_double),
        ("nx", c_int),
        ("ny", c_int),
        ("echant", c_int),
        ("exc", c_double),
        ("excmin", c_double),
        ("invmat", POINTER(POINTER(c_double)))
    ]

class g_mode(Structure):
    _fields_ = [
        ("verbose", c_int),
        ("sort", c_int),
        ("grille", c_int),
        ("ngrille", c_int),
        ("zgrille", c_double),
        ("inverse", c_int),
        ("itmax", c_int),
        ("rate", c_double),
        ("minchi0", c_double),
        ("ichi2", c_int),
        ("image", c_int),
        ("imafile", c_char * FILENAME_SIZE),
        ("source", c_int),
        ("sourfile", c_char * FILENAME_SIZE),
        ("sof", c_int),
        ("imsfile", c_char * FILENAME_SIZE),
        ("sfile", c_char * FILENAME_SIZE),
        ("icorshear", c_int),
        ("corshfile", c_char * FILENAME_SIZE),
        ("local", c_int),
        ("localfile", c_char * FILENAME_SIZE),
        ("study", c_int),
        ("studyfile", c_char * FILENAME_SIZE),
        ("fake", c_int),
        ("mean", c_int),
        ("seeing", c_double),
        ("iampli", c_int),
        ("nampli", c_int),
        ("zampli", c_double),
        ("amplifile", c_char * FILENAME_SIZE),
        ("ipoten", c_int),
        ("npoten", c_int),
        ("zpoten", c_double),
        ("potenfile", c_char * FILENAME_SIZE),
        ("imass", c_int),
        ("nmass", c_int),
        ("zmass", c_double),
        ("massfile", c_char * FILENAME_SIZE),
        ("idpl", c_int),
        ("ndpl", c_int),
        ("zdpl", c_double),
        ("dplxfile", c_char * FILENAME_SIZE),
        ("dplyfile", c_char * FILENAME_SIZE),
        ("icurv", c_int),
        ("ncurv", c_int),
        ("zcurv", c_double),
        ("cxxfile", c_char * FILENAME_SIZE),
        ("cxyfile", c_char * FILENAME_SIZE),
        ("cyyfile", c_char * FILENAME_SIZE),
        ("ishearf", c_int),
        ("zshearf", c_double),
        ("shearffile", c_char * FILENAME_SIZE),
        ("nshearf", c_int),
        ("iamplif", c_int),
        ("zamplif", c_double),
        ("ampliffile", c_char * FILENAME_SIZE),
        ("ishear", c_int),
        ("nshear", c_int),
        ("zshear", c_double),
        ("shearfile", c_char * FILENAME_SIZE),
        ("itime", c_int),
        ("ntime", c_int),
        ("ztime", c_double),
        ("timefile", c_char * FILENAME_SIZE),
        ("prop", c_int),
        ("nprop", c_int),
        ("zprop", c_double),
        ("propfile", c_char * FILENAME_SIZE),
        ("radius", c_double),
        ("masse", c_double),
        ("pixel", c_int),
        ("npixel", c_int),
        ("pixelfile", c_char * FILENAME_SIZE),
        ("cube", c_int),
        ("nslices", c_int),
        ("cubefile", c_char * FILENAME_SIZE),
        ("iref", c_int),
        ("ref_ra", c_double),
        ("ref_dec", c_double),
        ("marker", c_int),
        ("zmarker", c_double),
        ("markfile", c_char * FILENAME_SIZE),
        ("radial", c_int),
        ("zradial", c_double),
        ("theta", c_double),
        ("iclean", c_int),
        ("zclean", c_double),
        ("flux", c_int),
        ("output", c_int),
        ("centerfile", c_char * FILENAME_SIZE),
        ("restart", c_int),
        ("restart_seed", c_int),
        ("servermode_url", c_char * 200),
    ]

class g_pot(Structure):
    _fields_ = [
        ("potid", c_int),
        ("ftype", c_int),
        ("potfile", c_char * FILENAME_SIZE),
        ("type", c_int),
        ("zlens", c_double),
        ("core", c_double),
        ("corekpc", c_double),
        ("mag0", c_double),
        ("select", c_int),
        ("ircut", c_int),
        ("cut", c_double),
        ("cut1", c_double),
        ("cut2", c_double),
        ("cutkpc1", c_double),
        ("cutkpc2", c_double),
        ("isigma", c_int),
        ("sigma", c_double),
        ("sigma1", c_double),
        ("sigma2", c_double),
        ("islope", c_int),
        ("slope", c_double),
        ("slope1", c_double),
        ("slope2", c_double),
        ("ivdslope", c_int),
        ("vdslope", c_double),
        ("vdslope1", c_double),
        ("vdslope2", c_double),
        ("ivdscat", c_int),
        ("vdscat", c_double),
        ("vdscat1", c_double),
        ("vdscat2", c_double),
        ("ircutscat", c_int),
        ("rcutscat", c_double),
        ("rcutscat1", c_double),
        ("rcutscat2", c_double),
        ("ia", c_int),
        ("a", c_double),
        ("a1", c_double),
        ("a2", c_double),
        ("ib", c_int),
        ("b", c_double),
        ("b1", c_double),
        ("b2", c_double),
        ("opt", c_int)
    ]

class g_frame(Structure):
    _fields_ = [
	("xmin", c_double),
	("xmax", c_double),
	("ymin", c_double),
	("ymax", c_double),
	("lmin", c_double),
	("lmax", c_double),
	("rmax", c_double)
    ]

class g_cline(Structure):
    _fields_ = [
        ("nplan", c_int),
        ("cz", c_double * NPZMAX),
        ("zone", c_int),
        ("npzone", c_int),
        ("zonefile", c_char * FILENAME_SIZE),
        ("cpas", c_double),
        ("dmax", c_double),
        ("algorithm", c_char * 20),
        ("limitHigh", c_double)
    ]

class g_cosmo(Structure):
    _fields_ = [
        ("model", c_int),
        ("omegaM", c_double),
        ("omegaX", c_double),
        ("kcourb", c_double),
        ("wX", c_double),
        ("wa", c_double),
        ("H0", c_double),
        ("h", c_double)
    ]

class pot(Structure):
    _fields_ = [
        ("type", c_int),
        ("n", c_char * IDSIZE),
        ("C", point),
        ("epot", c_double),
        ("emass", c_double),
        ("theta", c_double),
        ("phi", c_double),
        ("sigma", c_double),
        ("rc", c_double),
        ("rckpc", c_double),
        ("rcut", c_double),
        ("rcutkpc", c_double),
        ("alpha", c_double),
        ("beta", c_double),
        ("rcslope", c_double),
        ("z", c_double),
        ("dlsds", c_double),
        ("psicut", c_double),
        ("psimcut", c_double),
        ("psiccut", c_double),
        ("b0", c_double),
        ("masse", c_double),
        ("pmass", c_double),
        ("cr", c_double),
        ("ct", c_double),
        ("mag", c_double),
        ("lum", c_double),
        ("mtol", c_double),
        ("effradius", c_double),
        ("costheta", c_double),
        ("sintheta", c_double),
        ("dplxmap", POINTER(g_pixel)),
        ("dplymap", POINTER(g_pixel)),
        ("kappamap", POINTER(g_pixel)),
        ("gamma1map", POINTER(g_pixel)),
        ("gamma2map", POINTER(g_pixel)),
        ("potenmap", POINTER(g_pixel)),
        ("Bspline", POINTER(Bspline2D)) # TODO: implement Bspline2D type
    ]

class galaxie(Structure):
    _fields_ = [
        ("n", c_char * IDSIZE),
        ("C", point),
        ("Grad", point),
        ("E", ellipse),
        ("E2", ellipse),
        ("c", c_char),
        ("type", c_int),
        ("magabs", c_double),
        ("mag", c_double),
        ("mag2", c_double),
        ("flux", c_double),
        ("mu", c_double),
        ("I0", c_double),
        ("z", c_double),
        ("dl0s", c_double),
        ("dos", c_double),
        ("dr", c_double),
        ("q", c_double),
        ("eps", c_double),
        ("eps2", c_double),
        ("tau", c_double),
        ("dis", c_double),
        ("A", c_double),
        ("ep", c_double),
        ("tp", c_double),
        ("dp", c_double),
        ("thp", c_double),
        ("taux", c_double),
        ("tauy", c_double),
        ("time", c_double),
        ("kappa", c_double),
        ("gamma1", c_double),
        ("gamma2", c_double),
        ("var1", c_double),
        ("var2", c_double),
        ("grad", point),
        ("grad2", matrix),
        ("np_grad", POINTER(point)),
        ("np_grad2a", POINTER(c_double)),
        ("np_grad2b", POINTER(c_double)),
        ("np_grad2c", POINTER(c_double)),
        ("gsource", POINTER(POINTER(POINTER(point)))),
        ("grid_dr", c_double)
    ]
