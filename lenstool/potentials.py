import numpy
import ctypes
from . import DBL_MAX
from .structure import *

class sis(pot):
    def __init__(self, x, y, z, sigma):
        """ Initialize a SIS potential """
        self.type = 1
        self.C.x = x
        self.C.y = y
        self.z = z
        self.sigma = sigma

class dpie(pot):
    def __init__(self, x, y, emass, theta, z, sigma, rc=None, rcut=None, rckpc=None, rcutkpc=None):
        """ Initialize a dPIE potential """
        self.type = 81
        self.C.x = x
        self.C.y = y
        self.emass = emass
        self.theta = numpy.radians(theta)
        self.costheta = numpy.cos(numpy.radians(theta))
        self.sintheta = numpy.sin(numpy.radians(theta))
        self.z = z
        self.sigma = sigma
        if rc and rckpc is None:
            self.rc, self.rckpc = rc, 0.
        elif rckpc and rc is None:
            self.rc, self.rckpc = 0., rckpc
        else:
            raise RuntimeError("Invalid model definition with rc and rckpc")

        if rcut and rcutkpc is None:
            self.rcut, self.rcutkpc = rcut, numpy.finfo('float64').max
        elif rcutkpc and rcut is None:
            self.rcut, self.rcutkpc = numpy.finfo('float64').max, rcutkpc
        else:
            raise RuntimeError("Invalid model definition with rcut and rcutkpc")

class nfw(pot):
    def __init__(self, x, y, emass, theta, z, sigma=None, rs=None, rskpc=None, r200=None, r200kpc=None, m200=None, c200=None):
        """ Initialize an elliptical NFW potential following the Golse et al. 2002 prescription
        
        Parameters
        ----------
            x, y : floats
                Distance to the center of the field in arcsec
            emass, theta : floats
                Ellipticity and orientation of the mass distribution
            z : float
                Redshift of the potential
            sigma : float
                Velocity dispersion of the NFW potential in km/s
            rs : float
                Scale radius of the NFW in arcsec
            r200 : float
                Radius at 200 times the critical density (3H(z)^2/8piG) in arcsec
            rskpc : float
                Scale radius of the NFW in kpc
            r200kpc : float
                Radius at 200 times the critical density (3H(z)^2/8piG) in kpc
            m200 : float
                Mass at 200 times the critical density in Msun
            c200 : float
                Concentration parameter rs / r200
        """
        self.type = 12
        self.C.x = x
        self.C.y = y
        self.emass = emass
        self.theta = numpy.radians(theta)
        self.costheta = numpy.cos(numpy.radians(theta))
        self.sintheta = numpy.sin(numpy.radians(theta))
        self.z = z
        self.sigma = sigma if sigma else 0.
        self.masse = m200 if m200 else 0.
        self.beta = c200 if c200 else 0.
        self.rc = rs if rs else 0.
        self.rckpc = rskpc if rskpc else 0.
        self.rcut = r200 if r200 else DBL_MAX
        self.rcutkpc = r200kpc if r200kpc else DBL_MAX
