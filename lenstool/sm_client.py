# small class for send and recive messages from lenstool in server mode
# we use zeromq for send/recive messages. 
# messages are passing in form of ascii string
# logically message consist of
# 1 integer (tag) and several double (vector)
from __future__ import print_function
import zmq
import sys
import numpy as np
import tempfile
import subprocess
import os

class client:    
    context = 0
    socket  = 0

    def __init__(self, addr):
        self.context = zmq.Context()
        self.socket  = self.context.socket(zmq.REQ)
        self.socket.connect(addr)
    def send(self, tag, vec):
        msg = str(tag)
        for d in vec:
            msg = msg + " " + "%.18g" % d
        msg = msg.encode('ascii')
        self.socket.send(msg)
    def recv(self):
        vstr = self.socket.recv().split()
        if (len(vstr) == 0):
            sys.exit("Error, we've recived empty message")
        tag = int(vstr[0])
        vstr.pop(0)
        return tag, list(map(float, vstr))

    
    # some "high level" functions for communicate with lenstool

    def req_ndim(self):
        self.send(1, []) # request ndim from lenstool
        [ndim, vec] = self.recv()
        return ndim
    
    def req_calc_lnprob(self, x):
        self.send(2, x) # request to calculte prob from lenstool        
        [error, vec] = self.recv()
        if error: 
            return -np.inf
        return vec[0]
    
    # request lenstool to initialize output files (burnin.dat/bayes.dat)
    def req_init_files(self):
        self.send(10, [])
        [error, vec] = self.recv()
        if (error):
            sys.exit("Error, Bad response from the server (req_init_files)")
    

    def req_write_wtag(self, idx, lnprob, vx, tag):
        # vx could be numpy array, so we must not use "+" 
        to_send = [float(idx)] + [lnprob];
        for x in vx:
            to_send.append(x)
        self.send(tag, to_send)
        [error, vec] = self.recv()
        if (error):
            sys.exit("Error, Bad response from the server (req_write_tag) " + str(tag))
            
    def req_write_burnin(self, idx, lnprob, vx):
        self.req_write_wtag(idx, lnprob, vx, 11)

    def req_write_bayes(self, idx, lnprob, vx):
        self.req_write_wtag(idx, lnprob, vx, 12)

    def req_write_bayes_withchi2(self, idx, lnprob, vx):
        self.req_write_wtag(idx, lnprob, vx, 13)


# class for launch lenstool server
class slauncher:
    ipc_url = ""
    def __init__(self, lenstool, global_parfile, nthreads, index):
        self.local_parfile = global_parfile + '_' + str(index)
        self.ipc_file = tempfile.NamedTemporaryFile(prefix="lenstool_")
        self.ipc_url  = "ipc://" + self.ipc_file.name
        print(f"[{os.getpid()}] Create url {self.ipc_url}")
        
        # in local_parfile, replace "inverse 6" with "inverse 6 ipc_url" and create local_parfile        
        print(f"[{os.getpid()}] Create {self.local_parfile}")
        with open(self.local_parfile, "w") as out:
            with open(global_parfile, "r") as ifile:
                for line in ifile.readlines():
                    if 'inverse' in line:
                        out.write(f"    inverse 6 {self.ipc_url}\n")
                    else:
                        out.write(line)
        
        # launch lenstool subprocesses
        print(f"[{os.getpid()}] Create screen_out_{index}")
        self.lenstool_out     = open("screen_out_" + str(index), "w")
        self.lenstool_process = subprocess.Popen([lenstool, self.local_parfile, "-n"], 
                                                 env = dict(os.environ, **{"OMP_NUM_THREADS" : str(nthreads)}), 
                                                 stdout=self.lenstool_out, 
                                                 stderr=self.lenstool_out)
    def clean(self):
        print(f"[{os.getpid()}] Terminate process")
        self.lenstool_process.terminate()
        self.lenstool_process.wait()
#        os.unlink(self.local_parfile)

    def __del__(self):        
        self.clean()


