
NGGMAX 	 = 2000   # maximum grid points in the I->S mapping 
NAMAX 	 = 200000   # maximum number of arclets
ASMAX 	 = 1000  # maximum number of arclets for study
IDSIZE	 = 10    # size in characters of the id of clumps and images
ZMBOUND  =   10    # maximum number of redshift bounded families
NGMAX 	 = 500  	# maximum number of point with the grille command 
NMAX 	 = 200000  # maximum number of segments for the critical lines 
NPMAX 	 = 5000
NPZMAX	 = 9	# maximum number of critical lines in g_cline struct*/
NLMAX 	 = 4000    # maximum number of clumps in the lens[] array
NIMAX 	 = 8       # maximum images per family 
NFMAX 	 = 50000    # maximum number of families
NPAMAX 	 = 187    # number of free parameters (see #define in structure.h)
NTMAX 	 = 1024
NPOINT 	 = 5000  # Number of contour points in cleanlens mode
NPARMAX  = 50	
NMCMAX   = 600	
NSRCFIT  = 30    # Number of points for source plane fitting
NPOTFILE =  6    # Maximum number of potfiles

ARRAY_SIZE = 500000

# zero pour les calculs de dichotomie, amplification infinie, pente nulle
PREC_ZERO = 	.00001
# erreur sur dlsds pour le calcul inverse source->image
PREC_DLSDS = 	.00001
# nombre maximal de points sur une ligne critique tangentielle ou radiale*/
NTLINEMAX =    250
NRLINEMAX =    250
DMIN =	1e-4	# distance minimale de convergence dans le plan image (in arcsec)	
NITMAX =	100

IDPARAM1 = 1   # column of the 1st physical parameter in array (cf readBayesModel.c)
LINESIZE = 16000  # size of a line in bayes.dat  
FILENAME_SIZE  = 100  # size of a filename in .par file
DISTCOSMO2_ZPREC = 1e-3  # precision below which 2 redshifts are considered the same
LHOOD_BUFNAME = "restart.dat"  # name of the restart file
