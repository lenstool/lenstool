#! /bin/bash

set -ex

./configure --prefix=$CONDA_PREFIX --with-cfitsio-prefix=$CONDA_PREFIX --with-wcslib-include-path=$CONDA_PREFIX/include/wcslib --with-wcslib-lib-path=$CONDA_PREFIX/lib --with-gsl-prefix=$CONDA_PREFIX || { cat config.log ; exit 1 ; }

make -j
make install

python3 -m pip install -vv --no-deps --ignore-installed .
