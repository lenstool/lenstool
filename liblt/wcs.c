#include<assert.h>
#include<string.h>
#include <wcslib.h>
#include "lt.h"
#include "errors.h"

/****************************************************************/
/*      nom:        wcs.c            */
/*      auteur:     Eric Jullo          */
/*      date:       11/08/2022            */
/*      place:      Santiago            */
/****************************************************************/

/* Some user WCS friendly methods */

struct wcsprm* initwcs(char* header)
{
    int nkey, nreject, nwcs;
    struct wcsprm *wcs;

    assert( strlen(header) % 80 == 0 );
    nkey = strlen(header) / 80;

    if ( wcspih(header, nkey, 0, 2, &nreject, &nwcs, &wcs) )
        RAISE(E_FILE, "ERROR: reading WCS information\n")
    
    if ( wcsset(wcs) ) 
        RAISE(E_FILE, "ERROR: setting up wcsprm struct\n")
    else 
        return wcs;
}

// Return the image center and size in relative coordinates
void wcsfull(struct g_pixel *ps, double *ra, double *dec, double *width, double *height)
{
    double world[5*2], imgcrd[5*2], pixcrd[5*2], phi[5], theta[5];
    int stat[5];
    pixcrd[0] = ps->nx * 0.5 + 0.5;  // ra
    pixcrd[1] = ps->ny * 0.5 + 0.5;  // dec
    pixcrd[2] = 0.500001; // xmin
    pixcrd[3] = ps->ny * 0.5 + 0.5;  // dec
    pixcrd[4] = ps->nx + 0.499999;  // xmax
    pixcrd[5] = ps->ny * 0.5 + 0.5;  // dec
    pixcrd[6] = ps->nx * 0.5 + 0.5;  // ra
    pixcrd[7] = 0.500001;  // ymin
    pixcrd[8] = ps->nx * 0.5 + 0.5;  // ra
    pixcrd[9] = ps->ny + 0.499999;  // ymax
    wcsp2s(ps->wcsinfo, 5, 2, pixcrd, phi, theta, imgcrd, world, stat);
    *ra = world[0]; 
    *dec = world[1];
    *width = sqrt (((world[5]-world[3]) * (world[5]-world[3])) + 
                   ((world[4]-world[2]) * (world[4]-world[2])));
    *height = sqrt (((world[9]-world[7]) * (world[9]-world[7])) + 
                    ((world[8]-world[6]) * (world[8]-world[6])));
}



