/********************************************************************/
/* Print out cfitsio error messages and exit program */
/*****************************************************/
#include<stdlib.h>
#include "fitsio.h"
#include "lt.h"
#include "errors.h"

void printerror( int status)
{

    if (status)
    {
       fits_report_error(stderr, status); /* print error report */

       fits_get_errstatus(status, error_string);
       error_number = status;
       abort();    /* terminate the program, returning SIGABRT signal */
    }
}
