#ifndef ERRORS_H
#define ERRORS_H
#include<stdio.h>
#include<string.h>
#include<signal.h>

// Error exit codes
#define E_COSMO_MODEL -10
#define E_ARG_TOO_LONG -20
#define E_ZALIM_MISSING -30
#define E_NMAX_REACHED -40
#define E_PARFILE -50
#define E_NLMAX_REACHED -60
#define E_FILE -70
#define E_RUNTIME -80
#define E_MEMORY -90

extern char error_string[500];
extern int  error_number;
#define RAISE(err_no, args...) \
    { sprintf(error_string, args); \
    error_number = err_no; \
    fprintf(stderr, "%s", error_string); \
    raise(SIGTERM); }

#endif // if ERRORS_H
