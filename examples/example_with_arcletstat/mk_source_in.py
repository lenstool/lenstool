import numpy as np

e_rms = 0.3
scat = np.loadtxt('source.dat')
e = np.random.normal(scale=e_rms*np.sqrt(2.), size=scat.shape[0])
phi = np.random.uniform(size=scat.shape[0])*180
e[e>0.95] = 0.95
e[e<-0.95] = -0.95
a = np.sqrt(1+e)
b = np.sqrt(1-e)
scat[:,3] = a
scat[:,4] = b
scat[:,5] = phi
with open('source_in.dat', 'w') as fh:
    fh.write('#REFERENCE 3 0 0\n')
    np.savetxt(fh,scat,fmt='%d %.10f %.10f %.6f %.6f %.4f %.3f %.2f')

