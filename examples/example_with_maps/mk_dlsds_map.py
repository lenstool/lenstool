# Rescale maps to DLS/DS = 1
from astropy.cosmology import FlatLambdaCDM
from astropy.io import fits

cosmo = FlatLambdaCDM(70, 0.3)

dlsds = cosmo.angular_diameter_distance_z1z2(0.3, 1.0) / cosmo.angular_diameter_distance(1.0)

for f in ['kappa_ref.fits','gamma1_ref.fits','gamma2_ref.fits','dplx_ref.fits','dply_ref.fits']:
    hdul = fits.open(f)
    hdul[0].data /= dlsds.value
    hdul.writeto(f[:-5] + '_dlsds.fits', overwrite=True)

hdul = fits.open('poten_ref.fits')
hdul[0].data /= dlsds.value * cosmo.angular_diameter_distance(0.3).value
hdul.writeto('poten_ref_dlsds.fits', overwrite=True)
