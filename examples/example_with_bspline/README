EXAMPLE OF MODEL WITH A B-SPLINE PERTURBATION:

For more details about the implementation, see Jullo et al. (2007) for the non-perturbative part and Beauchesne et al. (2021)
for the B-spline surfaces. There is also the Bspline-potential-Documentation.pdf file in this directory that contains some 
information for end-users.

There are five directories, each representing one of the following models:
	-fiducial: Model without a B-spline perturbation. An MCMC run of this model is used to create the others models.
                   The parameter file is fiducial.par, and simulation0.par is used to create the multiple images.
	-N3: Model build with the bestopt.par from the fiducial directory where a perturbation with n=3 is added. The parameter
             file is opt_bplisne.par and has already been optimised
	-N4: same as N3 but for n=4
	-N5: same as N3 but for n=5
	-N6: same as N3 but for n=6

For each directory, the file gal.cat and mul.cat respectively contain the data about the cluster members following the scaling
relations and the multiple-image positions.

For each directory containing a B-spline potential, the file coeff.txt has the unoptimised model's B-spline coefficients (i.e.
 opt_bspline.par) Coeff_best_1.dat have the optimised one associated with best.par and bestopt.par.

The other files are the same as for any other lenstool model.
