Examples for Lenstool
=====================

Listing :
--------
example_draw_giant_arc : 
 Plot cusp giant arcs and illustrate how to produce noise.

example_with_images :
 Optimise a potential with 3 systems of multiple images. 
 Produce the best mass map.
 Produce the shear field.
 Produce a 2 related grid : regular source plane --> lensed in image plane.

example_with_sources :
 Lens 3 sources.
 Produce a mass map and a shear field.

example_with_galaxies :
 Optimise a cluster-scale potential and 12 galaxy-scale subhalos with 
 2 systems of multiple images.

example_with_errors :
 Optimise the cluster-scale potential as well as the noise itselt 
 introduced on the image catalog (Gaussian noise of 0.1").
 (See log file)

example_with_bspline:
 Optimise models with bspline perturbation. These models are 
 created with a parametric-only lenstool model.
 (see README file for more infos)

How to :
--------

In all these examples, plot the mass map with DS9:
ds9 ./mass.fits

Then, run the test example (with the extension .par) :
lenstool ./test_galaxies

Then, plot the results on the DS9 frame:
./draw


You can also plot the bayes.dat pdf with ([RETURN] to go forward):
pdfcheck.pl

or print a summary of the optimized parameters:
bayesResults.pl

or plot a 2D map of 2 optimized parameters with:
Histogram2D

